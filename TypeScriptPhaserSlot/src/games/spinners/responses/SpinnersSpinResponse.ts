﻿module TypeScriptPhaserSlot.common {

    export class SpinnersSpinResponse implements IResponse {
        protected _status: number;
        protected _commands: any[];
        protected _jackpot: Jackpot;
        protected _results: SpinResult[];
        protected _win: number;
        protected _totalWin: number;
        protected _multiplier: number;
        protected _state: State;
        protected _user: User;
        protected _serverInfo: ServerInfo;
        protected _rawData:any;
        protected _bonusResponce: BonusResponse;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): SpinnersSpinResponse {
            //this._status = rawData.status;
            //this._commands = rawData.commands;
            //this._jackpot = new Jackpot(rawData.response.jackpot);

            this._rawData = rawData;

            this._results = [];
            this._results.push(new SpinnersSpinResult(rawData));

            this._win = rawData.winnings.total;
            this._totalWin = rawData.winnings.total;
            this._multiplier = rawData.additional_info.multiplier || 1;

            let fakeStateData: any = {};
            //fakeStateData.freespins = rawData.bonuses[0].amount || 0;
            fakeStateData.freespins = rawData.bonuses_summary.freespins_remainder || 0;
            fakeStateData.points = 0;
            fakeStateData.hasBonus = rawData.bonuses.filter(function (element:any) {
                if(element.type == 'automatic' && element.state == 1) {
                    return true;
                }
                return undefined;
            }).length > 0;
            fakeStateData.coins = rawData.balance;
            fakeStateData.total = rawData.bonuses_summary.total;

            this._state = new State(fakeStateData);

            if(rawData.bonuses[0]) {
                this._bonusResponce = new BonusResponse(rawData.bonuses[0]);
            }
            //this._user = new User(rawData.response.user);
            //this._serverInfo = new ServerInfo(rawData.serverInfo);
            return this;

            /*this._status = rawData.status;
            this._commands = rawData.commands;
            this._jackpot = new Jackpot(rawData.response.jackpot);
            
            this._results = [];
            for (let i: number = 0; i < rawData.response.result.length; i++) {
                this._results.push(new SpinResult(rawData.response.result[i]));
            }
            this._win = rawData.response.win;
            this._totalWin = rawData.response.totalWin;
            this._state = new State(rawData.response.state);
            this._user = new User(rawData.response.user);
            this._serverInfo = new ServerInfo(rawData.serverInfo);
            return this;*/
        }

        public get status(): number {
            return this._status;
        }
        public get commands(): any[] {
            return this._commands;
        }
        public get jackpot(): Jackpot {
            return this._jackpot;
        }
        public get results(): SpinResult[] {
            return this._results;
        }
        public get win(): number {
            return this._win;
        }
        public get totalWin(): number {
            return this._totalWin;
        }
        public get multiplier(): number {
            return this._multiplier;
        }
        public get state(): State {
            return this._state;
        }
        public get user(): User {
            return this._user;
        }
        public get serverInfo(): ServerInfo {
            return this._serverInfo;
        }
        public get bonusResponce(): BonusResponse {
            return this._bonusResponce;
        }
        public get rawData(): any {
            return this._rawData;
        }
    }
}