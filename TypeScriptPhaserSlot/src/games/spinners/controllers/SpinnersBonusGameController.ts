///<reference path='../../../engine/controller/BonusGameController.ts' />

module TypeScriptPhaserSlot.common {

    export class SpinnersBonusGameController extends BonusGameController {

        protected pattern: any;
        protected view:SimpleSprite;
        protected layer: Phaser.Group;
        protected wheel: SpinnersBonusWheel;
        protected rays:SimpleSprite;
        protected button: BaseButton;
        protected winnerAnimation: AnimatedSprite;
        protected winningId:number;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, gameConfiguration: GameConfiguration, pattern: any, gameStateResponse: GameStateResponse) {
            super(game, name, layer, debugMode, gameConfiguration, pattern, gameStateResponse);
            this.init();
        }

        public init():void {
            if(this.button) {
                return;
            }

            this.winningId = -1;
            //this.layer.visible = true;

            let gray:Phaser.Graphics = new Phaser.Graphics(this.game);
            gray.beginFill(0x0, 0.7);
            gray.drawRect(0, 0, 1366, 768);
            gray.endFill();
            this.layer.add(gray);

            let wheelPatternData:any = this.getPatternData(this.pattern.bonusGame, 'bonusWheel');
            this.wheel = new SpinnersBonusWheel(this.game, wheelPatternData.x, wheelPatternData.y, wheelPatternData);
            this.wheel.actionSignal.add(this.onWheelAction, this);
            this.layer.add(this.wheel);

            this.view = new SimpleSprite(this.game, 0, 0, 'bonus_wheel_decoration', this.debugMode);
            this.layer.add(this.view);

            this.rays = new SimpleSprite(this.game, 682, 442, 'bonus_rays');
            this.rays.scale.set(3, 3);
            this.rays.anchor.set(0.5, 0.5);
            this.rays.alpha = 0;
            this.layer.add(this.rays);

            let buttonActions:any = {
                mouseUp: {
                    action: '',
                    params: null,
                    sound: null,
                    enabledAfter: undefined
                }
            };
            let bottonConfigurationParams:any = {
                name: 'bonusStartSpin_btn',
                className:  'BaseButton',
                key: 'bonus_button',
                caption: '',
                textStyle: 'style1',
                overFrame: 'button/over',
                outFrame: 'button/enabled',
                downFrame: 'button/down',
                upFrame: 'button/enabled',
                disabledFrame: 'button/disabled',
                onDownSoundKey: 'clicksound',
                onOverSoundKey: '',
                onOutSoundKey: '',
                onUpSoundKey: ''
            };
            let buttonConfiguration = new ButtonConfiguration(bottonConfigurationParams);
            let buttonPatternData:any = this.getPatternData(this.pattern.bonusGame, 'bonusButton');
            this.button = new BaseButton(this.game, buttonConfiguration, buttonPatternData.x, buttonPatternData.y, this.onButtonClick, this);
            this.button.actions = buttonActions;
            this.button.enabled = true;
            this.layer.add(this.button);

            this.winnerAnimation = new AnimatedSprite(this.game, 0, 0, 'bonus_winner_animation', this.debugMode);
            this.winnerAnimation.actionSignal.add(this.onShowWinningsComplete, this);
            this.winnerAnimation.visible = false;
            this.winnerAnimation.y = 768 / 2;
            this.layer.add(this.winnerAnimation);
        }

        public start(data?:any):void {
            super.start(data);
            this.restore();

            let currentBonusData:any = data.data.bonuses ? data.data.bonuses[0] : data.data;
            this.wheel.createAwards(currentBonusData.additional_info.indexes_info);
        }

        protected onWheelAction(data:any):void {
            switch (data.action) {
                case 'started':
                    this.rays.alpha = 0;
                    this.game.add.tween(this.rays).to({alpha:1}, 1000, Phaser.Easing.Linear.None, true);
                    break;
                case 'rotate':
                    this.rays.frame = Math.round(41 / 360 * data.angle);
                    break;
                case 'readyToStop':
                    if(this.winningId != -1) {
                        this.wheel.stop(this.winningId);
                    }
                    break;
                case 'stoped':
                    this.rays.alpha = 1;
                    this.game.add.tween(this.rays).to({alpha:0}, 1000, Phaser.Easing.Linear.None, true);
                    this.actionSignal.dispatch({action:'showWinningsComplete'});


                    if(this.winningId != 3) {
                        this.winnerAnimation.start();
                        this.winnerAnimation.visible = true;
                        SoundController.instance.playSound('winner', 'bonus', 1, false);
                    } else {
                        this.onShowWinningsComplete();
                    }
                    break
            }
        }

        protected onShowWinningsComplete(data?:any):void {
            setTimeout(function () {
                this.winnerAnimation.stop(true);
                this.actionSignal.dispatch({action:'bonusGameComplete'});
                this.stop();
            }.bind(this), 1000);
        }

        protected onButtonClick(data?:any):void {
            this.button.enabled = false;
            this.wheel.start();
            SoundController.instance.playSound('spinners_wheel', 'bonus', 1, false);
            this.actionSignal.dispatch({action:'choice'});
        }

        public showWinning(data: any): void {
            console.log(data);

            let currentBonusData:any = data.data.bonuses ? data.data.bonuses[0] : data.data;

            if(currentBonusData.state != 4) {
                return;
            }

            this.winningId = currentBonusData.additional_info.choice;
            if(this.wheel.state == 'readyToStop' || this.wheel.state == 'idle') {
                this.wheel.stop(this.winningId);
            }

            this.rays.alpha = 1;
            this.button.enabled = false;
        }

        public restore():void {
            this.winnerAnimation.visible = false;
            this.button.enabled = true;
            this.rays.frame = 1;
            this.rays.alpha = 0;
            this.wheel.restore();
        }

        protected getPatternData(parent:any, childName:string):any {
            for(let i:number = 0; i < parent.child.length; i++) {
                let childPattern:any = this.pattern.bonusGame.child[i];
                if (childPattern.name == childName) {
                    return childPattern;
                }
            }
            return null;
        }
    }
}