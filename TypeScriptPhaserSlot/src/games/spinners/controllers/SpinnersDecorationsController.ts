﻿///<reference path="../../baseGame/controllers/BaseGameDecorationsController.ts" />

module TypeScriptPhaserSlot.common {

    export class SpinnersDecorationsController extends BaseGameDecorationsController {

        protected logoAnimation:AnimatedSprite;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, layer, debugMode, pattern);
        }

        protected createLogo():void {
            super.createLogo();

            let locale:string = this.game.textFactory.currLocale;
            this.logo = new SimpleSprite(this.game, 690, 70, 'game_logo', this.debugMode, 'logo_' + locale);
            this.logo.anchor.set(0.5, 0.5);
            this.layer.add(this.logo);

            this.logoAnimation = new AnimatedSprite(this.game, 407, -14, 'logo_animation', this.debugMode);
            this.animateLogo();
            this.layer.add(this.logoAnimation);

            this.game.textFactory.actionSignal.add(this.onLocaleChanged, this);
        }

        protected onLocaleChanged(data:any):void {
            this.logo.frameName = 'logo_' + data.data;
        }

        protected animateLogo():void {
            this.logoAnimation.start();
            setTimeout(function () {
                this.animateLogo();
            }.bind(this), 20000 + Math.random() * 10000);
        }
    }
}