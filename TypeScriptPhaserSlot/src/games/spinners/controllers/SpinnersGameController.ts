﻿///<reference path="../../baseGame/controllers/BaseGameGameController.ts" />

///<reference path="./SpinnersBackController.ts" />
///<reference path="./SpinnersLinesController.ts" />
///<reference path="./SpinnersReelsController.ts" />

module TypeScriptPhaserSlot.common {

    export class SpinnersGameController extends BaseGameGameController {

        protected _bonusGameController:SpinnersBonusGameController;
        protected _backgroundController: SpinnersBackController;

        /**
         *
         * @param {IState} state
         * @param {any} data
         * @param {boolean} isRequiredAction
         */
        protected modelSays(state: IState, data: any, isRequiredAction: boolean): void {
            super.modelSays(state, data, isRequiredAction);

            //let isRequired: boolean = !isRequiredAction || this.model.isAutoPlay ? false : true;
            let isRequired: boolean = !(!isRequiredAction || this.model.isAutoPlay);

            if (!isRequiredAction) {
                this.debuglog("do work for " + state.name);
            }
            this._interfaceController.stateChanged(state.name);

            if(state.name == StatesConstants.IDLE_STATE) {
                this._interfaceController.changeBalance(this.model.coins);
                this._boongoGameRunnerController.sayToGameRuner('ready');

                this.onBetChangedHandler(this.model.betPerLine);

                if(this.model.multiplier > 1) {
                    this._interfaceController.disableButtons(['betDec_btn', 'betInc_btn', 'maxBet_btn']);
                }
            }

            if (!isRequired)
                switch (state.name) {

                    case StatesConstants.IDLE_STATE:
                        this._linesController.stop();
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();

                        this._interfaceController.changeBalance(this.model.coins);

                        if(this.model.isAutoPlay && this.model.coins < this.model.totalBet && this.model.freespins == 0) {
                            this.model.auto(false);
                            this.onInterfaceControllerAction(ButtonActionConstants.AUTO_ACTION);
                            break;
                        }

                        state.finishWork();
                        break;

                    case StatesConstants.PRESPIN_STATE:
                        state.finishWork();
                        break;

                    case StatesConstants.GET_SPIN_RESPONSE_STATE:
                        if (this.model.isFreespins) {
                            this.appProxy.freespin(1);
                        } else {
                            this.appProxy.spin(data);
                        }
                        break;

                    case StatesConstants.START_SPIN_STATE:
                        this._linesController.stop();
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();

                        this._interfaceController.resetWinText();
                        this._reelsController.start();

                        if(this.model.isFreespins) {
                            this._freeSpinsController.leftFreeSpins(this.model.freespins);
                        }
                        this._backgroundController.startSpin();
                        this._interfaceController.changeBalance(this.model.coins - (this.model.isFreespins ? 0 :this.model.totalBet));
                        this._interfaceController.hidePopupWindow();

                        state.finishWork();
                        break;

                    case StatesConstants.START_RESPIN_STATE:
                        this._linesController.stop();
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();
                        this._reelsController.respin();
                        //this._reelsController.respin(data);
                        state.finishWork();
                        break;
                    case StatesConstants.GET_RESPIN_RESPONSE_STATE:
                        this.appProxy.respin();
                        break;

                    case StatesConstants.STOP_RESPIN_STATE:
                        this._reelsController.stopRespin(data);
                        break;

                    case StatesConstants.STOP_SPIN_STATE:
                        this._reelsController.stop(data);
                        break;

                    case StatesConstants.EMOTIONS_STATE:
                        this._emotionsController.start(data);
                        break;

                    case StatesConstants.START_SHOW_WINNING_STATE:
                        this.lastSpinResult = data;

                        this._linesController.start(data);

                        this._emotionsController.stopEmotion();

                        //this._reelsController.animateSymbols(data);
                        //this._symbolsAnimationsController.animateSymbols(data);
                        break;

                    case StatesConstants.STOP_SHOW_WINNING_STATE:
                        this._interfaceController.resetWinText();
                        this._interfaceController.changeBalance(this.model.coins);

                        this._winningOnCenterController.hideWinning();

                        state.finishWork();
                        SoundController.instance.playSound('ching', 'default', 0.5);
                        break;

                    case StatesConstants.COLLECT_STATE:
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();
                        state.finishWork();
                        break;

                    case StatesConstants.START_FREESPINS_STATE:
                        this._freeSpinsController.startFreeSpins(this.model.freespins);
                        this._backgroundController['changeBackground'](true);
                        this._decorationsController['changeFrame'](true);
                        this._interfaceController['showAutoplayText'](false);
                        break;

                    case StatesConstants.ADD_FREESPINS_STATE:
                        this._freeSpinsController.addedFreeSpins(this.model.freeSpinsAdded);
                        break;

                    case StatesConstants.FINISH_FREESPINS_STATE:
                        this._freeSpinsController.endOfFreeSpins(this.model.bonusTotalWin);
                        this._backgroundController['changeBackground'](false);
                        this._decorationsController['changeFrame'](false);
                        break;

                    case StatesConstants.GET_GAMBLE_RESPONSE_STATE:
                        state.finishWork();
                        break;

                    case StatesConstants.GAMBLE_STATE:
                        this.appProxy.gamble();
                        break;

                    case StatesConstants.START_BONUS_GAME_STATE:
                        this._bonusGameController.start(data);
                        let bgSound:Phaser.Sound = SoundController.instance.getSound('background_sound', 'music');
                        if(bgSound) {
                            bgSound.pause();
                        }
                        SoundController.instance.playSound('double_theme', 'music', 1, true);
                        state.finishWork();
                        break;

                    case StatesConstants.SHOW_WINNING_BONUS_GAME_STATE:
                        this._bonusGameController.showWinning(data);
                        //state.finishWork();
                        break;

                    case StatesConstants.FINISH_BONUS_GAME_STATE:
                        console.log('');
                        this._interfaceController.changeWinText(this.model.totalWin);
                        this._interfaceController.changeBalance(this.model.coins);
                        
                        SoundController.instance.getSound('background_sound', 'music').resume();
                        SoundController.instance.stopSound('double_theme', 'music');
                        state.finishWork();
                        break;
                }
        }

        /**
         * Callback function to handle getting the server response "GetState"
         * Initialising all helper/controllers
         *-ReelsController
         *-InterfaceController
         *-LineController
         *-TextController
         * @param {GameStateResponse} data
         */
        protected onGetGameState(data: GameStateResponse): void {
            let contentLayer:Phaser.Group = new Phaser.Group(this.game);

            let backLayer: Phaser.Group = new Phaser.Group(this.game);
            backLayer.name = 'backLayer';
            let reelsLayer: Phaser.Group = new Phaser.Group(this.game);
            reelsLayer.name = 'reelsLayer';
            let linesLayer: Phaser.Group = new Phaser.Group(this.game);
            linesLayer.name = 'linesLayer';
            let wildsAnimationsLayer: Phaser.Group = new Phaser.Group(this.game);
            wildsAnimationsLayer.name = 'wildsAnimationsLayer';
            let symbolsAnimationsLayer: Phaser.Group = new Phaser.Group(this.game);
            symbolsAnimationsLayer.name = 'symbolsAnimationsLayer';
            let decorationsLayer: Phaser.Group = new Phaser.Group(this.game);
            decorationsLayer.name = 'decorationsLayer';
            let bonusGameLayer: Phaser.Group = new Phaser.Group(this.game);
            bonusGameLayer.name = 'bonusGameLayer';

            let consoleLayer: Phaser.Group = new Phaser.Group(this.game);
            consoleLayer.name = 'consoleLayer';
            //consoleLayer.scale.setTo(scale, scale);

            let winningOnCenterLayersLayer: Phaser.Group = new Phaser.Group(this.game);
            winningOnCenterLayersLayer.name = 'winningOnCenterLayersLayer';
            let emotionsLayer: Phaser.Group = new Phaser.Group(this.game);
            emotionsLayer.name = 'emotionsLayer';
            let freeSpinsLayer: Phaser.Group = new Phaser.Group(this.game);
            freeSpinsLayer.name = 'freeSpinsLayer';
            let paytableLayer: Phaser.Group = new Phaser.Group(this.game);
            paytableLayer.name = 'paytableLayer';

            let toolbarLayer: Phaser.Group = new Phaser.Group(this.game);
            toolbarLayer.name = 'toolbarLayer';
            //toolbarLayer.scale.setTo(scale, scale);

            contentLayer.add(backLayer);
            contentLayer.add(reelsLayer);
            contentLayer.add(linesLayer);
            contentLayer.add(wildsAnimationsLayer);
            contentLayer.add(symbolsAnimationsLayer);
            contentLayer.add(decorationsLayer);
            contentLayer.add(winningOnCenterLayersLayer);
            contentLayer.add(emotionsLayer);
            contentLayer.add(freeSpinsLayer);
            contentLayer.add(bonusGameLayer);
            contentLayer.add(paytableLayer);

            //contentLyer.add(toolbarLayer);

            this.game.add.existing(contentLayer);
            this.game.add.existing(toolbarLayer);
            this.game.add.existing(consoleLayer);

            let backControllerName: string = this.getControllerName("BackController");
            this._backgroundController = new TypeScriptPhaserSlot.common[backControllerName](this.game, backControllerName, backLayer, true, this.game.pattern);


            let decorationsControllerName: string = this.getControllerName("DecorationsController");
            this._decorationsController = new TypeScriptPhaserSlot.common[decorationsControllerName](this.game, decorationsControllerName, decorationsLayer, true, this.game.pattern);


            let icn:string = 'InterfaceController';
            let icp:any = this.game.pattern.console;
            if(this.game.device.iPhone || this.game.device.android) {
                icn = 'InterfaceControllerMobile';
                icp = this.game.pattern.console_mobile;
            }
            if(this.game.device.iPad) {
                icn = 'InterfaceControllerIPad';
                icp = this.game.pattern.console_ipad;
            }
            let interfaceControllerName: string = this.getControllerName(icn);
            this._interfaceController = new TypeScriptPhaserSlot.common[interfaceControllerName](this.game, interfaceControllerName, consoleLayer, true, this.configuration.buttons, icp);
            this._interfaceController.actionSignal.add(this.onInterfaceControllerAction, this);

            /*
             ReelsController creates and manages all amount of <IReel>
             */
            let reelsControllerName: string = this.getControllerName("ReelsController");
            this._reelsController = new TypeScriptPhaserSlot.common[reelsControllerName](this.game, reelsControllerName, reelsLayer, true, this.configuration.symbols, this.configuration, this.game.pattern, data);
            this._reelsController.actionSignal.add(this.onReelsControllerAction, this);

            /*
             WinningController creates and manages all stuff of showing the visual results
             like lines, borders and so on.
             */
            let linesControllerName: string = this.getControllerName("LinesController");
            this._linesController = new TypeScriptPhaserSlot.common[linesControllerName](this.game, linesControllerName, linesLayer, true, this.configuration, this.game.pattern);
            this._linesController.actionSignal.add(this.onLinesControllerAction, this);
            this._linesController.finishShowingSignal.add(this.onLinesControllerFinishShowLines, this);


            let scattersControllerName: string = this.getControllerName("ScattersController");
            this._scattersController = new TypeScriptPhaserSlot.common[scattersControllerName](this.game, scattersControllerName, linesLayer, true, this.configuration, this.game.pattern);
            this._scattersController.actionSignal.add(this.onScattersControllerAction, this);
            this._scattersController.finishShowingSignal.add(this.onScattersControllerFinish, this);

            /*
             Animations controller
             */
            let symbolsAnimationsControllerName: string = this.getControllerName("SymbolsAnimationController");
            this._symbolsAnimationsController = new TypeScriptPhaserSlot.common[symbolsAnimationsControllerName](this.game, symbolsAnimationsControllerName, symbolsAnimationsLayer, true, this.configuration.symbols, this.configuration, this.game.pattern, data);
            this._symbolsAnimationsController.actionSignal.add(this.onReelsControllerAction, this);
            this._symbolsAnimationsController['wildLayer'] = wildsAnimationsLayer;


            let soundControllerName: string = this.getControllerName("SoundController");
            this._soundController = new TypeScriptPhaserSlot.common[soundControllerName](this.game, soundControllerName, true, this.configuration, data);
            this._soundController.actionSignal.add(this.onReelsControllerAction, this);

            new MoneyFormatController(this.game, 'MoneyFormatController', true, this.configuration, data);

            let winningOnCenterControllerName: string = this.getControllerName("WinningOnCenterController");
            this._winningOnCenterController = new TypeScriptPhaserSlot.common[winningOnCenterControllerName](this.game, winningOnCenterControllerName, winningOnCenterLayersLayer, true);
            this._winningOnCenterController.actionSignal.add(this.onEmotionsControllerAction, this);


            let emotionsControllerName: string = this.getControllerName("EmotionsController");
            this._emotionsController = new TypeScriptPhaserSlot.common[emotionsControllerName](this.game, emotionsControllerName, emotionsLayer, true, this.configuration, this.game.pattern, data);
            this._emotionsController.actionSignal.add(this.onEmotionsControllerAction, this);


            let freeSpinsControllerName:string = this.getControllerName("FreeSpinsController");
            this._freeSpinsController = new TypeScriptPhaserSlot.common[freeSpinsControllerName](this.game, freeSpinsControllerName, freeSpinsLayer, true, this.configuration, this.game.pattern, data);
            this._freeSpinsController.actionSignal.add(this.onFreeSpinsControllerAction, this);


            let paytableControllerName:string = this.getControllerName("PaytableController");
            this._paytableController = new TypeScriptPhaserSlot.common[paytableControllerName](this.game, paytableControllerName, paytableLayer, true, this.configuration, this.game.pattern, data);


            let toolbarControllerName:string = this.getControllerName("ToolbarController");
            this._toolbarController = new TypeScriptPhaserSlot.common[toolbarControllerName](this.game, toolbarControllerName, toolbarLayer, true, this.configuration, this.game.pattern, data);
            this._toolbarController.actionSignal.add(this.onToolbarControllerAction, this);


            let bonusGameControllerName: string = this.getControllerName("BonusGameController");
            this._bonusGameController = new TypeScriptPhaserSlot.common[bonusGameControllerName](this.game, bonusGameControllerName, bonusGameLayer, true, this.configuration, this.game.pattern, data);
            this._bonusGameController.actionSignal.add(this.onBonusGameControllerAction, this);


            this._boongoGameRunnerController = new BoongoGameRunerController(this.game, 'BoongoGameRunerController', this.debugMode);
            this._boongoGameRunnerController.actionSignal.add(this.onBoongoGameRunnesControllerAction, this);
            this._boongoGameRunnerController.sayToGameRuner('gameReady');

            SoundController.instance.playSound('background_sound', 'music', 0.3, true);

            //Sending game state data to the model to init a game proccess.
            this.model.onGetGameState(data);

            //Shwitch autoplay on if Freespins are here
            if (this.model.isFreespins) {
                this.model.auto(true);
            }

            this._backgroundController['changeBackground'](this.model.isFreespins);
            this._decorationsController['changeFrame'](this.model.isFreespins);

            //this.game.camera.resetFX();

            this.correctSize(this.game.scale, this.game.scale.width, this.game.scale.height);

            this._interfaceController['changePositions']();
        }

        protected onReelsControllerAction(data?: any): void {
            super.onReelsControllerAction(data);
            if (data == ReelsController.STOP) {
                this._backgroundController.stopSpin();
            }
        }

        protected onBonusGameControllerAction(data?: any): void {
            super.onBonusGameControllerAction();

            if(data) {
                switch(data.action) {
                    case 'choice':
                        this.appProxy.bonus(data);
                        break;
                    case 'showWinningsComplete':
                        //this.currState.finishWork();
                        break;
                    case 'bonusGameComplete':
                        this.model.goToState(StatesConstants.FINISH_BONUS_GAME_STATE);
                        break;
                }
            }
        }

    }

}