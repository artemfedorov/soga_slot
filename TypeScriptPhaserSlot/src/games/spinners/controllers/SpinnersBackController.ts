﻿///<reference path="../../../engine/controller/interfaces/IActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class SpinnersBackController extends ActionController implements IActionController {

        private view: SimpleSprite;
        private freeSpinsBG: SimpleSprite;
        private layer: Phaser.Group;
        private pattern: any;
        private starsList: Phaser.Graphics[];
        private starsTweens: Phaser.Tween[];
        private tweenSpeedScale: number = 0.5;
        private starsGroup: Phaser.Group;
        private starsSprite: Phaser.Sprite;
        private rotationTween:Phaser.Tween;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, debugMode);
            this.layer = layer;
            this.pattern = pattern;
            this.starsTweens = [];
            this.init(pattern);
        }
        /**
        *
        */
        public init(pattern: any): void {
            this.starsList = [];
            this.view = new SimpleSprite(this.game, pattern.background.x, pattern.background.y, pattern.background.name, this.debugMode);
            this.layer.add(this.view);
            this.starsSprite = new Phaser.Sprite(this.game, 0, 0);

            this.layer.add(this.starsSprite);
            this.starsSprite.anchor.setTo(0.5, 0.5);
            this.rotationTween = this.game.add.tween(this.starsSprite).to({rotation: 360 * Math.PI / 180}, 100000, Phaser.Easing.Linear.None, true, 0, -1);
            this.rotationTween.start();
            this.starsSprite.x += this.layer.width / 2;
            this.starsSprite.y += this.layer.height / 2;
            this.freeSpinsBG = new SimpleSprite(this.game, pattern.background.x, pattern.background.y, 'background_freeSpins', this.debugMode);
            this.freeSpinsBG.visible = false;
            this.layer.add(this.freeSpinsBG);
            for(let i: number = 0; i < 500; i++){
                this.starsMoving();
            }
            //this.layer.add(new Phaser.Image(this.game, pattern.usual_frame_bg.x, pattern.usual_frame_bg.y, pattern.usual_frame_bg.name));
        }


        private starsMoving(dt?: Phaser.Graphics): void {
            let radius:number = Math.random() * 5;
            let dot: Phaser.Graphics = this.buildDot(radius, dt);
            let angle: number = ((Math.random() * 360) * Math.PI) / 180;
            dot.x = 0;
            dot.y = 0;
            let dx: number = dot.x + 500 * Math.cos(angle);
            let dy: number = dot.y + 500 * Math.sin(angle);
            dot.x += (Math.random() * 200) * Math.cos(angle);
            dot.y += (Math.random() * 200) * Math.sin(angle);
            //this.starsGroup.add(dot);
            this.starsSprite.addChild(dot);
            let t: number = (30000 - (radius * 6000));
            let tween = this.game.add.tween(dot).to({x: dx, y: dy}, t, Phaser.Easing.Linear.None);
            tween.timeScale = this.tweenSpeedScale;
            tween.onComplete.addOnce(this.removeDot, this, 0, dot);
            tween.start();
            this.starsTweens.push(tween);
        }

        private removeTween(arg1:any, arg2:any): void {
            this.starsTweens.splice(this.starsTweens.indexOf(arg2),1);
        }
        private removeDot(x: any, y:any, dot: Phaser.Graphics): void {
            this.starsMoving(dot);
            this.removeTween(x, y);
        }

        private buildDot(radius: number, dt?: Phaser.Graphics): Phaser.Graphics {
            let dot: Phaser.Graphics = dt || new Phaser.Graphics(this.game);
            dot.clear();
            dot.beginFill(0xffffff, Math.random()+0.2);
            dot.drawCircle(0,0, radius);
            dot.endFill();
            return dot;
        }

        public stopSpin() {
            this.tweenSpeedScale = 0.5;
            this.rotationTween.timeScale = this.tweenSpeedScale;
            for(let i: number = 0; i < this.starsTweens.length; i++) {
                this.starsTweens[i].timeScale = this.tweenSpeedScale;
            }
        }

        public startSpin() {
            this.tweenSpeedScale = 3;
            this.rotationTween.timeScale = this.tweenSpeedScale;
            for(let i: number = 0; i < this.starsTweens.length; i++) {
                this.starsTweens[i].timeScale = this.tweenSpeedScale;
            }
        }

        /**
         *
         * @param freeSpins
         */
        public changeBackground(freeSpins:boolean): void {
            let tween:Phaser.Tween;
            this.freeSpinsBG.visible = true;
            if(freeSpins) {
                this.freeSpinsBG.alpha = 0;
                tween = this.game.add.tween(this.freeSpinsBG).to({alpha:1}, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                tween.onComplete.add(function ():void {
                    this.view.visible = false;
                    SoundController.instance.playSound('fs_backsnd', 'default', 0.4, true);
                }, this);
            } else {
                this.view.visible = true;
                tween = this.game.add.tween(this.freeSpinsBG).to({alpha:0}, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                tween.onComplete.add(function ():void {
                    this.freeSpinsBG.visible = false;
                    let stopSound: Phaser.Sound = SoundController.instance.getSound("fs_backsnd", 'default');
                    if (stopSound) {
                        stopSound.stop();
                    }
                }, this);
            }
        }

    }
}