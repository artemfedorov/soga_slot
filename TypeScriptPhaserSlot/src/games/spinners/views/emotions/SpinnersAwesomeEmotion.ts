/**
 * Created by rocket on 04.07.2017.
 */
module TypeScriptPhaserSlot.common {

    export class SpinnersAwesomeEmotion extends BaseEmotion {

        private _timeOut:number;

        constructor(game: Game, config:EmotionConfiguration) {
            super(game, config);
        }

        /**
         *
         */
        protected init():void {

        }

        /**
         *
         */
        public start():void {
            this.animate();
            SoundController.instance.playSound('hammer', 'emotions', 0.1);
        }

        /**
         *
         */
        public stop():void {
            clearInterval(this._timeOut);
            this._timeOut = 0;
            super.stop();
        }

        /**
         *
         */
        protected animate():void {
            let gameWidth:number = 1366;
            let gameHeight:number = 768;

            let backgroundAnimation: AnimatedSprite = new AnimatedSprite(this.game, 0, 0, this.config.emotion);
            backgroundAnimation.anchor.set(0.5, 0.5);
            backgroundAnimation.x = gameWidth / 2;
            backgroundAnimation.y = gameHeight / 2;
            backgroundAnimation.start();
            backgroundAnimation.alpha = 0;
            this.add(backgroundAnimation);
            this._tweens.push(this.game.add.tween(backgroundAnimation).to({alpha: 1}, 700, Phaser.Easing.Cubic.Out, true));
            this._tweens.push(this.game.add.tween(backgroundAnimation).to({alpha: 0}, 700, Phaser.Easing.Cubic.Out, false, 1000));
            this._tweens[0].chain(this._tweens[1]);


            let firstImage: SimpleSprite = new SimpleSprite(this.game, 0, 0, 'emotions_images', false, 'texts/awesome');

            firstImage.anchor.set(0.5, 0.5);
            firstImage.y = gameHeight / 2 - 50;

            this.add(firstImage);
            firstImage.x = -firstImage.width;

            this._tweens.push(this.game.add.tween(firstImage).to({x: gameWidth / 2}, 450, Phaser.Easing.Cubic.Out, true));
            this._tweens.push(this.game.add.tween(firstImage).to({x: gameWidth / 2 + 70}, 1000, Phaser.Easing.Default));
            this._tweens.push(this.game.add.tween(firstImage).to({x: gameWidth + firstImage.width}, 450, Phaser.Easing.Cubic.Out));
            this._tweens[2].chain(this._tweens[3]);
            this._tweens[3].chain(this._tweens[4]);

            this._tweens[this._tweens.length - 1].onComplete.addOnce(this.onAnimationsComplete, this);

            SoundController.instance.playSound('awesome', 'default', 0.2, false);
        }

        /**
         *
         * @param data
         */
        protected onAnimationsComplete(data?:any):void {
            this._actionSignal.dispatch(AnimatedSprite.ALL_ANIMATIONS_COMPLETE);
        }
    }
}