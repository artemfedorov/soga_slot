﻿///<reference path="../../../engine/view/reels/BaseReelView.ts" />
///<reference path="../../../engine/view/reels/ISymbol.ts" />
///<reference path="./SpinnersSymbol.ts" />

module TypeScriptPhaserSlot.common {

    export class SpinnersReelView extends BaseReelView {

        constructor(game: Game, index: number, name: string, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfig: GameConfiguration, pattern: any, data: number[], mapReel?: number[], mapReelFreespins?: number[], borderLayer?: Phaser.Group) {
            super(game, index, name, debugMode, symbolProperties, gameConfig, pattern, data, mapReel, mapReelFreespins, borderLayer);
            this.symbolHeight = this.symbolProperties[0].height;
            this.mapReelFreespins = mapReelFreespins ? mapReelFreespins : [];
            this.mapReel = mapReel ? mapReel : [];
            this.mapReelFreespinsShift = 0;
            this.mapReelShift = 0;
            this.topVisibledLimit = 0;
            this.setRegularMap();
            this.symbolHeight = this.gameConfig.standartSymbolSetting.height;
            this.bottomVisibleLimit = this.symbolHeight * this._reelConfig.visibledSymbols;
            this.bottomRemovedLimit = this.symbolHeight * (this._reelConfig.visibledSymbols + 1);

            let symbolsCount:number[] = [24, 36, 49, 58, 70];
            this.generateSymbolsBand(symbolsCount[index], 0, this.symbolProperties.length - 1);
            this.updater = this.emptyFunction;
            this.init();
        }

        protected init(): void {
            this._symbols = [];
            //this.currentShift = 0;

            for(let i: number = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                this.initData.unshift(this.getNextKey());
            }

            for(let j: number = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                this.initData.push(this.getNextKey());
            }

            let addWild:any = this.checkForWild(this.initData);
            for(let k:number = addWild.startIndex; k < addWild.startIndex + addWild.changeCount; k++) {
                this.initData[k] = 10;
            }

            this.addSymbols(this.initData);
            for(let k:number = 0; k < this._symbols.length; k++) {
                this._symbols[k].y -= this.symbolHeight * this._reelConfig.topInvisibleSymbolsCount;
            }
        }

        /**
         * Overriden function called by ReelsController to begin rolling
         * @param {any} data
         */
        public start(data: any): void {
            super.start(data);
            for(let i:number = 0; i < this.symbols.length; i++) {
                this._symbols[i].stop();
            }
            this.tweenToTop();
        }

        /**
         *
         * @param reel
         * @returns {any}
         */
        protected checkForWild(reel: number[]): any {
            let checkResult:any = {};
            checkResult.changeCount = 0;
            checkResult.startIndex = 0;

            let count:number = 3;
            let startIndex:number = reel.indexOf(10);
            let index:number = startIndex;
            while(reel[index] == 10) {
                count--;
                index++
            }
            checkResult.changeCount = count == 3 ? 0 : count;
            checkResult.startIndex = startIndex == this.reelConfig.topInvisibleSymbolsCount ? this.reelConfig.topInvisibleSymbolsCount - checkResult.changeCount : this.reelConfig.topInvisibleSymbolsCount + this.reelConfig.visibledSymbols;
            return checkResult;
        }

        /**
         *
         * @param data
         * @param additionalSymbols
         */
        protected realStop(data: any, additionalSymbols?: number): void {
            this.updater = null;
            this.game.tweens.removeFrom(this.tweenedObject);
            this.resultData = data;

            this.currentShift = 0;

            for(let i: number = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                this.resultData.unshift(this.getNextKey());
            }

            for(let j: number = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                this.resultData.push(this.getNextKey());
            }

            for(let j: number = 0; j < additionalSymbols; j++) {
                this.resultData.push(this.getNextKey());
            }

            this.addSymbols(this.resultData, true);
            this.resultedTween();
        }

        /**
         *
         */
        protected addNewSymbolsInStaticRolling(): void {
            let nextKeys: number[] = this.getNextKeys(1);
            this.addSymbols(nextKeys, true);
        }


        /**
         * Overriden just for animate symbols on the reel when they finnaly stopped
         */
        protected onCompleteTweenUpturn(): void {
            super.onCompleteTweenUpturn();
            for(let i:number = 0; i < this.symbols.length; i++) {
                this._symbols[i].start();
            }
        }

    }
}