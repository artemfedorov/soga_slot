module TypeScriptPhaserSlot.common {

    export class SpinnersBonusWheel extends Phaser.Group {

        protected pattern: any;
        protected debugMode: boolean;
        protected _actionSignal: Phaser.Signal;
        protected _wheel:Phaser.Group;
        protected _started:boolean;
        protected _staticTween:boolean;
        protected _state:string;
        protected _awards:Phaser.Group[];

        constructor(game: Game, x: number, y: number, pattern:any, debugMode?: boolean) {
            super(game);
            this.x = x;
            this.y = y;
            this.pattern = pattern;
            this.debugMode = debugMode;

            this.init();
            this.addListeners();
        }

        protected init(): void {
            this._state = 'idle';
            this._actionSignal = new Phaser.Signal();
            this._awards = [];

            let wheelEffect:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'bonus_wheel_effect');
            wheelEffect.anchor.set(0.5, 0.5);
            this.add(wheelEffect);

            this._wheel = new Phaser.Group(this.game);

            let wheel = new SimpleSprite(this.game, 0, 0, 'bonus_wheel');
            this._wheel.add(wheel);

            /*let dot:Phaser.Graphics = new Phaser.Graphics(this.game, wheel.width / 2, 20);
            dot.beginFill(0xFF0000, 1);
            dot.drawCircle(0, 0, 20);
            dot.endFill();
            this._wheel.add(dot);*/

            this._wheel.pivot.set(wheel.width / 2, wheel.height / 2);
            this.add(this._wheel);
        }

        protected addListeners(): void {

        }

        public showWinning(data: any): void {

        }

        public restore():void {
            this._wheel.rotation = 0;
            this._state = 'idle';
            this._awards.forEach(function(item:Phaser.Group):void {
                this._wheel.remove(item);
            }, this);
            this._awards = [];
        }

        /**
         *
         * @param data
         */
        public start(data?: any): void {
            this._started = true;
            this._state = 'started';
            this.actionSignal.dispatch({action:'started'});

            let deltaTime:number = 3000 / 360;
            let startTweenAngle:number = 90;
            let startTween:Phaser.Tween = this.game.add.tween(this._wheel).to({rotation: this._wheel.rotation + (Math.PI * startTweenAngle) / 180}, startTweenAngle * deltaTime, Phaser.Easing.Quintic.In, true);
            startTween.onComplete.add(function () {
                this._staticTween = true;
                this._state = 'readyToStop';
                this.actionSignal.dispatch({action:'readyToStop'});
            }, this);
        }

        public createAwards(data:any):void {
            //console.log(data);
            let patternData:any = this.getPatternData(this.pattern, 'wheelElements');
            data.forEach(function (element:string, index:number) {
                let itemPattern:any = this.getPatternData(patternData, 'item_' + index);
                this.createElement(element, itemPattern);

            }, this);
        }

        protected createElement(data:any, pattern:any):void {
            let params:string[] = data.split('#');

            let item:Phaser.Group;
            switch(params[0]) {
                case 'free_spins':
                    item = this.createFreeSpinsText(params);
                    break;
                case 'instantaneous':
                    item = this.createWinText(params);
                    break;
            }

            item.x = pattern.x + 34;
            item.y = pattern.y + 27;
            //item.pivot.set(item.width / 2, item.height / 2);
            item.rotation = (Math.PI * pattern.rotation) / 180;
            this._wheel.add(item);

            this._awards.push(item);
        }

        protected createFreeSpinsText(data:any):Phaser.Group {
            let group:Phaser.Group = new Phaser.Group(this.game);

            let freeText:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'bonus_red_text', this.debugMode, 'red_text/free');
            freeText.anchor.set(0.5, 0);

            let spinsText:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'bonus_red_text', this.debugMode, 'red_text/spins');
            spinsText.anchor.set(0.5, 0);
            spinsText.y = 50;

            let numberGroup:Phaser.Group = this.createNumberText(data[1], 'bonus_red_text', 'red_text/');
            numberGroup.pivot.set(numberGroup.width / 2, 0);
            numberGroup.y = 120;

            group.add(freeText);
            group.add(spinsText);
            group.add(numberGroup);

            group.scale.set(0.7, 0.7);

            group.pivot.set(0, 60);
            return group;
        }

        protected createWinText(data:any):Phaser.Group {
            let group:Phaser.Group = new Phaser.Group(this.game);

            /*let chars:string[] = data[2].split('');
            chars.forEach(function (char:string, index:number) {
                let text:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'bonus_yellow_text', this.debugMode, 'yellow_text/' + char);
                text.x = 50 *  index;
                group.add(text);
            }, this);*/

            let numberGroup:Phaser.Group = this.createNumberText(data[2], 'bonus_yellow_text', 'yellow_text/');
            numberGroup.pivot.set(numberGroup.width / 2, 0);

            group.add(numberGroup);

            group.scale.set(0.7, 0.7);

            group.pivot.set(0, 0);
            return group;
        }

        protected createNumberText(data:string, key:string, frame:string):Phaser.Group {
            let charSize:any = {
                '1': 45,
                '2': 55,
                '3': 55,
                '4': 55,
                '5': 55,
                '6': 55,
                '7': 55,
                '8': 55,
                '9': 55,
                '0': 55,
                '.': 20
            };
            let group:Phaser.Group = new Phaser.Group(this.game);
            let chars:string[] = data.split('');
            let currentCharEndPosition:number = 0;
            chars.forEach(function (char:string, index:number) {
                let text:SimpleSprite = new SimpleSprite(this.game, 0, 0, key, this.debugMode, frame + char);
                text.x = currentCharEndPosition;
                currentCharEndPosition = text.x + charSize[char];
                group.add(text);
            }, this);
            return group;
        }

        /**
         *
         * @param {any} data
         */
        public stop(data: any): void {
            this._staticTween = false;

            let deltaTime:number = 3000 / 360;
            let currentAngle:number = (this._wheel.rotation * 180) / Math.PI;
            //let newAngle:number = 360 - (currentAngle % 360) + (72 *  (5 - data)) + this.getRandomInt(-33, 33);
            let newAngle:number = 360 - (currentAngle % 360) + (36 *  (10 - data)) + this.getRandomInt(-16, 16);
            newAngle = newAngle % 360;
            newAngle += 360;
            let stopTween:Phaser.Tween = this.game.add.tween(this._wheel).to({rotation: this._wheel.rotation + (Math.PI * newAngle) / 180}, newAngle * deltaTime, Phaser.Easing.Quintic.Out, true);
            stopTween.onComplete.add(function () {
                this._started = false;
                this._state = 'stoped';
                this.actionSignal.dispatch({action:'stoped'});
            }, this);
        }

        protected getRandomInt(min:number, max:number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        public update():void {
            super.update();

            if(this._staticTween) {
                this._wheel.rotation += (Math.PI * 7) / 180;
            }

            if(this.started) {
                let currentAngle:number = (this._wheel.rotation * 180) / Math.PI;
                this.actionSignal.dispatch({action: 'rotate', angle: currentAngle % 360});
            }
        }

        protected getPatternData(parent:any, childName:string):any {
            for(let i:number = 0; i < parent.child.length; i++) {
                let childPattern:any = parent.child[i];
                if (childPattern.name == childName) {
                    return childPattern;
                }
            }
            return null;
        }

        /**
         *
         * @returns {Phaser.Signal}
         */
        public get actionSignal(): Phaser.Signal {
            return this._actionSignal;
        }

        public get started(): boolean {
            return this._started;
        }

        public get state(): string {
            return this._state;
        }

    }
}