﻿///<reference path="../../../engine/model/states/BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class SpinnersBonusIdleState extends BaseState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.IDLE_BONUS_GAME_STATE;
        }

        public checkData(): boolean {
            return this.model.hasBonus;
        }
    }
}

