﻿///<reference path="../../../engine/controller/interfaces/IActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class DragonGirlDecorationsController extends BaseGameDecorationsController {

        protected columnLeftFire:AnimatedSprite;
        protected columnRightFire:AnimatedSprite;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, layer, debugMode, pattern);
        }

        public init(pattern: any): void {
            this.frame = new SimpleSprite(this.game, pattern.usual_frame.x, pattern.usual_frame.y, pattern.usual_frame.name, this.debugMode);
            this.layer.add(this.frame);

            this.freeSpinsFrame = new SimpleSprite(this.game, pattern.x, pattern.y, 'usual_frame_freeSpins', this.debugMode);
            this.freeSpinsFrame.visible = false;
            this.layer.add(this.freeSpinsFrame);

            let woodLeft:SimpleSprite = new SimpleSprite(this.game, pattern.wood_left.x - 7, pattern.wood_left.y, 'decoration_wood', this.debugMode);
            woodLeft.scale.x = -1;
            this.layer.add(woodLeft);

            let woodRight:SimpleSprite = new SimpleSprite(this.game, pattern.wood_right.x, pattern.wood_right.y, 'decoration_wood', this.debugMode);
            this.layer.add(woodRight);

            let locale:string = this.game.textFactory.currLocale;
            this.logo = new SimpleSprite(this.game, pattern.game_logo.x, pattern.game_logo.y, 'game_logo', this.debugMode, 'logo_' + locale);
            this.logo.anchor.set(0.5, 0.5);
            this.layer.add(this.logo);

            this.columnLeftFire = new AnimatedSprite(this.game, 0, 0, 'columnLeftFire_animation', this.debugMode);
            this.columnLeftFire.actionSignal.add(this.onFireAnimationComplete, this);
            //this.columnLeftFire.play('start');
            this.layer.add(this.columnLeftFire);

            this.columnRightFire = new AnimatedSprite(this.game, 1170, 0, 'columnRightFire_animation', this.debugMode);
            this.columnRightFire.actionSignal.add(this.onFireAnimationComplete, this);
            //this.columnRightFire.play('start');
            this.layer.add(this.columnRightFire);

            this.columnLeftFire.visible = false;
            this.columnRightFire.visible = false;

            this.game.textFactory.actionSignal.add(this.onLocaleChanged, this);
        }

        protected onFireAnimationComplete(data:any):void {
            if(data == AnimatedSprite.ALL_ANIMATIONS_COMPLETE) {
                this.columnLeftFire.visible = false;
                this.columnRightFire.visible = false;
            }
        }

        protected onLocaleChanged(data:any):void {
            this.logo.frameName = 'logo_' + data.data;
        }


        public changeFrame(freeSpins:boolean): void {
            let tween:Phaser.Tween;
            this.freeSpinsFrame.visible = true;
            if(freeSpins) {
                this.freeSpinsFrame.alpha = 0;
                tween = this.game.add.tween(this.freeSpinsFrame).to({alpha:1}, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                tween.onComplete.add(function ():void {
                    this.frame.visible = false;
                }, this);
                this.columnLeftFire.visible = true;
                this.columnRightFire.visible = true;
                this.columnLeftFire.play('start');
                this.columnRightFire.play('start');
            } else {
                this.frame.visible = true;
                tween = this.game.add.tween(this.freeSpinsFrame).to({alpha:0}, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                tween.onComplete.add(function ():void {
                    this.freeSpinsFrame.visible = false;
                }, this);
                this.columnLeftFire.play('end');
                this.columnRightFire.play('end');
            }
        }

    }
}