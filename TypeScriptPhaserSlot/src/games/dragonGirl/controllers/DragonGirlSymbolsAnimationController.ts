///<reference path="../../../engine/controller/SymbolsAnimationController.ts" />

module TypeScriptPhaserSlot.common {

    export class DragonGirlSymbolsAnimationController extends SymbolsAnimationController {

        private _wildLayer:Phaser.Group;

        /**
         *
         */
        protected correctSpinResult():void {
            for(let i:number = 0; i < this.spinResult.reels.length; i++) {

                //Трансформация принцесс в ведьм
                //if (/*this.game.model.isFreespins &&*/ this.checkForWild()) {
                    for(let k:number = 0; k < this.spinResult.reels[i].length; k++) {
                        if(this.spinResult.reels[i][k] == 12 || this.spinResult.reels[i][k] == 13) {
                            this.spinResult.reels[i][k] = 200;
                        }
                    }
                //}

                let startIndex:number = this.spinResult.reels[i].indexOf(10);
                let endIndex:number = this.spinResult.reels[i].lastIndexOf(10);
                let count:number = 0;
                while(this.spinResult.reels[i][startIndex + count] == 10) {
                    count++;
                }

                let arr:number[] = [];
                if(startIndex == 0 && count == 1) {
                    arr = [102];
                }
                if(startIndex == 0 && count == 2) {
                    arr = [101, 102];
                }
                if(startIndex == 0 && count == 3) {
                    arr = [100, 101, 102];
                }
                if(startIndex == 1 && count == 2) {
                    arr = [100, 101];
                }
                if(startIndex == 2 && count == 1) {
                    arr = [100];
                }

                for(let j:number = startIndex; j < endIndex + 1; j++) {
                    this.spinResult.reels[i][j] = arr.shift();
                }
            }
        }

        /**
         *
         * @returns {boolean}
         */
        private checkForWild():boolean {
            /*let wilds:number[] = [10,100,101,102];
            for(let i:number = 0; i < this.game.model.result.reels.length; i++) {
                for(let k:number = 0; k < this.game.model.result.reels[i].length; k++) {
                    if (wilds.indexOf(this.game.model.result.reels[i][k]) > -1) {
                        return true;
                    }
                }
            }
            return false;*/

            for(let i:number = 0; i < this.game.model.result.reels.length; i++) {
                let witches:number[] = this.game.model.result.reels[i].filter(function (value:number):boolean {
                    return value == 10;
                });
                if(witches.length == this.game.model.result.reels[i].length) {
                    return true;
                }
            }
            return false;
        }

        /**
         *
         * @param reelIndex
         * @param symbolIndex
         * @param symbolData
         */
        protected animatSymbol(reelIndex: number, symbolIndex: number, symbolData:any): void {
            let animation: AnimatedSprite = this.createAnimation(symbolData);
            animation.x = this.pattern.reels[reelIndex].x;
            animation.y = this.pattern.reels[reelIndex]["icon" + symbolIndex].y;

            animation.actionSignal.addOnce(this.onAnimationAction, this);

            if(symbolData < 100) {
                this.layer.add(animation);
            } else {
                this.wildLayer.add(animation);
            }


            this.animations.push(animation);

            animation.start();
        }

        /**
         *
         * @returns {Phaser.Group}
         */
        public get wildLayer():Phaser.Group {
            return this._wildLayer;
        }

        /**
         *
         * @param value
         */
        public set wildLayer(value: Phaser.Group) {
            this._wildLayer = value;
            this._wildLayer.x = this.pattern.x;
            this._wildLayer.y = this.pattern.y;

            let mask = new Phaser.Graphics(this.game, 0, 0);
            mask.beginFill(0xffffff, 0.5);
            let w:number = 171;
            let h:number = 171;
            mask.drawRect(-(w / 2), -(h / 2), w * 5 + w, h * 3 + h);

            this._wildLayer.add(mask);
            this._wildLayer.mask = mask;
        }
    }
}