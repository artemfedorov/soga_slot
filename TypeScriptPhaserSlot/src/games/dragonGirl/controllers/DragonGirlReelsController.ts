﻿///<reference path="../../../engine/controller/ReelsController.ts" />
///<reference path="../views/DragonGirlReelView.ts" />

module TypeScriptPhaserSlot.common {

    export class DragonGirlReelsController extends ReelsController {

        private respinReelView: IReelView;
        private intrigueBorder: SimpleSprite;
        private waveAnimation: AnimatedSprite;
        private isRespin: boolean = false;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfiguration: GameConfiguration, pattern: any, gameStateResponse: GameStateResponse) {
            super(game, name, layer, debugMode, symbolProperties, gameConfiguration, pattern, gameStateResponse);
            this.waveAnimation = new AnimatedSprite(this.game, -120, -this.layer.y, 'respin_wave_animation');
            this.waveAnimation.actionSignal.add(this.onWaveAnimationComplete, this);

            let mask = new Phaser.Graphics(this.game, 0, 0);
            mask.beginFill(0xffffff, 0.5);
            let w: number = 171;
            let h: number = 171;
            mask.drawRect(-(w / 2), -(h / 2), w * 5 + w, h * 3 + h);

            //TODO: вернуть маску если нужно
            this.layer.add(mask);
            this.layer.mask = mask;

            this.intrigueBorder = new SimpleSprite(this.game, -55, -90, 'reel_intrigue');
            let intrigueFrames: number[] = [];
            for (let i: number = 0; i < 17; i++) {
                intrigueFrames.push(i);
            }
            this.intrigueBorder.animations.add('play', intrigueFrames, 40, true, true);
            //this.intrigueBorder.scale.set(2, 2);
        }

        /**
         *
         * @param sprite
         * @param animation
         */
        private onWaveAnimationComplete(data): void {
            if(data != AnimatedSprite.ALL_ANIMATIONS_COMPLETE) {
                return;
            }
            if(this.waveAnimation.parent) {
                this.waveAnimation.parent.removeChild(this.waveAnimation);
            }
        }

        /**
         * DO NOT CALL SUPER!
         * @param {number} index
         * @param {string} name
         * @param {boolean} debugMode
         * @param {SymbolProperties[]} symbolProperties
         * @param {GameConfiguration} gameConfiguration
         * @param {any} pattern
         * @param {number[]} dataReel
         * @returns
         */
        protected buildReel(index: number, name: string, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfiguration: GameConfiguration, pattern: any, dataReel: number[], borderLayer?: Phaser.Group): IReelView {
            let reelView: IReelView;
            reelView = new DragonGirlReelView(this.game, index, name, debugMode, symbolProperties, gameConfiguration, pattern, dataReel, null, null, this.borderLayer);
            reelView.justTouchedStopSignal.add(this.onJustTouchedReelStop, this);
            reelView.actionSignal.add(this.onReelFinnalyStopped, this);
            reelView.x = this.pattern.reels[index].x;
            reelView.y = this.pattern.reels[index].y;
            return reelView;
        }

        /**
         *
         * @param data
         */
        public respin(data?: any): void {
            super.respin(data);
            this.respinReelView = this.buildReel(0, "PrincessFrogReelView", this.debugMode, this.symbolProperties, this.gameConfiguration, this.pattern, [1, 1, 1]/*data.reels[0]*/, this.borderLayer);
            this.respinReelView.x = this.pattern.reels[0].x - 171;
            this.reelViews.unshift(this.respinReelView);
            this.layer.add(this.respinReelView);
            this.respinReelView.alpha = 0;
            this.respinReelView.changeSpeed(1);
            this.isRespin = true;
            this.respinReelView.start();
            for (let i: number = 0; i < this.reelViews.length; i++) {
                this.reelViews[i].index = i;
                this.game.add.tween(this.reelViews[i]).to({x: this.reelViews[i].x + 171}, 500, Phaser.Easing.Cubic.Out, true,);
                if (i == 0) {
                    this.game.add.tween(this.reelViews[i]).to({alpha: 1}, 500, Phaser.Easing.Cubic.Out, true);
                }
                if (i == this.reelViews.length - 1) {
                    this.game.add.tween(this.reelViews[i]).to({alpha: 0}, 500, Phaser.Easing.Cubic.Out, true);
                }
            }

            this.waveAnimation.play('start');
            SoundController.instance.playSound('respin', 'default');
            this.layer.add(this.waveAnimation);

            setTimeout(() => {
                SoundController.instance.playSound('start_spin_sound', 'default', 0.1, true);
            }, 300);
        }

        /**
         *
         * @param data
         */
        public stopRespin(data: any): void {
            this.alignReels();

            this.respinData = data;
            this.respinReelView.stop(this.respinData.reels[0].concat(), this.additionalSymbolsNumbers[0]);
            /*this.waveAnimation.visible = false;
             this.waveAnimation.animations.stop();*/
            this.reelViews[this.reelViews.length - 1].destroy(true);
            this.reelViews.splice(this.gameConfiguration.reels.length);
            this.isPendingRespinComplete = true;
        }

        /**
         * Overriden.
         * Signals when particular reel touched the ground:) before ending.
         * @param {number} reelIndex
         */
        protected onJustTouchedReelStop(reelIndex: number): void {
            super.onJustTouchedReelStop(reelIndex);

            for (let l: number = 0; l < this.reelViews[reelIndex].symbols.length; l++) {
                let item: ISymbol = this.reelViews[reelIndex].symbols[l];
                item['actionOnStopReel']();
            }

            if (this.checkForWild(reelIndex)) {
                SoundController.instance.playSound('wild_boom', 'default', 1, false);
                let tween:Phaser.Tween = this.game.tweens.create(this.layer).to({ y: this.pattern.y + 20}, 50, Phaser.Easing.Cubic.Out, true, 0, 2, true);
                tween.onComplete.addOnce(function ():void {
                    this.layer.y = this.pattern.y;
                }, this);
            }
            if (this.isRespin) {
                let stopSound: Phaser.Sound = SoundController.instance.getSound("start_spin_sound", 'default');
                if (stopSound) {
                    stopSound.stop();
                }
            }
        }

        /**
         *
         * @param reelIndex
         * @returns {boolean}
         */
        protected checkForWild(reelIndex: number): boolean {
            let data: any = this.isRespin ? this.respinData : this.stopSpinData;
            for (let i: number = 0; i < data.reels[reelIndex].length; i++) {
                if (data.reels[reelIndex][i] == 10) {
                    return true;
                }
            }
            return false;
        }

        /**
         *
         * @param reelIndex
         */
        protected onReelFinnalyStopped(reelIndex: number): void {
            super.onReelFinnalyStopped(reelIndex);

            //this.game.tweens.create(this.reelViews[reelIndex]).to({ alpha: 1 }, 600, Phaser.Easing.Default, true);
        }


        /**
         *
         */
        protected startIntrigue(reelIndex: number): void {
            this.reelViews[reelIndex].changeSpeed(1);
            this.intrigueBorder.x = this.pattern.reels[reelIndex].x - 105;
            this.layer.add(this.intrigueBorder);
            if (!this.intrigueBorder.animations.currentAnim.isPlaying) {
                this.intrigueBorder.animations.play("play");
                SoundController.instance.playSound('intrigue', 'default', 0.3, false);
            }

        }

        /**
         * Override this to stop your specific intrigue like a animation and so on.
         */
        protected stopIntrigue(): void {
            let stopSound: Phaser.Sound = SoundController.instance.getSound("intrigue", 'default');
            if (stopSound) {
                stopSound.stop();
            }
            this.intrigueBorder.animations.stop();
            this.intrigueBorder.animations.frame = 0;
            this.layer.removeChild(this.intrigueBorder);
        }

        /**
         *
         * @param line
         */
        public animateSymbolsOfLine(line: Payline): void {
            super.animateSymbolsOfLine(line);

            let index: number;
            for (let j: number = line.offset; j < line.offset + line.icon_count; j++) {
                index = line.line[j];
                this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount].visible = false;

                if (this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount - 1].ownIndex == 100) {
                    this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount - 1].visible = false;
                    this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount + 1].visible = false;
                }

                if (this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount - 1].ownIndex == 101) {
                    this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount - 1].visible = false;
                    this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount - 2].visible = false;
                }
            }
        }

        /**
         *
         * @param data
         */
        public start(data?: any): void {
            super.start(data);
            this.isRespin = false;
            //for(let i:number = 0; i < this.reelViews.length; i++) {
            //    this.game.tweens.create(this.reelViews[i]).to({ alpha: 0.5 }, 600, Phaser.Easing.Default, true);
            //}
        }

        /**
         *
         * @param data
         * @param reelIndex
         */
        protected reelStop(data: TypeScriptPhaserSlot.common.SpinResult, reelIndex: number): void {
            super.reelStop(data, reelIndex);

            //this.game.tweens.create(this.reelViews[reelIndex]).to({ alpha: 1 }, 600, Phaser.Easing.Default, true);
        }

        /**
         *
         */
        public transformPrincess(): boolean {
            SoundController.instance.playSound('transform', 'default', 0.4, false);
            let result:boolean = false;
            for (let i: number = 0; i < this.reelViews.length; i++) {
                for (let j: number = 0; j < this.reelViews[i].symbols.length; j++) {
                    let item: ISymbol = this.reelViews[i].symbols[j];
                    if(item.ownIndex == 12 || item.ownIndex == 13) {
                        item['transformToWild']();
                        item.changeKey(this.game, this.symbolProperties[4]);
                        result = true;
                    }
                }
            }
            return result;
        }
    }
}