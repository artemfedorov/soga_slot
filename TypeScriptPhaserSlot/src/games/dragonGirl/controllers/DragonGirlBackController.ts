﻿///<reference path="../../baseGame/controllers/BaseGameBackController.ts" />

module TypeScriptPhaserSlot.common {

    export class DragonGirlBackController extends BaseGameBackController {

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, layer, debugMode, pattern);
        }

        /**
        * Building reel views
        */
        public init(pattern: any): void {
            this.view = new SimpleSprite(this.game, pattern.background.x, pattern.background.y, pattern.background.name, this.debugMode);
            this.layer.add(this.view);

            this.freeSpinsBG = new SimpleSprite(this.game, pattern.background.x, pattern.background.y, 'background_freeSpins', this.debugMode);
            this.freeSpinsBG.visible = false;
            this.layer.add(this.freeSpinsBG);

            //this.layer.add(new Phaser.Image(this.game, pattern.usual_frame_bg.x, pattern.usual_frame_bg.y, pattern.usual_frame_bg.name));
        }

        /**
         *
         * @param freeSpins
         */
        public changeBackground(freeSpins:boolean): void {
            let tween:Phaser.Tween;
            this.freeSpinsBG.visible = true;
            if(freeSpins) {
                this.freeSpinsBG.alpha = 0;
                tween = this.game.add.tween(this.freeSpinsBG).to({alpha:1}, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                tween.onComplete.add(function ():void {
                    this.view.visible = false;
                    SoundController.instance.playSound('fs_backsnd', 'default', 0.4, true);
                }, this);
            } else {
                this.view.visible = true;
                tween = this.game.add.tween(this.freeSpinsBG).to({alpha:0}, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                tween.onComplete.add(function ():void {
                    this.freeSpinsBG.visible = false;
                    let stopSound: Phaser.Sound = SoundController.instance.getSound("fs_backsnd", 'default');
                    if (stopSound) {
                        stopSound.stop();
                    }
                }, this);
            }
        }

    }
}