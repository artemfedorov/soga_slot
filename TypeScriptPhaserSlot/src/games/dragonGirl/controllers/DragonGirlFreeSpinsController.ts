///<reference path="../../../engine/controller/FreeSpinsController.ts" />

module TypeScriptPhaserSlot.common {

    export class DragonGirlFreeSpinsController extends FreeSpinsController {

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, gameConfiguration: GameConfiguration, pattern: any, gameStateResponse: GameStateResponse) {
            super(game, name, layer, debugMode, gameConfiguration, pattern, gameStateResponse);

        }

        public init():void {
            super.init();

        }

        /**
         *
         * @param count
         */
        public startFreeSpins(count:number):void {
            let text:LocaleText = this.game.textFactory.getTextWithStyle("start_free_spins", "FREE_SPINS_TEXT_style");
            text.anchor.set(0.5, 0.5);
            text.x = 683;
            text.y = 384;
            text.text = count + text.text;
            text.alpha = 0;
            this.layer.add(text);

            let tween:Phaser.Tween = this.game.add.tween(text).to({alpha:1}, 500, Phaser.Easing.Linear.None, true, 0, 0, true);
            tween.yoyoDelay(1000);
            tween.onComplete.addOnce(function ():void {
                text.destroy(true);
                this.actionSignal.dispatch({type:'start'});
            }, this);
        }

        /**
         *
         * @param count
         */
        public addedFreeSpins(count:number):void {
            let text:LocaleText = this.game.textFactory.getTextWithStyle("added_free_spins", "FREE_SPINS_TEXT_style");
            text.anchor.set(0.5, 0.5);
            text.x = 683;
            text.y = 384;
            text.text = count + text.text;
            text.alpha = 0;
            this.layer.add(text);

            let tween:Phaser.Tween = this.game.add.tween(text).to({alpha:1}, 500, Phaser.Easing.Linear.None, true, 0, 0, true);
            tween.yoyoDelay(1000);
            tween.onComplete.addOnce(function ():void {
                text.destroy(true);
                this.actionSignal.dispatch({type:'added'});
            }, this);
        }

        /**
         *
         */
        public endOfFreeSpins(data:any):void {
            let text:LocaleText = this.game.textFactory.getTextWithStyle("end_free_spins", "FREE_SPINS_TEXT_style");
            text.anchor.set(0.5, 0.5);
            text.x = 683;
            text.y = 384;
            text.alpha = 0;
            text.text += MoneyFormatController.instance.format(data, true);
            this.layer.add(text);

            let tween:Phaser.Tween = this.game.add.tween(text).to({alpha:1}, 500, Phaser.Easing.Linear.None, true, 0, 0, true);
            tween.yoyoDelay(1000);
            tween.onComplete.addOnce(function ():void {
                text.destroy(true);
                this.actionSignal.dispatch({type:'end'});
            }, this);
        }

        /**
         *
         * @param left
         */
        public leftFreeSpins(left:number):void {
            let text:LocaleText = this.game.textFactory.getTextWithStyle("left_free_spins", "FREE_SPINS_TEXT_style");
            text.anchor.set(0.5, 0.5);
            text.x = 683;
            text.y = 384;

            let textString:string = (left - 1) + text.text;
            if(left == 1) {
                textString = "Last Free Spin";
            }

            text.text = textString;
            text.alpha = 0;
            this.layer.add(text);

            let tween:Phaser.Tween = this.game.add.tween(text).to({alpha:1}, 500, Phaser.Easing.Linear.None, true, 0, 0, true);
            tween.yoyoDelay(1000);
            tween.onComplete.addOnce(function ():void {
                text.destroy(true);
            }, this);
        }
    }
}