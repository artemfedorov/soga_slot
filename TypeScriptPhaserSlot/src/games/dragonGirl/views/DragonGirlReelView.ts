﻿///<reference path="../../../engine/view/reels/BaseReelView.ts" />
///<reference path="../../../engine/view/reels/ISymbol.ts" />
///<reference path="./DragonGirlSymbol.ts" />

module TypeScriptPhaserSlot.common {

    export class DragonGirlReelView extends BaseReelView {

        constructor(game: Game, index: number, name: string, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfig: GameConfiguration, pattern: any, data: number[], mapReel?: number[], mapReelFreespins?: number[], borderLayer?: Phaser.Group) {
            super(game, index, name, debugMode, symbolProperties, gameConfig, pattern, data, mapReel, mapReelFreespins, borderLayer);
            this.symbolHeight = this.symbolProperties[0].height;
            this.mapReelFreespins = mapReelFreespins ? mapReelFreespins : [];
            this.mapReel = mapReel ? mapReel : [];
            this.mapReelFreespinsShift = 0;
            this.mapReelShift = 0;
            this.topVisibledLimit = 0;
            this.setRegularMap();
            this.symbolHeight = this.gameConfig.standartSymbolSetting.height;
            this.bottomVisibleLimit = this.symbolHeight * this._reelConfig.visibledSymbols;
            this.bottomRemovedLimit = this.symbolHeight * (this._reelConfig.visibledSymbols + 1);

            let symbolsCount:number[] = [24, 36, 49, 58, 70];
            this.generateSymbolsBand(symbolsCount[index], 4, 11);
            this.updater = this.emptyFunction;
            this.init();
        }

        protected init(): void {
            this._symbols = [];
            //this.currentShift = 0;

            for(let i: number = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                this.initData.unshift(this.getNextKey());
            }

            for(let j: number = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                this.initData.push(this.getNextKey());
            }

            let addWild:any = this.checkForWild(this.initData);
            for(let k:number = addWild.startIndex; k < addWild.startIndex + addWild.changeCount; k++) {
                this.initData[k] = 10;
            }

            this.addSymbols(this.initData);

            let wildIndex:number = this.initData.indexOf(10);
            this.transformWild(wildIndex);

            for(let k:number = 0; k < this._symbols.length; k++) {
                this._symbols[k].y -= this.symbolHeight * this._reelConfig.topInvisibleSymbolsCount;
            }
        }

        /**
         *
         */
        protected generateSymbolsBand(length: number, minNumber: number, maxNumber: number): void {
            //super.generateSymbolsBand(length, minNumber, maxNumber);
            for (let i: number = 0; i < length; i++) {
                this.mapReel.push(this.getRandomKey(minNumber, maxNumber));
            }

            this.mapReel.forEach(function (symbolIndex:number, index:number, array:number[]):void {
                if(symbolIndex == 10) {
                    array.splice(index, 1);
                }
            }, this);

            this.mapReel.push(10);
            this.mapReel.push(10);
            this.mapReel.push(10);
        }


        /**
         * Overriden function called by ReelsController to begin rolling
         * @param {any} data
         */
        public start(data: any): void {
            /*this.mapReel = [];
            let symbolsCount:number[] = [24, 36, 49, 58, 70];
            this.generateSymbolsBand(symbolsCount[this.index], 4, this.symbolProperties.length);*/
            this.mapReel.forEach(function (symbolIndex:number, index:number, arr:number[]):void {
                if(symbolIndex == 100 || symbolIndex == 101 || symbolIndex == 102) {
                    arr[index] = 10;
                }
            }, this);
            this.currentMap = this.mapReel;

            super.start(data);
            this.tweenToTop();
        }

        /**
         *
         * @param reel
         * @returns {any}
         */
        protected checkForWild(reel: number[]): any {
            let checkResult:any = {};
            checkResult.changeCount = 0;
            checkResult.startIndex = 0;

            let count:number = 3;
            let startIndex:number = reel.indexOf(10);
            let index:number = startIndex;
            while(reel[index] == 10) {
                count--;
                index++
            }
            checkResult.changeCount = count == 3 ? 0 : count;
            checkResult.startIndex = startIndex == this.reelConfig.topInvisibleSymbolsCount ? this.reelConfig.topInvisibleSymbolsCount - checkResult.changeCount : this.reelConfig.topInvisibleSymbolsCount + this.reelConfig.visibledSymbols;
            return checkResult;
        }

        /**
         *
         * @param data
         * @param additionalSymbols
         */
        protected realStop(data: any, additionalSymbols?: number): void {
            this.updater = null;
            this.game.tweens.removeFrom(this.tweenedObject);
            this.resultData = data;

            this.currentShift = 0;

            for(let i: number = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                this.resultData.unshift(this.getNextKey());
            }

            for(let j: number = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                this.resultData.push(this.getNextKey());
            }

            for(let j: number = 0; j < additionalSymbols; j++) {
                this.resultData.push(this.getNextKey());
            }

            let addWild:any = this.checkForWild(this.resultData);
            for(let k:number = addWild.startIndex; k < addWild.startIndex + addWild.changeCount; k++) {
                this.resultData[k] = 10;
            }

            this.addSymbols(this.resultData, true);

            let wildIndex:number = this.resultData.indexOf(10);
            this.transformWild(wildIndex);

            this.resultedTween();
        }

        /**
         *
         */
        protected addNewSymbolsInStaticRolling(): void {
            let nextKeys: number[] = this.getNextKeys(1);
            this.addSymbols(nextKeys, true);

            if(nextKeys[0] == 10) {
                this.transformWild(0);
                this._symbols.forEach(function (symbol:ISymbol):void {
                    symbol.blur();
                }, this);
            }
        }

        /**
         *
         * @param startIndex
         */
        private transformWild(startIndex:number = 0):void {
            if(startIndex == -1) {
                return;
            }

            let count:number = 0;
            let indexes:number[] = [10, 100, 101, 102];
            while(indexes.indexOf(this._symbols[startIndex + count].ownIndex) > -1) {
                count++;
            }
            for(let i:number = startIndex; i < startIndex + count; i++) {
                //if(count == 3 && i == startIndex) {
                //    this._symbols[i].changeKey(this.game, this.getSymbolPropertiesByIndex(10));
                //} else {
                    let properties:SymbolProperties = this.getSymbolPropertiesByIndex(100 + (i - startIndex))
                    this._symbols[i].changeKey(this.game, properties);
                //}
            }
        }
    }
}