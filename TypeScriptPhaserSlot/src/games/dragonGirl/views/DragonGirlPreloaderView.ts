/**
 * Created by rocket on 21.07.2017.
 */
///<reference path="../../../engine/view/BaseView.ts" />

module TypeScriptPhaserSlot.common {

    export class DragonGirlPreloaderView extends BaseView implements IPreloaderView {

        private pattern: any;
        private animation:AnimatedSprite;

        constructor(game: Game, name: string, pattern: any, debugMode?: boolean) {
            super(game, name, debugMode);
            this.game = game;
            this.pattern = pattern;

            this.init();
        }

        private init(): void {
            let preloader:Phaser.Group = new Phaser.Group(this.game);

            let preloaderBackground:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'preloader_bg', this.debugMode);

            let image1:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'preloader_i1', this.debugMode);
            let image2:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'preloader_i2', this.debugMode);

            let progressbar:Phaser.Group = new Phaser.Group(this.game);

            let data:object = {
                key: 'preloader_progressbar',
                scale: {x: 1, y: 1},
                pivot: {x: 0, y: 0},
                anchor: {x: 0, y: 0},
                animations: [
                    {
                        name: 'start',
                        startFrame: 1,
                        endFrame: 50,
                        isLoop: true,
                        frameRate: 30
                    }
                ]
            };
            let params:AnimationConfiguration = new AnimationConfiguration(data);
            this.animation = new AnimatedSprite(this.game, 65, 175/*300*/, params, this.debugMode);
            this.animation.start();

            let mask:Phaser.Graphics = new Phaser.Graphics(this.game, 125, 215);
            mask.beginFill(0xffffff, 1);
            mask.drawCircle(0, 0, 100);
            mask.endFill();

            progressbar.add(image2);

            progressbar.add(mask);
            progressbar.add(this.animation);

            progressbar.add(image1);


            progressbar.x = 558;
            progressbar.y = 270;

            preloader.add(preloaderBackground);
            preloader.add(progressbar);

            this.animation.mask = mask;

            this.add(preloader);
        }

        public onLoadingUpdate(progress:number, cacheKey:string, success:boolean, totalLoaded:number, totalFiles:number):void {
            /*let max:number = 220 / totalFiles;
            let percent:number = max % progress;
            this.animation.y = 520 - ((max * totalLoaded) + percent);*/

            //let oneFileHeight:number = 110 / totalFiles;
            //this.animation.y = 260 - ((oneFileHeight * totalLoaded) + (oneFileHeight % progress));

            let percent:number = (100 / totalFiles) * totalLoaded + (progress / 100) - 1;
            this.animation.y = 260 - (110 / 100 * percent);

            if(window['GR']) {

                window['GR'].Events.game.progress({ progress: percent });
            }
        }

        public onLoadingComplete():void {

        }
    }
}