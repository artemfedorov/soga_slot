﻿///<reference path="../../../engine/view/reels/BaseReelSymbol.ts" />

module TypeScriptPhaserSlot.common {

    export class DragonGirlSymbol extends BaseReelSymbol {

        protected _disabledFrame:SimpleSprite;
        protected _blurredFrame:SimpleSprite;

        constructor(game: Phaser.Game, x: number, y: number, symbolProperties: SymbolProperties, debugMode?: boolean) {
            super(game, x, y, symbolProperties, debugMode);
        }

        protected init(): void {
            super.init();
            //this.symbolImage.x -= 11.5;
            //this.symbolImage.y -= 7.5;

            this.symbolImage.x -= this.currentSymbolProperties.pivot.x;
            this.symbolImage.y -= this.currentSymbolProperties.pivot.y;

            this._disabledFrame = new SimpleSprite(this.game, 0, 0, 'symbols_disabled', this.debugMode, this.currentSymbolProperties.key + '_disabled');
            this._disabledFrame.x -= this.currentSymbolProperties.pivot.x;
            this._disabledFrame.y -= this.currentSymbolProperties.pivot.y;
            this._disabledFrame.visible = false;
            this.add(this._disabledFrame);

            this._blurredFrame = new SimpleSprite(this.game, 0, 0, 'symbols_blurred', this.debugMode, this.currentSymbolProperties.key + '_blurred');
            this._blurredFrame.x -= this.currentSymbolProperties.pivot.x;
            this._blurredFrame.y -= this.currentSymbolProperties.pivot.y;
            this._blurredFrame.visible = false;
            this.add(this._blurredFrame);
        }

        public changeKey(game: Phaser.Game, symbolProperties: SymbolProperties): void {
            this.ownIndex = symbolProperties.index;
            this.symbolImage.frameName = symbolProperties.key;
            this._disabledFrame.frameName = symbolProperties.key + '_disabled';
            this._blurredFrame.frameName = symbolProperties.key + '_blurred';
            this.game = game;
            this._borderKey = symbolProperties.borderKeys[0];
            this._currentSymbolProperties = symbolProperties;

            this.symbolImage.x = -this.currentSymbolProperties.pivot.x;
            this.symbolImage.y = -this.currentSymbolProperties.pivot.y;
            this._disabledFrame.x = -this.currentSymbolProperties.pivot.x;
            this._disabledFrame.y = -this.currentSymbolProperties.pivot.y;
            this._disabledFrame.visible = false;
            this._blurredFrame.visible = false;
        }

        public activate():void {
            if(this.symbolImage.frameName == 'icon_empty') {
                this._disabledFrame.visible = false;
                return;
            }

            //this._disabledFrame.visible = false;
            this.game.tweens.removeFrom(this._disabledFrame, true);
            this._disabledFrame.alpha = 1;
            this.game.add.tween(this._disabledFrame).to({ alpha: 0 }, 300, "Linear", true).onComplete.addOnce(function () {
                this._disabledFrame.visible = false;
            }.bind(this));

            /*if(this.scale.x == 1) {
                return;
            }
            this.game.tweens.removeFrom(this, true);
            this.game.add.tween(this).to({ alpha: 1 }, 100, "Linear", true);
            this.game.add.tween(this.scale).to({ x: 1, y: 1 }, 100, "Linear", true);
            let delta:number = ((1 - this.scale.x) * 342) / 2;
            this.game.add.tween(this).to({ x: this.x - delta, y: this.y - delta }, 100, "Linear", true);*/
        }

        public deactivate():void {
            if(this.symbolImage.frameName == 'icon_empty') {
                this._disabledFrame.visible = false;
                return;
            }

            this._disabledFrame.visible = true;
            this.game.tweens.removeFrom(this._disabledFrame, true);
            this._disabledFrame.alpha = 0;
            this.game.add.tween(this._disabledFrame).to({ alpha: 1 }, 300, "Linear", true);

            /*if(this.scale.x != 1) {
                return;
            }
            let newScale:number = 0.9;
            this.game.tweens.removeFrom(this, true);
            this.game.add.tween(this).to({ alpha: 0.9 }, 100, "Linear", true);
            this.game.add.tween(this.scale).to({ x: newScale, y: newScale }, 100, "Linear", true);
            let delta:number = ((this.scale.x - newScale) * 342) / 2;
            this.game.add.tween(this).to({ x: this.x + delta, y: this.y + delta }, 100, "Linear", true);*/
        }

        public actionOnStopReel():void {
            if(this.symbolImage.frameName == 'icon_witch_3') {
                let text:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'symbols', this.debugMode, 'wild_txt');
                text.x = 10   ;
                text.y = 110;
                text.alpha = 0;
                this.game.add.tween(text).to({ alpha: 1 }, 500, "Linear", true, 0, 0, true).onComplete.add(function () {
                    text.parent.removeChild(text);
                    text.destroy(true);
                }.bind(this));
                this.addChild(text);
            }
        }

        public transformToWild():void {
            let data:any = {};
            data.key = 'transform_to_wild';
            data.scale = {x:1, y:1};
            data.pivot = {x:50, y:46};
            data.anchor = {x:0, y:0};
            data.animations = [
                {
                    name: 'start',
                    startFrame: 1,
                    endFrame: 36,
                    isLoop: false,
                    frameRate: 30
                }
            ];
            let params:AnimationConfiguration = new AnimationConfiguration(data);
            let animation: AnimatedSprite = new AnimatedSprite(this.game, 0, 0, params, this.debugMode);
            animation.actionSignal.add(function () {
                animation.parent.removeChild(animation);
                this.symbolImage.visible = true;
            }, this);
            animation.start();
            this.addChild(animation);
            this.symbolImage.visible = false;
        }

        public blur():void {
            this.symbolImage.visible = false;
            this._blurredFrame.visible = true;
        }

        public unblur():void {
            this.symbolImage.visible = true;
            this._blurredFrame.visible = false;
        }
    }
}