﻿///<reference path="../../../engine/model/states/BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class DragonGirlIdleState extends BaseState implements IState {

        private first:boolean = false;

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.IDLE_STATE;
        }

        public checkData(): any {
            return true;
        }

        protected doWork(data: any, isRequiredAction: boolean): void {
            super.doWork(data, isRequiredAction);

            if(this.first) {
                return;
            }
            this.first = true;
            if(this._model.result && (this._model.result.paylines.length > 0 || this._model.result.scatters.length > 0)) {
                this._model.goToState(StatesConstants.PRINCESS_FROG_TRANSFORM_PRINCESS_STATE);
            }
        }

        public finishWork(): void {
            this.debuglog("- finish work");
            this.finishWorkSignal.dispatch(true);
        }
    }
}

