/**
 * Created by rocket on 21.07.2017.
 */
///<reference path="../../../engine/view/BaseView.ts" />

module TypeScriptPhaserSlot.common {

    export class CaribbeanGirlsPreloaderView extends BaseView implements IPreloaderView {

        private pattern: any;
        private animation:AnimatedSprite;

        constructor(game: Game, name: string, pattern: any, debugMode?: boolean) {
            super(game, name, debugMode);
            this.game = game;
            this.pattern = pattern;

            this.init();
        }

        private init(): void {
            let preloader:Phaser.Group = new Phaser.Group(this.game);

            let preloaderBackground:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'preloader_bg', this.debugMode);

            let image1:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'preloader_i1', this.debugMode);
            image1.anchor.set(0.5, 0.5);
            image1.x += image1.width / 2;
            image1.y += image1.height / 2;
            
            this.game.add.tween(image1).to({rotation:(Math.PI * 360) / 180}, 1000, Phaser.Easing.Linear.None, true, 0, -1);

            let progressbar:Phaser.Group = new Phaser.Group(this.game);

            progressbar.add(image1);

            progressbar.x = (preloaderBackground.width - progressbar.width) / 2;
            progressbar.y = 500;

            preloader.add(preloaderBackground);
            preloader.add(progressbar);


            this.add(preloader);
        }

        public onLoadingUpdate(progress:number, cacheKey:string, success:boolean, totalLoaded:number, totalFiles:number):void {

        }

        public onLoadingComplete():void {

        }
    }
}