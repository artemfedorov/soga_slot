﻿///<reference path="../../../engine/view/reels/BaseReelSymbol.ts" />

module TypeScriptPhaserSlot.common {

    export class CaribbeanGirlsSymbol extends BaseReelSymbol {

        private staticAnimation: Phaser.Tween;
        private staticY: number;
        protected _disabledFrame:SimpleSprite;
        protected _blurredFrame:SimpleSprite;

        constructor(game: Phaser.Game, x: number, y: number, symbolProperties: SymbolProperties, debugMode?: boolean) {
            super(game, x, y, symbolProperties, debugMode);
        }

        protected init(): void {
            super.init();

            this.symbolImage.x -= this._currentSymbolProperties.pivot.x;
            this.symbolImage.y -= this._currentSymbolProperties.pivot.y;
            this.staticY = this.symbolImage.y;

            this._disabledFrame = new SimpleSprite(this.game, 0, 0, 'symbols_disabled', this.debugMode, this.currentSymbolProperties.key + '_disabled');
            this._disabledFrame.x -= this._currentSymbolProperties.pivot.x;
            this._disabledFrame.y -= this._currentSymbolProperties.pivot.y;
            this._disabledFrame.visible = false;
            this.add(this._disabledFrame);


            this._blurredFrame = new SimpleSprite(this.game, 0, 0, 'symbols_blurred', this.debugMode, this.currentSymbolProperties.key + '_blurred');
            this._blurredFrame.x -= this._currentSymbolProperties.pivot.x;
            this._blurredFrame.y -= this._currentSymbolProperties.pivot.y;
            this._blurredFrame.visible = false;
            this.add(this._blurredFrame);
        }

        public changeKey(game: Phaser.Game, symbolProperties: SymbolProperties): void {
            this.ownIndex = symbolProperties.index;
            this.symbolImage.frameName = symbolProperties.key;
            this._disabledFrame.frameName = symbolProperties.key + '_disabled';
            this.game = game;
            this._borderKey = symbolProperties.borderKeys[0];
            this._currentSymbolProperties = symbolProperties;
        }

        public activate():void {
            if(this.symbolImage.frameName == 'icon_empty') {
                this._disabledFrame.visible = false;
                return;
            }
            //this._disabledFrame.visible = false;
            this.game.tweens.removeFrom(this._disabledFrame, true);
            this._disabledFrame.alpha = 1;
            this.game.add.tween(this._disabledFrame).to({ alpha: 0 }, 300, "Linear", true).onComplete.addOnce(function () {
                this._disabledFrame.visible = false;
            }.bind(this));
        }

        /**
         *
         * @param {any} data
         */
        public start(data?: any): void {
            super.start(data);
            this.staticY = this.symbolImage.y;

            this.staticAnimation = this.game.add.tween(this.symbolImage).to({y: this.symbolImage.y + 10}, 1000, Phaser.Easing.Sinusoidal.InOut, false, 0, -1, true);
            this.staticAnimation.onUpdateCallback(this.animateSymbol, this);
            this.staticAnimation.onComplete.add(this.animateSymbol, this);
            this.staticAnimation.onLoop.add(this.animateSymbol, this);
            this.staticAnimation.onRepeat.add(this.animateSymbol, this);
            this.staticAnimation.onStart.add(this.animateSymbol, this);
            this.staticAnimation.start();
        }

        /**
         *
         */
        private animateSymbol(): void {
            this._disabledFrame.y = this.symbolImage.y;
        }
        /**
         *
         * @param data
         */
        public stop(data?: any): void {
            super.stop(data);
            if(this.staticAnimation) {
                this.staticAnimation.stop(true);
                this.symbolImage.y = this.staticY;
                this._disabledFrame.y = this.staticY;
            }
        }

        public deactivate():void {
            if(this.symbolImage.frameName == 'icon_empty') {
                this._disabledFrame.visible = false;
                return;
            }
            this._disabledFrame.visible = true;
            this.game.tweens.removeFrom(this._disabledFrame, true);
            this._disabledFrame.alpha = 0;
            this.game.add.tween(this._disabledFrame).to({ alpha: 1 }, 300, "Linear", true);
        }

        public actionOnStopReel():void {

        }

        public blur():void {
            this.symbolImage.visible = false;
            this._blurredFrame.visible = true;
        }

        public unblur():void {
            this.symbolImage.visible = true;
            this._blurredFrame.visible = false;
        }
    }
}