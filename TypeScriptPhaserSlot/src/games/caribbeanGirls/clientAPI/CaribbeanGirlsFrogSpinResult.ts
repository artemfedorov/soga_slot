﻿module TypeScriptPhaserSlot.common {

    export class CaribbeanGirlsSpinResult extends SpinResult {

        protected parse(rawData: any): SpinResult {
            //this._freespin_icons = rawData.freespin_icons;
            //this._bet = rawData.bet;
            this._payout = rawData.winnings.total;
            this._reels = rawData.combination;
            //this._mult = rawData.mult;
            this._paylines = [];
            this._scatters = [];
            for (let i: number = 0; i < rawData.winnings.info.length; i++) {
                let winItem: any = rawData.winnings.info[i];
                if(winItem.type == 'line') {
                    this._paylines.push(new CaribbeanGirlsPayline(rawData.winnings.info[i]));
                }
                if(winItem.type == 'scatter') {
                    this._scatters.push(new Scatter(rawData.winnings.info[i]));
                }
            }

            /*this._freespin_icons = rawData.freespin_icons;
            this._bet = rawData.bet;
            this._payout = rawData.payout;
            this._reels = rawData.reels;
            this._mult = rawData.mult;
            this._paylines = [];
            for (let i: number = 0; i < rawData.paylines.length; i++) {
                this._paylines.push(new Payline(rawData.paylines[i]));
            }*/

            return this;
        }
    }

    export class CaribbeanGirlsPayline extends Payline {
        protected parse(rawData: any): Payline {
            this._index = rawData.line_number;
            this._offset = 0;
            this._direction = "left";
            this._line = [];
            for(let i:number = 0; i < 5; i++) {
                let dataForPush:number = 0;
                if(rawData.indexes[i]) {
                    dataForPush = rawData.indexes[i][1];
                }
                this._line.push(dataForPush);
            }
            this._icon = 1;
            this._icon_count = rawData.indexes.length;
            this._coin_multiplier = rawData.multiplier;
            this._sum = rawData.sum;

           /* this._index = rawData.index;
            this._offset = rawData.offset;
            this._direction = rawData.direction;
            this._line = rawData.line;
            this._icon = rawData.icon;
            this._icon_count = rawData.icon_count;
            this._coin_multiplier = rawData.coin_multiplier;*/
            return this;
        }
    }
}