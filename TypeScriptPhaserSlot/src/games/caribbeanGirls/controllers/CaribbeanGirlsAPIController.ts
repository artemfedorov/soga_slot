///<reference path="../../../engine/controller/BaseController.ts" />
// We can get session right here: http://test-api.playvulkan.com/get-session

module TypeScriptPhaserSlot.common {

    export class CaribbeanGirlsAPIController extends BaseController implements IAPIController {

        private spinCounter: number;
        private isLocal: boolean = false;

        private sessionId:string = '';
        private userid:string = '';

        private gameName: string;
        private _gameList_signal: Phaser.Signal;
        private _gameState_signal: Phaser.Signal;
        private _spin_signal: Phaser.Signal;
        private _collect_signal: Phaser.Signal;
        private _gamble_signal: Phaser.Signal;
        private _exit_signal: Phaser.Signal;
        private _bonusGame_signal: Phaser.Signal;

        private lastResponseJsonObject:any;

        constructor(url: string, name: string, debugMode: boolean = false) {
            super(name, debugMode);
            this._gameList_signal = new Phaser.Signal();
            this._gameState_signal = new Phaser.Signal();
            this._spin_signal = new Phaser.Signal();
            this._collect_signal = new Phaser.Signal();
            this._gamble_signal = new Phaser.Signal();
            this._exit_signal = new Phaser.Signal();
            this._bonusGame_signal = new Phaser.Signal();
            this.spinCounter = 0;

            this.sessionId = window['gameConfig']['session_id'] || '';
        }

        protected encodeData(data:string):string {
            let keyHEX:any = CryptoJS.enc.Utf8.parse('xdP2dvQk');
            let encryptedString:CryptoJS.WordArray = CryptoJS.DES.encrypt(data, keyHEX, {mode : CryptoJS.mode.ECB, padding : CryptoJS.pad.Pkcs7});
            return encryptedString.toString();
        }

        protected decodeData(data:string):string {
            let keyHEX = CryptoJS.enc.Utf8.parse('xdP2dvQk');
            let dataStr:CryptoJS.DecryptedMessage = CryptoJS.DES.decrypt({ciphertext : CryptoJS.enc.Base64.parse(data)} as CryptoJS.WordArray, keyHEX, {mode : CryptoJS.mode.ECB, padding : CryptoJS.pad.Pkcs7});
            return dataStr.toString(CryptoJS.enc.Utf8);
        }

        protected getHash(data:string):string {
            return CryptoJS.MD5(data).toString();
            //let sss:any = CryptoJS.MD5(data);
            //return (sss as CryptoJS.WordArray).toString(CryptoJS.enc.Hex);
        }

        protected getParams(data:any): string {
            let arr:string[] = [];
            arr.push('data=' + encodeURIComponent(data.data));
            arr.push('hash=' + data.hash);
            return arr.join('&');
        }
        
        protected createParams(data:any):string {
            let jsonString:string = JSON.stringify(data);
            let encryptedData:string = this.encodeData(jsonString);
            let hash:string = this.getHash(jsonString);

            return this.getParams({data:encryptedData, hash:hash});
        }

        /**
         * Server requests
         */
        //Making a connection to server
        public getGameList(data?: any): void {
            //send request to server
            //------------------------

            //send signal to ApplicationProxy.ts
            this.gameList_signal.dispatch();
        }

        public getGameState(gameName: string): void {
            if (!this.isLocal) {
                this.gameName = gameName;
                let oReq = new XMLHttpRequest();
                oReq.onload = this.onGetGameState.bind(this);
                oReq.onerror = this.onError.bind(this);

                oReq.open('post', 'https://stage.lemurgamesystem.com/v1/games/spinners_way/init', true);
                oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                let paramsObj:object = {
                    session_id: this.sessionId,
                    platform_id: 2
                };
                let params:string = this.createParams(paramsObj);

                //let data:string[] = [];
                //data.push('session_id=' + this.sessionId);
                //data.push('platform_id=2');
                //let params: String = data.join('&');

                oReq.send(params);
            } else {
                this.onGetGameState();
            }
        }


        private onGetGameState(e?: any): void {
            let parseData: any = e ? e.target.response : CaribbeanGirlsResponseExamples.getGameState;

            parseData = this.decodeData(JSON.parse(parseData).data);

            this.debuglog("onGetGameState: " + parseData);
            let jsonObject: Object = JSON.parse(parseData);

            this.lastResponseJsonObject = jsonObject;
            this.gameState_signal.dispatch(new CaribbeanGirlsGameStateResponse(jsonObject));
        }

        private onError(e: Event): void {

        }

        /**
         *
         * @param data
         */
        public spin(data: any): void {
            if (!this.isLocal) {
                let oReq = new XMLHttpRequest();
                oReq.onload = this.onSpin.bind(this);
                oReq.onerror = this.onError;

                oReq.open('post', 'https://stage.lemurgamesystem.com/v1/games/spinners_way/events/spins', true);
                oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                //console.log("spin" + req);

                let paramsObj:object = {
                    session_id: this.sessionId,
                    bet_on_line: data.bet,
                    lines_amount: data.lines,
                    coins: 1,
                    denomination: 1,
                    /*shifter_combination: '[[4,11,10],[1,11,10],[10,11,3],[10,10,3],[10,10,10]]'*/
                };

                if(window.hasOwnProperty('shift') && window['shift'] != null) {
                    paramsObj['shifter_combination'] = window['shift'];
                    window['shift'] = null;
                }

                let params:string = this.createParams(paramsObj);

                oReq.send(params);
            } else {
                this.onSpin();
            }
        }

        public respin(data?:any): void {
            if (!this.isLocal) {
                let oReq = new XMLHttpRequest();
                oReq.onload = this.onSpin.bind(this);
                oReq.onerror = this.onError;

                oReq.open('post', 'https://stage.lemurgamesystem.com/v1/games/spinners_way/events/bonuses/automatic', true);
                oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                //console.log("spin" + req);

                let paramsObj:object = {
                    session_id: this.sessionId
                };
                let params:string = this.createParams(paramsObj);

                //let data:string[] = [];
                //data.push('session_id=' + this.sessionId);
                //let params: String = data.join('&');

                oReq.send(params);
            } else {
                this.onSpin();
            }
        }

        public freespin(data: any): void {
            if (!this.isLocal) {
                let oReq = new XMLHttpRequest();
                oReq.onload = this.onSpin.bind(this);
                oReq.onerror = this.onError;

                oReq.open('post', 'https://stage.lemurgamesystem.com/v1/games/spinners_way/events/bonuses/freespins', true);
                oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                //console.log("spin" + req);
                let paramsObj:object = {
                    session_id: this.sessionId
                };

                if(window.hasOwnProperty('shift') && window['shift'] != null) {
                    paramsObj['shifter_combination'] = window['shift'];
                    window['shift'] = null;
                }

                let params:string = this.createParams(paramsObj);

                //let data:string[] = [];
                //data.push('session_id=' + this.sessionId);
                //let params: String = data.join('&');

                oReq.send(params);
            } else {
                this.onSpin();
            }
        }

        /**
         *
         * @param {any} e?
         */
        private onSpin(e?: any): void {
            let parseData: any = e ? e.target.response : CaribbeanGirlsResponseExamples.res[this.spinCounter];
            this.spinCounter = this.spinCounter + 1 < CaribbeanGirlsResponseExamples.res.length ? this.spinCounter + 1 : 0;

            parseData = this.decodeData(JSON.parse(parseData).data);

            console.log("onSpin" + parseData);
            let jsonObject: Object = JSON.parse(parseData);

            this.lastResponseJsonObject = jsonObject;
            this.spin_signal.dispatch(new CaribbeanGirlsSpinResponse(jsonObject));
        }


        /**
         *
         *
         * @param data
         */
        public bonus(data: any): void {
            if (!this.isLocal) {
                let oReq = new XMLHttpRequest();
                oReq.onload = this.onBonus.bind(this);
                oReq.onerror = this.onError;

                oReq.open('post', 'https://stage.lemurgamesystem.com/v1/games/spinners_way/events/bonuses/automatic', true);
                oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                //console.log("spin" + req);

                let paramsObj:object = {
                    session_id: this.sessionId,
                };
                let params:string = this.createParams(paramsObj);

                oReq.send(params);
            } else {
                this.onBonus();
            }
        }

        /**
         *
         * @param {any} e?
         */
        private onBonus(e?: any): void {
            let parseData: any = e ? e.target.response : CaribbeanGirlsResponseExamples.res[this.spinCounter];
            this.spinCounter = this.spinCounter + 1 < CaribbeanGirlsResponseExamples.res.length ? this.spinCounter + 1 : 0;

            parseData = this.decodeData(JSON.parse(parseData).data);

            console.log("onBonus" + parseData);
            let jsonObject: Object = JSON.parse(parseData);

            this.lastResponseJsonObject = jsonObject;
            this._bonusGame_signal.dispatch(new BonusResponse(jsonObject));
        }


        //
        public collect(data?: any): void {
            //send request to server
            //------------------------

            this.debuglog(CaribbeanGirlsResponseExamples.collect);
            //send signal to ApplicationProxy.ts
            let jsonObject: Object = JSON.parse(CaribbeanGirlsResponseExamples.collect);
            this.collect_signal.dispatch(jsonObject);
        }

        //
        public gamble(data?: any): void {
            //send request to server
            //------------------------

            //send signal to ApplicationProxy.ts
            let jsonObject: Object = JSON.parse(CaribbeanGirlsResponseExamples.gamble);
            this.gamble_signal.dispatch(jsonObject);
        }

        //
        public exit(data?: any): void {
            //send request to server
            //------------------------

            //send signal to ApplicationProxy.ts
            let jsonObject: Object = JSON.parse(CaribbeanGirlsResponseExamples.exit);
            this.exit_signal.dispatch(jsonObject);
        }

        /************************************************************
         Getters and Setters
         *************************************************************/
        public get gameList_signal(): Phaser.Signal {
            return this._gameList_signal;
        }

        public get gameState_signal(): Phaser.Signal {
            return this._gameState_signal;
        }

        public get spin_signal(): Phaser.Signal {
            return this._spin_signal;
        }

        public get collect_signal(): Phaser.Signal {
            return this._collect_signal;
        }

        public get gamble_signal(): Phaser.Signal {
            return this._gamble_signal;
        }

        public get exit_signal(): Phaser.Signal {
            return this._exit_signal;
        }

        public get bonusGame_signal(): Phaser.Signal {
            return this._bonusGame_signal;
        }
    }
}