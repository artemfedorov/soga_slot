﻿///<reference path="../../baseGame/controllers/BaseGameDecorationsController.ts" />

module TypeScriptPhaserSlot.common {

    export class CaribbeanGirlsDecorationsController extends BaseGameDecorationsController {

        protected logo:AnimatedSprite;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, layer, debugMode, pattern);
        }

        protected createLogo():void {
            super.createLogo();

            this.logo = new AnimatedSprite(this.game, 412, -14, 'logo_animation', this.debugMode);
            this.animateLogo();
            this.layer.add(this.logo);
        }

        protected animateLogo():void {
            this.logo.start();
            setTimeout(function () {
                this.animateLogo();
            }.bind(this), 20000 + Math.random() * 10000);
        }
    }
}