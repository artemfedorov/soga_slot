﻿///<reference path="../../../engine/controller/interfaces/IActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class CaribbeanGirlsBackController extends ActionController implements IActionController {

        private view: SimpleSprite;
        private freeSpinsBG: SimpleSprite;
        private layer: Phaser.Group;
        private pattern: any;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, debugMode);
            this.layer = layer;
            this.pattern = pattern;
            this.init(pattern);
        }
        /**
        *
        */
        public init(pattern: any): void {
            this.view = new SimpleSprite(this.game, pattern.background.x, pattern.background.y, pattern.background.name, this.debugMode);
            this.layer.add(this.view);
        }

        public stopSpin() {

        }

        public startSpin() {

        }

        /**
         *
         * @param freeSpins
         */
        public changeBackground(freeSpins:boolean): void {
            /*let tween:Phaser.Tween;
            this.freeSpinsBG.visible = true;
            if(freeSpins) {
                this.freeSpinsBG.alpha = 0;
                tween = this.game.add.tween(this.freeSpinsBG).to({alpha:1}, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                tween.onComplete.add(function ():void {
                    this.view.visible = false;
                    SoundController.instance.playSound('fs_backsnd', 'default', 0.4, true);
                }, this);
            } else {
                this.view.visible = true;
                tween = this.game.add.tween(this.freeSpinsBG).to({alpha:0}, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                tween.onComplete.add(function ():void {
                    this.freeSpinsBG.visible = false;
                    let stopSound: Phaser.Sound = SoundController.instance.getSound("fs_backsnd", 'default');
                    if (stopSound) {
                        stopSound.stop();
                    }
                }, this);
            }*/
        }

    }
}