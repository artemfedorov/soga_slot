﻿///<reference path="../../../engine/controller/LinesController.ts" />

module TypeScriptPhaserSlot.common {

    export class CaribbeanGirlsLinesController extends LinesController {

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, configuration: GameConfiguration) {
            super(game, name, layer, debugMode, configuration);
        }
    }
}