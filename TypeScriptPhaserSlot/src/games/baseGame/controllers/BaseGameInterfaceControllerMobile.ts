﻿///<reference path="./BaseGameInterfaceController.ts" />

module TypeScriptPhaserSlot.common {

    export class BaseGameInterfaceControllerMobile extends BaseGameInterfaceController {

        /**
        * Initial creation
        */
        public init(): void {
            this.layer.fixedToCamera = true;

            super.init();

            this.game.scale.onSizeChange.add(function () {
                this.changePositions();
            },this, -1);
        }

        protected createElements():void {
            let bg:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 768 - 80);
            bg.name = 'background';
            bg.beginFill(0xCCCCCC, 0.2);
            bg.drawRect(0, 0, 1366, 80);
            bg.endFill();
            this.layer.add(bg);

            this.label_balance = this.game.textFactory.getTextWithStyle("balance_mobile");
            this.label_balance.name = 'label_balance';
            this.setTextPosition(this.label_balance, this.pattern.label_balance);

            this.balanceValueText = this.game.textFactory.getTextWithStyle("0", "style1_mobile");
            this.balanceValueText.name = 'value_balance';
            this.setTextPosition(this.balanceValueText, this.pattern.value_balance);

            this.label_win = this.game.textFactory.getTextWithStyle("win_mobile");
            this.label_win.name = 'label_win';
            this.setTextPosition(this.label_win, this.pattern.label_win);

            this.winValueText = this.game.textFactory.getTextWithStyle("0", "style1_mobile");
            this.winValueText.name = 'value_win';
            this.setTextPosition(this.winValueText, this.pattern.value_win);

            this.label_bet = this.game.textFactory.getTextWithStyle("bet_mobile");
            this.label_bet.name = 'label_bet';
            this.setTextPosition(this.label_bet, this.pattern.label_bet);

            this.betValueText = this.game.textFactory.getTextWithStyle("0", "style1_mobile");
            this.betValueText.name = 'value_bet';
            this.setTextPosition(this.betValueText, this.pattern.value_bet);

            this.label_lines = this.game.textFactory.getTextWithStyle("lines_mobile");
            this.label_lines.name = 'label_lines';
            this.setTextPosition(this.label_lines, this.pattern.label_lines);

            this.linesValueText = this.game.textFactory.getTextWithStyle("0", "style1_mobile");
            this.linesValueText.name = 'value_lines';
            this.setTextPosition(this.linesValueText, this.pattern.value_lines);

            this.label_totalBet = this.game.textFactory.getTextWithStyle("totalBet_mobile");
            this.label_totalBet.name = 'label_totalBet';
            this.setTextPosition(this.label_totalBet, this.pattern.label_totalBet);

            this.totalBetValueText = this.game.textFactory.getTextWithStyle("0", "style1_mobile");
            this.totalBetValueText.name = 'value_totalBet';
            this.setTextPosition(this.totalBetValueText, this.pattern.value_totalBet);

            this.label_autoplay = this.game.textFactory.getTextWithStyle("autoplay_mobile");
            this.label_autoplay.name = 'label_autoplay';
            this.setTextPosition(this.label_autoplay, this.pattern.label_autoplay);
            this.label_autoplay.visible = false;
        }

        public changePositions():void {
            let ratio:number = this.game.camera.width / this.game.camera.height;

            this.layer.children.forEach(function (child:any):void {
                //child.x -= this.game.camera.x / this.layer.scale.x;
                /*if(!child['baseX']) {
                    child['baseX'] = child.x;
                }
                child.x = child['baseX'] + ((1366 - this.game.camera.width) / 2);*/

                switch (child.name) {
                    case 'startSpin_btn':
                        child.x = 768 * ratio - /*child.width*/ 331;
                        child.y = (768 - /*child.width*/ 331) / 2;
                        child.alpha = 0.5;
                        break;
                    case 'stopSpin_btn':
                        child.x = 768 * ratio - /*child.width*/ 331;
                        child.y = (768 - /*child.width*/ 331) / 2;
                        child.alpha = 0.5;
                        break;
                    case 'maxBet_btn':
                        child.x = 768 * ratio - /*child.width*/ 195;
                        child.y = (768 - /*child.height*/ 195) / 2 + 240;
                        child.alpha = 0.5;
                        break;
                    case 'auto_btn':
                        child.x = 768 * ratio - /*child.width*/ 155;
                        child.y = (768 - /*child.height*/ 155) / 2 - 210;
                        child.alpha = 0.5;
                        break;
                    case 'info_btn':
                        child.x = 0;
                        child.y = 768 - 160;
                        child.alpha = 0.5;
                        break;
                    case 'background':
                        child.width = 768 * ratio;
                        child.x = 0;
                        break;
                    case 'bottomPanel':
                        child.x = ((768 * ratio) - 800) / 2;
                        break;
                }
            }, this);
        }
    }
}