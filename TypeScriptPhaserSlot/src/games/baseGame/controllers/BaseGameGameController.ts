﻿///<reference path="../../../engine/controller/GameController.ts" />
///<reference path="../../../engine/controller/configuration/GameConfiguration.ts" />

module TypeScriptPhaserSlot.common {

    export class BaseGameGameController extends GameController {

        protected _backgroundController: IActionController;
        protected _decorationsController: IActionController;
        protected _reelsController: IReelController;
        protected _interfaceController: IInterfaceController;
        protected _linesController: ILinesController;
        protected _scattersController: ILinesController;
        protected _symbolsAnimationsController: ISymbolsAnimationController;
        protected _soundController: ISoundController;
        protected _winningOnCenterController: IWinningOnCenterController;
        protected _emotionsController: IEmotionsController;
        protected _freeSpinsController: IFreeSpinsController;
        protected _paytableController: IPaytableController;
        protected _toolbarController: IActionController;
        protected _boongoGameRunnerController: BoongoGameRunerController;
        protected _url: string;

        protected lastSpinResult: SpinResult;

        protected _appProxy: ApplicationProxy;

        protected start() {
            super.start();
            this.model.betChangedSignal.add(this.onBetChangedHandler, this);
            this.model.linesChangedSignal.add(this.onLinesChangedHandler, this);
            this.model.autoChangedSignal.add(this.onAutoChangedHandler, this);
            /*
             Creation outside Api Controller to comunicate to third-party server API
             */
            let apiControllerName: string = window['GR'] ? 'BoongoAPIController' : String(this.configs[0].gameName + 'APIController');
            let apiController: IAPIController = new TypeScriptPhaserSlot.common[apiControllerName](this.url, apiControllerName, this.debugMode);
            /*
             Class which provides translation between side-APIController and internal API Standart and sends the data to Model instance
             */
            this._appProxy = new ApplicationProxy(apiController, "ApplicationProxy", this.debugMode);

            this._appProxy.spinResponseSignal.add(this.onSpinResponse, this);
            this._appProxy.getGameStateSignal.add(this.onGetGameState, this);
            this._appProxy.bonusGameResponseSignal.add(this.onBonusResponse, this);

            this.appProxy.getGameState(this.configuration.gameName);

            this.game.scale.onSizeChange.add(this.correctSize, this);

            this.game.input.mouse.enabled = this.game.device.desktop || false;
        }

        protected correctSize(scaleManager:Phaser.ScaleManager, screenWidth:number, screenHeight:number):void {
            if(/*this.game.scale.isPortrait*/screenWidth < screenHeight) {
                return;
            }

            if(!this.game.world.children || this.game.world.children.length == 0) {
                return;
            }

            let w:number = 1366;
            let h:number = 768;
            let g:any = this.game.world.getChildAt(0);

            let removed:any[] = [];
            this.game.world.children[0]['forEach'](function (child:any, index:number) {
                if(child.x + child.width > 1366) {
                    console.log('removed:', child.name);
                    removed.push({parent:child.parent, index:child.parent.getChildIndex(child), child:child});
                    child.parent.removeChild(child);
                }
            }, this);

            //this.game.canvas.width = screenWidth;
            //this.game.canvas.height = screenHeight;

            console.log('screenWidth', screenWidth);
            console.log('screenHeight', screenHeight);

            this.game.camera.setSize(screenWidth, screenHeight);

            if(this.game.camera.height != h) {
                let scale:number = this.game.camera.height / h;
                console.log('gameScale', scale);
                g.scale.set(scale, scale);
                this.game.world.getChildAt(1).scale.set(scale, scale);
                this.game.world.getChildAt(2).scale.set(scale, scale);
            }

            let boundsXoffset:number = g.width < this.game.camera.width ? (this.game.camera.width - g.width) / 2 * -1 : 0;
            this.game.world.resize(g.width, g.height);
            this.game.world.setBounds(boundsXoffset, 0, g.width, g.height);

            let cameraX:number = (g.width - this.game.camera.width) / 2;
            this.game.camera.x =  cameraX;

            this.game.stage.smoothed = true;
            this.game.antialias = true;

            removed.forEach(function (element:any) {
                element.parent.addChildAt(element.child, element.index);
            }, this);
            removed = [];
        }

        /**
         *
         * @param value
         */
        protected onBetChangedHandler(value: number): void {
            this._interfaceController.changeBetText(value);
            this._interfaceController.changeTotalBetText(this.model.totalBet);
        }

        /**
         *
         * @param value
         */
        protected onLinesChangedHandler(value: number): void {
            this._interfaceController.changeLinesText(value);
            this._interfaceController.changeTotalBetText(this.model.totalBet);
        }

        /**
         *
         * @param {number} value
         */
        protected onAutoChangedHandler(value: boolean): void {
            SoundController.instance.playSound(this.model.isAutoPlay ? 'auto' : 'auto');

            if(this.model.isAutoPlay) {
                this._boongoGameRunnerController.sayToGameRuner('autogameStart', {rounds: 999});
            } else {
                this._boongoGameRunnerController.sayToGameRuner('autogameStop');
            }

            this._interfaceController['showAutoplayText'](this.model.isAutoPlay);
            this._interfaceController.autoChanged(this.model.isAutoPlay);

            if(this.model.coins < this.model.totalBet && this.model.freespins == 0) {
                if(this.model.isAutoPlay) {
                    this.onInterfaceControllerAction(ButtonActionConstants.AUTO_ACTION);
                }
            }
        }


        /**
         *
         * @param {IState} state
         * @param {any} data
         * @param {boolean} isRequiredAction
         */
        protected modelSays(state: IState, data: any, isRequiredAction: boolean): void {
            super.modelSays(state, data, isRequiredAction);

            if(state) {
                return;
            }

            //let isRequired: boolean = !isRequiredAction || this.model.isAutoPlay ? false : true;
            let isRequired: boolean = !(!isRequiredAction || this.model.isAutoPlay);

            if (!isRequiredAction) {
                this.debuglog("do work for " + state.name);
            }
            this._interfaceController.stateChanged(state.name);

            if(state.name == StatesConstants.IDLE_STATE) {
                this._interfaceController.changeBalance(this.model.coins);
                this._boongoGameRunnerController.sayToGameRuner('ready');
            }

            if (!isRequired)
                switch (state.name) {

                    case StatesConstants.IDLE_STATE:
                        this._linesController.stop();
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();

                        this._interfaceController.changeBalance(this.model.coins);

                        if(this.model.isAutoPlay && this.model.coins < this.model.totalBet && this.model.freespins == 0) {
                            this.model.auto(false);
                            this.onInterfaceControllerAction(ButtonActionConstants.AUTO_ACTION);
                            break;
                        }

                        state.finishWork();
                        break;

                    case StatesConstants.PRESPIN_STATE:
                        state.finishWork();
                        break;

                    case StatesConstants.GET_SPIN_RESPONSE_STATE:
                        if (this.model.isFreespins) {
                            this.appProxy.freespin(1);
                        } else {
                            this.appProxy.spin(data);
                        }
                        break;

                    case StatesConstants.START_SPIN_STATE:
                        this._linesController.stop();
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();

                        this._interfaceController.resetWinText();
                        this._reelsController.start();

                        if(this.model.isFreespins) {
                            this._freeSpinsController.leftFreeSpins(this.model.freespins);
                        }

                        this._interfaceController.changeBalance(this.model.coins - (this.model.isFreespins ? 0 :this.model.totalBet));

                        state.finishWork();
                        break;

                    case StatesConstants.START_RESPIN_STATE:
                        this._linesController.stop();
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();
                        this._reelsController.respin();
                        //this._reelsController.respin(data);
                        state.finishWork();
                        break;
                    case StatesConstants.GET_RESPIN_RESPONSE_STATE:
                        this.appProxy.respin();
                        break;

                    case StatesConstants.STOP_RESPIN_STATE:
                        this._reelsController.stopRespin(data);
                        break;

                    case StatesConstants.STOP_SPIN_STATE:
                        this._reelsController.stop(data);
                        break;

                    case StatesConstants.PRINCESS_FROG_TRANSFORM_PRINCESS_STATE:
                        let result:boolean = this._reelsController['transformPrincess']();
                        if(result) {
                            setTimeout(function () {
                                state.finishWork();
                            }.bind(this), 1000);
                        } else {
                            state.finishWork();
                        }
                        break;

                    case StatesConstants.EMOTIONS_STATE:
                        this._emotionsController.start(data);
                        break;

                    case StatesConstants.START_SHOW_WINNING_STATE:
                        this.lastSpinResult = data;

                        this._linesController.start(data);

                        this._emotionsController.stopEmotion();

                        //this._reelsController.animateSymbols(data);
                        //this._symbolsAnimationsController.animateSymbols(data);
                        break;

                    case StatesConstants.STOP_SHOW_WINNING_STATE:
                        this._interfaceController.resetWinText();
                        this._interfaceController.changeBalance(this.model.coins);

                        this._winningOnCenterController.hideWinning();

                        state.finishWork();
                        SoundController.instance.playSound('ching', 'default', 0.3);
                        break;

                    case StatesConstants.COLLECT_STATE:
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();
                        state.finishWork();
                        break;

                    case StatesConstants.START_FREESPINS_STATE:
                        this._boongoGameRunnerController.sayToGameRuner('ready');
                        this._boongoGameRunnerController.sayToGameRuner('freespin_init');
                        this._freeSpinsController.startFreeSpins(this.model.freespins);
                        this._backgroundController['changeBackground'](true);
                        this._decorationsController['changeFrame'](true);
                        this._interfaceController['showAutoplayText'](false);
                        break;

                    case StatesConstants.ADD_FREESPINS_STATE:
                        this._freeSpinsController.addedFreeSpins(this.model.freeSpinsAdded);
                        break;

                    case StatesConstants.FINISH_FREESPINS_STATE:
                        this._boongoGameRunnerController.sayToGameRuner('ready');
                        this._boongoGameRunnerController.sayToGameRuner('freespin_stop');
                        this._freeSpinsController.endOfFreeSpins(this.model.bonusTotalWin);
                        this._backgroundController['changeBackground'](false);
                        this._decorationsController['changeFrame'](false);
                        break;

                    case StatesConstants.GET_GAMBLE_RESPONSE_STATE:
                        state.finishWork();
                        break;

                    case StatesConstants.GAMBLE_STATE:
                        this.appProxy.gamble();
                        break;

                    case StatesConstants.START_BONUS_GAME_STATE:

                        break;

                    case StatesConstants.FINISH_BONUS_GAME_STATE:
                        state.finishWork();
                        break;
            }
        }

        /**
         *
         */
        protected onNotEnoughMoney(): void {
            super.onNotEnoughMoney();

            if(this.model.isAutoPlay && this.model.coins < this.model.totalBet && this.model.freespins == 0) {
                this.model.auto(false);
                this.onInterfaceControllerAction(ButtonActionConstants.AUTO_ACTION);
            }

            if(!window['GR']) {
                this._interfaceController.showPopupWindow({type:'onNotEnoughMoney'});
            } else {
                let bet:number = this.model.allowedBets[this.model.currBetIndex] * this.model.denomination;
                let lines:number = this.model.allowedLines[this.model.currLinesIndex];
                let paramsObj: object = {
                    action: {
                        name: 'spin',
                        params: {
                            lines: lines,
                            bet_per_line: bet
                        }
                    },
                    bet: lines * bet
                };
                this._boongoGameRunnerController.sayToGameRuner('play', paramsObj);
            }
        }

        /**
         *
         * @param {SpinResponse} data
         */
        protected onSpinResponse(data: SpinResponse): void {
            this.model.onSpinResponse(data);
            this.currState.finishWork();
        }

        /**
         *
         * @param {TypeScriptPhaserSlot.common.BonusResponse} data
         */
        protected onBonusResponse(data: BonusResponse): void {
            this.model.onBonusGameResponse(data);
            this.model.goToState(StatesConstants.SHOW_WINNING_BONUS_GAME_STATE);
        }

        /**
         * Callback function to handle getting the server response "GetState"
         * Initialising all helper/controllers
         *-ReelsController
         *-InterfaceController
         *-LineController
         *-TextController
         * @param {GameStateResponse} data
         */
        protected onGetGameState(data: GameStateResponse): void {
            let contentLayer:Phaser.Group = new Phaser.Group(this.game);

            let backLayer: Phaser.Group = new Phaser.Group(this.game);
            backLayer.name = 'backLayer';
            let reelsLayer: Phaser.Group = new Phaser.Group(this.game);
            reelsLayer.name = 'reelsLayer';
            let linesLayer: Phaser.Group = new Phaser.Group(this.game);
            linesLayer.name = 'linesLayer';
            let paytableLayer: Phaser.Group = new Phaser.Group(this.game);
            paytableLayer.name = 'paytableLayer';
            let wildsAnimationsLayer: Phaser.Group = new Phaser.Group(this.game);
            wildsAnimationsLayer.name = 'wildsAnimationsLayer';
            let decorationsLayer: Phaser.Group = new Phaser.Group(this.game);
            decorationsLayer.name = 'decorationsLayer';
            let symbolsAnimationsLayer: Phaser.Group = new Phaser.Group(this.game);
            symbolsAnimationsLayer.name = 'symbolsAnimationsLayer';

            let consoleLayer: Phaser.Group = new Phaser.Group(this.game);
            consoleLayer.name = 'consoleLayer';
            //consoleLayer.scale.setTo(scale, scale);

            let winningOnCenterLayersLayer: Phaser.Group = new Phaser.Group(this.game);
            winningOnCenterLayersLayer.name = 'winningOnCenterLayersLayer';
            let emotionsLayer: Phaser.Group = new Phaser.Group(this.game);
            emotionsLayer.name = 'emotionsLayer';
            let freeSpinsLayer: Phaser.Group = new Phaser.Group(this.game);
            freeSpinsLayer.name = 'freeSpinsLayer';

            let toolbarLayer: Phaser.Group = new Phaser.Group(this.game);
            toolbarLayer.name = 'toolbarLayer';
            //toolbarLayer.scale.setTo(scale, scale);

            contentLayer.add(backLayer);
            contentLayer.add(reelsLayer);
            contentLayer.add(linesLayer);
            contentLayer.add(paytableLayer);
            contentLayer.add(wildsAnimationsLayer);
            contentLayer.add(decorationsLayer);
            contentLayer.add(symbolsAnimationsLayer);

            //contentLyer.add(consoleLayer);

            contentLayer.add(winningOnCenterLayersLayer);
            contentLayer.add(emotionsLayer);
            contentLayer.add(freeSpinsLayer);

            //contentLyer.add(toolbarLayer);

            this.game.add.existing(contentLayer);
            this.game.add.existing(toolbarLayer);
            this.game.add.existing(consoleLayer);

            let backControllerName: string = this.getControllerName("BackController");
            this._backgroundController = new TypeScriptPhaserSlot.common[backControllerName](this.game, backControllerName, backLayer, true, this.game.pattern);


            let decorationsControllerName: string = this.getControllerName("DecorationsController");
            this._decorationsController = new TypeScriptPhaserSlot.common[decorationsControllerName](this.game, decorationsControllerName, decorationsLayer, true, this.game.pattern);


            let icn:string = 'InterfaceController';
            let icp:any = this.game.pattern.console;
            if(this.game.device.iPhone || this.game.device.android) {
                icn = 'InterfaceControllerMobile';
                icp = this.game.pattern.console_mobile;
            }
            if(this.game.device.iPad) {
                icn = 'InterfaceControllerIPad';
                icp = this.game.pattern.console_ipad;
            }
            let interfaceControllerName: string = this.getControllerName(icn);
            this._interfaceController = new TypeScriptPhaserSlot.common[interfaceControllerName](this.game, interfaceControllerName, consoleLayer, true, this.configuration.buttons, icp);
            this._interfaceController.actionSignal.add(this.onInterfaceControllerAction, this);

            /*
             ReelsController creates and manages all amount of <IReel>
             */
            let reelsControllerName: string = this.getControllerName("ReelsController");
            this._reelsController = new TypeScriptPhaserSlot.common[reelsControllerName](this.game, reelsControllerName, reelsLayer, true, this.configuration.symbols, this.configuration, this.game.pattern, data);
            this._reelsController.actionSignal.add(this.onReelsControllerAction, this);

            /*
             WinningController creates and manages all stuff of showing the visual results
             like lines, borders and so on.
             */
            let linesControllerName: string = this.getControllerName("LinesController");
            this._linesController = new TypeScriptPhaserSlot.common[linesControllerName](this.game, linesControllerName, linesLayer, true, this.configuration, this.game.pattern);
            this._linesController.actionSignal.add(this.onLinesControllerAction, this);
            this._linesController.finishShowingSignal.add(this.onLinesControllerFinishShowLines, this);


            let scattersControllerName: string = this.getControllerName("ScattersController");
            this._scattersController = new TypeScriptPhaserSlot.common[scattersControllerName](this.game, scattersControllerName, linesLayer, true, this.configuration, this.game.pattern);
            this._scattersController.actionSignal.add(this.onScattersControllerAction, this);
            this._scattersController.finishShowingSignal.add(this.onScattersControllerFinish, this);

            /*
             Animations controller
             */
            let symbolsAnimationsControllerName: string = this.getControllerName("SymbolsAnimationController");
            this._symbolsAnimationsController = new TypeScriptPhaserSlot.common[symbolsAnimationsControllerName](this.game, symbolsAnimationsControllerName, symbolsAnimationsLayer, true, this.configuration.symbols, this.configuration, this.game.pattern, data);
            this._symbolsAnimationsController.actionSignal.add(this.onReelsControllerAction, this);
            this._symbolsAnimationsController['wildLayer'] = wildsAnimationsLayer;


            let soundControllerName: string = this.getControllerName("SoundController");
            this._soundController = new TypeScriptPhaserSlot.common[soundControllerName](this.game, soundControllerName, true, this.configuration, data);
            this._soundController.actionSignal.add(this.onReelsControllerAction, this);

            new MoneyFormatController(this.game, 'MoneyFormatController', true, this.configuration, data);

            let winningOnCenterControllerName: string = this.getControllerName("WinningOnCenterController");
            this._winningOnCenterController = new TypeScriptPhaserSlot.common[winningOnCenterControllerName](this.game, winningOnCenterControllerName, winningOnCenterLayersLayer, true);
            this._winningOnCenterController.actionSignal.add(this.onEmotionsControllerAction, this);


            let emotionsControllerName: string = this.getControllerName("EmotionsController");
            this._emotionsController = new TypeScriptPhaserSlot.common[emotionsControllerName](this.game, emotionsControllerName, emotionsLayer, true, this.configuration, this.game.pattern, data);
            this._emotionsController.actionSignal.add(this.onEmotionsControllerAction, this);


            let freeSpinsControllerName:string = this.getControllerName("FreeSpinsController");
            this._freeSpinsController = new TypeScriptPhaserSlot.common[freeSpinsControllerName](this.game, freeSpinsControllerName, freeSpinsLayer, true, this.configuration, this.game.pattern, data);
            this._freeSpinsController.actionSignal.add(this.onFreeSpinsControllerAction, this);


            //let paytableControllerName:string = this.getControllerName("PaytableController");
            //this._paytableController = new TypeScriptPhaserSlot.common[paytableControllerName](this.game, paytableControllerName, paytableLayer, true, this.configuration, this.game.pattern, data);


            let toolbarControllerName:string = this.getControllerName("ToolbarController");
            this._toolbarController = new TypeScriptPhaserSlot.common[toolbarControllerName](this.game, toolbarControllerName, toolbarLayer, true, this.configuration, this.game.pattern, data);
            this._toolbarController.actionSignal.add(this.onToolbarControllerAction, this);

            this._boongoGameRunnerController = new BoongoGameRunerController(this.game, 'BoongoGameRunerController', this.debugMode);
            this._boongoGameRunnerController.actionSignal.add(this.onBoongoGameRunnesControllerAction, this);


            this.correctSize(this.game.scale, this.game.scale.width, this.game.scale.height);

            //Sending game state data to the model to init a game proccess.
            this.model.onGetGameState(data);

            let paytableControllerName:string = this.getControllerName("PaytableController");
            this._paytableController = new TypeScriptPhaserSlot.common[paytableControllerName](this.game, paytableControllerName, paytableLayer, true, this.configuration, this.game.pattern, data);

            //Shwitch autoplay on if Freespins are here
            if (this.model.isFreespins) {
                this.model.auto(true);
            }

            this._backgroundController['changeBackground'](this.model.isFreespins);
            this._decorationsController['changeFrame'](this.model.isFreespins);

            SoundController.instance.playSound('background_sound', 'music', 0.3, true);

            //this.game.camera.resetFX();

            //this.correctSize(this.game.scale, this.game.scale.width, this.game.scale.height);

            this._interfaceController['changePositions']();
        }

        /**
         * Listener of LinesController action
         * Handles the begin showing each lines to animate
         * according symbols on the reels
         * @param data
         */
        protected onLinesControllerAction(data?: any): void {
            this._reelsController.animateSymbolsOfLine(data);
            this._symbolsAnimationsController.animateSymbolsOfLine(data, this.model.result);
            this._interfaceController.changeWinText(data.sum);

            this._winningOnCenterController.hideWinning();
            if(data.sum) {
                this._winningOnCenterController.showWinning(data.sum);
            }
        }

        /**
         *
         */
        protected onLinesControllerFinishShowLines(): void {
            this._reelsController.stopAnimation();
            this._symbolsAnimationsController.stopAnimation();

            //this.onReelsControllerAction(ReelsController.FINISH_SHOW_WINNING);

            this._scattersController.start(this.lastSpinResult);
        }

        /**
         *
         * @param data
         */
        protected onScattersControllerAction(data?: any): void {
            this._reelsController.animateScatters(data);
            this._symbolsAnimationsController.animateScatters(data, this.model.result);
            this._interfaceController.changeWinText(data.sum || 0);

            this._winningOnCenterController.hideWinning();
            if(data.sum) {
                this._winningOnCenterController.showWinning(data.sum);
            }
        }

        /**
         *
         */
        protected onScattersControllerFinish(): void {
            this._reelsController.stopAnimation();
            this._symbolsAnimationsController.stopAnimation();

            this.onReelsControllerAction(ReelsController.FINISH_SHOW_WINNING);
        }

        /**
         * Listener of Reels Controller action
         * @param data
         */
        protected onReelsControllerAction(data?: any): void {
            if (data == ReelsController.STOP && this.currState.name == StatesConstants.STOP_SPIN_STATE) {
                this.currState.finishWork();
            }

            if (data == ReelsController.FINISH_SHOW_WINNING && this.currState.name == StatesConstants.START_SHOW_WINNING_STATE) {
                this.currState.finishWork();
            }

            if (data == ReelsController.RESPIN_COMPLETE && this.currState.name == StatesConstants.STOP_RESPIN_STATE) {
                this.currState.finishWork();
            }

            if (data == ReelsController.INTRIGUE && this.currState.name == StatesConstants.SPIN_STATE) {

            }
        }

        /**
         *
         * @param data
         */
        protected onBoongoGameRunnesControllerAction(data?: any): void {
            switch (data.event) {
                case 'error':
                    if(data.data.type == 'crit') {
                        SoundController.instance.muteAll(true);
                    }
                    break;
                case 'gameMode':

                    this.game.textFactory.changeLocaleAndRefresh(window['GR'].Options.get('lang'));

                    //let sss:any = window['GR'].Storage.get('sound');
                    //SoundController.instance.muteAll(!window['GR'].Storage.get('sound'));
                    window['GR'].Storage.follow('sound', function(data:boolean):void {
                        SoundController.instance.muteAll(!data);
                    });

                    /*switch (data.data.mode) {
                        case 'stream':
                            this._interfaceController['setGameMode']('stream');
                            break;
                        case 'replay':
                            this._interfaceController['setGameMode']('stream');
                            break;
                    }*/
                    break;
                case 'stream':
                case 'replay':
                    //this._paytableController.hide();

                    let targetState:string = '';
                    let action:string = '';

                    if(data.data.action.action.name == 'spin') {
                        targetState = StatesConstants.GET_SPIN_RESPONSE_STATE;
                        action = 'spin';
                    }
                    /*if(data.data.action.action.name == 'freespin') {
                        targetState = StatesConstants.IDLE_STATE;
                        action = 'freespin';
                    }*/
                    /*if(data.data.action.action.name == 'freespin_init') {
                        targetState = StatesConstants.IDLE_STATE;
                        action = 'freespin_init';
                    }*/
                    /*if(data.data.action.action.name == 'freespin_stop') {
                        targetState = StatesConstants.IDLE_STATE;
                        action = 'freespin_stop';
                    }*/
                    if(data.data.action.action.name == 'respin') {
                        targetState = StatesConstants.GET_RESPIN_RESPONSE_STATE;
                        action = 'respin';
                    }

                    this._linesController.stop();
                    this._reelsController.stopAnimation();
                    this._symbolsAnimationsController.stopAnimation();

                    if(targetState != '') {
                        while(this.currState.name != targetState) {
                            this.currState.finishWork();
                        }
                    }

                    //this.model.goToState(targetState);

                    /*switch (action) {
                        case 'spin':
                            this._boongoGameRunnerController.sayToGameRuner('play', {action: 'spin'});
                            break;
                        case 'respin':
                            this._boongoGameRunnerController.sayToGameRuner('play', {action: 'respin'});
                            break;
                        case 'freespin_init':
                            this._boongoGameRunnerController.sayToGameRuner('freespin_init');
                            break;
                    }*/

                    //this._boongoGameRunnerController.sayToGameRuner('play', {action: 'spin'});
                    this._boongoGameRunnerController.sayToGameRuner('play', {action: action});
                    break;

                case 'available_actions':

                    break;
                case 'round_start':
                    this.model.setBet(data.data.bet);
                    this.onInterfaceControllerAction(ButtonActionConstants.SPIN_ACTION);
                    break;
                case 'round_result':

                    break;
                case 'state':
                case 'restart':
                    let spinData:any = data.data.context.spins || data.data.context.freespins;
                    switch(data.data.mode) {
                        case 'freebet':
                            this.model.setLines(spinData.lines);
                            this.model.setBet(spinData.bet_per_line);
                            //this._interfaceController['setGameMode']('freeBet');
                            break;
                        default:
                            this.model.setLines(spinData.lines);
                            this.model.setBet(spinData.bet_per_line);
                            //this._interfaceController['setGameMode']('');
                            break;
                    }
                    //this._boongoGameRunnerController.sayToGameRuner('ready');
                    if(this.model.isAutoPlay) {
                        this.model.auto(false);
                    }
                    this.restartGame(data.data);
                    break;
                case 'balance':
                    this.model.onBalanceChanged(data.data.balance);
                    this._interfaceController.changeBalance(this.model.coins);
                    break;
                case 'paytable':
                    if(data.data.visible) {
                        this._paytableController.show();
                    } else {
                        this._paytableController.hide();
                    }
                    break;
                case 'onSpinButton':
                    let statesForSkip:string[] = [];
                    statesForSkip.push(StatesConstants.STOP_SHOW_WINNING_STATE);
                    statesForSkip.push(StatesConstants.START_SHOW_WINNING_STATE);
                    statesForSkip.push(StatesConstants.EMOTIONS_STATE);

                    if(statesForSkip.indexOf(this.currState.name) > -1) {
                        this._linesController.stop();
                        this._scattersController.stop();
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();
                        this.currState.finishWork();
                    }
                    break;
            }
        }

        protected restartGame(data?:any):void {

        }

        /**
         *
         * @param data
         */
        protected onEmotionsControllerAction(data?: any): void {
            if (data == "startShowedEmotions" && this.currState.name == StatesConstants.EMOTIONS_STATE) {
                this._boongoGameRunnerController.sayToGameRuner('bigwin_start', this.model.totalWin);
            }
            if (data == "stopShowedEmotions" && this.currState.name == StatesConstants.EMOTIONS_STATE) {
                this._boongoGameRunnerController.sayToGameRuner('bigwin_finish');
            }
            if (data == "emotionsComplete" && this.currState.name == StatesConstants.EMOTIONS_STATE) {
                this.currState.finishWork();
            }
        }

        /**
         *
         * @param data
         */
        protected onFreeSpinsControllerAction(data?: any): void {
            switch (data.type) {
                case 'start':
                    this.model.auto(true);
                    this.currState.finishWork();
                    break;
                case 'added':
                    this.currState.finishWork();
                    break;
                case 'end':
                    this.model.auto(false);
                    this.currState.finishWork();
                    break;
            }
        }

        protected onBonusGameControllerAction(data?: any): void {

        }

        /**
         *
         * @param event
         */
        protected onToolbarControllerAction(event:string): void {
            switch (event) {
                case 'exit':
                    if(window['GR']) {
                        window['GR'].Events.navigate.exit();
                        break;
                    }

                    let exitURL:string = 'http://google.com';
                    if(window['gameConfig'] &&  window['gameConfig']['lobbyPath']) {
                        exitURL = window['gameConfig']['lobbyPath'];
                    }
                    window.top.location.replace(exitURL);
                    break;
                case 'switch_fullscreen':
                    if (this.game.scale.isFullScreen) {
                        this.game.scale.stopFullScreen();
                    } else {
                        this.game.scale.startFullScreen(true);
                    }
                    window.scrollTo(0, 1);
                    this.game.stage.smoothed = true;
                    //this.game.scale.refresh();
                    break;
                case 'all_on':
                    SoundController.instance.muteAll(false);
                    this._boongoGameRunnerController.sayToGameRuner('sound_changed', true);
                    break;
                case 'music_off':
                    SoundController.instance.getGroup('music').mute = true;
                    break;
                case 'all_off':
                    SoundController.instance.muteAll(true);
                    this._boongoGameRunnerController.sayToGameRuner('sound_changed', false);
                    break;
            }
        }

        /**
         * Listener of Interface Controller action.
         * Finish work current Model State if it's required.
         * @param action
         */
        protected onInterfaceControllerAction(action: string): void {
            switch (action) {
                //case ButtonActionConstants.COLLECT_ACTION:
                //    if (this.currentStateRequiredAction && this.currState.name == StatesConstants.STOP_SHOW_WINNING_STATE)
                //        this.currState.finishWork();
                //    break;
                case ButtonActionConstants.MAXBET_ACTION:
                    this.model.maxBet();

                    let statesForSkip:string[] = [];
                    statesForSkip.push(StatesConstants.IDLE_STATE);
                    statesForSkip.push(StatesConstants.STOP_SHOW_WINNING_STATE);
                    statesForSkip.push(StatesConstants.START_SHOW_WINNING_STATE);
                    statesForSkip.push(StatesConstants.EMOTIONS_STATE);

                    if(!this._boongoGameRunnerController.enable) {
                        this._paytableController.hide();
                    }
                    this._interfaceController.stateChanged(this.currState.name);

                    //if (this.model.isAutoPlay) return;
                    /*if (/!*this.currentStateRequiredAction && *!/(this.currState.name == StatesConstants.IDLE_STATE ||
                    this.currState.name == StatesConstants.STOP_SHOW_WINNING_STATE || this.currState.name == StatesConstants.START_SHOW_WINNING_STATE)){*/

                    if(statesForSkip.indexOf(this.currState.name) > -1) {
                        this._linesController.stop();
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();

                        this._boongoGameRunnerController.sayToGameRuner('gamePlay', {action:'spin'});

                        this.currState.finishWork();
                    }
                    break;

                case ButtonActionConstants.SPIN_ACTION:
                    statesForSkip = [];
                    statesForSkip.push(StatesConstants.IDLE_STATE);
                    statesForSkip.push(StatesConstants.STOP_SHOW_WINNING_STATE);
                    statesForSkip.push(StatesConstants.START_SHOW_WINNING_STATE);
                    statesForSkip.push(StatesConstants.EMOTIONS_STATE);

                    if(!this._boongoGameRunnerController.enable) {
                        this._paytableController.hide();
                    }
                    this._interfaceController.stateChanged(this.currState.name);

                    //if (this.model.isAutoPlay) return;
                    /*if (/!*this.currentStateRequiredAction && *!/(this.currState.name == StatesConstants.IDLE_STATE ||
                    this.currState.name == StatesConstants.STOP_SHOW_WINNING_STATE || this.currState.name == StatesConstants.START_SHOW_WINNING_STATE)){*/

                    if(statesForSkip.indexOf(this.currState.name) > -1) {
                        this._linesController.stop();
                        this._scattersController.stop();
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();

                        //this._boongoGameRunnerController.sayToGameRuner('play', {action:'spin'});

                        this.currState.finishWork();
                    }
                    break;

                case ButtonActionConstants.STOP_ACTION:
                    this._reelsController.urgentStop();
                    break;

                case ButtonActionConstants.AUTO_ACTION:
                    this.model.auto();
                    break;

                case ButtonActionConstants.BET_DEC_ACTION:
                    SoundController.instance.playSound('decrease_bet_sound', "default", 0.5);
                    this.model.prevBet();
                    break;

                case ButtonActionConstants.BET_INC_ACTION:
                    SoundController.instance.playSound('increase_bet_sound', "default", 0.5);
                    this.model.nextBet();
                    break;

                case ButtonActionConstants.LINES_DEC_ACTION:
                    SoundController.instance.playSound('decrease_bet_sound', "default", 0.5);
                    this.model.prevLine();
                    this._linesController.showLines(this.model.allowedLines[this.model.currLinesIndex]);
                    break;

                case ButtonActionConstants.LINES_INC_ACTION:
                    SoundController.instance.playSound('increase_bet_sound', "default", 0.5);
                    this.model.nextLine();
                    this._linesController.showLines(this.model.allowedLines[this.model.currLinesIndex]);
                    break;

                case ButtonActionConstants.INFO_ACTION:
                    this._paytableController.switch();
                    if(this._paytableController.isShown) {
                        this._interfaceController.stateChanged('paytable');
                    } else {
                        this._interfaceController.stateChanged(this.currState.name);
                    }
                    break;

                case "paytable_next":
                    this._paytableController.goToPage('increase');
                    break;

                case "paytable_prev":
                    this._paytableController.goToPage('decrease');
                    break;
            }
        }

        /************************************************************
         Getters and Setters
         *************************************************************/
        public get appProxy(): ApplicationProxy {
            return this._appProxy;
        }

        public get url(): string {
            return this._url;
        }


    }

}