﻿///<reference path="../../../engine/controller/InterfaceController.ts" />

module TypeScriptPhaserSlot.common {
    
    export class BaseGameInterfaceController extends InterfaceController {

        /**
        * Called by GameController
        * Listener model changing
        * @param {string} stateName
        */

        protected balanceValueText: LocaleText;
        protected winValueText: LocaleText;
        protected betValueText: LocaleText;
        protected linesValueText: LocaleText;
        protected totalBetValueText: LocaleText;
        protected label_balance: LocaleText;
        protected label_win: LocaleText;
        protected label_bet: LocaleText;
        protected label_lines: LocaleText;
        protected label_totalBet: LocaleText;
        protected label_autoplay: LocaleText;
        protected bottomPanel: Phaser.Group;
        protected popupWindow: Phaser.Group;
        protected gameMode:string = '';

        protected showedWin: number;

        protected alphaTween:Phaser.Tween;
        protected winTween:Phaser.Tween;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, configuration: ButtonConfiguration[], pattern: any) {
            super(game, name, layer, debugMode, configuration, pattern);
        }

        /**
        * Initial creation
        */
        public init(): void {
            this.showedWin = 0;
            this.createElements();
            this.createPopupWindow();
            super.init();
            this.addBottomPanel();
            //this.changePositions();

            //this.game.scale.onSizeChange.add(function () {
            //    this.changePositions();
            //},this, -1);
        }

        protected createElements():void {
            let bg:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 768 - 40);
            bg.name = 'background';
            bg.beginFill(0xCCCCCC, 0.2);
            bg.drawRect(0, 0, 1366, 40);
            bg.endFill();
            this.layer.add(bg);

            this.label_balance = this.game.textFactory.getTextWithStyle("balance");
            this.label_balance.name = 'label_balance';
            this.setTextPosition(this.label_balance, this.pattern.label_balance);

            this.balanceValueText = this.game.textFactory.getTextWithStyle("0", "style1");
            this.balanceValueText.name = 'value_balance';
            this.setTextPosition(this.balanceValueText, this.pattern.value_balance);

            this.label_win = this.game.textFactory.getTextWithStyle("win");
            this.label_win.name = 'label_win';
            this.setTextPosition(this.label_win, this.pattern.label_win);

            this.winValueText = this.game.textFactory.getTextWithStyle("0", "style1");
            this.winValueText.name = 'value_win';
            this.setTextPosition(this.winValueText, this.pattern.value_win);

            this.label_bet = this.game.textFactory.getTextWithStyle("bet");
            this.label_bet.name = 'label_bet';
            this.setTextPosition(this.label_bet, this.pattern.label_bet);

            this.betValueText = this.game.textFactory.getTextWithStyle("0", "style1");
            this.betValueText.name = 'value_bet';
            this.setTextPosition(this.betValueText, this.pattern.value_bet);

            this.label_lines = this.game.textFactory.getTextWithStyle("lines");
            this.label_lines.name = 'label_lines';
            this.setTextPosition(this.label_lines, this.pattern.label_lines);

            this.linesValueText = this.game.textFactory.getTextWithStyle("0", "style1");
            this.linesValueText.name = 'value_lines';
            this.setTextPosition(this.linesValueText, this.pattern.value_lines);

            this.label_totalBet = this.game.textFactory.getTextWithStyle("totalBet");
            this.label_totalBet.name = 'label_totalBet';
            this.setTextPosition(this.label_totalBet, this.pattern.label_totalBet);

            this.totalBetValueText = this.game.textFactory.getTextWithStyle("0", "style1");
            this.totalBetValueText.name = 'value_totalBet';
            this.setTextPosition(this.totalBetValueText, this.pattern.value_totalBet);

            this.label_autoplay = this.game.textFactory.getTextWithStyle("autoplay");
            this.label_autoplay.name = 'label_autoplay';
            this.setTextPosition(this.label_autoplay, this.pattern.label_autoplay);
            this.label_autoplay.visible = false;
        }

        protected addBottomPanel():void {
            this.bottomPanel = new Phaser.Group(this.game);
            this.bottomPanel.name = 'bottomPanel';
            this.layer.add(this.bottomPanel);

            let xxx:number = this.layer.getByName('label_balance').x;
            let childNames:string[] = ['betInc_btn', 'betDec_btn', 'label_balance', 'value_balance',
            'label_lines', 'value_lines', 'label_bet', 'value_bet', 'label_totalBet', 'value_totalBet',
            'label_win', 'value_win'];

            childNames.forEach(function(childName:string):void {
                let child:any = this.layer.getByName(childName);
                if(child) {
                    this.bottomPanel.add(child);
                }
            }, this);

            this.bottomPanel.children.forEach(function(child:any):void {
                child.x -= xxx;
            }, this);
            this.bottomPanel.x = xxx;
        }

        protected createPopupWindow():void {
            this.popupWindow = new Phaser.Group(this.game);
            let popupWindowBackground:Phaser.Graphics = new Phaser.Graphics(this.game);
            popupWindowBackground.beginFill(0xffffff, 0.9);
            popupWindowBackground.drawRoundedRect(0, 0, 400, 200, 10);
            popupWindowBackground.endFill();

            let popupText = this.game.textFactory.getTextWithStyle('notEnoughMoney');
            popupText.anchor.set(0.5, 0.5);
            popupText.x = popupWindowBackground.width / 2;
            popupText.y = popupWindowBackground.height / 2;

            let buttonActions:any = {
                mouseUp: {
                    action: '',
                    params: null,
                    sound: null,
                    enabledAfter: undefined
                }
            };
            let bottonConfigurationParams:any = {
                name: 'bonusStartSpin_btn',
                className:  'BaseButton',
                key: 'ui_buttons',
                caption: '',
                textStyle: 'style1',
                overFrame: 'popupClose_btn/over',
                outFrame: 'popupClose_btn/enabled',
                downFrame: 'popupClose_btn/down',
                upFrame: 'popupClose_btn/enabled',
                disabledFrame: 'popupClose_btn/disabled',
                onDownSoundKey: 'clicksound',
                onOverSoundKey: '',
                onOutSoundKey: '',
                onUpSoundKey: ''
            };
            let buttonConfiguration = new ButtonConfiguration(bottonConfigurationParams);
            let popupCloseButton:BaseButton = new BaseButton(this.game, buttonConfiguration, popupWindowBackground.width, 0, this.onPopupCloseButtonClick, this);
            popupCloseButton.actions = buttonActions;
            popupCloseButton.anchor.set(0.5, 0.5);

            this.popupWindow.add(popupWindowBackground);
            this.popupWindow.add(popupText);
            this.popupWindow.add(popupCloseButton);

            this.popupWindow.pivot.set(this.popupWindow.width / 2, this.popupWindow.height / 2);
            this.popupWindow.x = 1366 / 2;
            this.popupWindow.y = 768 / 2;
            this.popupWindow.visible = false;
            this.layer.add(this.popupWindow);
        }

        public setGameMode(data:any):void {
            this.gameMode = data;
            this.stateChanged();
        }

        public stateChanged(stateName?: string): void {
            super.stateChanged(stateName);

            if(this.gameMode == 'freeBet') {
                this.disableButton('maxBet_btn');
                this.disableButton('betInc_btn');
                this.disableButton('betDec_btn');
                this.disableButton('linesInc_btn');
                this.disableButton('linesDec_btn');
            }

            if(this.gameMode == 'stream') {
                this.disableButton('auto_btn');
                this.disableButton('startSpin_btn');
                this.disableButton('stopSpin_btn');
                this.disableButton('maxBet_btn');
                this.disableButton('betInc_btn');
                this.disableButton('betDec_btn');
                this.disableButton('linesInc_btn');
                this.disableButton('linesDec_btn');
                this.disableButton('info_btn');
            }
        }

        protected onPopupCloseButtonClick():void {
            this.hidePopupWindow();
        }

        protected setTextPosition(textField:LocaleText, pattern:any):void {
            if(!textField || !pattern) {
                return;
            }

            switch (pattern.align) {
                case 'left':
                    textField.align = 'left';
                    textField.x = pattern.x;
                    textField.y = pattern.y;
                    textField.anchor.set(0, 0);
                    break;
                case 'right':
                    textField.align = 'right';
                    textField.x = pattern.x + pattern.width;
                    textField.y = pattern.y;
                    textField.anchor.set(1, 0);
                    break;
                case 'center':
                    textField.align = 'center';
                    textField.x = pattern.x + pattern.width / 2;
                    textField.y = pattern.y;
                    textField.anchor.set(0.5, 0);
                    break;
            }

            if(textField && !textField.parent) {
                this.layer.add(textField);
            }
        }

        protected changePositions():void {
            let ratio:number = this.game.camera.width / this.game.camera.height;
            this.layer.children.forEach(function (child:any):void {
                //child.x -= this.game.camera.x / this.layer.scale.x;
                if(child.name.indexOf('_btn') > -1) {
                    child.scale.set(0.5, 0.5);
                }
            }, this);

            this.layer.getByName('bottomPanel').children.forEach(function (child:any):void {
                //child.x -= this.game.camera.x / this.layer.scale.x;
                if(child.name.indexOf('_btn') > -1) {
                    child.scale.set(0.5, 0.5);
                }
            }, this);
        }

        public showAutoplayText(autoplay:boolean): void {
            this.label_autoplay.visible = autoplay;
        }

        /**
         * 
         * @param value
         */
        public changeBetText(value: number): void {
            super.changeBetText(value);
            this.betValueText.text = MoneyFormatController.instance.format(value, true);
        }

        /**
         *
         * @param value
         */
        public changeLinesText(value: number): void {
            super.changeBetText(value);
            this.linesValueText.text = String(value);
        }

        /**
         *
         * @param value
         */
        public changeTotalBetText(value: number): void {
            super.changeBetText(value);
            this.totalBetValueText.text = MoneyFormatController.instance.format(value, true);
        }

        /**
         * 
         * @param value
         */
        public changeWinText(value: number): void {
            if(value == 0) {
                return;
            }
            super.changeWinText(value);
            let obj:any = {
                winValue:0
            };
            this.winTween = this.game.add.tween(obj).to({winValue:value}, 500, Phaser.Easing.Linear.None, true);
            this.winTween.onUpdateCallback(function(tween:Phaser.Tween):void {
                this.winValueText.text = MoneyFormatController.instance.format(Math.ceil(this.showedWin + tween.target.winValue), true);
            }, this);
            this.winTween.onComplete.addOnce(function(obj:any):void {
                this.showedWin += value;
                this.winValueText.text = MoneyFormatController.instance.format(this.showedWin, true);
            }, this);
            //this.showedWin +=value;
            //this.winValueText.text = String(this.showedWin);
        }

        /**
         *
         */
        public resetWinText(): void {
            super.resetWinText();

            if(this.winTween) {
                this.winTween.stop(false);
            }

            this.showedWin = 0;
            this.winValueText.text = "";
        }

        /**
         *
         * @param value
         */
        public changeBalance(value: number): void {
            if(this.gameMode == 'freeBet' && value < parseInt(this.balanceValueText.text)) {
                return;
            }
            super.changeBalance(value);
            this.balanceValueText.text = MoneyFormatController.instance.format(value, true);
        }

        public showPopupWindow(data?:any):void {
            super.showPopupWindow(data);

            this.popupWindow.visible = true;
        }

        public hidePopupWindow(data?:any):void {
            super.showPopupWindow(data);

            this.popupWindow.visible = false;
        }
    }
}