﻿///<reference path="../../../engine/controller/ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class BaseGameDecorationsController extends ActionController {

        protected frame: SimpleSprite;
        protected freeSpinsFrame: SimpleSprite;
        protected logo: SimpleSprite | AnimatedSprite;
        protected layer: Phaser.Group;
        protected pattern: any;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, debugMode);
            this.layer = layer;
            this.pattern = pattern;
            this.init(pattern);
        }

        /**
        * Building reel views
        */
        public init(pattern: any): void {
            this.frame = new SimpleSprite(this.game, pattern.usual_frame.x, pattern.usual_frame.y, pattern.usual_frame.name, this.debugMode);
            this.layer.add(this.frame);

            this.createLogo();
        }

        /**
         *
         */
        protected createLogo():void {

        }

        /**
         *
         * @param data
         */
        public changeFrame(data?:any): void {

        }

    }
}