﻿///<reference path="../../../engine/controller/ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class BaseGameBackController extends ActionController {

        protected view: SimpleSprite;
        protected freeSpinsBG: SimpleSprite;
        protected layer: Phaser.Group;
        protected pattern: any;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, debugMode);
            this.layer = layer;
            this.pattern = pattern;
            this.init(pattern);
        }

        /**
         *
         * @param pattern
         */
        public init(pattern: any): void {
            this.view = new SimpleSprite(this.game, pattern.background.x, pattern.background.y, pattern.background.name, this.debugMode);
            this.layer.add(this.view);
        }

        /**
         *
         * @param data
         */
        public changeBackground(data:any): void {

        }

    }
}