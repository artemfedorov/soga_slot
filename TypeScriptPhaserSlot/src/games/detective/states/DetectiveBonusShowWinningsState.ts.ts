﻿///<reference path="../../../engine/model/states/BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class DetectiveBonusShowWinningsState extends BaseState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.SHOW_WINNING_BONUS_GAME_STATE;
        }

        public checkData(): any {
            if(this.model.hasBonus) {
                return this.model.bonusResponse || {};
            }
            return null;
        }
    }
}

