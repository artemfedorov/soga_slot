﻿///<reference path="../../../engine/model/states/BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class DetectiveRemoveSymbolsState extends BaseState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.REMOVE_SYMBOLS_STATE;
        }

        public checkData(): boolean {
            if (this.model.result.scatters && this.model.result.scatters.length > 0) {
                return true;
            }
            return null;
        }
    }
}

