﻿///<reference path="../../../engine/view/reels/BaseReelView.ts" />
///<reference path="../../../engine/view/reels/ISymbol.ts" />
///<reference path="./DetectiveSymbol.ts" />

module TypeScriptPhaserSlot.common {

    export class DetectiveReelView extends BaseReelView {

        private static SPEED_FALLING: number = 300;
        public goingToBeRemoved: number = 0;
        private symbolCounter: number = 0;
        private isNewDoubleSymbol: boolean;


        constructor(game: Game, index: number, name: string, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfig: GameConfiguration, pattern: any, data: number[], mapReel?: number[], mapReelFreespins?: number[], borderLayer?: Phaser.Group) {
            super(game, index, name, debugMode, symbolProperties, gameConfig, pattern, data, mapReel, mapReelFreespins, borderLayer);
            this.symbolHeight = this.symbolProperties[0].height;
            this.symbolHeight = this.gameConfig.standartSymbolSetting.height;
            this.bottomVisibleLimit = this.symbolHeight * this._reelConfig.visibledSymbols;
            this.bottomRemovedLimit = this.symbolHeight * (this._reelConfig.visibledSymbols + 1);
            this.resultData = this.initData;
            this.init();
            let i = this._symbols.length - 1;
            while (i >= 0) {
                this.bottomLayer.bringToTop(this._symbols[i]);
                i--;
            }
        }

        public removeSymbol(index: number): void {
            let symbol: ISymbol = this.symbols[index];
            symbol.userdata = -1;
            this.goingToBeRemoved++;
            let tween = this.game.add.tween(symbol).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true, this.index * 100);
            tween.onComplete.addOnce(this.onRemoveSymbolComplete, this);
        }

        protected onRemoveSymbolComplete():void {
            setTimeout(function () {
                this.respineSignal.dispatch("removeComplete");
            }.bind(this), 500);

        }

        public stop(data: any, additionalSymbols?: number): void {
            this.resultData = data;
            this.goingToBeRemoved = 0;
            this.addSymbols(this.resultData, true);
            this.fall();
        }

        private fall(): void {
            let delay: number = 0;
            let distance: number = this.symbolHeight * (this._symbols.length - 3);
            for (let i = this.symbols.length - 1; i >= 0; i--) {
                let tween = this.game.add.tween(this.symbols[i]).to({y: this.symbols[i].y + distance}, DetectiveReelView.SPEED_FALLING, Phaser.Easing.Cubic.In, true, delay);
                tween.onComplete.addOnce(this.fallingComplete, this);
                delay += DetectiveReelView.SPEED_FALLING / 10;
                this.symbolCounter++;
            }
        }


        private fallingComplete(): void {
            this.symbolCounter--;
            if(this.symbolCounter == 0) {
                SoundController.instance.playSound('fall_sound','default', 0.6);
                this.justTouchedStopSignal.dispatch(this.index);
                this.actionSignal.dispatch(this.index);
                for (let i = 0; i < 3; i++) {
                    this.poolIn(this.symbols[this.symbols.length - 1]);
                    this.symbols[this.symbols.length - 1].alpha = 1;
                    this.symbols.pop();
                }
            }
            if(this.checkForSymbol(8)) {
                SoundController.instance.playSound('double_apperance');
            }
        }



        public respin(data?: any, timeout?: number): void {
            let holes: number = 0;
            data.splice(this.goingToBeRemoved);
            this.isNewDoubleSymbol = data.indexOf(8) != -1;
            if(this.goingToBeRemoved > 0) {
                this.respineSignal.dispatch("start");
            }
            this.addSymbols(data, true);

            for (let i = this.symbols.length - 1; i >= 0; i--) {
                if (this.symbols[i].userdata == -1) {
                    holes++;
                } else {
                    this.symbols[i].userdata = holes;
                }
            }
            for (let i = 0; i < this.symbols.length; i++) {
                if (this.symbols[i].userdata == -1) {
                    this.symbols[i].userdata = 0;
                    this.poolIn(this.symbols[i]);
                    this.symbols[i].alpha = 1;
                    this.symbols.splice(i, 1);
                    i = 0;
                }
            }
            let delay: number = 0;
            let tween: Phaser.Tween;
            for (let i = this.symbols.length - 1; i >= 0; i--) {
                let distance: number = this.symbolHeight * (this._symbols[i].userdata);
                this.symbols[i].userdata = 0;
                if(distance == 0) {
                    continue;
                }
                tween = this.game.add.tween(this.symbols[i]).to({y: this.symbols[i].y + distance}, DetectiveReelView.SPEED_FALLING, Phaser.Easing.Cubic.In, true, delay + timeout);
                tween.onComplete.addOnce(this.respinComplete, this);
                this.symbolCounter++;
                delay += DetectiveReelView.SPEED_FALLING / 10;
            }
        }


        private respinComplete(): void {
            this.symbolCounter--;
            if(this.symbolCounter == 0) {
                SoundController.instance.playSound('fall_sound');
                console.log("fall_sound");

                this.goingToBeRemoved = 0;
                setTimeout(function () {
                    this.respineSignal.dispatch("stop");
                }.bind(this), 1000);
            }
            if(this.isNewDoubleSymbol) {
                SoundController.instance.playSound('double_apperance');
            }
        }


    }
}