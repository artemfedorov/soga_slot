///<reference path="../../../engine/view/reels/BaseReelSymbol.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DetectiveSymbol = /** @class */ (function (_super) {
            __extends(DetectiveSymbol, _super);
            function DetectiveSymbol(game, x, y, symbolProperties, debugMode) {
                return _super.call(this, game, x, y, symbolProperties, debugMode) || this;
            }
            DetectiveSymbol.prototype.init = function () {
                _super.prototype.init.call(this);
                this.symbolImage.x -= 11.5;
                this.symbolImage.y -= 7.5;
                this._disabledFrame = new common.SimpleSprite(this.game, 0, 0, 'symbols_disabled', this.debugMode, this.currentSymbolProperties.key + '_disabled');
                this._disabledFrame.x -= 11.5;
                this._disabledFrame.y -= 7.5;
                this._disabledFrame.visible = false;
                this.add(this._disabledFrame);
            };
            DetectiveSymbol.prototype.changeKey = function (game, symbolProperties) {
                this.ownIndex = symbolProperties.index;
                this.symbolImage.frameName = symbolProperties.key;
                this._disabledFrame.frameName = symbolProperties.key + '_disabled';
                this.game = game;
                this._borderKey = symbolProperties.borderKeys[0];
                this._currentSymbolProperties = symbolProperties;
            };
            DetectiveSymbol.prototype.activate = function () {
                if (this.symbolImage.frameName == 'icon_empty') {
                    this._disabledFrame.visible = false;
                    return;
                }
                //this._disabledFrame.visible = false;
                this.game.tweens.removeFrom(this._disabledFrame, true);
                this._disabledFrame.alpha = 1;
                this.game.add.tween(this._disabledFrame).to({ alpha: 0 }, 300, "Linear", true).onComplete.addOnce(function () {
                    this._disabledFrame.visible = false;
                }.bind(this));
                /*if(this.scale.x == 1) {
                    return;
                }
                this.game.tweens.removeFrom(this, true);
                this.game.add.tween(this).to({ alpha: 1 }, 100, "Linear", true);
                this.game.add.tween(this.scale).to({ x: 1, y: 1 }, 100, "Linear", true);
                let delta:number = ((1 - this.scale.x) * 342) / 2;
                this.game.add.tween(this).to({ x: this.x - delta, y: this.y - delta }, 100, "Linear", true);*/
            };
            DetectiveSymbol.prototype.deactivate = function () {
                if (this.symbolImage.frameName == 'icon_empty') {
                    this._disabledFrame.visible = false;
                    return;
                }
                this._disabledFrame.visible = true;
                this.game.tweens.removeFrom(this._disabledFrame, true);
                this._disabledFrame.alpha = 0;
                this.game.add.tween(this._disabledFrame).to({ alpha: 1 }, 300, "Linear", true);
                /*if(this.scale.x != 1) {
                    return;
                }
                let newScale:number = 0.9;
                this.game.tweens.removeFrom(this, true);
                this.game.add.tween(this).to({ alpha: 0.9 }, 100, "Linear", true);
                this.game.add.tween(this.scale).to({ x: newScale, y: newScale }, 100, "Linear", true);
                let delta:number = ((this.scale.x - newScale) * 342) / 2;
                this.game.add.tween(this).to({ x: this.x + delta, y: this.y + delta }, 100, "Linear", true);*/
            };
            return DetectiveSymbol;
        }(common.BaseReelSymbol));
        common.DetectiveSymbol = DetectiveSymbol;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
