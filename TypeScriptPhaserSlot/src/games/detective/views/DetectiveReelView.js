///<reference path="../../../engine/view/reels/BaseReelView.ts" />
///<reference path="../../../engine/view/reels/ISymbol.ts" />
///<reference path="./DetectiveSymbol.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DetectiveReelView = /** @class */ (function (_super) {
            __extends(DetectiveReelView, _super);
            function DetectiveReelView(game, index, name, debugMode, symbolProperties, gameConfig, pattern, data, mapReel, mapReelFreespins, borderLayer) {
                var _this = _super.call(this, game, index, name, debugMode, symbolProperties, gameConfig, pattern, data, mapReel, mapReelFreespins, borderLayer) || this;
                _this.symbolHeight = _this.symbolProperties[0].height;
                _this.mapReelFreespins = mapReelFreespins ? mapReelFreespins : [];
                _this.mapReel = mapReel ? mapReel : [];
                _this.mapReelFreespinsShift = 0;
                _this.mapReelShift = 0;
                _this.topVisibledLimit = 0;
                _this.setRegularMap();
                _this.symbolHeight = _this.gameConfig.standartSymbolSetting.height;
                _this.bottomVisibleLimit = _this.symbolHeight * _this._reelConfig.visibledSymbols;
                _this.bottomRemovedLimit = _this.symbolHeight * (_this._reelConfig.visibledSymbols + 1);
                var symbolsCount = [24, 36, 49, 58, 70];
                _this.generateSymbolsBand(symbolsCount[index], 4, _this.symbolProperties.length);
                _this.updater = _this.emptyFunction;
                _this.init();
                return _this;
            }
            DetectiveReelView.prototype.init = function () {
                this._symbols = [];
                //this.currentShift = 0;
                for (var i = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                    this.initData.unshift(this.getNextKey());
                }
                for (var j = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                    this.initData.push(this.getNextKey());
                }
                var addWild = this.checkForWild(this.initData);
                for (var k = addWild.startIndex; k < addWild.startIndex + addWild.changeCount; k++) {
                    this.initData[k] = 10;
                }
                this.addSymbols(this.initData);
                for (var k = 0; k < this._symbols.length; k++) {
                    this._symbols[k].y -= this.symbolHeight * this._reelConfig.topInvisibleSymbolsCount;
                }
            };
            /**
             * Overriden function called by ReelsController to begin rolling
             * @param {any} data
             */
            DetectiveReelView.prototype.start = function (data) {
                this.mapReel = [];
                var symbolsCount = [24, 36, 49, 58, 70];
                this.generateSymbolsBand(symbolsCount[this.index], 4, this.symbolProperties.length);
                this.currentMap = this.mapReel;
                _super.prototype.start.call(this, data);
                this.tweenToTop();
            };
            /**
             *
             * @param reel
             * @returns {any}
             */
            DetectiveReelView.prototype.checkForWild = function (reel) {
                var checkResult = {};
                checkResult.changeCount = 0;
                checkResult.startIndex = 0;
                var count = 3;
                var startIndex = reel.indexOf(10);
                var index = startIndex;
                while (reel[index] == 10) {
                    count--;
                    index++;
                }
                checkResult.changeCount = count == 3 ? 0 : count;
                checkResult.startIndex = startIndex == this.reelConfig.topInvisibleSymbolsCount ? this.reelConfig.topInvisibleSymbolsCount - checkResult.changeCount : this.reelConfig.topInvisibleSymbolsCount + this.reelConfig.visibledSymbols;
                return checkResult;
            };
            /**
             *
             * @param data
             * @param additionalSymbols
             */
            DetectiveReelView.prototype.realStop = function (data, additionalSymbols) {
                this.updater = null;
                this.game.tweens.removeFrom(this.tweenedObject);
                this.resultData = data;
                this.currentShift = 0;
                for (var i = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                    this.resultData.unshift(this.getNextKey());
                }
                for (var j = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                    this.resultData.push(this.getNextKey());
                }
                for (var j = 0; j < additionalSymbols; j++) {
                    this.resultData.push(this.getNextKey());
                }
                var addWild = this.checkForWild(this.resultData);
                for (var k = addWild.startIndex; k < addWild.startIndex + addWild.changeCount; k++) {
                    this.resultData[k] = 10;
                }
                this.addSymbols(this.resultData, true);
                this.resultedTween();
            };
            /**
             *
             */
            DetectiveReelView.prototype.addNewSymbolsInStaticRolling = function () {
                var nextKeys = this.getNextKeys(1);
                this.addSymbols(nextKeys, true);
            };
            return DetectiveReelView;
        }(common.BaseReelView));
        common.DetectiveReelView = DetectiveReelView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
