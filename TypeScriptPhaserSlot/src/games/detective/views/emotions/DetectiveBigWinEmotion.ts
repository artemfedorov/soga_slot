/**
 * Created by rocket on 04.07.2017.
 */
module TypeScriptPhaserSlot.common {

    export class DetectiveBigWinEmotion extends BaseEmotion {

        private _timeOut:number;

        constructor(game: Game, config:EmotionConfiguration) {
            super(game, config);
        }

        /**
         *
         */
        protected init():void {

        }

        /**
         *
         */
        public start():void {
            this.animate();
            SoundController.instance.playSound('hammer', 'emotions', 0.1);
        }

        /**
         *
         */
        public stop():void {
            clearInterval(this._timeOut);
            this._timeOut = 0;
            super.stop();
        }

        /**
         *
         */
        protected animate():void {
            this.onAnimationsComplete();
        }

        /**
         *
         * @param data
         */
        protected onAnimationsComplete(data?:any):void {
            this._actionSignal.dispatch(AnimatedSprite.ALL_ANIMATIONS_COMPLETE);
        }
    }
}