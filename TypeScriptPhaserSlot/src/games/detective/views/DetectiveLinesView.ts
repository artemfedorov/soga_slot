﻿///<reference path="../../../engine/view/lines/LinesView.ts" />

module TypeScriptPhaserSlot.common {

    export class DetectiveLinesView extends LinesView {

        constructor(game: Game, name: string, configuration: WinLineConfiguration, pattern: any, debugMode?: boolean) {
            super(game, name, configuration, pattern, debugMode);
        }
    }
}