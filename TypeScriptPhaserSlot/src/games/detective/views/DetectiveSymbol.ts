﻿///<reference path="../../../engine/view/reels/BaseReelSymbol.ts" />

module TypeScriptPhaserSlot.common {
    export class DetectiveSymbol extends BaseReelSymbol {

        protected _disabledFrame:SimpleSprite;

        constructor(game: Phaser.Game, x: number, y: number, symbolProperties: SymbolProperties, debugMode?: boolean) {
            super(game, x, y, symbolProperties, debugMode);
        }

        protected init(): void {
            super.init();

            /*let debugRect:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
            debugRect.beginFill(0xff0000, 0.7);
            debugRect.drawRect(0, 0, 190, 190);
            debugRect.endFill();
            this.add(debugRect);*/

            this.symbolImage.x -= this.currentSymbolProperties.pivot.x;
            this.symbolImage.y -= this.currentSymbolProperties.pivot.y;

            this._disabledFrame = new SimpleSprite(this.game, 0, 0, 'symbols_disabled', this.debugMode, this.currentSymbolProperties.key + '_disabled');
            this._disabledFrame.x -= this.currentSymbolProperties.pivot.x;
            this._disabledFrame.y -= this.currentSymbolProperties.pivot.y;
            this._disabledFrame.visible = false;
            this.add(this._disabledFrame);

            /*let sss:Phaser.Graphics = new Phaser.Graphics(this.game, 0 ,0);
            sss.beginFill(0xFF0000, 0.5);
            sss.drawRect(0, 0, 190, 190);
            sss.endFill();
            this.add(sss);*/
        }

        public changeKey(game: Phaser.Game, symbolProperties: SymbolProperties): void {
            this.game = game;
            this._currentSymbolProperties = symbolProperties;
            this.symbolImage.frameName = this.currentSymbolProperties.key;
            this._disabledFrame.frameName = this.currentSymbolProperties.key + '_disabled';
            this._borderKey = this.currentSymbolProperties.borderKeys[0];
            this.ownIndex = this.currentSymbolProperties.index;

            this.symbolImage.x = -this.currentSymbolProperties.pivot.x;
            this.symbolImage.y = -this.currentSymbolProperties.pivot.y;
            this._disabledFrame.x = -this.currentSymbolProperties.pivot.x;
            this._disabledFrame.y = -this.currentSymbolProperties.pivot.y;
            this._disabledFrame.visible = false;
        }

        public activate():void {
            this._disabledFrame.visible = false;

            /*this.game.tweens.removeFrom(this._disabledFrame, true);
            this._disabledFrame.alpha = 1;
            this.game.add.tween(this._disabledFrame).to({ alpha: 0 }, 300, "Linear", true).onComplete.addOnce(function () {
                this._disabledFrame.visible = false;
            }.bind(this));*/
        }

        public deactivate():void {
            this._disabledFrame.visible = true;
            /*this.game.tweens.removeFrom(this._disabledFrame, true);
            this._disabledFrame.alpha = 0;
            this.game.add.tween(this._disabledFrame).to({ alpha: 1 }, 300, "Linear", true);*/
        }
    }
}