/**
 * Created by rocket on 21.07.2017.
 */
///<reference path="../../../engine/view/BaseView.ts" />

module TypeScriptPhaserSlot.common {

    export class DetectivePreloaderView extends BaseView implements IPreloaderView {

        private pattern: any;
        private progressBar:SimpleSprite;

        constructor(game: Game, name: string, pattern: any, debugMode?: boolean) {
            super(game, name, debugMode);
            this.game = game;
            this.pattern = pattern;

            this.init();
        }

        private init(): void {
            let preloaderBackground:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'preloader_bg', this.debugMode);
            this.add(preloaderBackground);

            let progressbar:Phaser.Group = new Phaser.Group(this.game);
            let progress:SimpleSprite = new SimpleSprite(this.game, 464, 292, 'preloader_progress', this.debugMode);
            progressbar.add(progress);

            let mask:Phaser.Graphics = new Phaser.Graphics(this.game, 612, 292);
            mask.beginFill(0xffffff, 1);
            mask.drawRoundedRect(0, 0, progress.width, progress.height, 5);
            mask.endFill();
            progressbar.add(mask);

            progress.mask = mask;

            this.add(progressbar);

            this.progressBar = progress;
        }

        public onLoadingUpdate(progress:number, cacheKey:string, success:boolean, totalLoaded:number, totalFiles:number):void {
            let oneFileHeight:number = 148 / totalFiles;
            this.progressBar.x = 459 + ((oneFileHeight * totalLoaded) + (oneFileHeight % progress));
        }

        public onLoadingComplete():void {

        }
    }
}