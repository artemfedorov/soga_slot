module TypeScriptPhaserSlot.common {

    export class DetectiveBonusFolder extends Phaser.Group {

        protected id:number;
        protected folderName: string;
        protected folderImage: SimpleSprite;
        protected highlightImage: SimpleSprite;
        protected photoImage: SimpleSprite;
        protected statusImage: SimpleSprite;
        protected debugMode: boolean;
        protected _actionSignal: Phaser.Signal;
        protected winningText:LocaleText;
        protected isOpened:boolean;

        constructor(game: Game, id:number, x: number, y: number, folderName:string, debugMode?: boolean) {
            super(game);
            this.id = id;
            this.folderName = folderName;
            this.x = x;
            this.y = y;
            this.debugMode = debugMode;

            this.init();
            this.addListeners();
        }

        protected init(): void {
            this._actionSignal = new Phaser.Signal();
            this.isOpened = false;

            //this.inputEnableChildren = true;

            this.folderImage = new SimpleSprite(this.game, 0, 0, 'bonus_folders', this.debugMode, 'folders/' + this.folderName);
            this.highlightImage = new SimpleSprite(this.game, 0, 0, 'bonus_folders', this.debugMode, 'folders/folder_glow');
            this.photoImage = new SimpleSprite(this.game, 40, 12, 'bonus_photos', this.debugMode, 'photos/foto_1');
            this.statusImage = new SimpleSprite(this.game, 54, 347, 'bonus_status', this.debugMode, 'status/guilty');
            this.statusImage.rotation = (Math.PI * 15) / 180;

            this.statusImage.visible = false;
            this.highlightImage.visible = false;
            this.photoImage.visible = false;

            this.winningText = this.game['textFactory'].getTextWithStyle("bonus_game_title", "BONUS_GAME_TITLE_TEXT_style");
            this.winningText.anchor.set(0.5, 0.5);
            this.winningText.x = 200;
            this.winningText.y = 300;
            this.winningText.text = '0';
            this.winningText.visible = false;

            this.add(this.highlightImage);
            this.add(this.folderImage);
            this.add(this.photoImage);
            this.add(this.winningText);
            this.add(this.statusImage);
        }

        protected addListeners(): void {
            this.folderImage.events.onInputDown.add(this.onDown, this);
            this.folderImage.events.onInputOver.add(this.onOver, this);
            this.folderImage.events.onInputOut.add(this.onOut, this);
        }

        protected onDown():void {
            if(!this.isOpened) {
                this.actionSignal.dispatch({item:this, id:this.id});
            }
        }

        protected onOver():void {
            this.highlightImage.visible = true;
        }

        protected onOut():void {
            this.highlightImage.visible = false;
        }

        public showWinning(data: any): void {
            this.isOpened = true;
            let params:string[] = data.split('#');

            this.folderImage.frameName = 'folders/' + this.folderName + '_open';

            this.photoImage.frameName = 'photos/foto_' + +params[1];
            this.photoImage.visible = true;

            //this.winningText.text = (+params[0]).toFixed(2);
            //this.winningText.visible = true;

            this.statusImage.frameName = 'status/not_guilty';
            this.statusImage.visible = true;

            this.remove(this.highlightImage);
        }

        public setStatus(status:boolean):void {
            this.statusImage.frameName = status ? 'status/not_guilty' : 'status/guilty';
        }

        public restore():void {
            this.isOpened = false;
            this.folderImage.frameName = 'folders/' + this.folderName;
            this.highlightImage.visible = false;
            this.photoImage.visible = false;
            this.winningText.text = '0';
            this.winningText.visible = false;
            this.statusImage.visible = false;
            this.add(this.highlightImage);
        }

        /**
         *
         * @param data
         */
        public start(data?: any): void {

        }

        /**
         *
         * @param {any} data
         */
        public stop(data: any): void {

        }

        /**
         *
         */
        public activate():void {
            this.folderImage.inputEnabled = true;
        }

        /**
         *
         */
        public deactivate():void {
            this.folderImage.inputEnabled = false;
        }

        /**
         *
         * @param key
         */
        public changeKey(key: string): void {
            this.folderImage.frameName = key;
        }

        /************************************************************
         Getters and Setters
         *************************************************************/

        /*public get key(): string | Phaser.RenderTexture | Phaser.BitmapData | Phaser.Video | PIXI.Texture {
            return this.folderImage.key;
        }*/

        public get actionSignal(): Phaser.Signal {
            return this._actionSignal;
        }

    }
}