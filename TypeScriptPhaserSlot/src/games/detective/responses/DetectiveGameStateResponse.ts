﻿module TypeScriptPhaserSlot.common {

    export class DetectiveGameStateResponse implements IResponse {
        protected _status: number;
        protected _initialBet: number;
        protected _initialLines: number;
        protected _coins: number;
        protected _line: number;
        protected _bet: number;
        protected _hasBonus: boolean;
        protected _bStage: number;
        protected _freespins: number;
        protected _points: number;
        protected _fsTotal: number;
        protected _fsMult: number;
        protected _bonusType: number;
        protected _bonusGame: any[];
        protected _giftspins: number;
        protected _jackpot0: number;
        protected _jackpot1: number;
        protected _betsPreselect: number[];
        protected _lineCount: number;
        protected _lineCountFreespins: number;
        protected _denomination: number;

        protected _bets: number[];
        protected _lines: number[];
        protected _reels: number[][];
        protected _paylines: number[][];
        protected _paylinesFreespins: number[][];
        protected _paytable: any;
        protected _userCoints: number;
        protected _reelMap: number[][];
        protected _reelMapFreespins: number[][];

        protected _multipliers: number[];
        protected _multipliersFreespins: number[];

        protected _commands: any[];
        //protected _response: any;
       
        protected _serverInfo: any;

        protected _spinRespince: DetectiveSpinResponse;
        protected _bonusResponce: BonusResponse;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): DetectiveGameStateResponse {
            //this._status = rawData.status;

            //this._denomination = rawData.initial.denomination;
            this._denomination = rawData.denomination;
            this._initialBet = rawData.initial.bet_on_line;
            this._initialLines = +rawData.initial.lines_amount;
            this._coins = rawData.balance;

            //his._hasBonus = rawData.response.state.hasBonus;
            //this._bStage = rawData.response.state.bStage;

            this._reels = rawData.combination;

            //this._freespins = rawData.response.state.freespins;
            //this._points = rawData.response.state.points;
            //this._fsTotal = rawData.response.state.fsTotal;
            //this._fsMult = rawData.response.state.fsMult;
            //this._bonusType = rawData.response.state.bonusType;
            //this._bonusGame = rawData.response.state.bonusGame;
            //this._giftspins = rawData.response.state.giftspins;
            //this._jackpot0 = rawData.response.state.jackpot0;
            //this._jackpot1 = rawData.response.state.jackpot1;

            this._bets = rawData.ranges.bets;
            this._lines = rawData.ranges.lines;

            //this._betsPreselect = rawData.response.betsPreselect;

            //this._lineCount = rawData.response.lineCount;
            //this._lineCountFreespins = rawData.response.lineCountFreespins;
            //this._paylines = rawData.response.paylineMap;
            //this._paylinesFreespins = rawData.response.paylineMapFreespins;
            //this._paytable = rawData.response.paytable;

            this._userCoints = rawData.balance;
            //this._reelMap = rawData.response.reelMap;

            //this._reelMapFreespins = rawData.response.reelMapFreespins;
            //this._multipliers = rawData.response.multipliers;
            //this._multipliersFreespins = rawData.response.multipliersFreespins;
            //this._commands = rawData.commands;
            //this._serverInfo = rawData.serverInfo;

            if(rawData.events.length > 0) {
                rawData.events.forEach(function (element:any) {
                    /*if(element.type == 'spin') {
                        this._reels = element.combination || this._reels;
                        this._spinRespince = new DetectiveSpinResponse(element);

                        this._freespins = element.bonuses_summary.freespins_remainder || 0;
                    }*/

                    if(element.type == 'automatic') {
                        this._reels = element.combination || this._reels;
                        this._spinRespince = new DetectiveSpinResponse(element);

                        this._freespins = element.bonuses_summary.freespins_remainder || 0;
                    }

                    if(element.type == 'choice') {
                        this._hasBonus = true;
                        this._bonusResponce = new BonusResponse(element);
                    }
                }, this);
            }

            /*this._status = rawData.status;
            this._initialBet = rawData.response.initialBet;
            this._coins = rawData.response.user.coins;
            this._lines = rawData.response.state.lines;
            this._hasBonus = rawData.response.state.hasBonus;
            this._bStage = rawData.response.state.bStage;
            this._reels = rawData.response.state.reels;
            this._freespins = rawData.response.state.freespins;
            this._points = rawData.response.state.points;
            this._fsTotal = rawData.response.state.fsTotal;
            this._fsMult = rawData.response.state.fsMult;
            this._bonusType = rawData.response.state.bonusType;
            this._bonusGame = rawData.response.state.bonusGame;
            this._giftspins = rawData.response.state.giftspins;
            this._jackpot0 = rawData.response.state.jackpot0;
            this._jackpot1 = rawData.response.state.jackpot1;
            this._bets = rawData.response.bets;
            this._betsPreselect = rawData.response.betsPreselect;
            this._lineCount = rawData.response.lineCount;
            this._lineCountFreespins = rawData.response.lineCountFreespins;
            this._paylines = rawData.response.paylineMap;
            this._paylinesFreespins = rawData.response.paylineMapFreespins;
            this._paytable = rawData.response.paytable;

            this._userCoints = rawData.response.user.coins;
            this._reelMap = rawData.response.reelMap;

            this._reelMapFreespins = rawData.response.reelMapFreespins;
            this._multipliers = rawData.response.multipliers;
            this._multipliersFreespins = rawData.response.multipliersFreespins;
            this._commands = rawData.commands;
            this._serverInfo = rawData.serverInfo;*/

            return this;
        }




        public get status(): number {
            return this._status;
        }
        public get initialBet(): number {
            return this._initialBet;
        }
        public get initialLines(): number {
            return this._initialLines;
        }
        public get coins(): number {
            return this._coins;
        }
        public get line(): number {
            return this._line;
        }
        public get bet(): number {
            return this._bet;
        }
        public get hasBonus(): boolean {
            return this._hasBonus;
        } 
        public get bStage(): number {
            return this._bStage;
        }
        public get freespins(): number {
            return this._freespins;
        }
        public get points(): number {
            return this._points;
        }
        public get fsTotal(): number {
            return this._fsTotal;
        }
        public get fsMult(): number {
            return this._fsMult;
        }
        public get bonusType(): string {
            return this.bonusType;
        }
        public get bonusGame(): any[] {
            return this._bonusGame;
        }
        public get giftspins(): number {
            return this._giftspins;
        }
        public get jackpot0(): number {
            return this._jackpot0;
        }
        public get jackpot1(): number {
            return this._jackpot1;
        }
        public get betsPreselect(): number[] {
            return this._betsPreselect;
        }
        public get lineCount(): number {
            return this._lineCount;
        }
        public get lineCountFreespins(): number {
            return this._lineCountFreespins;
        }

        public get bets(): number[] {
            return this._bets;
        }
        public get lines(): number[] {
            return this._lines;
        }
        public get reels(): number[][] {
            return this._reels
        }
       
        public get paytable(): any {
            return this._paytable;
        }
        public get userCoints(): number {
            return this._userCoints;
        }
        public get paylines(): number[][] {
            return this._paylines;
        }
        public get paylinesFreespins(): number[][] {
            return this._paylinesFreespins;
        }
        public get reelMap(): number[][] {
            return this._reelMap;
        }
        public get reelMapFreespins(): number[][] {
            return this._reelMapFreespins;
        }

        public get multipliers(): number[] {
            return this._multipliers;
        }
        public get multipliersFreespins(): number[] {
            return this._multipliersFreespins;
        }

        public get commands(): any[] {
            return this._commands;
        } 
        //public get response(): any {
        //    return this._response;
        //}

        public get serverInfo(): any {
            return this._serverInfo; 
        }

        public get spinRespince(): DetectiveSpinResponse {
            return this._spinRespince;
        }

        public get bonusRespince(): BonusResponse {
            return this._bonusResponce;
        }

        public get denomination(): number {
            return this._denomination;
        }
    }
}