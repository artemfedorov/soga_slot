///<reference path="../../../engine/controller/BonusGameController.ts" />

module TypeScriptPhaserSlot.common {

    export class DetectiveBonusGameController extends BonusGameController {

        protected pattern: any;
        protected bonusGameMuiltiplier: number;
        protected view:SimpleSprite;
        protected layer: Phaser.Group;
        protected folders:DetectiveBonusFolder[];
        protected titleTextField:LocaleText;
        protected multiplierTextField:LocaleText;
        protected winningTextField:LocaleText;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, gameConfiguration: GameConfiguration, pattern: any, gameStateResponse: GameStateResponse) {
            super(game, name, layer, debugMode, gameConfiguration, pattern, gameStateResponse);
            this.init();
        }

        public init():void {
            this.bonusGameMuiltiplier = 1;

            //this.view = new SimpleSprite(this.game, this.pattern.background.x, this.pattern.background.y, this.pattern.background.name, this.debugMode);
            this.view = new SimpleSprite(this.game, 0, 0, 'bonus_bg', this.debugMode);
            this.layer.add(this.view);
            //this.layer.visible = true;

            this.folders = [];
            for(let i:number = 0; i < this.pattern.bonusGame.child.length; i++) {
                let childPattern:any = this.pattern.bonusGame.child[i];
                if (childPattern.name.indexOf('folder_') == -1) {
                    continue;
                }
                let folder:DetectiveBonusFolder = new DetectiveBonusFolder(this.game, this.folders.length, childPattern.x, childPattern.y, childPattern.name, this.debugMode);
                folder.actionSignal.add(this.onItemSelected, this);
                folder.rotation = (Math.PI * childPattern.rotation) / 180;
                folder.scale.x = childPattern.scaleY;
                folder.scale.y = childPattern.scaleX;
                folder.deactivate();
                this.layer.add(folder);

                this.folders.push(folder);
            }

            this.titleTextField = this.game.textFactory.getTextWithStyle("bonus_game_title", "BONUS_GAME_TITLE_TEXT_style");
            this.titleTextField.anchor.set(0.5, 0.5);
            this.titleTextField.x = 700;
            this.titleTextField.y = 120;
            this.layer.add(this.titleTextField);

            this.multiplierTextField = this.game.textFactory.getTextWithStyle('bonus_game_multiplier', "BONUS_GAME_TITLE_TEXT_style");
            this.multiplierTextField.anchor.set(0.5, 0.5);
            this.multiplierTextField.x = 450;
            this.multiplierTextField.y = 50;
            this.multiplierTextField.text = this.game.textFactory.getStringByIdentifier('bonus_game_multiplier') + '1';
            this.layer.add(this.multiplierTextField);

            this.winningTextField = this.game.textFactory.getTextWithStyle('bonus_game_winning', "BONUS_GAME_TITLE_TEXT_style");
            this.winningTextField.anchor.set(0.5, 0.5);
            this.winningTextField.x = 950;
            this.winningTextField.y = 50;
            this.winningTextField.text = this.game.textFactory.getStringByIdentifier('bonus_game_winning') + this.getWinning();
            this.layer.add(this.winningTextField);
        }

        private getWinning(data?:any):number {
            let win:number = 0;
            if(this.game.model.bonusResponse) {
                win = this.game.model.bonusResponse.data.bonuses_summary.total;
            } else if(this.game.model.spinResponse) {
                win = this.game.model.spinResponse.state.bonusTotalWin;
            } else if(data && data.data.winnings) {
                win = data.data.winnings.total;
            }
            return win * this.bonusGameMuiltiplier;
        }

        protected onItemSelected(data:any) {
            data.item.parent.add(data.item);
            let tween:Phaser.Tween = this.game.add.tween(data.item).to({rotation:0, y:250}, 100);
            tween.onComplete.addOnce(this.onSelectedItemTweenComplete, this);
            tween.start();

            this.changeInteractive(false);
            this.titleTextField.visible = false;
        }

        protected onSelectedItemTweenComplete(param1:any, param2:any):void {
            console.log('BONUS SELECTED ITEM TWEEN COMPLETE');
            this.actionSignal.dispatch({action:'choice', id:param1.id});
        }

        protected restoreItems(data:any): void {
            let additionalInfo:any = data.additional_info;
            for(let param in additionalInfo.indexes_info) {
                let folder:DetectiveBonusFolder = this.folders[+param];
                this.setFolderToWinning(folder, additionalInfo.indexes_info[param], additionalInfo.choices.indexOf(+param) != -1);
            }
        }

        public choiceFolders(data?: any): void {
            this.changeInteractive(true);
            this.titleTextField.visible = true;
        }

        public showWinning(data: any): void {
            console.log(data);

            if(!data.data) {
                this.restore();
                this.actionSignal.dispatch({action:'showWinningsComplete'});
                return;
            }

            this.changeInteractive(false);
            this.titleTextField.visible = false;

            //let currentBonusData:any = data.data.bonuses[data.data.bonuses_summary.next_bonus_index];
            let currentBonusData:any = data.data.bonuses.filter(function(element:any):boolean {
                return element.type === 'choices';
            }, this)[0];
            this.restoreItems(currentBonusData);

            this.bonusGameMuiltiplier = currentBonusData.additional_info.multiplier || 1;
            this.multiplierTextField.text = this.game.textFactory.getStringByIdentifier('bonus_game_multiplier') + this.bonusGameMuiltiplier;
            this.winningTextField.text = this.game.textFactory.getStringByIdentifier('bonus_game_winning') + this.getWinning(data);

            let newIndex:number = data.data.winnings.info[0].indexes[0];
            let winningFolder:DetectiveBonusFolder = this.folders[newIndex];
            this.setFolderToWinning(winningFolder, data.data.winnings.info[0].winning, true);

            if(currentBonusData.remainder == 0) {
                winningFolder.setStatus(false);
            }

            setTimeout(function () {
                this.actionSignal.dispatch({action:'showWinningsComplete'});

                if(currentBonusData.remainder == 0) {
                    this.layer.visible = false;
                    this.actionSignal.dispatch({action:'bonusGameComplete'});
                }
            }.bind(this), currentBonusData.remainder == 0 ? 1000 : 100);
        }

        protected setFolderToWinning(folder:DetectiveBonusFolder, winning:any, open:boolean):void {
            folder.y = open ? 250 : folder.y;
            folder.showWinning(winning);
        }

        public restore():void {
            this.bonusGameMuiltiplier = 1;
            this.titleTextField.visible = true;
            for(let i:number = 0; i < this.folders.length; i++) {
                this.folders[i].restore();
                this.folders[i].y = 369;
            }
        }

        protected changeInteractive(type:boolean):void {
            this.folders.forEach(function (element:DetectiveBonusFolder) {
                if(type) {
                    element.activate();
                } else {
                    element.deactivate();
                }
            }, this);
        }
    }
}