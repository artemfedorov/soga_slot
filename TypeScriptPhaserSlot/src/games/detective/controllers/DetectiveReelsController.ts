﻿///<reference path="../../../engine/controller/ReelsController.ts" />
///<reference path="../views/DetectiveReelView.ts" />

module TypeScriptPhaserSlot.common {

    export class DetectiveReelsController extends ReelsController {

        private respinReelView: IReelView;
        private isRespin: boolean = false;
        private reelCounter: number = 0;
        private respinTimeout: number = 0;



        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfiguration: GameConfiguration, pattern: any, gameStateResponse: GameStateResponse) {
            super(game, name, layer, debugMode, symbolProperties, gameConfiguration, pattern, gameStateResponse);

            let mask = new Phaser.Graphics(this.game, 0, 0);
            mask.beginFill(0xffffff, 0.5);
            let w: number = 190;
            let h: number = 190;
            mask.drawRect(-(w / 2), -(h / 2), w * 5 + w, h * 3 + h);

            this.layer.add(mask);
            this.layer.mask = mask;
        }

        protected buildReels(gameStateResponse: GameStateResponse): void {
            this.layer.x = this.pattern.x;
            this.layer.y = this.pattern.y;
            this.borderLayer = new Phaser.Group(this.game);
            let reelsAmount: number = this.gameConfiguration.reels.length;
            let reelView: IReelView;
            for (let i: number = 0; i < reelsAmount; i++) {
                reelView = this.buildReel(i, this.gameConfiguration.gameName + String(i), this.debugMode, this.symbolProperties,
                        this.gameConfiguration, this.pattern, gameStateResponse.reels[i]['concat'](), this.borderLayer);
                reelView.respineSignal.add(this.onRespineComplete, this);
                this.layer.add(reelView);
                this.reelViews.push(reelView);
            }
            this.layer.add(this.borderLayer);
        }

        /**
         * DO NOT CALL SUPER!
         * @param {number} index
         * @param {string} name
         * @param {boolean} debugMode
         * @param {SymbolProperties[]} symbolProperties
         * @param {GameConfiguration} gameConfiguration
         * @param {any} pattern
         * @param {number[]} dataReel
         * @returns
         */
        protected buildReel(index: number, name: string, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfiguration: GameConfiguration, pattern: any, dataReel: number[], borderLayer?: Phaser.Group): IReelView {
            let reelView: IReelView;
            reelView = new DetectiveReelView(this.game, index, name, debugMode, symbolProperties, gameConfiguration, pattern, dataReel, null, null, this.borderLayer);
            reelView.justTouchedStopSignal.add(this.onJustTouchedReelStop, this);
            reelView.actionSignal.add(this.onReelFinnalyStopped, this);
            reelView.x = this.pattern.reels[index].x;
            reelView.y = this.pattern.reels[index].y;
            return reelView;
        }

        public stop(data: SpinResult): void {
            let timeout: number = this.isUrgentStop ? 0 : 1000;
            this.stopSpinData = data;

            this.alignReels();
            this.handleTrackedSymbols();



            this.reelViews.forEach(function (item:IReelView, index:number):void {
                setTimeout(function () {
                    item.stop(data.reels[index]['concat'](), this.additionalSymbolsNumbers[index]);
                }.bind(this), 50 * index);
                }, this);

        }

        /**
         *
         * @param data
         */
        public respin(data?: any): void {
            super.respin(data);
            //this.start(data);
        }

        /**
         *
         * @param data
         */
        public stopRespin(data: any): void {
            super.stopRespin(data);
            //this.stop(data);

            this.stopCounter = 0;
            this.reelViews.forEach(function (item:DetectiveReelView, index:number) {
                if(item.goingToBeRemoved > 0) {
                    this.respinTimeout += 100;
                    item.respin(data.reels[index]['concat'](), this.respinTimeout);
                }

                /*setTimeout(function () {

                }.bind(this), 50 * index);*/
            }, this);
        }


        private onRespineComplete(data: any): void {
            if(data == "start") {
                this.reelCounter++;
            }
            if(data == "stop") {
                this.reelCounter--;
                if(this.reelCounter == 0) {
                    this.actionSignal.dispatch(ReelsController.RESPIN_COMPLETE);
                    this.respinTimeout = 0;
                }
            }

            if(data == 'removeComplete') {
                this.actionSignal.dispatch(ReelsController.FINISH_REMOVE_SYMBOLS);
            }
        }

        public removeSymbols(data: any): void {
            for(let i:number = 0; i < data.length; i++) {
                let scatter: any = data[i];
                for (let j: number = 0; j < scatter.indexes.length; j++) {
                    let indexes: number[] = scatter.indexes[j];

                    this.reelViews[indexes[0]]['removeSymbol'](indexes[1]);
                }
            }
            /*setTimeout(function () {
                this.actionSignal.dispatch(ReelsController.FINISH_REMOVE_SYMBOLS);
            }.bind(this), 500);*/
        }
    }
}