﻿///<reference path="../../../engine/controller/ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class DetectiveDecorationsController extends BaseGameDecorationsController {

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, layer, debugMode, pattern);
        }
        /**
        * Building reel views
        */
        public init(): void {
            let blackout:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'blackout', this.debugMode);
            let table:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'table', this.debugMode);

            let animation: AnimatedSprite = new AnimatedSprite(this.game, 0, 0, "smoke_animation", this.debugMode);
            animation.start();

            this.layer.add(table);
            this.layer.add(animation);
            this.layer.add(blackout);

            this.createLogo();
        }

        protected createLogo():void {
            super.createLogo();

            let logoAnimation: AnimatedSprite = new AnimatedSprite(this.game, 517, -10, "logo_animation", this.debugMode);
            animate();
            function animate() {
                logoAnimation.start();
                setTimeout(function () {
                    animate();
                }.bind(this), 20000 + Math.random() * 10000);
            }

            this.layer.add(logoAnimation);
        }
    }
}