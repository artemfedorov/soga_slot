///<reference path="../../../engine/controller/ReelsController.ts" />
///<reference path="../views/DetectiveReelView.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DetectiveReelsController = /** @class */ (function (_super) {
            __extends(DetectiveReelsController, _super);
            function DetectiveReelsController(game, name, layer, debugMode, symbolProperties, gameConfiguration, pattern, gameStateResponse) {
                var _this = _super.call(this, game, name, layer, debugMode, symbolProperties, gameConfiguration, pattern, gameStateResponse) || this;
                _this.isRespin = false;
                var mask = new Phaser.Graphics(_this.game, 0, 0);
                mask.beginFill(0xffffff, 0.5);
                var w = 190;
                var h = 190;
                mask.drawRect(-(w / 2), -(h / 2), w * 5 + w, h * 3 + h);
                _this.layer.add(mask);
                _this.layer.mask = mask;
                return _this;
            }
            /**
             * DO NOT CALL SUPER!
             * @param {number} index
             * @param {string} name
             * @param {boolean} debugMode
             * @param {SymbolProperties[]} symbolProperties
             * @param {GameConfiguration} gameConfiguration
             * @param {any} pattern
             * @param {number[]} dataReel
             * @returns
             */
            DetectiveReelsController.prototype.buildReel = function (index, name, debugMode, symbolProperties, gameConfiguration, pattern, dataReel, borderLayer) {
                var reelView;
                reelView = new common.DetectiveReelView(this.game, index, name, debugMode, symbolProperties, gameConfiguration, pattern, dataReel, null, null, this.borderLayer);
                reelView.justTouchedStopSignal.add(this.onJustTouchedReelStop, this);
                reelView.actionSignal.add(this.onReelFinnalyStopped, this);
                reelView.x = this.pattern.reels[index].x;
                reelView.y = this.pattern.reels[index].y;
                return reelView;
            };
            return DetectiveReelsController;
        }(common.ReelsController));
        common.DetectiveReelsController = DetectiveReelsController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
