﻿///<reference path="../../../engine/model/states/BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class PrincessFrogTransformPrincessState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.PRINCESS_FROG_TRANSFORM_PRINCESS_STATE;
        }

        public checkData(): boolean {
            //if (/*this.model.isFreespins && */this.checkForWild() && this.checkForFrincess()) {
            //    return true;
            //}
            if (this.model.rawData &&
                this.model.rawData.additional_info &&
                this.model.rawData.additional_info.changed_princess &&
                this.model.rawData.additional_info.changed_princess.length > 0) {
                return true;
            }
            return null;
        }

        protected checkForWild():boolean {
            /*for(let i:number = 0; i < this.model.result.reels.length; i++) {
                for(let k:number = 0; k < this.model.result.reels[i].length; k++) {
                    if (this.model.result.reels[i][k] == 10) {
                        return true;
                    }
                }
            }*/

            for(let i:number = 0; i < this.model.result.reels.length; i++) {
                let witches:number[] = this.model.result.reels[i].filter(function (value:number):boolean {
                    return value == 10;
                });
                if(witches.length == this.model.result.reels[i].length) {
                    return true;
                }
            }

            return false;
        }

        protected checkForFrincess():boolean {
            for(let i:number = 0; i < this.model.result.reels.length; i++) {
                for(let k:number = 0; k < this.model.result.reels[i].length; k++) {
                    if (this.model.result.reels[i][k] == 9) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

