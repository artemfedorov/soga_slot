﻿///<reference path="../../../engine/controller/interfaces/IActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class PrincessFrogDecorationsController extends BaseGameDecorationsController {

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, layer, debugMode, pattern);
        }

        public init(pattern: any): void {
            this.frame = new SimpleSprite(this.game, pattern.usual_frame.x, pattern.usual_frame.y, pattern.usual_frame.name, this.debugMode);
            this.layer.add(this.frame);

            this.freeSpinsFrame = new SimpleSprite(this.game, pattern.x, pattern.y, 'usual_frame_freeSpins', this.debugMode);
            this.freeSpinsFrame.visible = false;
            this.layer.add(this.freeSpinsFrame);

            let locale:string = this.game.textFactory.currLocale;
            this.logo = new SimpleSprite(this.game, 670, 64, 'game_logo', this.debugMode, 'logo_' + locale);
            this.logo.anchor.set(0.5, 0.5);
            this.layer.add(this.logo);

            this.game.textFactory.actionSignal.add(this.onLocaleChanged, this);
        }

        protected onLocaleChanged(data:any):void {
            this.logo.frameName = 'logo_' + data.data;
        }


        public changeFrame(freeSpins:boolean): void {
            let tween:Phaser.Tween;
            this.freeSpinsFrame.visible = true;
            if(freeSpins) {
                this.freeSpinsFrame.alpha = 0;
                tween = this.game.add.tween(this.freeSpinsFrame).to({alpha:1}, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                tween.onComplete.add(function ():void {
                    this.frame.visible = false;
                }, this);
            } else {
                this.frame.visible = true;
                tween = this.game.add.tween(this.freeSpinsFrame).to({alpha:0}, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                tween.onComplete.add(function ():void {
                    this.freeSpinsFrame.visible = false;
                }, this);
            }
        }

    }
}