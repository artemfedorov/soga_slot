/**
 * Created by rocket on 04.07.2017.
 */
module TypeScriptPhaserSlot.common {

    export class PrincessFrogFiveInLineEmotion extends BaseEmotion {

        private _timeOut:number;

        constructor(game: Game, config:EmotionConfiguration) {
            super(game, config);
        }

        /**
         *
         */
        protected init():void {

        }

        /**
         *
         */
        public start():void {
            this.animate();
            SoundController.instance.playSound('hammer', 'emotions', 0.1);
        }

        /**
         *
         */
        public stop():void {
            clearInterval(this._timeOut);
            this._timeOut = 0;
            super.stop();
        }

        /**
         *
         */
        protected animate():void {
            let gameWidth:number = 1366;
            let gameHeight:number = 768;

            let backgroundAnimation: AnimatedSprite = new AnimatedSprite(this.game, 0, 0, this.config.emotion);
            backgroundAnimation.anchor.set(0.5, 0.5);
            backgroundAnimation.x = gameWidth / 2;
            backgroundAnimation.y = gameHeight / 2;
            backgroundAnimation.start();
            backgroundAnimation.alpha = 0;
            this.add(backgroundAnimation);
            this._tweens.push(this.game.add.tween(backgroundAnimation).to({alpha: 1}, 700, Phaser.Easing.Cubic.Out, true));
            this._tweens.push(this.game.add.tween(backgroundAnimation).to({alpha: 0}, 700, Phaser.Easing.Cubic.Out, false, 1000));
            this._tweens[0].chain(this._tweens[1]);


            let fiveImage: SimpleSprite = new SimpleSprite(this.game, 0, 0, 'emotions_images', false, 'texts/five');
            let inImage: SimpleSprite = new SimpleSprite(this.game, 0, 0, 'emotions_images', false, 'texts/in');
            let lineImage: SimpleSprite = new SimpleSprite(this.game, 0, 0, 'emotions_images', false, 'texts/line');

            fiveImage.rotation = -5 * Math.PI / 180;
            lineImage.rotation = 1 * Math.PI / 180;
            fiveImage.anchor.set(0.5, 0.5);
            inImage.anchor.set(0.5, 0.5);
            lineImage.anchor.set(0.5, 0.5);
            fiveImage.x = gameWidth / 2;
            fiveImage.y = gameHeight / 2 - 175;

            inImage.x = fiveImage.x - 50;
            inImage.y = fiveImage.y + 100;

            lineImage.x = fiveImage.x;
            lineImage.y = inImage.y + 150;

            this.add(fiveImage);
            this.add(inImage);
            this.add(lineImage);
            fiveImage.scale.set(0, 0);
            inImage.scale.set(0, 0);
            lineImage.scale.set(0, 0);
            this._tweens.push(this.game.add.tween(fiveImage.scale).to({x: 1, y: 1}, 800, Phaser.Easing.Elastic.Out, true));
            this._tweens.push(this.game.add.tween(fiveImage).to({x: fiveImage.x - 70}, 1500, Phaser.Easing.Circular.Out, true, 0));

            this._tweens.push(this.game.add.tween(inImage.scale).to({x: 0.7, y: 0.7}, 800, Phaser.Easing.Elastic.Out, true, 200));

            this._tweens.push(this.game.add.tween(lineImage.scale).to({x: 1, y: 1}, 800, Phaser.Easing.Elastic.Out, true, 500));
            this._tweens.push(this.game.add.tween(lineImage).to({x: lineImage.x + 70}, 1500, Phaser.Easing.Circular.Out, true, 200));


            this._tweens[this._tweens.length - 1].onComplete.addOnce(this.onAnimationsComplete, this);
            SoundController.instance.playSound('fiveinline', 'default', 0.2, false);
        }

        /**
         *
         * @param data
         */
        protected onAnimationsComplete(data?:any):void {
            this._actionSignal.dispatch(AnimatedSprite.ALL_ANIMATIONS_COMPLETE);
        }
    }
}