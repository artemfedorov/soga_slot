﻿///<reference path="../../../engine/view/BaseView.ts" />

module TypeScriptPhaserSlot.common {

    export class PrincessFrogLinesView extends BaseView implements ILinesView {

        private lineSprites: LineSprite[];

        private _config: WinLineConfiguration;

        private pattern: any;

        private lineMask: Phaser.Graphics;

        private imagesGroup: Phaser.Group;

        protected frame: SimpleSprite;
        protected freeSpinsFrame: SimpleSprite;

        protected lines:SimpleSprite[];

        protected hideLinesTimeout:number = -1;

        constructor(game: Game, name: string, configuration: WinLineConfiguration, pattern: any, debugMode?: boolean) {
            super(game, name, debugMode);
            this.game = game;
            this.pattern = pattern;
            this._config = configuration;
            this.init();
        }

        private init(): void {
            this.createLines();
            this.createLineNumbers();
            this.hideLines();
        }

        /**
         * Creation all lines
         */
        private createLines(): void {
            this.lines = [];
            let positionsCoordinates:number[] = [0, 171, 342];
            let linesPositions:number[] = [1,0,2,0,0,0,1,1,0,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0];
            let linesContainer:Phaser.Group = new Phaser.Group(this.game);
            let line:SimpleSprite;
            for(let i:number = 0; i < 30; i++) {
                line = new SimpleSprite(this.game, 0, 0, 'line_' + (i + 1) + '_image');
                line.y = positionsCoordinates[linesPositions[i]];
                //line.scale.set(2, 2);
                linesContainer.add(line);
                this.lines.push(line);
            }

            linesContainer.x = 225;
            linesContainer.y = 195;
            this.add(linesContainer);
        }

        /**
         * Creation all line numbers
         */
        private createLineNumbers(): void {
           
        }

      
        /**
         * 
         */
        public hideLines(animated?:boolean): void {
            if(this.hideLinesTimeout != -1) {
                clearInterval(this.hideLinesTimeout);
                this.hideLinesTimeout = -1;
            }
            let lineImage:SimpleSprite;
            for(let i:number = 0; i < this.lines.length; i++) {
                lineImage = this.lines[i];
                if(!animated) {
                    lineImage.visible = false;
                } else {
                    this.game.add.tween(lineImage).to({alpha:0}, 500, Phaser.Easing.Linear.None, true).onComplete.add(function () {
                        lineImage.visible = false;
                    }, true);
                }
            }
        }

        /**
         *
         * @param count
         */
        public showLines(count:number): void {
            if(this.hideLinesTimeout != -1) {
                clearInterval(this.hideLinesTimeout);
                this.hideLinesTimeout = -1;
            }

            let lineImage:SimpleSprite;
            for(let i:number = 0; i < this.lines.length; i++) {
                lineImage = this.lines[i];

                this.game.tweens.removeFrom(lineImage, true);

                lineImage.alpha = 1;
                lineImage.visible = i + 1 <= count;
            }

            this.hideLinesTimeout = setTimeout(function () {
                this.hideLines(true);
            }.bind(this), 2000);
        }

        /**
         * 
         * @param {PayLine} line
         */
        public showWinLine(line: Payline): void {
            let lineImage:SimpleSprite;
            for(let i:number = 0; i < this.lines.length; i++) {
                lineImage = this.lines[i];
                if(i == line.index) {
                    lineImage.visible = true;
                    lineImage.alpha = 0;
                    this.game.add.tween(lineImage).to({alpha:1}, 250, Phaser.Easing.Linear.None, true, 0, 1, true);
                } else {
                    lineImage.visible = false;
                }
                //lineImage.visible = i == line.index;

            }

            /*var lineConfig: WinLineConfiguration = this._config[line.index];
            for (let i: number = 0; i < line.icon_count; i++) {
                 
            }*/
        }

    }
}