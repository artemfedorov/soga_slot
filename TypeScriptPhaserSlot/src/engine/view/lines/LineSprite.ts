﻿///<reference path="../SimpleSprite.ts" />

module TypeScriptPhaserSlot.common {

    export class LineSprite extends SimpleSprite {

        private pattern: any;
        protected config: WinLineConfiguration;

        constructor(game: Phaser.Game, x: number, y: number, key: string, config: WinLineConfiguration, pattern:any, debugMode: boolean, frame?: number) {
            super(game, x, y, key, debugMode, frame);
            this.pattern = pattern;            
            this.config = config;
        }

        
        public start(line: Payline): void {
            this.visible = true;
            this.startBlinking();
        }

        public stop(data?: any): void {
            this.visible = false;
        }


        private startBlinking(): void {
            this.alpha = 1;
            let blinkTween = this.game.add.tween(this).to({ alpha: 0.2 }, (this.config.timeout / 2) * Phaser.Timer.SECOND, Phaser.Easing.Cubic.In, true, 0, 999);

        }

        /************************************************************
            Getters and Setters
        *************************************************************/

    }
}