﻿module TypeScriptPhaserSlot.common {

    export interface ILinesView {

        showWinLine(line: Payline): void;
        showLines(count: number): void;
        hideLines(animated?:boolean): void;
    }
}

