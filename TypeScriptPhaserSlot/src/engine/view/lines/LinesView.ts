﻿///<reference path="../../../engine/view/BaseView.ts" />

module TypeScriptPhaserSlot.common {

    export class LinesView extends BaseView implements ILinesView {

        private _config:WinLineConfiguration;
        private pattern:any;

        protected lines:SimpleSprite[];

        constructor(game: Game, name: string, configuration: WinLineConfiguration, pattern: any, debugMode?: boolean) {
            super(game, name, debugMode);
            this.game = game;
            this.pattern = pattern;
            this._config = configuration;
            this.init();
        }

        private init(): void {
            this.createLines();
            this.createLineNumbers();
            this.hideLines();
        }

        /**
         * Creation all lines
         */
        private createLines(): void {

        }

        /**
         * Creation all line numbers
         */
        private createLineNumbers(): void {

        }

        /**
         *
         */
        public hideLines(animated?:boolean): void {

        }

        /**
         *
         * @param count
         */
        public showLines(count:number): void {

        }

        /**
         *
         * @param {PayLine} line
         */
        public showWinLine(line: Payline): void {

        }

    }
}