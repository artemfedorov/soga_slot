﻿///<reference path="../SimpleSprite.ts" />

module TypeScriptPhaserSlot.common {

    export class BaseReelSymbol extends Phaser.Group implements ISymbol {

        protected backImage: SimpleSprite;
        protected symbolImage: SimpleSprite;
        protected animationContainer: SimpleSprite;
        protected _ownIndex: number;
        protected _borderKey: string;
        protected _currentSymbolProperties: SymbolProperties;
        protected debugMode: boolean;
        public userdata: any;

        constructor(game: Phaser.Game, x: number, y: number, symbolProperties: SymbolProperties, debugMode?: boolean) {
            super(game);
            this.x = x;
            this.y = y;
            this.debugMode = debugMode;
            this._currentSymbolProperties = symbolProperties;
            this._borderKey = symbolProperties.borderKeys[0] ? symbolProperties.borderKeys[0] : "";

            this.init();
        }

        protected init(): void {
            this.symbolImage = new SimpleSprite(this.game, 0, 0, 'symbols', this.debugMode, this.currentSymbolProperties.key);
            //this.symbolImage.anchor.setTo(0.5, 0.5);
            this.add(this.symbolImage);
        }

        /**
         *
         * @param data
         */
        public start(data?: any): void {

        }

        /**
         * 
         * @param {any} data
         */
        public stop(data?: any): void {

        }

        /**
         *
         */
        public activate():void {

        }

        /**
         *
         */
        public deactivate():void {

        }

        /**
         *
         */
        public blur():void {

        }

        /**
         * 
         */
        public unblur():void {

        }

        /**
         *
         * @param game
         * @param symbolProperties
         */
        public changeKey(game: Phaser.Game, symbolProperties: SymbolProperties): void {
            this.ownIndex = symbolProperties.index;
            this.symbolImage.frameName = symbolProperties.key;
            this.game = game;
            this._borderKey = symbolProperties.borderKeys[0];
            this._currentSymbolProperties = symbolProperties;
        }

        public shadow():void {

        }

        public show(): void {
            this.visible = true;
        }

        public hide(): void {
            this.visible = false;
        }

        /************************************************************
            Getters and Setters
        *************************************************************/
        public get ownIndex(): number {
            return this._ownIndex;
        }

        public get currentSymbolProperties(): SymbolProperties {
            return this._currentSymbolProperties;
        }

        public get borderKey(): string {
            return this._borderKey;
        }

        public set ownIndex(value: number) {
            this._ownIndex = value;
        }

        public get Key(): string | Phaser.RenderTexture | Phaser.BitmapData | Phaser.Video | PIXI.Texture {
            return this.symbolImage.key;
        }

    }
}