///<reference path="../SimpleSprite.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseReelSymbol = /** @class */ (function (_super) {
            __extends(BaseReelSymbol, _super);
            function BaseReelSymbol(game, x, y, symbolProperties, debugMode) {
                var _this = _super.call(this, game) || this;
                _this.x = x;
                _this.y = y;
                _this.debugMode = debugMode;
                _this._currentSymbolProperties = symbolProperties;
                _this._borderKey = symbolProperties.borderKeys[0] ? symbolProperties.borderKeys[0] : "";
                _this.init();
                return _this;
            }
            BaseReelSymbol.prototype.init = function () {
                this.symbolImage = new common.SimpleSprite(this.game, 0, 0, 'symbols', this.debugMode, this.currentSymbolProperties.key);
                //this.symbolImage.anchor.setTo(0.5, 0.5);
                this.add(this.symbolImage);
            };
            /**
             *
             * @param data
             */
            BaseReelSymbol.prototype.start = function (data) {
                this.symbolImage.visible = false;
            };
            /**
             *
             * @param {any} data
             */
            BaseReelSymbol.prototype.stop = function (data) {
                this.symbolImage.visible = true;
            };
            /**
             *
             */
            BaseReelSymbol.prototype.activate = function () {
            };
            /**
             *
             */
            BaseReelSymbol.prototype.deactivate = function () {
            };
            /**
             *
             * @param game
             * @param symbolProperties
             */
            BaseReelSymbol.prototype.changeKey = function (game, symbolProperties) {
                this.ownIndex = symbolProperties.index;
                this.symbolImage.frameName = symbolProperties.key;
                this.game = game;
                this._borderKey = symbolProperties.borderKeys[0];
                this._currentSymbolProperties = symbolProperties;
            };
            BaseReelSymbol.prototype.shadow = function () {
            };
            Object.defineProperty(BaseReelSymbol.prototype, "ownIndex", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._ownIndex;
                },
                set: function (value) {
                    this._ownIndex = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelSymbol.prototype, "currentSymbolProperties", {
                get: function () {
                    return this._currentSymbolProperties;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelSymbol.prototype, "borderKey", {
                get: function () {
                    return this._borderKey;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelSymbol.prototype, "Key", {
                get: function () {
                    return this.symbolImage.key;
                },
                enumerable: true,
                configurable: true
            });
            return BaseReelSymbol;
        }(Phaser.Group));
        common.BaseReelSymbol = BaseReelSymbol;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
