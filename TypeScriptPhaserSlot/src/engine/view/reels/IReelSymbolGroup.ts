﻿module TypeScriptPhaserSlot.common {

    export interface IReelSymbolGroup {

        addSymbols(symbolIndexes: number[]): void;
        stopAllAnimation(): void;
        removeSymbolsFromIndex(index: number, count?:number, connect?: boolean): void;
        animateSymbolByIndex(index: number, data?: any): void;
        connectSymbols(): void;
        symbols: ISymbol[];
        getTopSymbol(): ISymbol;
        getBottomSymbol(): ISymbol;
        symbolsCount: number;
        x: number;
        y: number;
        removeAll(): void;
        height: number;
        destroy(destroyChildren?: boolean, soft?: boolean): void;
    }
}