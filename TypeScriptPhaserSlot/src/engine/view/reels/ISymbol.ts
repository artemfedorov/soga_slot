﻿module TypeScriptPhaserSlot.common {

    export interface ISymbol extends Phaser.Group {
        
        
        start(data?: any): void;
        stop(data?: any): void;
        currentSymbolProperties: SymbolProperties;
        borderKey: string;
        changeKey(game: Phaser.Game, symbolProperties: SymbolProperties): void;
        ownIndex: number;
        Key: string | Phaser.RenderTexture | Phaser.BitmapData | Phaser.Video | PIXI.Texture
        activate(): void;
        deactivate(): void;
        shadow(): void;
        destroy(destroyChildren?: boolean, soft?: boolean): void;
        hide():void;
        show():void;
        blur():void;
        unblur():void;
        x: number;
        y: number;
        visible: boolean;
        alpha: number;
        width: number;
        height: number;
        userdata: any;
    }
}

