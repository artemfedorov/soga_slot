﻿///<reference path="../BaseView.ts" />

module TypeScriptPhaserSlot.common {

    import Tween = Phaser.Tween;
    export abstract class BaseReelView extends BaseView implements IReelView {

        /**
         Optional layer for special mechanics.
         */
        protected optionalLayer: Phaser.Group;

        /**
         Layer for others
         */
        protected topLayer: Phaser.Group;

        /**
         Layer for symbols
         */
        protected bottomLayer: Phaser.Group;

        /**
         Reel's pattern/layout
         */
        protected _actionSignal: Phaser.Signal;

        /**
         Reel's pattern/layout
         */
        protected _respineSignal: Phaser.Signal;



        protected _justTouchedStopSignal: Phaser.Signal;


        protected _upturnSignal: Phaser.Signal;

        /*
         *Link to the general pattern
         * */
        protected pattern: any;

        /*
         *Speed list rolling allowed
         * */
        protected speeds: number[];

        /*
         *Static speed rolling
         * */
        protected speed: number;

        /*
         *Map of symbol key and indexes in the reels
         * */
        protected symbolProperties: SymbolProperties[];

        /*
         *Symbols init data
         * */
        protected initData: number[];

        /*
         *Configuration of exact reel
         * */
        protected _reelConfig: ReelConfiguration;

        /*
         *Game Configuration 
         * */
        protected gameConfig: GameConfiguration;

        /*
         *Configuration exact reel mechanics
         * */
        protected rollingMechanics: RollingMechanicsConfiguration;

        /*
         *Index of this reel
         * */
        protected _index: number;

        /**
         * Array of the reel's symbols
         */
        protected _symbols: Array<ISymbol>;

        /*
         *Store the server response data
         * */
        protected _resultData: number[];

        /*
         *Default function which is in the Group.update function.
         * */
        protected updater: Function;

        /*
         *Flag shows is stopping requires urgent/fast stop.
         * */
        protected isUrgentStop: boolean;

        /*
         *Reels in freespins mode
         * */
        protected mapReelFreespins: number[];

        /*
         *Shift in symbols into freespins reel
         * */
        protected mapReelFreespinsShift: number;

        /*
         *Reels in freespins mode
         * */
        protected mapReel: number[];

        /*
         *Shift in symbols into freespins reel
         * */
        protected mapReelShift: number;

        /*
         *Link to current map
         * */
        protected currentMap: number[];

        protected aloneStop: boolean;
        /*
         *Link to current shift
         * */
        protected _currentShift: number;

        /*
         *Top visibled Y - coordinate. e.g. Top coordinate Y of first visible symbol in the reel
         * */
        protected topVisibledLimit: number;

        /*
         *Bottom visibled Y - coordinate.e.g. bottom coordinate Y of last visible symbol in the reel
         * */
        protected bottomVisibleLimit: number;

        /*
         *Bottom limit Y. To get this limit means to move the symbols group on the top of previous symbols group. 
         * */
        protected bottomRemovedLimit: number;

        /*
         *Regular symbol height
         * */
        protected symbolHeight: number;

        /*
         Optional
         */
        protected borderLayer: Phaser.Group;

        protected borders: SimpleSprite[];

        protected _isIntrigued: boolean = false;

        protected symbolClassName: string;

        protected isReelStopped: boolean = false;

        protected isReelRolling: boolean = false;

        protected _symbolClassName: string;

        protected tweenedObject: any;

        constructor(game: Phaser.Game, index: number, name: string, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfig: GameConfiguration, pattern: any, data: number[], mapReel?: number[], mapReelFreespins?: number[], borderLayer?: Phaser.Group) {
            super(game, name, debugMode);
            this.initData = data;
            this.index = Number(index);
            this.gameConfig = gameConfig;
            this.symbolClassName = this.gameConfig.standartSymbolSetting.className;
            this._justTouchedStopSignal = new Phaser.Signal();
            this._upturnSignal = new Phaser.Signal();
            this._actionSignal = new Phaser.Signal();
            this._respineSignal = new Phaser.Signal();

            this.rollingMechanics = gameConfig.rollingMechanics;
            this.speeds = gameConfig.reels[index].speeds;
            this.changeSpeed(0);
            this.borderLayer = borderLayer;
            this.sortLayers();
            this.symbolProperties = symbolProperties;
            this._reelConfig = gameConfig.reels[index];
            this.pattern = pattern;
            this.createBorders();
            /*let mask = new Phaser.Graphics(this.game, 0, 0);
            mask.beginFill(0xffffff);
            mask.drawRect(0, 0, gameConfig.reels[index].width, gameConfig.reels[index].height);
            this.add(mask);
            this.mask = mask;*/

            this._currentShift = 0;
            this.tweenedObject = {};
        }

        protected init(): void {
            this._symbols = [];
            //this.currentShift = 0;

            for(let i: number = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                this.initData.unshift(this.getNextKey());
            }

            for(let j: number = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                this.initData.push(this.getNextKey());
            }

            this.addSymbols(this.initData);

            for(let k:number = 0; k < this._symbols.length; k++) {
                this._symbols[k].y -= this.symbolHeight * this._reelConfig.topInvisibleSymbolsCount;
            }
        }

        /**
         * Sort and initialize layers
         */
        protected sortLayers(): void {
            this.bottomLayer = new Phaser.Group(this.game);
            this.optionalLayer = new Phaser.Group(this.game);
            this.topLayer = new Phaser.Group(this.game);
            this.add(this.bottomLayer);
            this.add(this.optionalLayer);
            this.add(this.topLayer);
        }

        protected createBorders(): void {
            /*let border: SimpleSprite;
            this.borders = [];
            let borders: string[] = this.gameConfig.standartSymbolSetting.borders;
            for (let i: number = 0; i < borders.length; i++) {
                border = new SimpleSprite(this.game, 0, 0, borders[i]);
                this.addChild(border);
                border.visible = false;
                this.borderLayer.add(border);
                border.animations.add('play');
                this.borders.push(border);
            }*/
        }

        /**
         *
         * @param item
         */
        public poolIn(item: ISymbol): void {
            item.visible = false;
            Game.pool.push(item);
        }

        /**
         *
         * @param x
         * @param y
         * @param properties
         * @returns {ISymbol}
         */
        protected createSymbol(x: number, y: number, properties: SymbolProperties): ISymbol {
            let sym: ISymbol = this.poolOut(this.game, properties);
            if(this.isReelRolling) {
                sym.blur();
            } else {
                sym.unblur();
            }
            sym.x = x;
            sym.y = y;
            return sym;
        }

        /**
         * Creation of symbols definded with <symbolsPerReel> variable
         */
        public addSymbols(symbolIndexes: number[], onTop?: boolean): ISymbol[] {
            let concatSymbol: ISymbol;
            let addedSymbols:  ISymbol[] = [];
            let newSymbol: ISymbol;
            let sp: SymbolProperties;
            let symbolX: number = 0;
            let symbolY: number = 0;
            if (!onTop) {
                for (let i: number = 0; i < symbolIndexes.length; i++) {
                    concatSymbol = this._symbols[this._symbols.length - 1];
                    sp = this.getSymbolPropertiesByIndex(symbolIndexes[i]);

                    if (concatSymbol) {
                        symbolY = concatSymbol.y + this.symbolHeight;
                    }

                    newSymbol = this.createSymbol(symbolX, symbolY, sp);
                    newSymbol.ownIndex = symbolIndexes[i];
                    newSymbol.x = symbolX;
                    newSymbol.y = symbolY;
                    this.bottomLayer.add(newSymbol);
                    this._symbols.push(newSymbol);
                    addedSymbols.push(newSymbol);
                }
            } else {
                for (let i: number = symbolIndexes.length - 1; i >= 0; i--) {
                    concatSymbol = this._symbols[0];
                    sp = this.getSymbolPropertiesByIndex(symbolIndexes[i]);

                    if (concatSymbol) {
                        symbolY = concatSymbol.y - this.symbolHeight;
                    }

                    newSymbol = this.createSymbol(symbolX, symbolY, sp);
                    newSymbol.ownIndex = symbolIndexes[i];
                    newSymbol.x = symbolX;
                    newSymbol.y = symbolY;
                    this.bottomLayer.add(newSymbol);
                    this._symbols.unshift(newSymbol);
                    addedSymbols.unshift(newSymbol);
                }
            }
            return addedSymbols;
        }

        /**
         *
         * @param index
         * @returns {any}
         */
        protected getSymbolPropertiesByIndex(index: number): SymbolProperties {
            for (let i: number = 0; i < this.symbolProperties.length; i++) {
                if (this.symbolProperties[i].index == index)
                    return this.symbolProperties[i];
            }
            //TODO: вернуть как было
            //return null;
            return this.symbolProperties[0];
        }

        /**
         *
         * @param game
         * @param sp
         * @returns {ISymbol}
         */
        public poolOut(game: Phaser.Game, sp: SymbolProperties): ISymbol {
            let icon: ISymbol;

            let sss:any = Game.pool;
           // console.log(sss);

            if (Game.pool.length == 0) {
                if(!sp) {
                    //console.log();
                }

                let className: string = sp.className;

                if(!TypeScriptPhaserSlot.common || !TypeScriptPhaserSlot.common.hasOwnProperty(className)) {
                    //console.log();
                }

                icon = new TypeScriptPhaserSlot.common[className](game, 0, 0, sp);
                this.poolIn(icon);
            }
            icon = Game.pool.pop();
            icon.changeKey(game, sp);
            if(this.isReelRolling) {
                icon.blur();
            } else {
                icon.unblur();
            }
            icon.visible = true;
            return icon;
        }

        /**
         *
         * @param length
         * @param minNumber
         * @param maxNumber
         */
        protected generateSymbolsBand(length: number, minNumber: number, maxNumber: number): void {
            for (let i: number = 0; i < length; i++) {
                this.mapReel.push(this.getRandomKey(minNumber, maxNumber - minNumber));
            }
        }

        /**
         * Switched to freespins reel
         */
        public setFreespinsMap(): void {
            if (!this.mapReelFreespins) return;
            this.currentMap = this.mapReelFreespins;
            this.currentShift = this.mapReelFreespinsShift;
        }

        /**
         * Switched to regular reel
         */
        public setRegularMap(): void {
            if (!this.mapReel) return;
            this.currentMap = this.mapReel;
            this.currentShift = this.mapReelShift;
        }

        /**
         *
         * @param key
         * @param symbolIndex
         */
        protected animateBorderByKey(key: string, symbolIndex: number): void {
            for (let i: number = 0; i < this.borders.length; i++) {
                if (this.borders[i].key == key) {
                    this.borders[i].visible = true;
                    this.borders[i].animations.play('play', 24);
                    this.borders[i].events.onAnimationComplete.addOnce(this.onBorderAnimationComplete, this);
                    this.borders[i].x = this.pattern.reels[this.index].x;
                    this.borders[i].y = this.pattern.reels[this.index]["icon" + symbolIndex].y;
                }
            }
        }

        protected onBorderAnimationComplete(): void {
            for (let i: number = 0; i < this.borders.length; i++) {
                this.borders[i].visible = false;
            }
        }


        /**
         * function called by ReelsController to begin rolling
         * @param {any} data
         */
        public start(data: any): void {
            if(this.isReelRolling) {
                return;
            }

            this.topVisibledLimit = 0;
            this.isIntrigued = false;
            this.isUrgentStop = false;
            this.resultData = null;
            this.isReelStopped = false;
            this.isReelRolling = true;
        }

        /**
         * Call this before start() just to change speed rolling one-time
         * @param index
         */
        public changeSpeed(index: number): void {
            this.speed = this.speeds[index];
        }


        /**
         * First stage of rolling.
         */
        protected tweenToTop(): void {
            this.delta = 0;
            let d: number = Number(this.rollingMechanics.tweenToTop.pixels);
            if (d > 0) {
                this.tweenedObject.delta = 0;
                let t: number = Number(this.rollingMechanics.tweenToTop.time) * Phaser.Timer.SECOND;
                let tween = this.game.add.tween(this.tweenedObject).to({delta: -d}, t, this.rollingMechanics.tweenOf(this.rollingMechanics.tweenToTop.easingFunction));
                tween.onComplete.addOnce(this.tweenAccelaration, this);
                tween.onUpdateCallback(this.onTweenUpdate, this);
                tween.start();
            } else {
                this.tweenAccelaration();
            }
        }

        protected delta: number = 0;

        protected onTweenUpdate($first: any) {
            let delta = $first.target.delta - this.delta;
            this.delta = $first.target.delta;

            for (let i: number = 0; i < this._symbols.length; i++) {
                this._symbols[i].y += delta;
            }
        }

        /**
         * Second stage of rolling.
         */
        protected tweenAccelaration(): void {
            this.onTweenUpdate(arguments[1]);

            this.delta = 0;
            let d: number = Number(this.rollingMechanics.tweenAccelaration.pixels);
            let t: number = (d / ((this.speed / 4) * 60)) * Phaser.Timer.SECOND;
            this.tweenedObject.delta = 0;
            let tween = this.game.add.tween(this.tweenedObject).to({delta: d}, t, this.rollingMechanics.tweenOf(this.rollingMechanics.tweenAccelaration.easingFunction));
            tween.onComplete.addOnce(this.onCompleteTweenAccelaration, this);
            tween.onUpdateCallback(this.onTweenUpdate, this);
            tween.start();
        }

        protected onCompleteTweenAccelaration() {
            this.updater = this.staticRolling;
            //this.staticTween();

            this.symbols.forEach(function(symbol:ISymbol):void {
                symbol.blur();
            }, this);
        }

        /**
         * Third stage of rolling.
         */
        protected staticRolling(): void {
            for (let i: number = 0; i < this._symbols.length; i++) {
                this._symbols[i].y += this.speed;
            }

            let lastSymbol: ISymbol = this._symbols[this._symbols.length - 1];
            if (lastSymbol.y > this.bottomRemovedLimit) {
                this.poolIn(lastSymbol);
                this.symbols.pop();
                this.addNewSymbolsInStaticRolling();
            }
        }

        /**
         * This function adding next symbol on the top during static speed rolling
         * However it could be overriden to change behaviour e.g you have to check twiced or trippled
         * stacked icons.
         */
        protected addNewSymbolsInStaticRolling(): void {
            let nextKeys: number[] = this.getNextKeys(1);
            this.addSymbols(nextKeys, true);
        }


        /**
         * Fourth stage of rolling.
         */
        protected resultedTween(): void {
            this.delta = 0;
            let d: number = Math.abs(this.topVisibledLimit - this._symbols[this._reelConfig.topInvisibleSymbolsCount].y) - this._reelConfig.shift;
            let t: number = (d / (this.speed * 60)) * Phaser.Timer.SECOND;
            this.tweenedObject.delta = 0;
            let tween = this.game.add.tween(this.tweenedObject).to({delta: d}, t, Phaser.Easing.Linear.None);
            tween.onComplete.addOnce(this.tweenDecelaration, this);
            tween.onUpdateCallback(this.onTweenUpdate, this);
            tween.start();
        }

        /**
         * Fifth stage of rolling.
         */
        protected tweenDecelaration(): void {
            this.onTweenUpdate(arguments[1]);

            this.delta = 0;
            this.justTouchedStopSignal.dispatch(this.index);
            SoundController.instance.playSound('reel_' + this.index + '_stopped_sound','default', 2);
            let d: number = this._reelConfig.shift * 2;
            let t: number = (d / ((this.speed / 5) * 60)) * Phaser.Timer.SECOND;
            this.tweenedObject.delta = 0;
            let tween = this.game.add.tween(this.tweenedObject).to({delta: d}, t, Phaser.Easing.Quadratic.Out);
            tween.onComplete.addOnce(this.tweenUpturn, this);
            tween.onUpdateCallback(this.onTweenUpdate, this);
            tween.start();

            this.symbols.forEach(function(symbol:ISymbol):void {
                symbol.unblur();
            }, this);
        }

        /**
         * sixth stage of rolling.
         */
        protected tweenUpturn(): void {
            this.onTweenUpdate(arguments[1]);

            this.upturnSignal.dispatch(this.index);

            this.delta = 0;
            let d: number = this._reelConfig.shift;
            let t: number = this.rollingMechanics.tweenUpturn.time * Phaser.Timer.SECOND;
            this.tweenedObject.delta = 0;
            let tween = this.game.add.tween(this.tweenedObject).to({delta: -d}, t, this.rollingMechanics.tweenOf(this.rollingMechanics.tweenUpturn.easingFunction));
            tween.onUpdateCallback(this.onTweenUpdate, this);
            tween.onComplete.addOnce(this.onCompleteTweenUpturn, this);
            tween.start();
        }

        /**
         *
         */
        protected onCompleteTweenUpturn(): void {
            let maxLength: number = this._reelConfig.topInvisibleSymbolsCount +
                this._reelConfig.visibledSymbols +
                this._reelConfig.bottomInvisibleSymbolsCount;

            if (this._symbols.length >= maxLength) {
                for (let i: number = maxLength; i < this._symbols.length; i++) {
                    this.poolIn(this._symbols[i]);
                }
                this._symbols.splice(maxLength);
            }

            let startIndex:number = this.reelConfig.topInvisibleSymbolsCount + this.reelConfig.visibledSymbols + this.reelConfig.bottomInvisibleSymbolsCount - 1;
            for(let k:number = startIndex; k < this._symbols.length; k++) {
                this._symbols[k].destroy();
            }
            this._symbols.splice(startIndex);
            this.isReelRolling = false;

            this.actionSignal.dispatch(this.index);
            this.changeSpeed(0);
        }

        /**
         *
         * @param data
         * @param additionalSymbols
         */
        public stop(data: any, additionalSymbols?: number): void {
            let intriguedPause: number = this.isIntrigued ? 1000 * this.index : 0;
            setTimeout(() => {
                this.realStop(data, additionalSymbols)
            }, intriguedPause);
        }

        /**
         *
         * @param data
         * @param additionalSymbols
         */
        protected realStop(data: any, additionalSymbols?: number): void {
            this.updater = null;
            this.game.tweens.removeFrom(this.tweenedObject);
            this.resultData = data;

            this.currentShift = 0;

            for(let i: number = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                this.resultData.unshift(this.getNextKey());
            }

            for(let j: number = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                this.resultData.push(this.getNextKey());
            }

            for(let j: number = 0; j < additionalSymbols; j++) {
                this.resultData.push(this.getNextKey());
            }

            this.addSymbols(this.resultData, true);

            this.resultedTween();
        }

        /**
         *  It is actual only if mapping is there
         *  Getting the next indexes from the current reel (Optional)
         * @returns
         */
        protected getNextKeyFromBand(): number {
            let key: number = this.currentMap[this.currentShift++];
            return key;
        }

        /**
         * Optional.
         */
        public respin(data?: any): void {

        }

        public stopRespin(data?: any): void {

        }

        /**
         * Getting the next symbol index from the band
         * @returns
         */
        protected getNextKey(): number {
            return this.getNextKeyFromBand();
        }

        /**
         * Getting the next indexes from the current band
         * @param {number} keyAmount - how many indexes do you need to get
         * @returns
         */
        protected getNextKeys(keyAmount: number): number[] {
            let res: number[] = [];
            for (let i: number = 0; i < keyAmount; i++) {
                res.push(this.getNextKey());
            }
            return res;
        }

        /**
         *  Getting the next index randomly
         * @returns
         */
        protected getRandomKey(minId: number, maxId: number): number {
            let index: number = minId + Math.ceil(Math.random() * (maxId - minId));
            return this.symbolProperties[index].index;
        }

        /**
         *
         */
        public update() {
            if (this.updater) {
                this.updater();
            }
        }

        /**
         *
         */
        protected emptyFunction() {
        }

        public destroy(destroyChildren?: boolean, soft?: boolean): void {
            /*while(this.symbols.length > 0) {
                let symbol:ISymbol = this.symbols.shift();
                symbol.parent.removeChild(symbol);
                this.poolIn(symbol);
            }*/
            this.game.tweens.removeFrom(this.tweenedObject, true);

            super.destroy(destroyChildren, soft);

            this._actionSignal = null;

            this.removeAll(true, false, true);
        }


        /**
         * Checks if there any one symbol with Index in visible limit
         * @param index
         * @returns {boolean}
         */
        protected checkForSymbol(index: number): boolean {
            for (let i = this._reelConfig.topInvisibleSymbolsCount; i < this._reelConfig.visibledSymbols; i++) {
                if(this.symbols[i].ownIndex == index) return true;
            }
            return false;
        }
        /************************************************************
         Getters and Setters
         *************************************************************/

        public get currentShift(): number {
            return this._currentShift;
        }

        public set currentShift(value: number) {
            let newValue: number = value;
            if (this.currentMap.length > 0 && value >= this.currentMap.length) {
                newValue = value - this.currentMap.length;
            }

            this._currentShift = newValue;
        }

        public set index(value: number) {
            this._index = value;
        }

        public get index(): number {
            return this._index;
        }

        public set isIntrigued(value: boolean) {
            this._isIntrigued = value;
        }

        public get isIntrigued(): boolean {
            return this._isIntrigued;
        }


        public get symbols(): Array<ISymbol> {
            return this._symbols;
        }


        public get resultData(): number[] {
            return this._resultData;
        }

        public set resultData(value: number[]) {
            this._resultData = value;
        }

        public get actionSignal(): Phaser.Signal {
            return this._actionSignal;
        }

        public set actionSignal(value: Phaser.Signal) {
            this._actionSignal = value;
        }

        public get respineSignal(): Phaser.Signal {
            return this._respineSignal;
        }

        public set respineSignal(value: Phaser.Signal) {
            this._respineSignal = value;
        }

        public get justTouchedStopSignal(): Phaser.Signal {
            return this._justTouchedStopSignal;
        }

        public get upturnSignal(): Phaser.Signal {
            return this._upturnSignal;
        }

        public get reelConfig(): ReelConfiguration {
            return this._reelConfig;
        }
    }
}