﻿module TypeScriptPhaserSlot.common {

    export interface IReelView extends BaseView {
        start(data?: any): void;
        addSymbols(symbolIndexes: number[], onTop?: boolean): ISymbol[];
        stop(data: any, additionalSymbols?: number): void;
        respin(data?: any): void;
        stopRespin(data?: any): void;
        resultData: number[];
        x: number;
        y: number;
        symbols: ISymbol[];
        alpha: number;
        width: number;
        height: number;
        index: number;
        isIntrigued: boolean;
        justTouchedStopSignal: Phaser.Signal;
        respineSignal: Phaser.Signal;
        actionSignal: Phaser.Signal;
        reelConfig: ReelConfiguration;
        changeSpeed(value: number): void;

        destroy(destroyChildren?: boolean, soft?: boolean): void;
    }
}  

