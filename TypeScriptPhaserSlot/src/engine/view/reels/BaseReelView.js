///<reference path="../BaseView.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseReelView = /** @class */ (function (_super) {
            __extends(BaseReelView, _super);
            function BaseReelView(game, index, name, debugMode, symbolProperties, gameConfig, pattern, data, mapReel, mapReelFreespins, borderLayer) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this._isIntrigued = false;
                _this.isReelStopped = false;
                _this.delta = 0;
                _this.initData = data;
                _this.index = Number(index);
                _this.gameConfig = gameConfig;
                _this.symbolClassName = _this.gameConfig.standartSymbolSetting.className;
                _this._justTouchedStopSignal = new Phaser.Signal();
                _this._upturnSignal = new Phaser.Signal();
                _this._actionSignal = new Phaser.Signal();
                _this.rollingMechanics = gameConfig.rollingMechanics;
                _this.speeds = gameConfig.reels[index].speeds;
                _this.changeSpeed(0);
                _this.borderLayer = borderLayer;
                _this.sortLayers();
                _this.symbolProperties = symbolProperties;
                _this._reelConfig = gameConfig.reels[index];
                _this.pattern = pattern;
                _this.createBorders();
                /*let mask = new Phaser.Graphics(this.game, 0, 0);
                mask.beginFill(0xffffff);
                mask.drawRect(0, 0, gameConfig.reels[index].width, gameConfig.reels[index].height);
                this.add(mask);
                this.mask = mask;*/
                _this._currentShift = 0;
                _this.tweenedObject = {};
                return _this;
            }
            BaseReelView.prototype.init = function () {
                this._symbols = [];
                this.addSymbols(this.initData);
                var addSymbols = [];
                for (var i = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                    addSymbols.push(this.getNextKey());
                }
                this.addSymbols(addSymbols, true);
                addSymbols = [];
                for (var j = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                    addSymbols.push(this.getNextKey());
                }
                this.addSymbols(addSymbols, false);
                /*for(let i: number = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                    this.initData.unshift(this.getNextKey());
                }
    
                for(let j:number = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                    this.initData.push(this.getNextKey());
                }*/
            };
            /**
             * Sort and initialize layers
             */
            BaseReelView.prototype.sortLayers = function () {
                this.bottomLayer = new Phaser.Group(this.game);
                this.optionalLayer = new Phaser.Group(this.game);
                this.topLayer = new Phaser.Group(this.game);
                this.add(this.bottomLayer);
                this.add(this.optionalLayer);
                this.add(this.topLayer);
            };
            BaseReelView.prototype.createBorders = function () {
                /*let border: SimpleSprite;
                this.borders = [];
                let borders: string[] = this.gameConfig.standartSymbolSetting.borders;
                for (let i: number = 0; i < borders.length; i++) {
                    border = new SimpleSprite(this.game, 0, 0, borders[i]);
                    this.addChild(border);
                    border.visible = false;
                    this.borderLayer.add(border);
                    border.animations.add('play');
                    this.borders.push(border);
                }*/
            };
            /**
             *
             * @param item
             */
            BaseReelView.prototype.poolIn = function (item) {
                item.visible = false;
                Game.pool.push(item);
            };
            /**
             *
             * @param x
             * @param y
             * @param properties
             * @returns {ISymbol}
             */
            BaseReelView.prototype.createSymbol = function (x, y, properties) {
                var sym = this.poolOut(this.game, properties);
                sym.x = x;
                sym.y = y;
                return sym;
            };
            /**
             * Creation of symbols definded with <symbolsPerReel> variable
             */
            BaseReelView.prototype.addSymbols = function (symbolIndexes, onTop) {
                var concatSymbol;
                var addedSymbols = [];
                var newSymbol;
                var sp;
                var symbolX = 0;
                var symbolY = 0;
                if (!onTop) {
                    for (var i = 0; i < symbolIndexes.length; i++) {
                        concatSymbol = this._symbols[this._symbols.length - 1];
                        sp = this.getSymbolPropertiesByIndex(symbolIndexes[i]);
                        if (concatSymbol) {
                            symbolY = concatSymbol.y + this.symbolHeight;
                        }
                        newSymbol = this.createSymbol(symbolX, symbolY, sp);
                        newSymbol.ownIndex = symbolIndexes[i];
                        newSymbol.x = symbolX;
                        newSymbol.y = symbolY;
                        this.bottomLayer.add(newSymbol);
                        this._symbols.push(newSymbol);
                        addedSymbols.push(newSymbol);
                    }
                }
                else {
                    for (var i = symbolIndexes.length - 1; i >= 0; i--) {
                        concatSymbol = this._symbols[0];
                        sp = this.getSymbolPropertiesByIndex(symbolIndexes[i]);
                        if (concatSymbol) {
                            symbolY = concatSymbol.y - this.symbolHeight;
                        }
                        newSymbol = this.createSymbol(symbolX, symbolY, sp);
                        newSymbol.ownIndex = symbolIndexes[i];
                        newSymbol.x = symbolX;
                        newSymbol.y = symbolY;
                        this.bottomLayer.add(newSymbol);
                        this._symbols.unshift(newSymbol);
                        addedSymbols.unshift(newSymbol);
                    }
                }
                return addedSymbols;
            };
            /**
             *
             * @param index
             * @returns {any}
             */
            BaseReelView.prototype.getSymbolPropertiesByIndex = function (index) {
                for (var i = 0; i < this.symbolProperties.length; i++) {
                    if (this.symbolProperties[i].index == index)
                        return this.symbolProperties[i];
                }
                //TODO: вернуть как было
                //return null;
                return this.symbolProperties[0];
            };
            /**
             *
             * @param game
             * @param sp
             * @returns {ISymbol}
             */
            BaseReelView.prototype.poolOut = function (game, sp) {
                var icon;
                var sss = Game.pool;
                // console.log(sss);
                if (Game.pool.length == 0) {
                    if (!sp) {
                        //console.log();
                    }
                    var className = sp.className;
                    if (!TypeScriptPhaserSlot.common || !TypeScriptPhaserSlot.common.hasOwnProperty(className)) {
                        //console.log();
                    }
                    icon = new TypeScriptPhaserSlot.common[className](game, 0, 0, sp);
                    this.poolIn(icon);
                }
                icon = Game.pool.pop();
                icon.changeKey(game, sp);
                icon.visible = true;
                return icon;
            };
            /**
             *
             * @param length
             * @param minNumber
             * @param maxNumber
             */
            BaseReelView.prototype.generateSymbolsBand = function (length, minNumber, maxNumber) {
                for (var i = 0; i < length; i++) {
                    this.mapReel.push(this.getRandomKey(minNumber, maxNumber - minNumber));
                }
            };
            /**
             * Switched to freespins reel
             */
            BaseReelView.prototype.setFreespinsMap = function () {
                if (!this.mapReelFreespins)
                    return;
                this.currentMap = this.mapReelFreespins;
                this.currentShift = this.mapReelFreespinsShift;
            };
            /**
             * Switched to regular reel
             */
            BaseReelView.prototype.setRegularMap = function () {
                if (!this.mapReel)
                    return;
                this.currentMap = this.mapReel;
                this.currentShift = this.mapReelShift;
            };
            /**
             *
             * @param key
             * @param symbolIndex
             */
            BaseReelView.prototype.animateBorderByKey = function (key, symbolIndex) {
                for (var i = 0; i < this.borders.length; i++) {
                    if (this.borders[i].key == key) {
                        this.borders[i].visible = true;
                        this.borders[i].animations.play('play', 24);
                        this.borders[i].events.onAnimationComplete.addOnce(this.onBorderAnimationComplete, this);
                        this.borders[i].x = this.pattern.reels[this.index].x;
                        this.borders[i].y = this.pattern.reels[this.index]["icon" + symbolIndex].y;
                    }
                }
            };
            BaseReelView.prototype.onBorderAnimationComplete = function () {
                for (var i = 0; i < this.borders.length; i++) {
                    this.borders[i].visible = false;
                }
            };
            /**
             * function called by ReelsController to begin rolling
             * @param {any} data
             */
            BaseReelView.prototype.start = function (data) {
                this.topVisibledLimit = 0;
                this.isIntrigued = false;
                this.isUrgentStop = false;
                this.resultData = null;
                this.isReelStopped = false;
            };
            /**
             * Call this before start() just to change speed rolling one-time
             * @param index
             */
            BaseReelView.prototype.changeSpeed = function (index) {
                this.speed = this.speeds[index];
            };
            /**
             * First stage of rolling.
             */
            BaseReelView.prototype.tweenToTop = function () {
                this.delta = 0;
                var d = Number(this.rollingMechanics.tweenToTop.pixels);
                if (d > 0) {
                    this.tweenedObject.delta = 0;
                    var t = Number(this.rollingMechanics.tweenToTop.time) * Phaser.Timer.SECOND;
                    var tween = this.game.add.tween(this.tweenedObject).to({ delta: -d }, t, this.rollingMechanics.tweenOf(this.rollingMechanics.tweenToTop.easingFunction));
                    tween.onComplete.addOnce(this.tweenAccelaration, this);
                    tween.onUpdateCallback(this.onTweenUpdate, this);
                    tween.start();
                }
                else {
                    this.tweenAccelaration();
                }
            };
            BaseReelView.prototype.onTweenUpdate = function ($first) {
                var delta = $first.target.delta - this.delta;
                this.delta = $first.target.delta;
                for (var i = 0; i < this._symbols.length; i++) {
                    this._symbols[i].y += delta;
                }
            };
            /**
             * Second stage of rolling.
             */
            BaseReelView.prototype.tweenAccelaration = function () {
                this.onTweenUpdate(arguments[1]);
                this.delta = 0;
                var d = Number(this.rollingMechanics.tweenAccelaration.pixels);
                var t = (d / ((this.speed / 4) * 60)) * Phaser.Timer.SECOND;
                this.tweenedObject.delta = 0;
                var tween = this.game.add.tween(this.tweenedObject).to({ delta: d }, t, this.rollingMechanics.tweenOf(this.rollingMechanics.tweenAccelaration.easingFunction));
                tween.onComplete.addOnce(this.onCompleteTweenAccelaration, this);
                tween.onUpdateCallback(this.onTweenUpdate, this);
                tween.start();
            };
            BaseReelView.prototype.onCompleteTweenAccelaration = function () {
                this.updater = this.staticRolling;
                //this.staticTween();
            };
            /**
             * Third stage of rolling.
             */
            BaseReelView.prototype.staticRolling = function () {
                for (var i = 0; i < this._symbols.length; i++) {
                    this._symbols[i].y += this.speed;
                }
                var lastSymbol = this._symbols[this._symbols.length - 1];
                if (lastSymbol.y > this.bottomRemovedLimit) {
                    this.poolIn(lastSymbol);
                    this.symbols.pop();
                    this.addNewSymbolsInStaticRolling();
                }
            };
            /**
             * This function adding next symbol on the top during static speed rolling
             * However it could be overriden to change behaviour e.g you have to check twiced or trippled
             * stacked icons.
             */
            BaseReelView.prototype.addNewSymbolsInStaticRolling = function () {
                var nextKeys = this.getNextKeys(1);
                this.addSymbols(nextKeys, true);
            };
            /**
             * Fourth stage of rolling.
             */
            BaseReelView.prototype.resultedTween = function () {
                this.delta = 0;
                var d = Math.abs(this.topVisibledLimit - this._symbols[this._reelConfig.topInvisibleSymbolsCount].y) - this._reelConfig.shift;
                var t = (d / (this.speed * 60)) * Phaser.Timer.SECOND;
                this.tweenedObject.delta = 0;
                var tween = this.game.add.tween(this.tweenedObject).to({ delta: d }, t, Phaser.Easing.Linear.None);
                tween.onComplete.addOnce(this.tweenDecelaration, this);
                tween.onUpdateCallback(this.onTweenUpdate, this);
                tween.start();
            };
            /**
             * Fifth stage of rolling.
             */
            BaseReelView.prototype.tweenDecelaration = function () {
                this.onTweenUpdate(arguments[1]);
                this.delta = 0;
                this.justTouchedStopSignal.dispatch(this.index);
                SoundController.instance.playSound('reel_' + this.index + '_stopped_sound', 'default', 2);
                var d = this._reelConfig.shift * 2;
                var t = (d / ((this.speed / 5) * 60)) * Phaser.Timer.SECOND;
                this.tweenedObject.delta = 0;
                var tween = this.game.add.tween(this.tweenedObject).to({ delta: d }, t, Phaser.Easing.Quadratic.Out);
                tween.onComplete.addOnce(this.tweenUpturn, this);
                tween.onUpdateCallback(this.onTweenUpdate, this);
                tween.start();
            };
            /**
             * sixth stage of rolling.
             */
            BaseReelView.prototype.tweenUpturn = function () {
                this.onTweenUpdate(arguments[1]);
                this.upturnSignal.dispatch(this.index);
                this.delta = 0;
                var d = this._reelConfig.shift;
                var t = this.rollingMechanics.tweenUpturn.time * Phaser.Timer.SECOND;
                this.tweenedObject.delta = 0;
                var tween = this.game.add.tween(this.tweenedObject).to({ delta: -d }, t, this.rollingMechanics.tweenOf(this.rollingMechanics.tweenUpturn.easingFunction));
                tween.onUpdateCallback(this.onTweenUpdate, this);
                tween.onComplete.addOnce(this.onCompleteTweenUpturn, this);
                tween.start();
            };
            /**
             *
             */
            BaseReelView.prototype.onCompleteTweenUpturn = function () {
                var maxLength = this._reelConfig.topInvisibleSymbolsCount +
                    this._reelConfig.visibledSymbols +
                    this._reelConfig.bottomInvisibleSymbolsCount;
                if (this._symbols.length >= maxLength) {
                    for (var i = maxLength; i < this._symbols.length; i++) {
                        this.poolIn(this._symbols[i]);
                    }
                    this._symbols.splice(maxLength);
                }
                this.actionSignal.dispatch(this.index);
                this.changeSpeed(0);
                var startIndex = this.reelConfig.topInvisibleSymbolsCount + this.reelConfig.visibledSymbols + this.reelConfig.bottomInvisibleSymbolsCount - 1;
                for (var k = startIndex; k < this._symbols.length; k++) {
                    this._symbols[k].destroy();
                }
                this._symbols.splice(startIndex);
            };
            /**
             *
             * @param data
             * @param additionalSymbols
             */
            BaseReelView.prototype.stop = function (data, additionalSymbols) {
                var _this = this;
                var intriguedPause = this.isIntrigued ? 1000 * this.index : 0;
                setTimeout(function () {
                    _this.realStop(data, additionalSymbols);
                }, intriguedPause);
            };
            /**
             *
             * @param data
             * @param additionalSymbols
             */
            BaseReelView.prototype.realStop = function (data, additionalSymbols) {
                this.updater = null;
                this.game.tweens.removeFrom(this.tweenedObject);
                this.resultData = data;
                for (var i = 0; i < additionalSymbols; i++) {
                    this.resultData.push(this.getNextKey());
                }
                for (var i = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                    this.resultData.unshift(this.getNextKey());
                }
                this.addSymbols(this.resultData, true);
                this.resultedTween();
            };
            /**
             *  It is actual only if mapping is there
             *  Getting the next indexes from the current reel (Optional)
             * @returns
             */
            BaseReelView.prototype.getNextKeyFromBand = function () {
                var key = this.currentMap[this.currentShift++];
                return key;
            };
            /**
             * Optional.
             */
            BaseReelView.prototype.respin = function (data) {
            };
            /**
             * Getting the next symbol index from the band
             * @returns
             */
            BaseReelView.prototype.getNextKey = function () {
                return this.getNextKeyFromBand();
            };
            /**
             * Getting the next indexes from the current band
             * @param {number} keyAmount - how many indexes do you need to get
             * @returns
             */
            BaseReelView.prototype.getNextKeys = function (keyAmount) {
                var res = [];
                for (var i = 0; i < keyAmount; i++) {
                    res.push(this.getNextKey());
                }
                return res;
            };
            /**
             *  Getting the next index randomly
             * @returns
             */
            BaseReelView.prototype.getRandomKey = function (minId, maxId) {
                var index = minId + Math.ceil(Math.random() * (maxId - minId));
                return this.symbolProperties[index].index;
            };
            /**
             *
             */
            BaseReelView.prototype.update = function () {
                if (this.updater) {
                    this.updater();
                }
            };
            /**
             *
             */
            BaseReelView.prototype.emptyFunction = function () {
            };
            BaseReelView.prototype.destroy = function (destroyChildren, soft) {
                /*while(this.symbols.length > 0) {
                    let symbol:ISymbol = this.symbols.shift();
                    symbol.parent.removeChild(symbol);
                    this.poolIn(symbol);
                }*/
                _super.prototype.destroy.call(this, destroyChildren, soft);
                this._actionSignal = null;
                this.removeAll(true, false, true);
            };
            Object.defineProperty(BaseReelView.prototype, "currentShift", {
                /************************************************************
                 Getters and Setters
                 *************************************************************/
                get: function () {
                    return this._currentShift;
                },
                set: function (value) {
                    var newValue = value;
                    if (this.currentMap.length > 0 && value >= this.currentMap.length) {
                        newValue = value - this.currentMap.length;
                    }
                    this._currentShift = newValue;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "index", {
                get: function () {
                    return this._index;
                },
                set: function (value) {
                    this._index = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "isIntrigued", {
                get: function () {
                    return this._isIntrigued;
                },
                set: function (value) {
                    this._isIntrigued = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "symbols", {
                get: function () {
                    return this._symbols;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "resultData", {
                get: function () {
                    return this._resultData;
                },
                set: function (value) {
                    this._resultData = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "actionSignal", {
                get: function () {
                    return this._actionSignal;
                },
                set: function (value) {
                    this._actionSignal = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "justTouchedStopSignal", {
                get: function () {
                    return this._justTouchedStopSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "upturnSignal", {
                get: function () {
                    return this._upturnSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "reelConfig", {
                get: function () {
                    return this._reelConfig;
                },
                enumerable: true,
                configurable: true
            });
            return BaseReelView;
        }(common.BaseView));
        common.BaseReelView = BaseReelView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
