﻿module TypeScriptPhaserSlot.common {

    export class ReelSymbolGroup extends BaseView implements IReelSymbolGroup {


        protected _symbols: ISymbol[];
        protected symbolProperties: SymbolProperties[];
        protected symbolsGap: number;
        protected _symbolClassName: string;

        public index: number;

        constructor(game: Phaser.Game, index: number, symbolProperties: SymbolProperties[], symbolsGap: number, symbolClassName:string, debugMode?: boolean) {
            super(game, "ReelSymbolGroup" + String(index), debugMode);
            this.index = index;
            this._symbolClassName = symbolClassName;
            this.symbolProperties = symbolProperties;
            this.symbolsGap = symbolsGap;
        }

        /**
        * Creation of symbols definded with <symbolsPerReel> variable
        */
        public addSymbols(symbolIndexes: number[]): void {
            this._symbols = [];
            let heightPrev: number = 0;
            let newSymbol: ISymbol;
            let sp: SymbolProperties;
            let symbolX: number = 0;
            let symbolY: number = 0;

            for (let i: number = 0; i < symbolIndexes.length; i++) {
                sp = this.getSymbolPropertiesByIndex(symbolIndexes[i]);
                symbolY = (this.symbolsGap + heightPrev);
                heightPrev += sp.height;
                newSymbol = this.createSymbol(symbolX, symbolY, sp);
                newSymbol.ownIndex = symbolIndexes[i];
                newSymbol.x = symbolX;
                newSymbol.y = symbolY;
                this.add(newSymbol);
                this.symbols.push(newSymbol);
            }
        }

        /**
         *
         * @param item
         */
        public poolIn(item: ISymbol): void {
            item.visible = false;
            Game.pool.push(item);
        }

        /**
         *
         * @param game
         * @param sp
         * @returns {ISymbol}
         */
        public poolOut(game: Phaser.Game, sp: SymbolProperties): ISymbol {
            let icon: ISymbol;
            if (Game.pool.length == 0) {
                let className: string = sp.className || this.SymbolClassName;
                icon = new TypeScriptPhaserSlot.common[className](game, 0, 0, sp);
                this.poolIn(icon);
            }
            icon = Game.pool.pop();
            icon.changeKey(game, sp);
            icon.visible = true;
            return icon;
        }


        /**
         *
         * @param x
         * @param y
         * @param properties
         * @returns {ISymbol}
         */
        protected createSymbol(x: number, y: number, properties: SymbolProperties): ISymbol {
            let sym: ISymbol = this.poolOut(this.game, properties);
            sym.x = x;
            sym.y = y;
            return sym;
        }

        /**
         *
         */
        public stopAllAnimation(): void {
            for (let i: number = 0; i < this._symbols.length; i++) {
                this._symbols[i].stop();
            }
        }

        /**
         *
         * @param index
         * @param count
         * @param connectSymbols
         */
        public removeSymbolsFromIndex(index: number, count?: number, connectSymbols?: boolean): void {
            let finalIndex:number = count ? index + count : this._symbols.length;
            for (let i: number = index; i < finalIndex; i++) {
                this.poolIn(this._symbols[i]);
            }
            count = count ? count : this._symbols.length;
            this._symbols.splice(index, count);

            if(connectSymbols) {
                this.connectSymbols();
            }
        }

        /**
         *
         */
        public connectSymbols(): void {
            let symbol: ISymbol;
            for(let i: number = 0; i < this._symbols.length; i++) {
                symbol = this._symbols[i];
                symbol.y = symbol.height * i;
            }
        }

        /**
         *
         * @param index
         */
        public animateSymbolByIndex(index: number, data?: any): void {
            this._symbols[index].start(data);
        }

        /**
         *
         * @param index
         * @returns {any}
         */
        protected getSymbolPropertiesByIndex(index: number): SymbolProperties {
            for (let i: number = 0; i < this.symbolProperties.length; i++) {
                if (this.symbolProperties[i].index == index) return this.symbolProperties[i];
            }
            return null;
        }

        /**
         *
         * @param destroyChildren
         * @param soft
         */
        public destroy(destroyChildren?: boolean, soft?: boolean): void {
            for (let i: number = 0; i < this.symbols.length; i++) {
                this.poolIn(this.symbols[i]);
            }
            this._symbols = null;
            super.destroy(destroyChildren, soft);
        }


        public get SymbolClassName(): string {
            return this._symbolClassName;
        }
        public get symbols(): ISymbol[] {
            return this._symbols;
        }

        public getTopSymbol(): ISymbol {
            return this.symbols[0];
        }

        public getBottomSymbol(): ISymbol {
            return this.symbols[this.symbols.length - 1];
        }
        
        public get symbolsCount(): number {
            return this.symbols.length;
        }

    }

}