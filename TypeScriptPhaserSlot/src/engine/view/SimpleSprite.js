var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var SimpleSprite = /** @class */ (function (_super) {
            __extends(SimpleSprite, _super);
            function SimpleSprite(game, x, y, key, debugMode, frame) {
                var _this = _super.call(this, game, x, y, key, frame) || this;
                if (typeof frame == 'number') {
                    _this.frame = frame;
                }
                else if (typeof frame == 'string') {
                    _this.frameName = frame;
                }
                _this._name = name;
                _this.debugMode = debugMode;
                _this.init();
                return _this;
            }
            SimpleSprite.prototype.init = function () {
            };
            SimpleSprite.prototype.start = function (data) {
                //start do any work 
            };
            SimpleSprite.prototype.stop = function (data) {
                //stop doing current work
            };
            SimpleSprite.prototype.changeKey = function (key) {
                this.key = key;
                this.loadTexture(key, 0);
            };
            Object.defineProperty(SimpleSprite.prototype, "debugMode", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._debugMode;
                },
                set: function (value) {
                    this._debugMode = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SimpleSprite.prototype, "name", {
                get: function () {
                    return this._name;
                },
                set: function (value) {
                    this._name = value;
                },
                enumerable: true,
                configurable: true
            });
            return SimpleSprite;
        }(Phaser.Sprite));
        common.SimpleSprite = SimpleSprite;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
