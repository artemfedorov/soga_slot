/**
 * Created by rocket on 21.07.2017.
 */
module TypeScriptPhaserSlot.common {

    export class PreloaderView extends BaseView implements IPreloaderView {

        private pattern: any;

        constructor(game: Game, name: string, pattern: any, debugMode?: boolean) {
            super(game, name, debugMode);
            this.game = game;
            this.pattern = pattern;

            this.init();
        }

        private init(): void {
            let preloader:Phaser.Group = new Phaser.Group(this.game);
            for(let i:number = 0; i < 6; i++) {
                let circle:Phaser.Graphics = new Phaser.Graphics(this.game);
                circle.beginFill(0xffffff, 1);
                circle.drawCircle(i * 40, 0, 25);
                circle.endFill();
                preloader.add(circle);
            }

            /*let preloaderBackground:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
            preloaderBackground.beginFill(0xCCCCCC, 1);
            preloaderBackground.drawRect(0, 0, 1000, 500);
            preloaderBackground.endFill();
            preloaderBackground.lineStyle(3, 0x0, 1);
            preloaderBackground.moveTo(0,0);
            preloaderBackground.lineTo(1000,0);
            preloaderBackground.lineTo(1000,500);
            preloaderBackground.lineTo(0,500);
            preloaderBackground.lineTo(0,0);
            preloader.add(preloaderBackground);*/

            preloader.y = (768 - preloader.height) / 2;

            this.add(preloader);
        }

        public onLoadingUpdate(progress:number, cacheKey:string, success:boolean, totalLoaded:number, totalFiles:number):void {
            let oneFilePercent:number = 100 / totalFiles;
            let onePercent:number = oneFilePercent / 100;
            let loadedPercents:number = (oneFilePercent * totalLoaded) + (onePercent * progress);
            console.log('===>', loadedPercents);
        }

        public onLoadingComplete():void {

        }
    }
}