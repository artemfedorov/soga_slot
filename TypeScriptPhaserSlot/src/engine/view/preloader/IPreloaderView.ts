/**
 * Created by rocket on 21.07.2017.
 */
module TypeScriptPhaserSlot.common {

    export interface IPreloaderView extends BaseView {

        onLoadingUpdate(progress:number, cacheKey:string, success:boolean, totalLoaded:number, totalFiles:number): void;
        onLoadingComplete(data?:any): void;
    }
}
