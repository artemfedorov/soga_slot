var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseView = /** @class */ (function (_super) {
            __extends(BaseView, _super);
            function BaseView(game, name, debugMode) {
                var _this = _super.call(this, game, null) || this;
                _this._name = name;
                _this.debugMode = debugMode;
                return _this;
            }
            BaseView.prototype.setPosition = function (x, y) {
                this.x = x;
                this.y = y;
            };
            /**
             * Specific console logger
             * @param message
             */
            BaseView.prototype.debuglog = function (message) {
                console.log(this.name, ":", message);
            };
            Object.defineProperty(BaseView.prototype, "debugMode", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._debugMode;
                },
                set: function (value) {
                    this._debugMode = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseView.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            return BaseView;
        }(Phaser.Group));
        common.BaseView = BaseView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
