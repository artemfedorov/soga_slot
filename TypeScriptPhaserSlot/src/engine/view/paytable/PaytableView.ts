///<reference path="../BaseView.ts" />

module TypeScriptPhaserSlot.common {

    export class PaytableView extends BaseView {

        protected pattern: any;
        protected pages: Phaser.Group[];
        protected currentPage:Phaser.Group;
        protected values:object;
        protected logo:SimpleSprite;
        public game:Game;

        constructor(game: Game, name: string, pattern: any, debugMode?: boolean) {
            super(game, name, debugMode);
            this.game = game;
            this.pattern = pattern.paytable;
            this.init();
        }

        /**
         *
         */
        protected init(): void {
            this.values = {
                "0":"40\n10\n5",
                "1":"50\n10\n5",
                "2":"75\n15\n5",
                "3":"100\n25\n10",
                "4":"125\n30\n15",
                "5":"200\n50\n15",
                "6":"250\n50\n25",
                "7":"300\n100\n50",
                "8":"300\n100\n50",
                "9":"300\n100\n50"
            };

            this.createBackground();
            this.createLogo();
            this.construct(this.pattern, this);
            this.createPages();
            this.createButtons();
            this.interactive = false;
        }

        protected createBackground():void {
            let background:SimpleSprite = new SimpleSprite(this.game, 0, 0, 'background_paytable', this.debugMode);
            this.add(background);
        }

        protected createLogo():void {
            let locale:string = this.game.textFactory.currLocale;
            let gameLogoPatternData:any = this.getPatternData(this.pattern, 'game_logo');
            if(!gameLogoPatternData) {
                gameLogoPatternData = { x: 0, y: 0};
            }
            this.logo = new SimpleSprite(this.game, gameLogoPatternData.x, gameLogoPatternData.y, 'game_logo', this.debugMode, 'logo_' + locale);
            this.logo.anchor.set(0.5, 0.5);
            if(this.logo.key != '__missing') {
                this.add(this.logo);
            }

            this.game.textFactory.actionSignal.add(this.onLocaleChanged, this);
        }

        protected onLocaleChanged(data:any):void {
            this.logo.frameName = 'logo_' + data.data;
        }

        protected createButtons(): void {
            let buttonActions:any = {
                mouseUp: {
                    action: '',
                    params: null,
                    sound: null,
                    enabledAfter: undefined
                }
            };
            let nextButtonConfigurationParams:any = {
                name: 'paytableNext_btn',
                className:  'BaseButton',
                key: 'paytable',
                caption: '',
                textStyle: 'style1',
                overFrame: 'infoNext_btn/over',
                outFrame: 'infoNext_btn/enabled',
                downFrame: 'infoNext_btn/down',
                upFrame: 'infoNext_btn/enabled',
                disabledFrame: 'infoNext_btn/disabled',
                onDownSoundKey: 'clicksound',
                onOverSoundKey: '',
                onOutSoundKey: '',
                onUpSoundKey: ''
            };
            let nextButtonConfiguration = new ButtonConfiguration(nextButtonConfigurationParams);
            let nextButtonPatternData:any = this.getPatternData(this.pattern, 'paytableNext_btn');
            let nextButton:BaseButton = new BaseButton(this.game, nextButtonConfiguration, nextButtonPatternData.x, nextButtonPatternData.y, this.onNextButtonClick, this);
            nextButton.actions = buttonActions;
            nextButton.enabled = true;
            this.add(nextButton);


            let prevButtonConfigurationParams:any = {
                name: 'paytablePrev_btn',
                className:  'BaseButton',
                key: 'paytable',
                caption: '',
                textStyle: 'style1',
                overFrame: 'infoPrew_btn/over',
                outFrame: 'infoPrew_btn/enabled',
                downFrame: 'infoPrew_btn/down',
                upFrame: 'infoPrew_btn/enabled',
                disabledFrame: 'infoPrew_btn/disabled',
                onDownSoundKey: 'clicksound',
                onOverSoundKey: '',
                onOutSoundKey: '',
                onUpSoundKey: ''
            };
            let prevButtonConfiguration = new ButtonConfiguration(prevButtonConfigurationParams);
            let prevButtonPatternData:any = this.getPatternData(this.pattern, 'paytablePrev_btn');
            let prevButton:BaseButton = new BaseButton(this.game, prevButtonConfiguration, prevButtonPatternData.x, prevButtonPatternData.y, this.onPrevButtonClick, this);
            prevButton.actions = buttonActions;
            prevButton.enabled = true;
            this.add(prevButton);
        }

        protected onNextButtonClick():void {
            this.goToPage('increase');
        }

        protected onPrevButtonClick():void {
            this.goToPage('decrease');
        }

        /**
         *
         */
        protected createPages():void {
            this.pages = [];
            let page: Phaser.Group;
            let pagesCount:number = this.pattern.child.filter(function(child:any):any {
                return child.name.indexOf('page') != -1;
            }.bind(this)).length;
            for(let i:number = 0; i < pagesCount; i++) {
                page = this.getByName('page' + (i + 1));
                this.pages.push(page);
                this.removeChild(page);
            }
        }

        /**
         *
         * @param data
         * @returns {Phaser.Sprite}
         */
        protected construct(data:any, parent?:Phaser.Group):Phaser.Group | LocaleText | SimpleSprite {
            let child:Phaser.Group | LocaleText | SimpleSprite;
            let type:string = 'sprite';

            if(data.name.indexOf('_image') > -1) {
                type = 'image';
            }
            if(data.name.indexOf('_text') > -1) {
                type = 'text';
            }
            if(data.name.indexOf('_textKey') > -1) {
                type = 'textKey';
            }

            switch (type) {
                case 'image':
                    let name:string = data.name;
                    name = name.replace('_image', '');
                    let splitted:string[] = name.split('$');
                    child = new SimpleSprite(this.game, 0, 0, splitted[0], this.debugMode);
                    child.frameName = splitted[1];
                    break;
                case 'text':
                    child = this.game.textFactory.getTextWithStyle('', 'PAYTABLE_VALUE_TEXT_style');
                    //child.text = '1000\n100\n10';

                    let symbolId:string = data.name.split('_')[1];

                    //let sss:any = this.game.state.getCurrentState()['model'].denomination;
                    let text:string = '';
                    this.values[symbolId].split('\n').forEach(function (element:string) {
                        text += (+element * this.game.state.getCurrentState()['model'].denomination).toFixed(2) + '\n';
                    }, this);

                    child.text = String(text);
                    break;
                case 'textKey':
                    let key = data.name;
                    key = key.replace('_textKey', '');
                    child = this.game.textFactory.getTextWithStyle(key);
                    break;
                case 'sprite':
                    child = parent || new Phaser.Group(this.game);
                    break;
            }

            if(data.hasOwnProperty('child')) {
                let newChild:Phaser.Group | Phaser.Text | SimpleSprite;
                for(let i:number = 0; i < data.child.length; i++) {
                    newChild = this.construct(data.child[i]);
                    child['add'](newChild);
                }
            }

            child.name = data.name;
            child.x = data.x;
            child.y = data.y;
            child.scale.x = data.scaleX;
            child.scale.y = data.scaleY;

            switch(type) {
                case 'text':
                case 'label':
                case 'textKey':
                    this.setTextPosition(child as LocaleText, data);
                    break;
            }

            return child;
        }

        protected setTextPosition(textField:LocaleText, pattern:any) {
            switch (pattern.align) {
                case 'left':
                    textField.align = 'left';
                    textField.x = pattern.x;
                    textField.y = pattern.y;
                    textField.anchor.set(0, 0);
                    break;
                case 'right':
                    textField.align = 'right';
                    textField.x = pattern.x + pattern.width;
                    textField.y = pattern.y;
                    textField.anchor.set(1, 0);
                    break;
                case 'center':
                    textField.align = 'center';
                    textField.x = pattern.x + pattern.width / 2;
                    textField.y = pattern.y;
                    textField.anchor.set(0.5, 0);
                    break;
            }
        }

        /**
         *
         * @param pageIndex
         */
        public goToPage(pageIndex:number | string):void {
            let newIndex:number;
            let currentPageIndex:number = this.pages.indexOf(this.currentPage);
            switch (pageIndex) {
                case 'increase':
                    newIndex = currentPageIndex + 1;
                    if(newIndex == this.pages.length) {
                        newIndex = 0
                    }
                    break;
                case 'decrease':
                    newIndex = currentPageIndex - 1;
                    if(newIndex == -1) {
                        newIndex = this.pages.length - 1;
                    }
                    break;
                default:
                    newIndex = pageIndex as number;
            }

            if(this.currentPage) {
                this.removeChild(this.currentPage);
            }
            this.currentPage = this.pages[newIndex];
            this.add(this.currentPage);
        }

        protected getPatternData(parent:any, childName:string):any {
            for(let i:number = 0; i < parent.child.length; i++) {
                let childPattern:any = parent.child[i];
                if (childPattern.name == childName) {
                    return childPattern;
                }
            }
            return null;
        }

        /**
         *
         */
        public show():void {
            this.goToPage(0);
            this.visible = true;
        }

        /**
         *
         */
        public hide():void {
            this.visible = false;
        }

    }
}