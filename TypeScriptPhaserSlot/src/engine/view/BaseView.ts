﻿module TypeScriptPhaserSlot.common {

    export abstract class BaseView extends Phaser.Group {

        private _debugMode: boolean;
        private _name: string;

        constructor(game: Phaser.Game, name: string, debugMode?: boolean) {
            super(game, null);
            this._name = name;
            this.debugMode = debugMode;
        }

        setPosition(x: number, y: number): void {
            this.x = x;
            this.y = y;
        }
        /**
         * Specific console logger
         * @param message
         */
        protected debuglog(message: string): void {
            console.log(this.name, ":", message);
        }

        /************************************************************
            Getters and Setters
        *************************************************************/
        public get debugMode(): boolean {
            return this._debugMode;
        }

        public set debugMode(value: boolean) {
            this._debugMode = value;
        }

        public get name(): string {
            return this._name;
        }
    }
}