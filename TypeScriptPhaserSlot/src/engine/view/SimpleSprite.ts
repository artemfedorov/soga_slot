﻿module TypeScriptPhaserSlot.common {

    export class SimpleSprite extends Phaser.Sprite {

        protected _debugMode: boolean;
        protected _name: string;
        protected _userData: any;

        constructor(game: Phaser.Game, x: number, y: number, key?: string, debugMode?: boolean, frame?: number | string) {
            super(game, x, y, key, frame);

            if(typeof frame == 'number') {
                this.frame = frame;
            } else if(typeof frame == 'string') {
                this.frameName = frame;
            }

            this._name = name;
            this.debugMode = debugMode;
            this.init();
        }


        protected init(): void {

        }


        public start(data: any): void {
            //start do any work 
        }

        public stop(data: any): void {
            //stop doing current work
        }

        public changeKey(key: string): void {
            this.key = key;
            this.loadTexture(key, 0);
        }


        /************************************************************
            Getters and Setters
        *************************************************************/
        public get debugMode(): boolean {
            return this._debugMode;
        }

        public set debugMode(value: boolean) {
            this._debugMode = value;
        }

        public get name(): string {
            return this._name;
        }

        public set name(value: string) {
            this._name = value;
        }

        public set userData(value: any) {
            this._userData = value;
        }

        public get userData(): any {
            return this._userData;
        }
    }
}