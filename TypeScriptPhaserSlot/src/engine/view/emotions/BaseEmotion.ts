/**
 * Created by rocket on 04.07.2017.
 */
module TypeScriptPhaserSlot.common {

    export abstract class BaseEmotion extends Phaser.Group implements IEmotion{

        //protected animation:AnimatedSprite;
        protected config: EmotionConfiguration;
        protected _actionSignal: Phaser.Signal;
        protected _tweens: Phaser.Tween[];

        constructor(game: Game, config:EmotionConfiguration) {
            super(game);
            this._tweens = [];
            this.config = config;
            this._actionSignal = new Phaser.Signal();

            this.init();
        }

        /**
         * This is an example how to we can init smth :
         */
        protected init():void {
            //this.animation = new AnimatedSprite(this.game, 0, 0, this.config.emotion);
            //this.animation.actionSignal.addOnce(this.onAnimationsComplete, this);
            //this.add(this.animation);
        }

        /**
         *
         */
        public start():void {
            this.animate();
        }

        /**
         *
         */
        public stop():void {
            for(let i: number = 0; i < this._tweens.length; i++) {
                this._tweens[i].stop(false);
            }

            //this.animation.stop();
            //this.animation.animations.destroy();
            //this.animation.destroy();
            //this.animation = null;
        }

        /**
         *
         */
        protected animate():void {
            //this.animation.start();
        }

        /**
         * Just finished all stuff
         */
        protected onAnimationsComplete(data?:any):void {

        }

        /**
         *
         * @returns {Phaser.Signal}
         */
        public get actionSignal(): Phaser.Signal {
            return this._actionSignal;
        }
    }
}