/**
 * Created by rocket on 04.07.2017.
 */
module TypeScriptPhaserSlot.common {

    export interface IEmotion extends Phaser.Group {

        start():void;
        stop():void;
        actionSignal:Phaser.Signal;

    }
}