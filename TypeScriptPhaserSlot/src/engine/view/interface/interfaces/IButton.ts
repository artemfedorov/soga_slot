﻿module TypeScriptPhaserSlot.common {

    export interface IButton extends Phaser.Image {

        name: string;
        action: string;
        actions: any;
        enabled: boolean;
        debugMode: boolean;
                
        //animateSpecificSymbolWithIndex(index: number): void;

    }
}  