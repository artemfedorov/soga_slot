﻿///<reference path="../../../engine/view/interface/interfaces/IButton.ts" />

module TypeScriptPhaserSlot.common {

    export class BaseButton extends Phaser.Button implements IButton {

        public static DOUBLE_CLICK_DELAY = 200;
        public static LONG_CLICK_DELAY = 1000;

        public static MOUSE_DOUBLE_CLICK: string = "mouseDoubleClick";
        public static MOUSE_LONG_DOWN: string = "mouseLongDown";
        public static MOUSE_CLICK: string = "mouseClick";
        public static MOUSE_OVER: string = "mouseOver";
        public static MOUSE_OUT: string = "mouseOut";
        public static MOUSE_DOWN: string = "mouseDown";
        public static MOUSE_UP: string = "mouseUp";

        protected _action: string;
        protected _actions: any;
        protected _debugMode: boolean;
        protected _enabled: boolean;
        protected _name: string;
        protected _callback: Function;
        protected _callbackContext: any;

        protected _caption: string;
        protected _textStyleName: string;
        protected _label: Phaser.Text;

        protected _data: ButtonConfiguration;

        protected _doubleClickTimer: Phaser.Timer;
        protected _longDownTimer: Phaser.Timer;

        public game: TypeScriptPhaserSlot.common.Game;

        constructor(game: TypeScriptPhaserSlot.common.Game, data: ButtonConfiguration, x: number, y: number, callback: Function, callbackContext: any, debugMode?: boolean) {
            super(game, x, y, data.key, null, null, data.overFrame, data.outFrame, data.downFrame, data.upFrame);
            this._name = data.name;
            this._action = data.action;
            this._caption = data.caption;
            this._textStyleName = data.textStyle;

            this._callback = callback;
            this._callbackContext = callbackContext;

            this._data = data;

            /*this.onDownSound = new Phaser.Sound(this.game, data.onDownSoundKey);
            this.onOverSound = new Phaser.Sound(this.game, data.onOverSoundKey);
            this.onOutSound = new Phaser.Sound(this.game, data.onOutSoundKey);
            this.onUpSound = new Phaser.Sound(this.game, data.onUpSoundKey);*/

            this._doubleClickTimer = game.time.create(false);
            this._longDownTimer = game.time.create(false);

            if (this._caption != "") {
                this._label = this.game.textFactory.getTextWithStyle(this._caption, this._textStyleName);
                this._label.anchor.set(0.5, 0.5);
                this.addChild(this._label);
            }

            this.onInputOver.add(this.onMouseOver, this);
            this.onInputOut.add(this.onMouseOut, this);
            this.onInputDown.add(this.onMouseDown, this);
            this.onInputUp.add(this.onMouseUp, this);

            this.debugMode = debugMode;
        }

        protected onDoubleTimer(): void {
            this.onButtonEvents(BaseButton.MOUSE_CLICK);
            this._doubleClickTimer.stop();
        }

        protected onLongTimer(): void {
            this.onButtonEvents(BaseButton.MOUSE_LONG_DOWN);
            this._longDownTimer.stop();
        }

        /**
         * 
         */
        protected onMouseOver(): void {
            this.onButtonEvents(BaseButton.MOUSE_OVER);
        }

        /**
         * 
         */
        protected onMouseOut(): void {
            this.onButtonEvents(BaseButton.MOUSE_OUT);

            this._longDownTimer.stop();
            this._doubleClickTimer.stop();
        }

        /**
         * 
         */
        protected onMouseDown(): void {
            this.onButtonEvents(BaseButton.MOUSE_DOWN);

            this._longDownTimer.add(BaseButton.LONG_CLICK_DELAY, this.onLongTimer, this);
            this._longDownTimer.start();
        }

        /**
         * 
         */
        protected onMouseUp(): void {
            this.onButtonEvents(BaseButton.MOUSE_UP);

            this._longDownTimer.stop(false);

            if (this._doubleClickTimer.running) {
                this.onButtonEvents(BaseButton.MOUSE_DOUBLE_CLICK);
                this._doubleClickTimer.stop();

            } else {
                this._doubleClickTimer.add(BaseButton.DOUBLE_CLICK_DELAY, this.onDoubleTimer, this);
                this._doubleClickTimer.start();
            }
        }

        /**
         * Обработка действий кнопки
         * @param $handlerType
         */
        protected onButtonEvents($handlerType: string): void {
            if (!this._actions || !this._actions.hasOwnProperty($handlerType)) {
                return;
            }

            let actionData: any = this._actions[$handlerType];

            this._action = actionData.action;

            this._callback.call(this._callbackContext, this);

            if(actionData.visibleAfter != undefined) {
                this.visible = actionData.visibleAfter;
            }

            if(actionData.enabledAfter != undefined) {
                this.enabled = actionData.enabledAfter;
            }

        }

        /**
         * Handle changing locale e.g. "pl", "en" etc.
         * @param {string} locale
         */
        protected onLocaleChanged(locale: any): void {
            this._caption = locale[this.action];
            this._label.text = this._caption;
        }



        /**
         * Specific console logger
         * @param message
         */
        protected debuglog(message: string): void {
            console.log(this.name, ":", message);
        }

        /************************************************************
            Getters and Setters
        *************************************************************/

        public get debugMode(): boolean {
            return this._debugMode;
        }

        public set debugMode(value: boolean) {
            this._debugMode = value;
        }

        public set action($value: string) {
            this._action = $value;
        }

        public get action(): string {
            return this._action;
        }

        public set actions($value: any) {
            this._actions = $value;
        }

        public get actions(): any {
            return this._actions;
        }

        public set enabled($value: boolean) {
            this._enabled = $value;

            //this.frame = this._enabled ? this.data.upFrame : this.data.disabledFrame;
            let newFrame:number | string = this._enabled ? this.data.upFrame : this.data.disabledFrame;
            switch (typeof newFrame) {
                case 'string':
                    this.frameName = newFrame.toString();
                    break;
                case 'number':
                    this.frame = newFrame;
                    break;
            }
            this.inputEnabled = this._enabled;
            this.freezeFrames = !this._enabled;
            //this.input.useHandCursor = this._enabled;
            //this.buttonMode = this._enabled;
        }

        public get enabled(): boolean {
            return this._enabled;
        }

        public set name($value: string) {
            this._name = $value;
        }

        public get name(): string {
            return this._name;
        }

        public set data($value: ButtonConfiguration) {
            this._data = $value;
        }

        public get data(): ButtonConfiguration {
            return this._data;
        }

        /*public set frame($frame: string | number) {
            console.log($frame);
        }*/

    }
}