﻿///<reference path="../../../engine/view/interface/interfaces/IButton.ts" />
///<reference path="../../../engine/view/interface/BaseButton.ts" />

module TypeScriptPhaserSlot.common {

    export class TriggerButton extends BaseButton implements IButton {

        private triggerStateFrames: number[];
        private currentTriggerState: number;

        public isTrigger: boolean;
        public triggerSounds: Phaser.Sound[];

        constructor(game: TypeScriptPhaserSlot.common.Game, data: ButtonConfiguration, x: number, y: number, callback: Function, callbackContext: any, debugMode?: boolean) {
            super(game, data, x, y, callback, callbackContext, debugMode);

            if (data.trigger) {
                this.currentTriggerState = 0;
                this.isTrigger = data.trigger;
                this.triggerStateFrames = data.triggerStateFrames;
                this.freezeFrames = this.isTrigger;
                this.triggerSounds = [];
                for (let i: number = 0; i < data.triggerSoundKeys.length; i++) {
                    this.triggerSounds[i] = new Phaser.Sound(this.game, data.triggerSoundKeys[i]);
                }
            }
        }

        /**
         *
         */
        public playTriggerStateSound(): void {
            if (this.triggerSounds.length > this.currentTriggerState) {
                this.triggerSounds[this.currentTriggerState].play();
            }
        }

        /**
         *
         */
        public switchToNextState(): void {
            if (this.isTrigger) {
                if (this.currentTriggerState + 1 < this.triggerStateFrames.length) {
                    this.currentTriggerState++;
                    this.frame = this.triggerStateFrames[this.currentTriggerState];
                } else {
                    this.currentTriggerState = 0;
                }
            }
        }

        /**
         *
         * @param {number} index
         */
        public forceTriggerState(index: number): void {
            if (this.isTrigger) {
                this.currentTriggerState = index;
                this.triggerStateFrames[this.currentTriggerState];
                this.frame = this.triggerStateFrames[this.currentTriggerState];
            }
        }
    }

}