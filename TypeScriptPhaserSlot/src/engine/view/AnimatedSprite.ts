﻿module TypeScriptPhaserSlot.common {

    export class AnimatedSprite extends Phaser.Sprite {

        public static ANIMATION_FIRST_LOOP_COMPLETE: string = 'animationFirstLoopComplete';
        public static ANIMATION_LOOPED: string = 'animationLooped';
        public static ANIMATION_COMPLETE: string = 'animationComplete';
        public static ALL_ANIMATIONS_COMPLETE: string = 'allAnimationsComplete';

        protected _debugMode: boolean;
        protected _name: string;
        protected _config: AnimationConfiguration;
        protected _actions: Phaser.Signal;
        protected _animationsData:any;
        protected _userData:any;

        constructor(game: Phaser.Game, x: number, y: number, config: AnimationConfiguration | string, debugMode?: boolean) {
            let configuration:AnimationConfiguration;
            if(typeof config == 'string') {
                configuration = (function ():AnimationConfiguration {
                    let newConfig:AnimationConfiguration;
                    let gameConfiguration:any = game.state.getCurrentState()['gameController'].configuration;
                    for(let i:number = 0; i < gameConfiguration.animations.length; i++) {
                        let animConfig:AnimationConfiguration = gameConfiguration.animations[i];
                        if(animConfig.key == config) {
                            newConfig = animConfig;
                            break;
                        }
                    }
                    return newConfig;
                })();
            } else {
                configuration = config
            }

            super(game, x, y, configuration.assetKey || configuration.key);
            this._name = name;
            this._config = configuration;
            this._debugMode = debugMode;
            this._animationsData = {};

            this.init();
        }

        protected getAnimationConfig(configName:string):any {

        }

        /**
         *
         */
        protected init(): void {
            this._actions = new Phaser.Signal();

            this.getAnimations();
            this.correctParams();
        }

        /**
         *
         */
        protected correctParams(): void {
            this.scale.set(this._config.scale.x, this._config.scale.y);
            this.pivot.set(this._config.pivot.x, this._config.pivot.y);
            this.anchor.set(this._config.anchor.x, this._config.anchor.y);
        }

        /**
         *
         */
        protected getAnimations(): void {
            let animationData: AnimationDataConfiguration;
            let selectedFrames: number[];
            for (let i: number = 0; i < this._config.animations.length; i++) {
                animationData = this._config.animations[i];
                selectedFrames = [];
                let animationName: string = animationData.name;
                let startFrame: number = animationData.startFrame;
                let endFrame: number = animationData.endFrame;
                if (startFrame < endFrame) {
                    for (let i: number = startFrame; i < endFrame + 1; i++) {
                        selectedFrames.push(i);
                    }
                } else {
                    for (let i: number = startFrame; i >= endFrame; i--) {
                        selectedFrames.push(i);
                    }
                }
                this.animations.add(animationName, selectedFrames, animationData.frameRate, animationData.isLoop, true);
                this._animationsData[animationName] = animationData;

                this.events.onAnimationLoop.addOnce(this.onAnimationFirstLoopedCallback, this);
            }
            this.events.onAnimationLoop.add(this.onAnimationLoopedCallback, this);
            this.events.onAnimationComplete.add(this.onAnimationCompleteCallback, this);
        }

        /**
         *
         * @param sprite
         * @param animation
         */
        protected onAnimationFirstLoopedCallback(sprite: SimpleSprite, animation: Phaser.Animation): void {
            this._actions.dispatch(AnimatedSprite.ANIMATION_FIRST_LOOP_COMPLETE);
        }

        /**
         *
         * @param sprite
         * @param animation
         */
        protected onAnimationLoopedCallback(sprite: SimpleSprite, animation: Phaser.Animation): void {
            this._actions.dispatch(AnimatedSprite.ANIMATION_LOOPED);
        }

        /**
         *
         * @param sprite
         * @param animation
         */
        protected onAnimationCompleteCallback(sprite: SimpleSprite, animation: Phaser.Animation): void {
            let currentAnimationData:any = this._animationsData[animation.name];
            let nextAnimationName:string = currentAnimationData.playOnCompleted;
            if(nextAnimationName) {
                this._actions.dispatch(AnimatedSprite.ANIMATION_COMPLETE);
                //sprite.animations.play(nextAnimationName);
                this.start(nextAnimationName);
            } else {
                this._actions.dispatch(AnimatedSprite.ALL_ANIMATIONS_COMPLETE);
            }
        }

        public start(animationName?: string): void {
            this.stop();

            let startAnimationName = animationName || 'start';

            this.events.onAnimationLoop.remove(this.onAnimationFirstLoopedCallback, this);
            this.events.onAnimationLoop.addOnce(this.onAnimationFirstLoopedCallback, this);

            this.animations.play(startAnimationName);
        }

        public stop(resetFrame = true): void {
            this.animations.currentAnim.stop(resetFrame, false);
            this.animations.stop();
        }

        public changeKey(key: string): void {
            this.key = key;
            this.loadTexture(key, 0);
        }

        public destroy(): void {
            super.destroy(true);
        }

        /************************************************************
            Getters and Setters
        *************************************************************/
        public get debugMode(): boolean {
            return this._debugMode;
        }

        public set debugMode(value: boolean) {
            this._debugMode = value;
        }

        public get name(): string {
            return this._name;
        }

        public set name(value: string) {
            this._name = value;
        }

        public get config(): any {
            return this._config;
        }

        public set config(value: any) {
            this._config = value;
        }

        public get actionSignal(): Phaser.Signal {
            return this._actions;
        }

        public set userData(value: any) {
            this._userData = value;
        }

        public get userData(): any {
            return this._userData;
        }
    }
}