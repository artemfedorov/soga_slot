﻿module TypeScriptPhaserSlot.common {

    export interface IToolbarButton extends SimpleSprite {

        actionSignal:Phaser.Signal;
    }
}

