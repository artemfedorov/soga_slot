/**
 * Created by rocket on 30.06.2017.
 */
module TypeScriptPhaserSlot.common {

    export class SoundButton extends SimpleSprite implements IToolbarButton{

        private _actionSignal: Phaser.Signal;

        protected statesFrames:string[];
        protected states:string[];
        protected stateIndex:number;

        constructor(game: TypeScriptPhaserSlot.common.Game, x: number, y: number) {
            super(game, x, y, 'toolbar');

            this.inputEnabled = true;

            this.stateIndex = 0;
            this.statesFrames = ['sound_1', 'sound_2', 'sound_3'];
            this.states = ['all_on', 'music_off', 'all_off'];

            this.frameName = 'toolbar/' + this.statesFrames[this.stateIndex];

            this._actionSignal = new Phaser.Signal();

            this.alpha = 0.7;

            this.events.onInputDown.add(this.onDown, this);
        }

        protected onDown():void {
            this.stateIndex++;
            if(this.stateIndex == this.states.length) {
                this.stateIndex = 0;
            }
            this.frameName = 'toolbar/' + this.statesFrames[this.stateIndex];
            this.actionSignal.dispatch(this.states[this.stateIndex]);
        }

        public get actionSignal(): Phaser.Signal {
            return this._actionSignal;
        }
    }
}