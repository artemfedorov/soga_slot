/**
 * Created by rocket on 30.06.2017.
 */
module TypeScriptPhaserSlot.common {

    export class FullScreenButton extends SimpleSprite implements IToolbarButton{

        private _actionSignal: Phaser.Signal;

        protected statesFrames:string[];
        protected states:string[];
        protected stateIndex:number;

        constructor(game: TypeScriptPhaserSlot.common.Game, x: number, y: number) {
            super(game, x, y, 'toolbar');

            this.inputEnabled = true;

            this.stateIndex = 0;
            this.statesFrames = ['full_screen_1', 'full_screen_2'];
            this.states = ['switch_fullscreen', 'switch_fullscreen'];

            this.frameName = 'toolbar/' + this.statesFrames[this.stateIndex];

            this._actionSignal = new Phaser.Signal();

            this.alpha = 0.7;

            //this.events.onInputDown.add(this.onDown, this);
            this.events.onInputUp.add(this.onDown, this);
        }

        protected onDown():void {
            this.stateIndex++;
            if(this.stateIndex == this.states.length) {
                this.stateIndex = 0;
            }
            this.frameName = 'toolbar/' + this.statesFrames[this.stateIndex];

            if(!this.game.device.desktop){
                console.log('MOBILE FULLSCREEN');

                if (this.game.scale.isFullScreen) {
                    this.game.scale.stopFullScreen();
                } else {
                    this.game.scale.startFullScreen(true);
                }
            } else{
                this.actionSignal.dispatch(this.states[this.stateIndex]);
            }
        }

        public get actionSignal(): Phaser.Signal {
            return this._actionSignal;
        }
    }
}