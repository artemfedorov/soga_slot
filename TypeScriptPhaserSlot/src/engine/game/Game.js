var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var Game = (function (_super) {
            __extends(Game, _super);
            function Game(browser, parent, options) {
                var _this = 
                //super(parent.offsetWidth || 1024, parent.offsetHeight || 768, Phaser.AUTO/*browser*/, parent || 'content', 'BootStage', false, false);
                _super.call(this, 1024, 768, Phaser.AUTO, parent || 'content', 'BootStage', false, false) || this;
                _this.stateIndex = 0;
                _this.stateList = ['BootStage', 'PreloaderStage', 'GameStage'];
                Game.pool = [];
                Game.bootOptions = options || { game_path: '' };
                _this.state.add("BootStage", common.BootStage, false);
                _this.state.add('PreloaderStage', common.PreloaderStage, false);
                _this.state.add('GameStage', GameStage, false);
                _this.startNextState();
                return _this;
            }
            Game.prototype.startNextState = function (args, clearWorld) {
                if (clearWorld == undefined) {
                    clearWorld = true;
                }
                this._currentState = this.stateList[this.stateIndex];
                this.state.start(this._currentState, clearWorld ? true : false, false, args);
                this.stateIndex++;
                console.log(this.currentState);
            };
            Object.defineProperty(Game.prototype, "textFactory", {
                get: function () {
                    return this._textFactory;
                },
                set: function (value) {
                    this._textFactory = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game.prototype, "currentState", {
                get: function () {
                    return this._currentState;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game.prototype, "pattern", {
                get: function () {
                    return this._pattern;
                },
                set: function (value) {
                    this._pattern = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game.prototype, "buttonsStates", {
                get: function () {
                    return this._buttonsStates;
                },
                set: function (value) {
                    this._buttonsStates = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game.prototype, "model", {
                get: function () {
                    return this._model;
                },
                set: function (value) {
                    this._model = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game, "pool", {
                get: function () {
                    return this._pool;
                },
                set: function (value) {
                    this._pool = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game, "bootOptions", {
                get: function () {
                    return this._bootOptions;
                },
                set: function (value) {
                    this._bootOptions = value;
                },
                enumerable: true,
                configurable: true
            });
            return Game;
        }(Phaser.Game));
        common.Game = Game;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
