﻿///<reference path="../../libs/types/crypto-js.d.ts" />
///<reference path="../../libs/phaserTypes/phaser.d.ts" />

module TypeScriptPhaserSlot.common {

    export class Game extends Phaser.Game {

        private _textFactory: TextFactory;
        private _model:IGameModel;
        private stateIndex = 0;
        private _pattern: any;
        private _buttonsStates: any;
        private _currentState: string;
        private stateList: string[] = ['BootStage', 'PreloaderStage', 'GameStage'];

        private static _pool: ISymbol[];
        private static _bootOptions: any;

        constructor(browser: number, parent?:any, options?:any) {
            //super(parent.offsetWidth || 1024, parent.offsetHeight || 768, Phaser.AUTO/*browser*/, parent || 'content', 'BootStage', false, false);
            super(parent.offsetWidth || 1024, parent.offsetHeight || 768, Phaser.CANVAS, parent || 'content', 'BootStage', true, true);

            console.log(parent);
            console.log(parent.offsetWidth);
            console.log(parent.offsetHeight);

            console.log('ver101');

            Game.pool = [];
            Game.bootOptions = options || {game_path:''};
            this.state.add("BootStage", BootStage, false);
            this.state.add('PreloaderStage', PreloaderStage, false);
            this.state.add('GameStage', GameStage, false);

            this.startNextState();
        }

        public startNextState(args?: any[], clearWorld?:boolean): void {
            if(clearWorld === undefined) {
                clearWorld = true;
            }
            
            this._currentState = this.stateList[this.stateIndex];
            this.state.start(this._currentState, clearWorld, false, args);
            this.stateIndex++;
            console.log(this.currentState);
        }


        public get textFactory(): TextFactory {
            return this._textFactory;
        }

        public set textFactory(value: TextFactory) {
            this._textFactory = value;
        }

        public get currentState() {
            return this._currentState;
        }

        public set pattern(value: any) {
            this._pattern = value;
        }

        public get pattern(): any {
            return this._pattern;
        }

        public set buttonsStates(value: any) {
            this._buttonsStates = value;
        }

        public get buttonsStates(): any {
            return this._buttonsStates;
        }

        public set model(value: IGameModel) {
            this._model = value;
        }

        public get model(): IGameModel {
            return this._model;
        }

        public static set pool(value: ISymbol[]) {
            this._pool = value;
        }

        public static get pool(): ISymbol[] {
            return this._pool;
        }

        public static set bootOptions(value: any) {
            this._bootOptions = value;
        }

        public static get bootOptions(): any {
            return this._bootOptions;
        }
    }
}

//console.log = function() {}