﻿///<reference path="./BaseStage.ts" />
module TypeScriptPhaserSlot.common {

    export class PreloaderStage extends BaseStage {
        private settings: any;
        private gameName: string;
        private preloader: IPreloaderView;

        init(gameName: string) {
            //this.gameName = "PrincessFrog";//gameName
            //this.gameName = "Detective";//gameName
            //this.gameName = "Spinners";//gameName

            if(window['GR']) {
                window['GR'].Events.game.start();
                window['GR'].Events.flow.state.on(function (data: string) {
                    window['windowBooongoStateData'] = data;
                }, this);
            }

            let preloaderConfig:any = this.game.cache.getJSON('preloaderConfig');
            this.gameName = preloaderConfig.gameName;

            let preloaderViewName: string = this.getClassName('PreloaderView');
            this.preloader = new TypeScriptPhaserSlot.common[preloaderViewName](this.game, preloaderViewName, null, this.debugMode);
            this.game.add.existing(this.preloader);
        }

        preload() {
            this.game.load.baseURL = Game.bootOptions.game_path;
            this.game.load.crossOrigin = true;
            //this.game.load.script('particlestorm', 'plugins/particle-storm.min.js');
            this.game.load.json('strings', "/assets/strings.json" + '?' + Math.random(), false);
            this.game.load.json('styles', "/assets/styles.json" + '?' + Math.random(), false);
            this.game.load.json('settings', "/assets/game.json" + '?' + Math.random(), false);
            this.game.load.json('buttonsStates', (this.game.device.desktop ? "/assets/buttonsStates.json" : "/assets/buttonsStates_mobile.json") + '?' + Math.random());
            this.game.load.json('pattern', "/assets/pattern.json" + '?' + Math.random());
            this.game.load.pack("assets", "/assets/asset.json" + '?' + Math.random());
            this.game.load.onFileComplete.add(this.onLoadingFileComplete, this);
            this.game.load.onLoadComplete.add(this.onLoadingComplete, this);
            this.game.load.start();

            window.addEventListener('resize', function () {
                this.game.scale.setGameSize(this.game.canvas.parentElement.offsetWidth, this.game.canvas.parentElement.offsetHeight);
            }.bind(this));

            this.game.scale.setGameSize(this.game.canvas.parentElement.offsetWidth, this.game.canvas.parentElement.offsetHeight);

            this.game.camera.width = this.game.canvas.parentElement.offsetWidth;
            this.game.camera.height = this.game.canvas.parentElement.offsetHeight;

            this.game.scale.onSizeChange.add(this.correctSize, this);
            this.correctSize(this.game.scale, this.game.scale.width, this.game.scale.height);
        }

        /**
         *
         * @param progress
         * @param cacheKey
         * @param success
         * @param totalLoaded
         * @param totalFiles
         */
        protected onLoadingFileComplete(progress: number, cacheKey: string, success: boolean, totalLoaded: number, totalFiles: number): void {
            this.preloader.onLoadingUpdate(progress, cacheKey, success, totalLoaded, totalFiles);
        }

        /**
         *
         */
        protected onLoadingComplete(): void {
            this.preloader.onLoadingComplete();
        }

        /**
         *
         * @param className
         * @returns {string}
         */
        protected getClassName(className: string): string {
            if (TypeScriptPhaserSlot.common[this.gameName + className]) {
                return this.gameName + className;
            }
            return className;
        }

        create() {
            if(window['GR']) {
                window['GR'].Events.game.loaded();
            }

            let settings: any = this.game.cache.getJSON('settings');
            let pattern: any = this.game.cache.getJSON('pattern');
            let strings: any = this.game.cache.getJSON('strings');
            let stylesData: any = this.game.cache.getJSON('styles');
            let buttonsStates: any = this.game.cache.getJSON('buttonsStates');

            let styles: TextStyle[] = [];
            for (let i: number = 0; i < stylesData.styles.length; i++) {
                styles.push(new TextStyle(stylesData.styles[i]));
            }
            this.game.textFactory = new TextFactory(this.game, styles, strings, "en");
            this.game.buttonsStates = buttonsStates;

            //this.game.camera.fade(0x0, 2000);

            /*setTimeout(function () {
             this.game.startNextState([settings, pattern]);
             }.bind(this), 3000);*/

            this.game.startNextState([settings, pattern]);
            this.game.scale.onSizeChange.remove(this.correctSize, this);
        }

        protected correctSize(scaleManager:Phaser.ScaleManager, screenWidth:number, screenHeight:number):void {
            if(screenWidth < screenHeight) {
                return;
            }

            if(!this.game.world.children || this.game.world.children.length == 0) {
                return;
            }

            let w:number = 1366;
            let h:number = 768;
            let g:any = this.game.world.getChildAt(0);

            console.log('Game WIDTH', this.game.width);
            console.log('Game HEIGHT', this.game.height);

            console.log('Camera WIDTH', this.game.camera.width);
            console.log('Camera HEIGHT', this.game.camera.height);

            if(this.game.camera.height != h) {
                let scale:number = this.game.camera.height / h;
                g.scale.set(scale, scale);
            }

            let boundsXoffset:number = g.width < this.game.camera.width ? (this.game.camera.width - g.width) / 2 * -1 : 0;
            this.game.world.resize(g.width, g.height);
            this.game.world.setBounds(boundsXoffset, 0, g.width, g.height);

            let cameraX:number = (g.width - this.game.camera.width) / 2;
            this.game.camera.x =  cameraX;
        }
    }

}