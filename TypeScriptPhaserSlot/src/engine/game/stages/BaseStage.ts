﻿//  Base abstract class inherited Phaser.State
//  to have an abillity to add any common properties or methods.
module TypeScriptPhaserSlot.common {

    export abstract class BaseStage extends Phaser.State {

        private _debugMode: boolean;
        private _name: string;
        public game: TypeScriptPhaserSlot.common.Game;

        constructor(game: TypeScriptPhaserSlot.common.Game) {
            super();
            this._name = name;
            this.debugMode = false;
            this.game = game;
        }


        preload() {
           
        }

        create() {
           
        }

        

        /**
         * Specific console logger
         * @param message
         */
        protected debuglog(message: string): void {
            console.log(name,":", message);
        }

        /************************************************************
            Getters and Setters
        *************************************************************/
        public get debugMode(): boolean {
            return this._debugMode;
        }

        public set debugMode(value: boolean) {
            this._debugMode = value;
        }

        public get name(): string {
            return this._name;
        }

    }

}