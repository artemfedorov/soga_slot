﻿///<reference path="./BaseStage.ts" />

// Game Phaser State
// Start of the Game

module TypeScriptPhaserSlot.common {

    export class GameStage extends BaseStage {

        private configs: any;
        private pattern: any;

        private modelStateList: Array<IState>;

        private _model: IGameModel;
        private _gameController: IGameController;


        /**
         * Getting started arguments like a settings or pre-defined configs
         * @param args
         */
        init(args: any[]) {
            /*this.game.tweens.frameBased = true;
            this.game.renderer.renderSession.roundPixels = true;
            this.game.forceSingleUpdate = true;

            this.game.scale.setGameSize(2732, 1536);

            if(this.game.device.desktop) {
                this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;

                this.game.scale.setMinMax(3, 4, window.screen.width, window.screen.height);
                this.game.scale.setUserScale(0.5, 0.5, 0, 0);

                this.game.scale.refresh();
            } else {
                this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

                this.game.scale.refresh();
            }

            this.game.scale.pageAlignHorizontally = true;
            this.game.scale.pageAlignVertically = true;

            this.game.stage.disableVisibilityChange = true;
            this.game.scale.refresh();

            this.game.scale.forceOrientation(true, false);
            this.game.scale.enterIncorrectOrientation.add(handleIncorrect, this);
            this.game.scale.leaveIncorrectOrientation.add(handleCorrect, this);

            this.game.scale.forceLandscape = true;

            if(this.game.scale.isLandscape) {
                handleCorrect.call(this);
            } else {
                handleIncorrect.call(this);
            }

            function handleIncorrect(){
                if(!this.game.device.desktop){
                    document.getElementById("turn").style.display="block";
                }
            }

            function handleCorrect(){
                if(!this.game.device.desktop){
                    document.getElementById("turn").style.display="none";
                }
            }*/

            /*this.game.scale.setResizeCallback(function () {
                //window.innerWidth
                this.game.scale.setMinMax(window.innerWidth, window.innerWidth / 1.33);
                this.game.scale.refresh();
            }, this);*/

            this.configs = args;
        }

        /**
         * Place to put the ordered list of states to the Model instance
         */
        preload() {
            // Game Model instance
            this._model = new GameModel(this.debugMode);
            // Game controller instance
            // will create rest of the controllers like:
            // <IAPIController>, <IGameController> including controllers
            // to work with server-side.
            let gameControllerName: string = String(this.configs[0].gameName + "GameController");
            this._gameController = new TypeScriptPhaserSlot.common[gameControllerName](this.game, gameControllerName, this.debugMode, this.configs);
        }


        /**
         * Getting binding main game controller class to the Model instance
         */
        create() {
            this.game.canvas.getContext('2d').webkitImageSmoothingEnabled = true;
            this.game.canvas.getContext('2d').oImageSmoothingEnabled = true;
            this.game.canvas.getContext('2d').mozImageSmoothingEnabled = true;
            this.game.canvas.getContext('2d').imageSmoothingEnabled = true;

            this.gameController.bindToModel(this.model);
            this.game.model = this.model;
        }

        /************************************************************
            Getters and Setters
        *************************************************************/

        public get model(): IGameModel {
            return this._model;
        }

        public get gameController(): IGameController {
            return this._gameController;
        }

    }
}