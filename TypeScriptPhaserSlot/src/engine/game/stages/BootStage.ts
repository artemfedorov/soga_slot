﻿module TypeScriptPhaserSlot.common {

    export class BootStage extends BaseStage {

        private gameName: string;

        init() {
            this.game.canvas.setAttribute('id', 'gameCanvas');

            this.game.tweens.frameBased = true;
            this.game.renderer.renderSession.roundPixels = false;
            this.game.forceSingleUpdate = true;

            this.game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.RESIZE;

            this.game.scale.pageAlignHorizontally = true;
            this.game.scale.pageAlignVertically = true;

            this.game.stage.disableVisibilityChange = true;
            this.game.scale.refresh();

            if(!window['GR']) {
                this.game.scale.enterIncorrectOrientation.add(handleIncorrect, this);
                this.game.scale.leaveIncorrectOrientation.add(handleCorrect, this);

                this.game.scale.forceOrientation(true, false);
                this.game.scale.forceLandscape = true;

                this.game.scale.onSizeChange.add(correctSize, this);

                if(this.game.scale.isLandscape) {
                    handleCorrect.call(this);
                } else {
                    handleIncorrect.call(this);
                }
            }

            /*this.game.onBlur.add(function (data:any) {
                this.game.paused = true;
            }, this);
            this.game.onFocus.add(function (data:any) {
                this.game.paused = false;
            }, this);*/

            function correctSize(scaleManager:Phaser.ScaleManager, screenWidth:number, screenHeight:number):void {
                //if(screenWidth < screenHeight) {
                if(!this.game.scale.isLandscape) {
                    handleIncorrect.call(this);
                } else {
                    handleCorrect.call(this);
                }
            }

            /**
             *
             */
            function handleIncorrect():void {
                if(!this.game.device.desktop){
                    console.log('handleIncorrect');
                    let image:any = document.getElementById("turn");
                    if(image) {
                        image.style.display="block"
                    }
                    this.game.scale.stopFullScreen();
                    this.game.paused = true;
                }
            }

            /**
             *
             */
            function handleCorrect():void {
                if(!this.game.device.desktop){
                    console.log('handleCorrect');
                    let image:any = document.getElementById("turn");
                    if(image) {
                        image.style.display="none"
                    }
                    this.game.paused = false;
                }
            }

            this.gameName = "PrincessFrog";
        }

        preload() {
            this.game.load.baseURL = Game.bootOptions.game_path;
            this.game.load.crossOrigin = true;

            //this.game.load.image('preloader_bg', "assets/preloader/preloader_bg.jpg", false);
            //this.game.load.image('preloader_i1', "assets/preloader/image_1.png", false);
            //this.game.load.image('preloader_i2', "assets/preloader/image_2.png", false);
            //this.game.load.json('preloaderConfig', "preloader.json", false);
            //this.game.load.atlas('preloader_progressbar', "assets/preloader/progerssbar.png", "assets/preloader/progerssbar.json");

            this.game.load.pack("assets", "/assets/preloader_asset.json");
            this.game.load.json('preloaderConfig', "preloader.json", false);

            this.game.load.start();
        }

        create() {

            if(!window['GR']) {
                if(this.game.scale.isLandscape) {
                    this.game.startNextState();
                } else {
                    this.game.scale.leaveIncorrectOrientation.addOnce(function():void {
                        console.log('startNextState');
                        this.game.startNextState();
                    }, this);
                    //this.game.scale.leaveIncorrectOrientation.addOnce(this.game.startNextState, this.game, -1);
                }
            } else {
                this.game.startNextState();
            }
        }
    }
}

