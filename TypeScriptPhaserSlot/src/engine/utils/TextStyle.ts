﻿module TypeScriptPhaserSlot.common {

    export class TextStyle {

        private _style: Phaser.PhaserTextStyle;
        private _name: string;
        private _gradientFromColour: string;
        private _gradientToColour: string;
        private _shadowFill: boolean;
        private _shadowStroke: boolean;
        private _strokeColors: string[];
        private _tint: number;

        constructor(rawData: any) {
            this._style = new Object();
            this._style.font = rawData.font;
            this._style.fill = rawData.fill;
            this._style.align = rawData.align;
            this._style.stroke = rawData.stroke;
            this._style.strokeThickness = rawData.strokeThickness;
            this._style.wordWrap = rawData.wordWrap;
            this._style.wordWrapWidth = rawData.wordWrapWidth;
            this._style.maxLines = rawData.maxLines;
            this._style.shadowOffsetX = rawData.shadowOffsetX;
            this._style.shadowOffsetY = rawData.shadowOffsetY;
            this._style.shadowColor = rawData.shadowColor;
            this._style.shadowBlur = rawData.shadowBlur;
            this._style.tab = rawData.tab;
            this._style.tabs = rawData.tabs;

            this._style.fontSize = rawData.fontSize;
            this._style.fontStyle = rawData.fontStyle;
            this._style.fontVariant = rawData.fontVariant;
            this._style.fontWeight = rawData.fontWeight;
            this._style.backgroundColor = rawData.backgroundColor;
            this._style.boundsAlignH = rawData.boundsAlignH;
            this._style.boundsAlignV = rawData.boundsAlignV;

            this._name = rawData.styleName;
            this._gradientFromColour = rawData.gradientFromColour;
            this._gradientToColour = rawData.gradientToColour;
            this._shadowFill = rawData.shadowFill;
            this._shadowStroke = rawData.shadowStroke;
            this._strokeColors = rawData.strokeColors;
            this._tint = rawData.tint;
        }


        public get style(): Phaser.PhaserTextStyle {
            return this._style;
        }
        public get name(): string {
            return this._name;
        }
        public get gradientFromColour(): string {
            return this._gradientFromColour;
        }
        public get gradientToColour(): string {
            return this._gradientToColour;
        }
        public get shadowFill(): boolean {
            return this._shadowFill;
        }
        public get shadowStroke(): boolean {
            return this._shadowStroke;
        }
        public get strokeColors(): string[] {
            return this._strokeColors;
        }
        public get tint(): number {
            return this._tint;
        }
    }
}