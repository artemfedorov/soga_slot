﻿module TypeScriptPhaserSlot.common {

    export class TextFactory {

        private game: Phaser.Game;
        private styles: TextStyle[];
        private strings: any;

        private _currLocale: string;
        private localeTextArray: LocaleText[];
        private _actionSignal:Phaser.Signal;


        constructor(game: Phaser.Game, styles: TextStyle[], strings: any, initLocale: string) {
            this.game = game;
            this.styles = styles;
            this.strings = strings;
            this.localeTextArray = [];
            this._currLocale = initLocale;
            this._actionSignal = new Phaser.Signal();
        }

        private getStyleByName(name: string): TextStyle {
            for (let i: number = 0; i < this.styles.length; i++) {
                if (this.styles[i].name == name) {
                    return this.styles[i];
                }
            }
            return null;
        }

        /**
         * 
         * @param {string} identifier
         * @param {string | TextStyle} style?
         * @returns
         */
        public getTextWithStyle(identifier: string|number, style?: string | TextStyle): LocaleText {
            let ts: TextStyle;
            let text: string;
            if (style) {
                ts = (typeof style === "string") ? this.getStyleByName(style) : style;
            } else {
                let sn: string = this.strings[identifier] ? this.strings[identifier][this._currLocale]["styleName"] : "defaultStyle";
                ts = this.getStyleByName(sn);
            }
            if (typeof identifier === "string") {
                text = this.strings[identifier] ? this.strings[identifier][this._currLocale].text : "";
            } 
            if (typeof identifier === "number") {
                text = String(identifier);
            }
            let pt: LocaleText = new LocaleText(this.game, 0, 0, text, ts.style, identifier);
            pt.name = ts.name;
            pt.shadowFill = ts.shadowFill;
            pt.shadowStroke = ts.shadowStroke;
            pt.strokeColors = ts.strokeColors;
            if (ts.gradientFromColour != "" && ts.gradientToColour != "") {
                let grd = pt.context.createLinearGradient(0, 0, 0, pt.canvas.height);
                grd.addColorStop(0, ts.gradientFromColour);
                grd.addColorStop(1, ts.gradientToColour);
                pt.fill = grd;
            }
            this.localeTextArray.push(pt);
            return pt;
        }


        public getStringByIdentifier(identifier: string | number): string {
            let text: string = '';
            if(this.strings[identifier]) {
                if(this.strings[identifier][this._currLocale]) {
                    text = this.strings[identifier][this._currLocale].text;
                } else {
                    text = 'NO LOCALE';
                }
            }

            //let text: string = this.strings[identifier] ? this.strings[identifier][this._currLocale].text : '';
            return text;
        }


        public changeLocaleAndRefresh(locale: string): void {
            this._currLocale = locale;
            for (let i: number = 0; i < this.localeTextArray.length; i++) {
                this.localeTextArray[i].text = this.getStringByIdentifier(this.localeTextArray[i].identifier);
            }
            this._actionSignal.dispatch({ action:'changeLocale', data:this._currLocale });
        }
        
        public get currLocale():string {
            return this._currLocale;
        }

        public get actionSignal():Phaser.Signal {
            return this._actionSignal;
        }

        /*public set currLocale(value:string) {
            this._currLocale = value;
        }*/
    }
}
