﻿module TypeScriptPhaserSlot.common {

    export class LocaleText extends Phaser.Text {

        public identifier: string | number;

        constructor(game: Phaser.Game, x: number, y: number, text: string, style: Phaser.PhaserTextStyle, identifier: string | number) {
            super(game, x, y, text, style);
            this.identifier = identifier;
        }

        
    }
}
