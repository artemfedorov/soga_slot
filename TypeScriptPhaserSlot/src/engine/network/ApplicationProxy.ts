﻿///<reference path="../controller/BaseController.ts" />

module TypeScriptPhaserSlot.common {

    export class ApplicationProxy extends BaseController implements IController {

        protected _getGameStateSignal: Phaser.Signal;
        protected _spinResponseSignal: Phaser.Signal;
        protected _bonusGameResponseSignal: Phaser.Signal;

        protected apiController: IAPIController;



        constructor(controller: IAPIController, name: string, debugMode: boolean) {
            super(name, debugMode);
            this._spinResponseSignal = new Phaser.Signal();
            this._getGameStateSignal = new Phaser.Signal();
            this._bonusGameResponseSignal = new Phaser.Signal();
            this.apiController = controller;
            this.bindToAPIController();
        }

        /**
         * APIcontroller requests
         */

        /**
         * Initial information to start the particular game
         * @data? any
         */
        public getGameState(gameName: string): void {
            this.apiController.getGameState(gameName);
        }

        protected onGameState(data: GameStateResponse): void {
            this.debuglog('onGameState' + data);
            this.getGameStateSignal.dispatch(data);
        }

        /**
         * Spin request to apicontroller
         * @param data
         */
        public spin(data:any): void {
            this.apiController.spin(data);
        }

        public respin(): void {
            this.apiController.respin();
        }

        /**
         * Freespin request to apicontroller
         * @param coins: any actual information which goes to server like:
         *  (coins, points)
         */
        public freespin(coins: number): void {
            this.apiController.freespin(coins);
        }

        protected onSpin(data: SpinResponse): void {
            this.debuglog('onSpin');
            this._spinResponseSignal.dispatch(data);
        }


        /**
         * After getting any winning sends the request to collect up the current winning
         * @param data
         */
        public collect(data?: any): void {
            this.apiController.collect(data);
        }

        protected onCollect(data: any): void {
            this.debuglog('onCollect');
            this._spinResponseSignal.dispatch(data);
        }

         /**
         * Sends player choice information
         * @param data
         */
        public gamble(data?: any): void {
            this.apiController.collect(data);
        }

        protected onGamble(data: any): void {
            this.debuglog('onGamble');
            this._spinResponseSignal.dispatch(data);
        }

        /**
         * Sends bonus game information
         * @param data
         */
        public bonus(data: any): void {
            this.apiController.bonus(data);
        }

        protected onBonus(data: BonusResponse): void {
            this.debuglog('onBonus');
            this._bonusGameResponseSignal.dispatch(data);
        }

        /**
         * get exit
         * @param data
         */
        public exit(data?: any): void {
            this.apiController.collect(data);
        }

        protected onExit(data: any): void {
            this.debuglog('onExit');
            this._spinResponseSignal.dispatch(data);
        }


        protected bindToAPIController(): void {
            this.apiController.bonusGame_signal.add(this.onBonus, this);
            this.apiController.gameState_signal.add(this.onGameState, this);
            this.apiController.spin_signal.add(this.onSpin, this);
            this.apiController.collect_signal.add(this.onCollect, this);
            this.apiController.gamble_signal.add(this.onGamble, this);
            this.apiController.exit_signal.add(this.onExit, this);
        }

         /************************************************************
            Getters and Setters
        *************************************************************/
        
        public get spinResponseSignal(): Phaser.Signal {
            return this._spinResponseSignal;
        }

        public get bonusGameResponseSignal(): Phaser.Signal {
            return this._bonusGameResponseSignal;
        }

        public get getGameStateSignal(): Phaser.Signal {
            return this._getGameStateSignal;
        }
    }
}