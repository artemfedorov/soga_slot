﻿module TypeScriptPhaserSlot.common {

    export interface IAPIController extends IController{

        gameList_signal: Phaser.Signal;
        gameState_signal: Phaser.Signal;
        spin_signal: Phaser.Signal;
        collect_signal: Phaser.Signal;
        gamble_signal: Phaser.Signal;
        exit_signal: Phaser.Signal;
        bonusGame_signal: Phaser.Signal;

        getGameState(data?: any): void;
        spin(data?: any): void;
        respin(data?: any): void;
        freespin(data?: any): void;
        collect(data?: any): void;
        gamble(data?: any): void;
        exit(data?: any): void;
        bonus(bonusType: string): void;

    }
}  