﻿module TypeScriptPhaserSlot.common {

    export class BoongoSpinResponse implements IResponse {
        protected _status: number;
        protected _commands: any[];
        protected _jackpot: Jackpot;
        protected _results: SpinResult[];
        protected _win: number;
        protected _totalWin: number;
        protected _state: State;
        protected _user: User;
        protected _serverInfo: ServerInfo;
        protected _rawData: any;
        protected _betPerLine: number;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): BoongoSpinResponse {
            //this._status = rawData.status;
            //this._commands = rawData.commands;
            //this._jackpot = new Jackpot(rawData.response.jackpot);
            this._rawData = rawData;

            let source:any = rawData.context.spins || rawData.context.freespins;

            this._results = [];
            this._results.push(new BoongoSpinResult(rawData));

            this._win = source.round_win;
            this._totalWin = source.round_win;

            let fakeStateData: any = {};
            //fakeStateData.freespins = rawData.bonuses[0].amount || 0;
            //fakeStateData.freespins = rawData.context.freespins || 0;

            fakeStateData.freespins = 0;
            if(rawData.context.freespins) {
                fakeStateData.freespins = rawData.context.freespins.rounds_left;
            } else if(source.winscatters[0]) {
                fakeStateData.freespins = source.winscatters[0].freespins;
            }

            fakeStateData.points = 0;
            fakeStateData.hasBonus = false;
            fakeStateData.coins = rawData.user.balance;
            fakeStateData.total = source.total_win || source.round_win;

            this._state = new State(fakeStateData);
            //this._user = new User(rawData.response.user);
            //this._serverInfo = new ServerInfo(rawData.serverInfo);

            this._betPerLine = source.bet_per_line;

            return this;

            /*this._status = rawData.status;
            this._commands = rawData.commands;
            this._jackpot = new Jackpot(rawData.response.jackpot);
            
            this._results = [];
            for (let i: number = 0; i < rawData.response.result.length; i++) {
                this._results.push(new SpinResult(rawData.response.result[i]));
            }
            this._win = rawData.response.win;
            this._totalWin = rawData.response.totalWin;
            this._state = new State(rawData.response.state);
            this._user = new User(rawData.response.user);
            this._serverInfo = new ServerInfo(rawData.serverInfo);
            return this;*/
        }

        public get status(): number {
            return this._status;
        }
        public get commands(): any[] {
            return this._commands;
        }
        public get jackpot(): Jackpot {
            return this._jackpot;
        }
        public get results(): SpinResult[] {
            return this._results;
        }
        public get win(): number {
            return this._win;
        }
        public get totalWin(): number {
            return this._totalWin;
        }
        public get state(): State {
            return this._state;
        }
        public get user(): User {
            return this._user;
        }
        public get serverInfo(): ServerInfo {
            return this._serverInfo;
        }
        public get rawData(): any {
            return this._rawData;
        }
        public get betPerLine(): number {
            return this._betPerLine;
        }
    }
}