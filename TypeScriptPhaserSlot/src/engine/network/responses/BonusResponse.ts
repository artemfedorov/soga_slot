﻿module TypeScriptPhaserSlot.common {

    export class BonusResponse implements IResponse {

        protected _data:any;
        protected _freespins:number;
        protected _hasBonus:boolean;
        protected _totalWin:number;
        protected _coins:number;
        protected _multiplier:number;

        constructor(rawData: any) {
            this.parse(rawData); 
        }

        protected parse(rawData: any): BonusResponse {
            this._data = rawData;
            this._hasBonus = rawData.state == 1 || rawData.state == 2;
            this._totalWin = rawData.winnings ? rawData.winnings.total : rawData.winning;
            this._coins = rawData.balance;
            this._multiplier = 1;
            if(rawData.bonuses_summary) {
                this._freespins = rawData.bonuses_summary.freespins_remainder || 0;
            }
            if(rawData.additional_info) {
                this._multiplier = rawData.additional_info.multiplier || 1;
            }

            return this;
        }

        public get data(): any {
            return this._data;
        }
        public get hasBonus(): boolean {
            return this._hasBonus;
        }
        public get freespins(): number {
            return this._freespins;
        }
        public get totalWin(): number {
            return this._totalWin;
        }
        public get coins(): number {
            return this._coins;
        }
        public get multiplier(): number {
            return this._multiplier;
        }
    }
}