﻿module TypeScriptPhaserSlot.common {

    export class SpinResult {

        protected _freespin_icons: number[];
        protected _bet: number;
        protected _payout: number;
        protected _paylines: Payline[];
        protected _scatters: Scatter[];
        protected _reels: number[][];
        protected _mult: number;
        protected _rawData:any;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): SpinResult {
            this._freespin_icons = rawData.freespin_icons;
            this._bet = rawData.bet;
            this._payout = rawData.payout;
            this._reels = rawData.reels;
            this._mult = rawData.mult;
            this._paylines = [];
            this._rawData = rawData;
            for (let i: number = 0; i < rawData.paylines.length; i++) {
                this._paylines.push(new Payline(rawData.paylines[i]));
            }
            return this;
        }

        public get freespin_icons(): number[] {
            return this._freespin_icons;
        }
        public get bet(): number {
            return this._bet;
        }
        public get payout(): number {
            return this._payout;
        }
        public get paylines(): Payline[] {
            return this._paylines;
        }
        public get scatters(): Scatter[] {
            return this._scatters;
        }
        public get reels(): number[][] {
            return this._reels;
        }
        public get mult(): number {
            return this._mult;
        }
        public get rawData(): any {
            return this._rawData;
        }
    }

    export class Payline {

        protected _index: number;
        protected _offset: number;
        protected _direction: string;
        protected _line: number[];
        protected _icon: number;
        protected _icon_count: number;
        protected _coin_multiplier: number;
        protected _sum: number;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): Payline {
            this._index = rawData.index;
            this._offset = rawData.offset;
            this._direction = rawData.direction;
            this._line = rawData.line;
            this._icon = rawData.icon;
            this._icon_count = rawData.icon_count;
            this._coin_multiplier = rawData.coin_multiplier;
            this._sum = 0;
            return this;
        }

        public get index(): number {
            return this._index;
        }
        public get offset(): number {
            return this._offset;
        }
        public get direction(): string {
            return this._direction;
        }
        public get line(): number[] {
            return this._line;
        }
        public get icon(): number {
            return this._icon;
        }
        public get icon_count(): number {
            return this._icon_count;
        }
        public get coin_multiplier(): number {
            return this._coin_multiplier;
        }
        public get sum(): number {
            return this._sum;
        }
    }

    export class Scatter {

        protected _icon: number;
        protected _icon_count: number;
        protected _coin_multiplier: number;
        protected _indexes: number[][];
        protected _sum: number;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): Scatter {
            this._icon = rawData.icon || 1;
            this._icon_count = rawData.icon_count;
            this._coin_multiplier = rawData.coin_multiplier;
            this._indexes = rawData.indexes;
            this._sum = rawData.sum;
            return this;
        }

        public get icon(): number {
            return this._icon;
        }
        public get icon_count(): number {
            return this._icon_count;
        }
        public get coin_multiplier(): number {
            return this._coin_multiplier;
        }
        public get indexes(): number[][] {
            return this._indexes;
        }
        public get sum(): number {
            return this._sum;
        }
    }
}