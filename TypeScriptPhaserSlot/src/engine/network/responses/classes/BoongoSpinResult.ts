﻿///<reference path="./SpinResult.ts" />

module TypeScriptPhaserSlot.common {

    export class BoongoSpinResult extends SpinResult {

        protected parse(rawData: any): SpinResult {
            //this._freespin_icons = rawData.freespin_icons;
            //this._bet = rawData.bet;

            let souce:any = rawData.context.spins || rawData.context.freespins;

            this._payout = souce.round_win;
            this._reels = souce.board;
            //this._mult = rawData.mult;
            this._paylines = [];
            this._scatters = [];

            for (let i: number = 0; i < souce.winlines.length; i++) {
                let winItem: any = souce.winlines[i];
                this._paylines.push(new BoongoPayline(winItem));
            }

            for (let j: number = 0; j < souce.winscatters.length; j++) {
                let winItem: any = souce.winscatters[j];
                this._scatters.push(new BoongoScatter(winItem));

                /*let scatters:any = souce.winscatters[0];
                for (let k: number = 0; k < scatters.occurrences; k++) {
                    let obj: any = {
                        icon: 11,
                        icon_count: 3,
                        coin_multiplier: 1,
                        indexes: [[0, 1], [1, 1], [2, 1],]
                    };
                    this._scatters.push(new Scatter(obj));
                }*/
            }

            /*this._freespin_icons = rawData.freespin_icons;
            this._bet = rawData.bet;
            this._payout = rawData.payout;
            this._reels = rawData.reels;
            this._mult = rawData.mult;
            this._paylines = [];
            for (let i: number = 0; i < rawData.paylines.length; i++) {
                this._paylines.push(new Payline(rawData.paylines[i]));
            }*/

            return this;
        }
    }

    export class BoongoPayline extends Payline {

        protected parse(rawData: any): Payline {
            this._index = rawData.line - 1;
            this._offset = 0;
            this._direction = "left";
            this._line = [];
            for(let i:number = 0; i < 5; i++) {
                let dataForPush:number = 0;
                if(rawData.positions[i]) {
                    dataForPush = rawData.positions[i][1];
                }
                this._line.push(dataForPush);
            }
            this._icon = rawData.symbol;
            this._icon_count = rawData.occurrences;
            this._coin_multiplier = rawData.multiplier;
            this._sum = rawData.amount;

            return this;
        }
    }

    export class BoongoScatter extends Scatter {

        protected parse(rawData: any): Scatter {
            this._icon = rawData.symbol;
            this._icon_count = rawData.occurrences;
            this._coin_multiplier = rawData.coin_multiplier;
            this._indexes = rawData.positions;
            return this;
        }
    }
}