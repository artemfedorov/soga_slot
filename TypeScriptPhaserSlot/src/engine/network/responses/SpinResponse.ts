﻿module TypeScriptPhaserSlot.common {

    export class SpinResponse implements IResponse {
        protected _status: number;
        protected _commands: any[];
        protected _jackpot: Jackpot;
        protected _results: SpinResult[];
        protected _win: number;
        protected _totalWin: number;
        protected _multiplier: number;
        protected _state: State;
        protected _user: User;
        protected _serverInfo: ServerInfo;
        protected _rawData:any;
        protected _bonusResponce:BonusResponse;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): SpinResponse {
            this._status = rawData.status;
            this._commands = rawData.commands;
            this._jackpot = new Jackpot(rawData.response.jackpot);
            
            this._results = [];
            for (let i: number = 0; i < rawData.response.result.length; i++) {
                this._results.push(new SpinResult(rawData.response.result[i]));
            }
            this._win = rawData.response.win;
            this._totalWin = rawData.response.totalWin;
            this._multiplier = 1;
            this._state = new State(rawData.response.state);
            this._user = new User(rawData.response.user);
            this._serverInfo = new ServerInfo(rawData.serverInfo);
            this._rawData = rawData;

            return this;
        }

        public get status(): number {
            return this._status;
        }
        public get commands(): any[] {
            return this._commands;
        }
        public get jackpot(): Jackpot {
            return this._jackpot;
        }
        public get results(): SpinResult[] {
            return this._results;
        }
        public get win(): number {
            return this._win;
        }
        public get totalWin(): number {
            return this._totalWin;
        }
        public get multiplier(): number {
            return this._multiplier;
        }
        public get state(): State {
            return this._state;
        }
        public get user(): User {
            return this._user;
        }
        public get serverInfo(): ServerInfo {
            return this._serverInfo;
        }
        public get rawData(): any {
            return this._rawData;
        }
        public get bonusResponce(): BonusResponse {
            return this._bonusResponce;
        }
    }

    export class User {

        protected _coins: number;
        protected _points: number;
        protected _actCS: number;
        protected _pWinSpins: number;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): User {
            this._coins = rawData.coins;
            this._points = rawData.points;
            this._actCS = rawData.actCS;
            this._pWinSpins = rawData.pWinSpins;
            return this;
        }

        public get coins(): number {
            return this._coins;
        }
        public get points(): number {
            return this._points;
        }
        public get actCS(): number {
            return this._actCS;
        }
        public get pWinSpins(): number {
            return this._pWinSpins;
        }
    }

    export class State {

        protected _freespins: number;
        protected _bonusTotalWin: number;
        protected _points: number;
        protected _hasBonus: boolean;
        protected _coins: number;
        protected _bonusResponce: BonusResponse;
        
        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): State {
            console.log(rawData as string);

            this._freespins = rawData.freespins;
            this._bonusTotalWin = rawData.total;
            this._points = rawData.points;
            this._hasBonus = rawData.hasBonus;
            this._coins = rawData.coins;
            return this;
        }

        public get coins(): number {
            return this._coins;
        }
        public get freespins(): number {
            return this._freespins;
        }
        public get bonusTotalWin(): number {
            return this._bonusTotalWin;
        }
        public get points(): number {
            return this._points;
        }
        public get hasBonus(): boolean {
            return this._hasBonus;
        }
    }

    export class ServerInfo {

        protected _time: number;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): ServerInfo {
            this._time = rawData.time;
            return this;
        }

        public get time(): number {
            return this._time;
        }
    }

    export class Jackpot {

        protected _winners: Winner[];

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): Jackpot {
            this._winners = [];
            for (let i: number = 0; i < rawData.winners.length; i++) {
                this._winners.push(new Winner(rawData.winners[i]));
            }
            return this;
        }

        public get winners(): Winner[] {
            return this._winners;
        }
    }

    export class Bonus {

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): Bonus {

            return this;
        }
    }

    export class Winner {

        protected _id: number;
        protected _type: string;
        protected _slotId: string;
        protected _userId: string;
        protected _winAmount: number;
        protected _name: string;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        protected parse(rawData: any): Winner {
            this._id = rawData.id;
            this._type = rawData.type;
            this._slotId = rawData.slotId;
            this._userId = rawData.userId;
            this._winAmount = rawData.winAmount;
            this._name = rawData.name;
            return this;
        }

        public get id(): number {
            return this._id;
        }
        public get type(): string {
            return this._type;
        }
        public get slotId(): string {
            return this._slotId;
        }
        public get userId(): string {
            return this._userId;
        }
        public get winAmount(): number {
            return this._winAmount;
        }
        public get name(): string {
            return this._name;
        }
    }
}