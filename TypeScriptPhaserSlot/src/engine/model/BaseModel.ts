﻿module TypeScriptPhaserSlot.common {

    /**
     * Base abstract Model created in the aspect of the concept MVC.
      The Model gets control the main states of the Game by State Manager like:
      Spin State, Collect State and so on.
      Also the Model can manage the whole list of resources and saves logic data.
      The Model sends signals and gets callbacks when works are done.
     */
    export abstract class BaseModel implements IModel {

        protected _currentStateIndex: number;
        protected _newStateIndex: number;
        protected _debugMode: boolean;
        protected _notEnoughMoney: boolean = false;
        protected _currentState: IState;
        protected _newState: IState;
        protected _stateList: Array<IState>;

        //The signal which should be dispatched when Model State has changed
        protected _modelSignal: Phaser.Signal;
        protected _notEnoughMoneySignal: Phaser.Signal;

        /**
         * Model constructor
         * @param stateList: Model should get the ordered list of actual states to manage them.
         */
        constructor(debugMode: boolean = false) {
            this.debugMode = debugMode;
        }


        /**
         * State manager list
         * @param stateList 
         */
        public setStateList(stateList: Array<IState>): void {
            this._stateList = stateList;
        }

        /**
         * Binding all signals from list of the states which designed for the interaction with Model
           e.g. finishWorkSignal - is Phaser.Signal which is going be dispatched when the current state is done.
           and Model have to try set next state up.
         */
        protected bindingStatesSignals(): void {
            for (let i: number = 0; i < this.stateList.length; i++) {
                this.stateList[i].startWorkSignal.remove(this.acceptState, this);
                this.stateList[i].finishWorkSignal.remove(this.nextState, this);
                this.stateList[i].cancelSignal.remove(this.nextState, this);

                this.stateList[i].startWorkSignal.add(this.acceptState, this);
                this.stateList[i].finishWorkSignal.add(this.nextState, this);
                this.stateList[i].cancelSignal.add(this.nextState, this);
            }
        }


        /**
         * Called when current state allows to be run
         * Acceptation state and doing according work
         * Model dispatches about that to anywhere
         * @param state
         * @param data
         * @param isRequiredAction
         */
        protected acceptState(state: IState, data: any, isRequiredAction: boolean): void {
            this.debugLog("starts work " + state.name);
            this._currentState = state;
            this._currentStateIndex = this.getStateIndexByName(this._currentState.name);
            this.modelSignal.dispatch(state, data, isRequiredAction);
        }

        /**
         * Check and set next state up.
         */
        protected nextState(isFinished: boolean): void {
            if (this.debugMode) console.log("----------------------------------------------------------------------------------");
            if(this.notEnoughMoney) {
                this._notEnoughMoneySignal.dispatch();
                this._newState = this.getStateByName(this._newState.gotoStateName);
                this._newStateIndex = this.getStateIndexByName(this._newState.name);
                this._newState.tryWork();
                this.debugLog("goto to state " + this._newState.name);
                this.notEnoughMoney = false;
                return;
            }
            if (isFinished && this._newState && this._newState.gotoStateName && this._newState.checkConditionForGotoState()) {
                this._newState = this.getStateByName(this._newState.gotoStateName);
                this._newStateIndex = this.getStateIndexByName(this._newState.name);
                this.debugLog("goto to state " + this._newState.name);
                this._newState.tryWork();
                return;
            }
            if (this.stateList.length - 1 >= this._newStateIndex + 1) {
                this._newStateIndex++;
            } else {
                this._newStateIndex = 0;
            }
            this._newState = this._stateList[this._newStateIndex];
            this.debugLog("switching to " + this._newState.name);
            this._newState.tryWork();
        }

        /**
         * Specific console logger
         * @param message
         */
        protected debugLog(message: string): void {
            console.log("MODEL:", message);
        }

        /**
         *
         */
        public goToState(name:string):void {
            this._newState = this.getStateByName(name);
            this._newStateIndex = this.getStateIndexByName(name);
            this._newState.tryWork();
        }

        /**
         * Search state <IState> by name in the state list 
         * @param {string} stateName
         * @returns
         */
        public getStateByName(stateName: string): IState {
            for (let i: number = 0; i < this.stateList.length; i++) {
                if (this.stateList[i].name == stateName) {
                    return this.stateList[i];
                }
            }
            // this.debugLog("getStateByName " + stateName + " no found");
            return null;
        }

        public getStateIndexByName(stateName: string): number {
            for (let i: number = 0; i < this.stateList.length; i++) {
                if (this.stateList[i].name == stateName) {
                    return i;
                }
            }
            return null;
        }


        /************************************************************
            Getters and Setters
        *************************************************************/

        public get currentStateIndex(): number {
            return this._currentStateIndex;
        }

        public get newStateIndex(): number {
            return this._newStateIndex;
        }

        public get currentState(): IState {
            return this._currentState;
        }

        public get newState(): IState {
            return this._newState;
        }

        public get stateList(): Array<IState> {
            return this._stateList;
        }

        public get notEnoughMoney(): boolean {
            return this._notEnoughMoney;
        }
        public get debugMode(): boolean {
            return this._debugMode;
        }
        public set notEnoughMoney(value: boolean) {
            this._notEnoughMoney = value;
        }

        public get modelSignal(): Phaser.Signal {
            return this._modelSignal;
        }

        public get notEnoughMoneySignal(): Phaser.Signal {
            return this._notEnoughMoneySignal;
        }
        public set debugMode(value: boolean) {
            this._debugMode = value;
        }


    }
}