﻿module TypeScriptPhaserSlot.common {

    export interface IModel {

        currentStateIndex: number;
        newStateIndex: number;

        stateList: Array<IState>;
        modelSignal: Phaser.Signal;
        notEnoughMoneySignal: Phaser.Signal;

        currentState: IState;
        newState: IState;

        debugMode: boolean;

        goToState(stateName: string):void;
        getStateIndexByName(stateName: string): number;
        getStateByName(stateName: string): IState;

    }
}