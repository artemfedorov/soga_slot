﻿///<reference path="BaseModel.ts" />

module TypeScriptPhaserSlot.common {

    export class GameModel extends BaseModel implements IGameModel {

        protected gameState: any;

        /*
        Spins number
        */
        protected _isGamble: boolean;
        /*
        Spins number
        */
        protected _isFreespins: boolean;
        /*
           Spins number
        */
        protected _isAutoPlay: boolean;

        /*
           Spins number
        */
        protected _spinsNumber: number;

        /*
            Current index in allowedBets<Array>
            Shows what is current bet:  = allowedBets[_currBetIndex]
        */
        protected _currBetIndex: number;
        protected _currLinesIndex: number;

        /*
            List of the allowed bets gets from server by <GameStateResponse>
        */
        protected _initialBets: Array<number>;
        protected _allowedBets: Array<number>;
        protected _allowedLines: Array<number>;

        /*
            Optional parameter which provides ability to parse more than one result in one server response.
        */
        protected _currResultIndex: number;

        /*
            List of the results gets by <SpinResponse>
        */
        protected _results: Array<SpinResult>;

        /*
            Freespin game information gets by <GameStateResponse> and <SpinResponse>
        */
        protected _freespins: number;


        protected _freeSpinsAdded: number;


        protected _freeSpinsTotal: number;

        /*
            Having Bonus game information gets by <GameStateResponse> and <SpinResponse>
        */
        protected _hasBonus: boolean;

        /*
            Having Bonus game information gets by <BonusResponse>
        */
        protected _bonusResponse: BonusResponse;

        /*
            Coins or any related resources
        */
        protected _coins: number;

        /*
            Parameter shows how many lines are enabled/choosed currently
        */
        protected _activeLines: number;

        /*
            Parameter shows how many lines are available in the Game
        */
        protected _totalLines: number;

        /*
            Parameter shows actual total winning amount
            paylines and jackpot
        */
        protected _totalWin: number;


        protected _bonusTotalWin: number;

        /*
            Parameter shows paylines winning amount only
        */
        protected _win: number;

        /*
            Parameter shows coins for cashBack
        */
        protected _cashBackCoins: number;
        /*
            List of server commands
        */
        protected _commands: any;

        protected _spinResponse: SpinResponse;

        protected _denomination: number;
        protected _multiplier: number;

        protected _rawData: any;

        protected _betChangedSignal: Phaser.Signal;
        protected _linesChangedSignal: Phaser.Signal;
        protected _autoChangedSignal: Phaser.Signal;

        /**
         * debugMode
         * @param debugMode
         */
        constructor(debugMode: boolean = false) {
            super(debugMode);
            this._isAutoPlay = false;
            this._spinsNumber = 0;
            this._notEnoughMoneySignal = new Phaser.Signal();
            this._modelSignal = new Phaser.Signal();
            this._betChangedSignal = new Phaser.Signal();
            this._linesChangedSignal = new Phaser.Signal();
            this._autoChangedSignal = new Phaser.Signal();

            this._denomination = 1;
            this._multiplier = 1;

            window['model'] = this;
        }

        /** 
         * Get spin response
         */
        public onSpinResponse(data: SpinResponse): void {
            this._bonusResponse = data.bonusResponce || null;
            this._freeSpinsAdded = data.state.freespins > this._freespins ? data.state.freespins - (this._freespins - 1) : 0;
            if(this._freespins == 0) {
                this._freeSpinsAdded = 0;
            }
            this._freespins = data.state.freespins;
            this._freeSpinsTotal = 0;
            this._hasBonus = data.state.hasBonus;
            if(data.state.coins != null && data.state.coins != undefined) {
                this._coins = data.state.coins;
            }
            this._currResultIndex = 0;
            this._spinResponse = data;
            this._win = data.win;
            this._totalWin = data.totalWin;
            this._multiplier = data.multiplier;
            this._bonusTotalWin = data.state.bonusTotalWin;
            this._spinsNumber++;
            this._rawData = data.rawData;

            let bet:number = data['betPerLine'] || this._allowedBets[this.currBetIndex];
            this.correctBets(bet);

            this.debugLog("spin #" + this._spinsNumber);
        }

        /** 
         * Get collect response (optional)
         */
        public onCollectResponse(data: any): void {

        }

        /** 
         * Get bonus game response (optional)
         */
        /*public onBonusGameResponse(data: BonusResponse): void {
            this._bonusResponse = data;

            this._freespins = data.freespins || this._freespins;
            //this._hasBonus = data.hasBonus || this._hasBonus;
        }*/

        public onBonusGameResponse(data: BonusResponse): void {
            this._hasBonus = true;
            this._bonusResponse = data;

            this._totalWin = data.totalWin;
            this._coins = data.coins || this._coins;
            if(data.multiplier) {
                this._multiplier = data.multiplier;
            }

            this._freespins = data.freespins || this._freespins;
        }

        public onBalanceChanged(data:number):void {
            this._coins = data;
        }

        /**
         *
         * @param value
         */
        public auto(value?:boolean): void {
            if(value != undefined) {
                this._isAutoPlay = value;
            } else {
                this._isAutoPlay = !this._isAutoPlay;
            }

            if (this._isAutoPlay && (this.currentState.name == StatesConstants.IDLE_STATE || this.currentState.name == StatesConstants.STOP_SHOW_WINNING_STATE)) {
                if(this.currentState.name == StatesConstants.IDLE_STATE && this.coins < this.totalBet && this.freespins == 0) {
                    console.log('');
                } else {
                    this.currentState.finishWork();
                }
            }

            this.autoChangedSignal.dispatch(this._isAutoPlay);
        }

        /** Once received. Get server game state information before running the Game State Mananger
          * Must be initiated by instance of GameController
          */
        public onGetGameState(data: GameStateResponse, goToNextState:boolean = true): void {
            this._freespins = data.freespins;
            if (this._freespins > 0) {
                this._isFreespins = true; 
            } else {
                this._isFreespins = false;
            }

            this._initialBets = data.bets.concat();

            this._allowedBets = data.bets;
            this._allowedLines = data.lines;
            this._hasBonus = data.hasBonus;

            this._currBetIndex = this._allowedBets.indexOf(data.initialBet);
            this._currLinesIndex = this._allowedLines.indexOf(data.initialLines);
            this._coins = data.coins;
            this._currentStateIndex = -1;
            this._denomination = data.denomination || 1;

            /*this.allowedBets.sort(function(a, b) {
                return a - b;
            });

            if(this._currBetIndex == -1) {
                this._currBetIndex = 0;
            }*/
            if(this._currLinesIndex == -1) {
                this._currLinesIndex = 0;
            }

            this.correctBets(data.initialBet);

            if(data.spinRespince) {
                this.onSpinResponse(data.spinRespince);
            }

            if(data.bonusRespince) {
                this.onBonusGameResponse(data.bonusRespince);
            }

            if(data.freespinResponse) {
                this.onSpinResponse(data.freespinResponse);
                this._isFreespins = this._freespins > 0;
            }

            this.bindingStatesSignals();
            if(goToNextState) {
                this.nextState(true);
            }

            this.setBetIndex(this.currBetIndex);
            this.setLinesIndex(this.currLinesIndex);
        }

        protected correctBets(bet:number):void {
            this._allowedBets = null;
            this._allowedBets = this._initialBets.concat();

            if (this._allowedBets.indexOf(bet) == -1) {
                this._allowedBets.push(bet);
            }

            this.allowedBets.sort(function(a, b) {
                return a - b;
            });

            this._currBetIndex = this._allowedBets.indexOf(bet);
            console.log(this._currBetIndex);
        }

        /**
         *
         * @param {number} bet
         */
        public setBet(bet:number): void {
            let betIndex:number = this._allowedBets.indexOf(bet);
            if(betIndex != -1) {
                this.setBetIndex(betIndex);
            }
        }

        /**
         * Change next allowed bet
         */
        public nextBet(): void {
            let betIndex:number = this._currBetIndex;
            if (betIndex + 1 < this._allowedBets.length) {
                betIndex++;
            } else {
                betIndex = 0;
            }
            this.setBetIndex(betIndex);
        }

        /**
         * Change previous allowed bet
         */
        public prevBet(): void {
            let betIndex:number = this._currBetIndex;
            if (betIndex - 1 >= 0) {
                betIndex--;
            } else {
                betIndex = this._allowedBets.length - 1;
            }
            this.setBetIndex(betIndex);
        }

        /**
         *
         * @param index
         */
        protected setBetIndex(index:number):void {
            this._currBetIndex = index;
            this.betChangedSignal.dispatch(this._allowedBets[this.currBetIndex] * this._denomination);
        }

        /**
         *
         * @param {number} lines
         */
        public setLines(lines:number): void {
            let lineIndex:number = this._allowedLines.indexOf(lines);
            if (lineIndex != -1) {
                this.setLinesIndex(lineIndex);
            }
        }

        /**
         * Change next allowed lines
         */
        public nextLine(): void {
            let lineIndex:number = this._currLinesIndex;
            if (lineIndex + 1 < this._allowedLines.length) {
                lineIndex++;
            } else {
                lineIndex = 0;
            }
            this.setLinesIndex(lineIndex);
        }

        /**
         * Change previous allowed lines
         */
        public prevLine(): void {
            let lineIndex:number = this._currLinesIndex;
            if (lineIndex - 1 >= 0) {
                lineIndex--;
            } else {
                lineIndex = this._allowedLines.length - 1;
            }
            this.setLinesIndex(lineIndex);
        }

        /**
         *
         * @param index
         */
        protected setLinesIndex(index:number):void {
            this._currLinesIndex = index;
            this.linesChangedSignal.dispatch(this._allowedLines[this.currLinesIndex]);
        }

        /**
         *
         * @param {boolean} changeLines
         * @param {boolean} changeBets
         */
        public maxBet(changeLines = true, changeBets = true): void {
            let lines:number = 0;
            let bet:number = 0;
            for (let i:number = this.allowedLines.length - 1; i >= (changeLines ? 0 : this.allowedLines.length - 1); i--) {
                lines = this.allowedLines[i];
                for (let j:number = this.allowedBets.length - 1; j >= (changeBets ? 0 : this.allowedLines.length - 1); j--) {
                    bet = this.allowedBets[j];
                    if(lines * bet <= this.coins) {
                        this.setLinesIndex(i);
                        this.setBetIndex(j);
                        return;
                    }
                }
            }
        }

        /************************************************************
            Getters and Setters
        *************************************************************/

        public get bonusResponse(): BonusResponse {
            return this._bonusResponse;
        }
        public get spinResponse(): SpinResponse {
            return this._spinResponse;
        }
        public get cashBackCoins():number {
            return this._cashBackCoins;
        }
        public get hasBonus(): boolean {
            return this._hasBonus;
        }
        public get isGamble(): boolean {
            return this._isGamble;
        }

        public get isAutoPlay(): boolean {
            return this._isAutoPlay;
        }
        public get isFreespins(): boolean {
            return this._isFreespins;
        }
        public  set isFreespins(value: boolean){
            this._isFreespins = value;
        }
        public get totalWin(): number {
            return this._totalWin;
        }
        public get bonusTotalWin(): number {
            return this._bonusTotalWin;
        }
        public get win(): number {
            return this._win;
        }
        public get spinsNumber(): number {
            return this._spinsNumber;
        }
        public get currBetIndex(): number {
            return this._currBetIndex;
        }
        public get currLinesIndex(): number {
            return this._currLinesIndex;
        }
        public get allowedBets(): Array<number> {
            return this._allowedBets;
        }
        public get allowedLines(): Array<number> {
            return this._allowedLines;
        }
        public get currResultIndex(): number {
            return this._currResultIndex;
        }
        public set currResultIndex(value: number) {
            this._currResultIndex = value;
        }
        public get results(): Array<SpinResult> {
            if(this._spinResponse && this._spinResponse.results) {
                return this._spinResponse.results;
            }
            return null;
        }
        public get result(): SpinResult {
            if (this.results && this._currResultIndex < this.results.length) {
                return this.results[this._currResultIndex];
            }
            return null;
        }
        public get freespins(): number {
            return this._freespins;
        }
        public get freeSpinsAdded(): number {
            return this._freeSpinsAdded;
        }
        public get freeSpinsTotal(): number {
            return this._freeSpinsTotal;
        }
        public get coins(): number {
            return this._coins;
        }
        public get activeLines(): number {
            return this._activeLines;
        }
        public get totalLines(): number {
            return this._totalLines;
        }

        public get betChangedSignal(): Phaser.Signal {
            return this._betChangedSignal;
        }

        public get linesChangedSignal(): Phaser.Signal {
            return this._linesChangedSignal;
        }

        public get autoChangedSignal(): Phaser.Signal {
            return this._autoChangedSignal;
        }

        public get totalBet(): number {
            return this._allowedBets[this.currBetIndex] * this._allowedLines[this.currLinesIndex] * this._denomination;
        }

        public get betPerLine(): number {
            return this._allowedBets[this.currBetIndex] * this._denomination;
        }

        public get denomination(): number {
            return this._denomination;
        }

        public get multiplier(): number {
            return this._multiplier;
        }

        public get rawData(): any {
            return this._rawData;
        }
    }
}