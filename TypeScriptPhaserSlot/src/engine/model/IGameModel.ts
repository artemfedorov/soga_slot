﻿module TypeScriptPhaserSlot.common {

    export interface IGameModel extends IModel {

        notEnoughMoney: boolean;

        totalWin: number;

        bonusTotalWin: number;

        win: number;

        isFreespins: boolean;

        hasBonus: boolean;

        bonusResponse: BonusResponse;

        isAutoPlay: boolean;

        currBetIndex: number;
        currLinesIndex: number;

        allowedBets: Array<number>;
        allowedLines: Array<number>;

        currResultIndex: number;

        results: Array<SpinResult>;

        freespins: number;

        freeSpinsAdded: number;

        freeSpinsTotal: number;

        auto(value?:boolean): void;

        coins: number;

        cashBackCoins: number;

        activeLines: number;

        totalLines: number;

        spinResponse: SpinResponse;

        onCollectResponse(data: any): void;

        onSpinResponse(data: SpinResponse): void;

        //collectResponse(data: any): void;

        result: SpinResult;

        onGetGameState(data: GameStateResponse, goToNextState?:boolean): void;

        onBonusGameResponse(data: BonusResponse): void;

        onBalanceChanged(data: number): void;

        setStateList(stateList: Array<IState>): void;

        isGamble: boolean;

        setBet(bet: number): void;

        nextBet(): void;

        prevBet(): void;

        setLines(lines: number): void;

        nextLine(): void;

        prevLine(): void;

        maxBet(changeLines?:boolean, changeBets?:boolean):void;

        betChangedSignal: Phaser.Signal;
        linesChangedSignal: Phaser.Signal;
        autoChangedSignal: Phaser.Signal;
        totalBet:number;
        multiplier:number;
        denomination:number;
        betPerLine:number;
        rawData:any;
    }
}