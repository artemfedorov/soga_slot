﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class EmotionsState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.EMOTIONS_STATE;
        }

        public checkData(): any {
            if (this.model.win > 0) {
                return this.model.spinResponse.results[this.model.currResultIndex];
            }
            return null;
        }

    }
}

