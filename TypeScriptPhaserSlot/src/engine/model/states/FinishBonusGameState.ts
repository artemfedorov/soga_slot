﻿///<reference path="./BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class FinishBonusGameState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.FINISH_BONUS_GAME_STATE;
        }

        public checkData(): any {
            if (this.model.hasBonus && this.model.bonusResponse) {
                return true; 
            }
            return null;
        }


    }
}

