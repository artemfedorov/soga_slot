﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class StartShowWinningState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.START_SHOW_WINNING_STATE;
        }

        public checkData(): any {
            //if (this.model.win > 0) {
            if (this.model.spinResponse.results[this.model.currResultIndex].paylines.length > 0 || this.model.spinResponse.results[this.model.currResultIndex].scatters.length > 0) {
                return this.model.spinResponse.results[this.model.currResultIndex];
            }
            return null;
        }

    }
}

