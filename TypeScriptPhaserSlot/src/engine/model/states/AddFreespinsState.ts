﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class AddFreespinsState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.ADD_FREESPINS_STATE;
        }

        public checkData(): boolean {
            if (this.model.freeSpinsAdded > 0 && this.model.isFreespins) {
                return true;
            }
            return null;
        }
    }
}

