﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class StopShowWinningState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.STOP_SHOW_WINNING_STATE;
        }

        public checkData(): any {
            if (this.model.result.paylines.length > 0 || this.model.result.scatters.length > 0) {
                return this.model.result.payout;
            }
            return null;

            /*if (this.model.totalWin > 0) {
                return this.model.result.payout;
            } else {
                return null;
            }*/
        }

    }
}

