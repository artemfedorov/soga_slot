﻿///<reference path="./BaseState.ts" />
module TypeScriptPhaserSlot.common {

    export class StopSpinState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.STOP_SPIN_STATE;
        }

        public checkData(): any {
            return this.model.spinResponse.results[this.model.currResultIndex];
            
        }
    }
}

