﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class StartRespinState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.START_RESPIN_STATE;
        }

        public checkData(): any {
            /*if (this.model.totalLines > 0) {
                return this.model.spinResponse.results[this.model.currResultIndex];
            }*/
            return this.model.totalWin > 0;
        }

        public checkConditionForGotoState(): boolean {
            return true;
        }

    }
}

