﻿module TypeScriptPhaserSlot.common {


    export interface IState {

        name: string;

        isRequiredAction: boolean;
        cancelSignal: Phaser.Signal;
        startWorkSignal: Phaser.Signal;
        finishWorkSignal: Phaser.Signal;
        gotoStateName:string;
        debugMode: boolean;

        finishWork(): void;
        tryWork(): void;

        checkData(): any;
        checkConditionForGotoState(): boolean;
    }
}