﻿///<reference path="./BaseState.ts" />
module TypeScriptPhaserSlot.common {

    export class GetRespinResponseState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.GET_RESPIN_RESPONSE_STATE;
        }

        public checkData(): any {
            super.checkData();
            return this.model.currentState.name == StatesConstants.START_RESPIN_STATE;
        }

    }
}