﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class StopRespinState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.STOP_RESPIN_STATE;
        }

        public checkData(): any {
            //return this.model.spinResponse.results[this.model.currResultIndex];
            if (this.model.currentState.name == StatesConstants.GET_RESPIN_RESPONSE_STATE) {
                return this.model.spinResponse.results[this.model.currResultIndex];
            }
            return null;
        }

        public checkConditionForGotoState(): boolean {
            return true;
            /*if (this.model.spinResponse.results[this.model.currResultIndex].paylines.length > 0) {
                return true;
            } else {
                return false;
            }*/
        }

    }
}

