﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class GambleState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.GAMBLE_STATE;
        }

        public checkData(): any {
            if (this.model.isGamble) {
                return true;
            }
            return null;
        }

    }
}

