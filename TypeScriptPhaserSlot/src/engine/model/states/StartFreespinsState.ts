﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class StartFreespinsState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.START_FREESPINS_STATE;
        }

        public checkData(): any {
            if (this.model.freespins > 0 && !this.model.isFreespins) {
                this.model.isFreespins = true;
                return true;
            }
            return null;
        }

    }
}

