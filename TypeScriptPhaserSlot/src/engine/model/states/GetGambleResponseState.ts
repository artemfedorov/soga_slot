﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class GetGambleResponseState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.GET_GAMBLE_RESPONSE_STATE;
        }

        public checkData(): any {
            return this.model.isGamble;
        }

        

    }
}

