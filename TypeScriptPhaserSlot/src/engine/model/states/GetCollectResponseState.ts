﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class GetCollectResponseState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.GET_COLLECT_RESPONSE_STATE;
        }

        public checkData(): any {
            if (this.model.totalWin > 0) {
                this.debuglog("totalWin " + this.model.totalWin);
                return true;
            }
        }

    }
}

