﻿///<reference path="./BaseState.ts" />
module TypeScriptPhaserSlot.common {

    export class StartSpinState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.START_SPIN_STATE;
        }

        public checkData(): any {
            return true;
            
        }
    }
}

