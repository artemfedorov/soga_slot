﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class CheckBalanceState extends BaseState implements IState {


        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.CHECK_BALANCE_STATE;
        }

        public checkData(): any {
            if(window['GR']) {
                return false;
            }

            if (this.model.coins >= this.model.totalBet || this.model.isFreespins) {
                this.model.notEnoughMoney = false;
                return null;
            }
            this.model.notEnoughMoney = true;
            return true;
        }
    }
}