﻿//  Base abstract State created in the aspect of the concept "State Manager" 
//  and designed for Model (MVC);
//  Inhereted class should be incapsuled state of the game where could be exists
//  all conditions to make a decision to turning on/off according state. 
//  Example of possible states: Spin State, Collect State and so on.

module TypeScriptPhaserSlot.common {

    export abstract class BaseState implements IState {

        protected _name: string;
        protected _isAction: boolean;
        protected _gotoStateName: string;
        protected _debugMode: boolean;
        protected _model: IGameModel;
        protected _cancelSignal: Phaser.Signal;
        protected _finishWorkSignal: Phaser.Signal;
        protected _startWorkSignal: Phaser.Signal;
        protected config: any;
        protected _isRequiredAction: boolean;

        /**
         *
         * @param {TypeScriptPhaserSlot.common.IGameModel} model
         * @param {TypeScriptPhaserSlot.common.StateConfiguration} stateConfiguration
         * @param {boolean} debugMode
         */
        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            this._model = model;
            this._gotoStateName = stateConfiguration.gotoStateName;
            this._isRequiredAction = stateConfiguration.isRequiredAction;
            this._isAction = stateConfiguration.isAction;
            this.debugMode = debugMode;
            this._cancelSignal = new Phaser.Signal();
            this._startWorkSignal = new Phaser.Signal();
            this._finishWorkSignal = new Phaser.Signal();
        }


        /**
         *
         * @param data
         * @param {boolean} isRequiredAction
         */
        protected doWork(data: any, isRequiredAction: boolean): void {
            this.debuglog(" - do work. is RequiredAction: " + isRequiredAction);
            this.startWorkSignal.dispatch(this, data, isRequiredAction);
        }

        /**
         * Called when work is done or canceled conditions
         * However we can do a check before finishing work by checking some data (optional)
         */
        public finishWork(): void {
            this.debuglog("- finish work");
            this.finishWorkSignal.dispatch(true);
        }

        /**
         *
         */
        public cancelState(): void {
            this.debuglog("- CANCELED");
            this.cancelSignal.dispatch(false);
        }


        /**
         * 
         * @param data: All Data could be checked to get continious
         */
        public tryWork(): void {
            var resultData: any = this.checkData();
            if (resultData != undefined && resultData != null && resultData != false) {
                this.debuglog("- conditions PASSED")
            } else {
                this.debuglog("- conditions FAIL")
            }
            if (!this.isAction) {
                this.debuglog("- NO action");
            }

            if (resultData != undefined && resultData != null && resultData != false) {
                if (!this.isAction) {
                    this.finishWork();
                } else {
                    this.doWork(resultData, this.isRequiredAction);
                }
            } else {
                this.cancelState();
            }
        }
 
        /**
         * Checks all conditions to make a dicision is current state is completely done
         * @param data
         */
        public checkData(data?: any): any {
            //overriden class instance should return TRUE or FALSE depending on checking conditions 
            return true;
        }

        /**
         * Check a condition to use GotoState 
         * @returns
         */
        public checkConditionForGotoState(): boolean {
            //overriden class instance should return TRUE or FALSE depending on checking conditions 
            return false;
        }

        /**
         * Specific console logger
         * @param message
         */
        protected debuglog(message: string): void {
            if (this.debugMode) console.log(this.name, message);
        }

        /************************************************************
            Getters and Setters
        *************************************************************/

        public get gotoStateName(): string {
            return this._gotoStateName;
        }

        public get name(): string {
            return this._name;
        }

        public set name(name: string) {
            this._name = name;
        }

        public get model(): IGameModel {
            return this._model;
        }
        
        public get isAction(): boolean {
            return this._isAction;
        }

        public get isRequiredAction(): boolean {
            return this._isRequiredAction;
        }

        public get debugMode(): boolean {
            return this._debugMode;
        }

        public set debugMode(value: boolean) {
            this._debugMode = value;
        }

        public get cancelSignal(): Phaser.Signal {
            return this._cancelSignal;
        }

        public get finishWorkSignal(): Phaser.Signal {
            return this._finishWorkSignal;
        }

        public get startWorkSignal(): Phaser.Signal {
            return this._startWorkSignal;
        }

    }
}