﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class CollectState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.COLLECT_STATE;
        }

        public checkData(): any {
            if (this.model.totalWin > 0) {
                return true;
            }
            return null;
        }

    }
}

