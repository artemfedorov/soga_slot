﻿module TypeScriptPhaserSlot.common {

    export class StatesConstants {

        static IDLE_STATE: string = "IDLE_STATE";
        static PRESPIN_STATE: string = "PRESPIN_STATE";
        static GET_SPIN_RESPONSE_STATE: string = "GET_SPIN_RESPONSE_STATE";
        static START_SPIN_STATE: string = "START_SPIN_STATE";
        static SPIN_STATE: string = "SPIN_STATE";
        static GET_RESPIN_RESPONSE_STATE: string = "GET_RESPIN_RESPONSE_STATE";
        static START_RESPIN_STATE: string = "START_RESPIN_STATE";
        static STOP_RESPIN_STATE: string = "STOP_RESPIN_STATE";
        static STOP_SPIN_STATE: string = "STOP_SPIN_STATE";
        static START_FREESPINS_STATE: string = "START_FREESPINS_STATE";
        static FINISH_FREESPINS_STATE: string = "FINISH_FREESPINS_STATE";
        static ADD_FREESPINS_STATE: string = "ADD_FREESPINS_STATE";
        static GET_COLLECT_RESPONSE_STATE: string = "GET_COLLECT_RESPONSE_STATE";
        static COLLECT_STATE: string = "COLLECT_STATE";
        static START_SHOW_WINNING_STATE: string = "START_SHOW_WINNING_STATE";
        static STOP_SHOW_WINNING_STATE: string = "STOP_SHOW_WINNING_STATE";
        static GAMBLE_STATE: string = "GAMBLE_STATE";
        static GET_GAMBLE_RESPONSE_STATE: string = "GET_GAMBLE_RESPONSE_STATE";
        static CHECK_BALANCE_STATE: string = "CHECK_BALANCE_STATE";
        static EMOTIONS_STATE: string = "EMOTIONS_STATE";

        static PRINCESS_FROG_TRANSFORM_PRINCESS_STATE: string = "PRINCESS_FROG_TRANSFORM_PRINCESS_STATE";

        static REMOVE_SYMBOLS_STATE: string = "REMOVE_SYMBOLS_STATE";
        static RESPIN_STATE: string = "RESPIN_STATE";

        static START_BONUS_GAME_STATE: string = "START_BONUS_GAME_STATE";
        static SHOW_WINNING_BONUS_GAME_STATE: string = "SHOW_WINNING_BONUS_GAME_STATE";
        static IDLE_BONUS_GAME_STATE: string = "IDLE_BONUS_GAME_STATE";
        static FINISH_BONUS_GAME_STATE: string = "FINISH_BONUS_GAME_STATE";

    }
}