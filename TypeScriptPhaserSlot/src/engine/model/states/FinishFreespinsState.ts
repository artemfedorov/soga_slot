﻿///<reference path="BaseState.ts" />

module TypeScriptPhaserSlot.common {

    export class FinishFreespinsState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.FINISH_FREESPINS_STATE;
        }

        public checkData(): any {
            if (this.model.freespins == 0 && this.model.isFreespins) {
                this.model.isFreespins = false;
                return true;
            } else
            return null;
        }

    }
}

