﻿///<reference path="./BaseState.ts" />
module TypeScriptPhaserSlot.common {

    export class GetSpinResponseState extends BaseState implements IState {

        constructor(model: IGameModel, stateConfiguration: StateConfiguration, debugMode: boolean = false) {
            super(model, stateConfiguration, debugMode);
            this.name = StatesConstants.GET_SPIN_RESPONSE_STATE;
        }

        public checkData(): any {
            super.checkData();
            let data:object = {
                bet:this.model.allowedBets[this.model.currBetIndex] * this.model.denomination,
                lines:this.model.allowedLines[this.model.currLinesIndex],
                denomination: this.model.denomination
            };
            return data;
        }
    }
}