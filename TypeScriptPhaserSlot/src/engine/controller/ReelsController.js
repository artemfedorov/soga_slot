///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ReelsController = /** @class */ (function (_super) {
            __extends(ReelsController, _super);
            function ReelsController(game, name, layer, debugMode, symbolProperties, gameConfiguration, pattern, gameStateResponse) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.isPendingRespinComplete = false;
                _this._symbolsEnabled = true;
                _this.layer = layer;
                _this.gameStateResponse = gameStateResponse;
                _this.mapReel = gameStateResponse.reelMap;
                _this.mapReelFreespins = gameStateResponse.reelMapFreespins;
                _this.symbolProperties = symbolProperties;
                _this.mainPattern = pattern;
                _this.addedSymbols = [];
                _this.additionalSymbolsNumbers = [];
                _this.pattern = pattern.reels;
                _this.gameConfiguration = gameConfiguration;
                _this.trackedSymbolsCalculation = [];
                _this.trackedSymbols = _this.gameConfiguration.standartSymbolSetting.trackedSymbols;
                _this.init(gameStateResponse);
                return _this;
            }
            /**
             * Building reel views
             */
            ReelsController.prototype.init = function (gameStateResponse) {
                this.reelViews = [];
                //Creating the reels
                this.buildReels(gameStateResponse);
            };
            /**
             * Called by GameController once Respin State comes
             */
            ReelsController.prototype.respin = function (data) {
                this.respinData = data;
                //please override this gently
            };
            ReelsController.prototype.stopRespin = function (data) {
                this.respinData = data;
                //please override this gently
            };
            /**
             *
             */
            ReelsController.prototype.start = function (data) {
                this.isStopping = false;
                this.isUrgentStop = false;
                this.stopSpinData = null;
                this.justTouchedCounter = 0;
                this.stopCounter = 0;
                var t = 200 + Number(this.gameConfiguration.rollingMechanics.tweenToTop.time) * Phaser.Timer.SECOND;
                setTimeout(function () {
                    SoundController.instance.playSound('start_spin_sound', 'default', 0.1, true);
                }, t);
                this.spin(0);
            };
            /**
             *
             * @param {number} reelIndex
             */
            ReelsController.prototype.spin = function (reelIndex) {
                var _this = this;
                var delay;
                this.isUrgentStop = false;
                this.trackedSymbolsCalculation.length = 0;
                this.reelViews[reelIndex].start();
                if (reelIndex + 1 < this.reelViews.length) {
                    delay = this.gameConfiguration.reels[reelIndex].startTimeout * Phaser.Timer.SECOND;
                    if (delay == 0 || reelIndex == 0) {
                        this.spin(reelIndex + 1);
                    }
                    else {
                        setTimeout(function () {
                            _this.spin(reelIndex + 1);
                        }, delay);
                    }
                }
            };
            /**
             * Stop showing
             */
            ReelsController.prototype.stop = function (data) {
                var _this = this;
                var timeout = this.isUrgentStop ? 0 : 1000;
                this.stopSpinData = data;
                this.alignReels();
                this.handleTrackedSymbols();
                this.spinTimeout = setTimeout(function () {
                    _this.reelStop(data, 0);
                }, timeout);
            };
            ReelsController.prototype.alignReels = function () {
                var maxLengthReelIndex = 0;
                var amount;
                for (var i = 0; i < this.reelViews.length; i++) {
                    maxLengthReelIndex = this.reelViews[i].symbols.length > this.reelViews[maxLengthReelIndex].symbols.length ? i : maxLengthReelIndex;
                }
                amount = this.reelViews[maxLengthReelIndex].symbols.length;
                for (var i = 0; i < this.reelViews.length; i++) {
                    this.debuglog("total symbols in reel#" + i + " = " + this.reelViews[i].symbols.length);
                    var addSymbols = amount - this.reelViews[i].symbols.length;
                    this.additionalSymbolsNumbers[i] = addSymbols;
                }
            };
            /**
             *
             * @param data
             * @param reelIndex
             */
            ReelsController.prototype.reelStop = function (data, reelIndex) {
                var _this = this;
                this.isStopping = true;
                var timeout = this.isUrgentStop ? 0 : this.gameConfiguration.reels[reelIndex].stopTimeout;
                this.reelViews[reelIndex].stop(data.reels[reelIndex].concat(), this.additionalSymbolsNumbers[reelIndex]);
                if (reelIndex + 1 < this.reelViews.length) {
                    setTimeout(function () {
                        _this.reelStop(data, reelIndex + 1);
                    }, timeout);
                }
            };
            /**
             *
             */
            ReelsController.prototype.urgentStop = function () {
                this.isUrgentStop = true;
                if (this.spinTimeout) {
                    clearTimeout(this.spinTimeout);
                    if (!this.isStopping && this.stopSpinData) {
                        this.reelStop(this.stopSpinData, 0);
                    }
                }
            };
            /**
             * Callback stopping the reel
             * @param data
             */
            ReelsController.prototype.onReelStoped = function (data) {
                this.state.finishWork();
            };
            /**
             *
             * @param {GameStateResponse} gameStateResponse
             */
            ReelsController.prototype.buildReels = function (gameStateResponse) {
                this.layer.x = this.pattern.x;
                this.layer.y = this.pattern.y;
                this.borderLayer = new Phaser.Group(this.game);
                var reelsAmount = this.gameConfiguration.reels.length;
                var reelView;
                for (var i = 0; i < reelsAmount; i++) {
                    reelView = this.buildReel(i, this.gameConfiguration.gameName + String(i), this.debugMode, this.symbolProperties, this.gameConfiguration, this.pattern, gameStateResponse.reels[i].concat(), this.borderLayer);
                    this.layer.add(reelView);
                    this.reelViews.push(reelView);
                }
                this.layer.add(this.borderLayer);
            };
            /**
             * JUST TO BE OVERRIDEN
             * DO NOT CALL SUPER!
             * @param index
             * @param name
             * @param debugMode
             * @param symbolProperties
             * @param gameConfiguration
             * @param pattern
             * @param dataReel
             * @param borderLayer
             * @returns {IReelView}
             */
            ReelsController.prototype.buildReel = function (index, name, debugMode, symbolProperties, gameConfiguration, pattern, dataReel, borderLayer) {
                var reelView;
                reelView.justTouchedStopSignal.add(this.onJustTouchedReelStop, this);
                reelView.actionSignal.add(this.onReelFinnalyStopped, this);
                return reelView;
            };
            /**
             *
             */
            ReelsController.prototype.handleTrackedSymbols = function () {
                var symbolIndex;
                for (var reelIndex = 0; reelIndex < this.stopSpinData.reels.length; reelIndex++) {
                    for (var i = 0; i < this.stopSpinData.reels[reelIndex].length; i++) {
                        symbolIndex = this.stopSpinData.reels[reelIndex][i];
                        if (this.trackedSymbols.indexOf(symbolIndex) > -1) {
                            if (!this.trackedSymbolsCalculation[symbolIndex]) {
                                this.trackedSymbolsCalculation[symbolIndex] = 0;
                            }
                            this.trackedSymbolsCalculation[symbolIndex]++;
                            if (this.trackedSymbolsCalculation[symbolIndex] == this.gameConfiguration.standartSymbolSetting.trackedSymbolsMin &&
                                this.gameConfiguration.reels.length - (reelIndex + 1) > 0) {
                                for (var j = reelIndex + 1; j < this.gameConfiguration.reels.length; j++) {
                                    this.reelViews[j].isIntrigued = true;
                                }
                            }
                        }
                    }
                }
            };
            /**
             * Signals when particular reel touched the ground:) before ending.
             * @param {number} reelIndex
             */
            ReelsController.prototype.onJustTouchedReelStop = function (reelIndex) {
                if (reelIndex + 1 < this.reelViews.length) {
                    if (this.reelViews[reelIndex + 1].isIntrigued) {
                        this.startIntrigue(reelIndex + 1);
                    }
                }
                else {
                    this.stopIntrigue();
                }
                this.justTouchedCounter++;
                if (this.justTouchedCounter == this.gameConfiguration.reels.length) {
                    var stopSound = SoundController.instance.getSound("start_spin_sound", 'default');
                    if (stopSound) {
                        stopSound.stop();
                    }
                }
            };
            /**
             *
             * @param reelIndex
             */
            ReelsController.prototype.onReelFinnalyStopped = function (reelIndex) {
                if (this.isPendingRespinComplete) {
                    this.actionSignal.dispatch(ReelsController.RESPIN_COMPLETE);
                    this.isPendingRespinComplete = false;
                    return;
                }
                this.stopCounter++;
                if (this.stopCounter == this.gameConfiguration.reels.length) {
                    this.stopIntrigue();
                    this.actionSignal.dispatch(ReelsController.STOP);
                }
            };
            ReelsController.prototype.animateSymbols = function (data) {
                this.animateSymbolsOfLine(data.paylines[0]);
            };
            /**
             *
             * @param line
             */
            ReelsController.prototype.animateSymbolsOfLine = function (line) {
                this.stopAnimation(true);
                if (this._symbolsEnabled) {
                    this.hideShowNoWinSymbols(false);
                }
                var index;
                for (var j = line.offset; j < line.offset + line.icon_count; j++) {
                    index = line.line[j];
                    this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount].visible = false;
                }
            };
            ReelsController.prototype.animateScatters = function (scatter) {
                this.stopAnimation(true);
                if (this._symbolsEnabled) {
                    this.hideShowNoWinSymbols(false);
                }
                var indexes;
                var symbolIndex;
                for (var j = 0; j < scatter.indexes.length; j++) {
                    indexes = scatter.indexes[j];
                    this.reelViews[indexes[0]].symbols[indexes[1] + this.reelViews[j].reelConfig.topInvisibleSymbolsCount].visible = false;
                }
                console.log();
            };
            ReelsController.prototype.stopAnimation = function (tween) {
                if (!tween) {
                    this.hideShowNoWinSymbols(true);
                }
                for (var i = 0; i < this.reelViews.length; i++) {
                    for (var j = 0; j < this.reelViews[i].symbols.length; j++) {
                        this.reelViews[i].symbols[j].visible = true;
                    }
                }
            };
            /**
             *
             * @param show
             */
            ReelsController.prototype.hideShowNoWinSymbols = function (show) {
                this._symbolsEnabled = show;
                for (var k = 0; k < this.reelViews.length; k++) {
                    for (var l = 0; l < this.reelViews[k].symbols.length; l++) {
                        var item = this.reelViews[k].symbols[l];
                        if (show) {
                            item.activate();
                        }
                        else {
                            item.deactivate();
                        }
                    }
                }
            };
            /**
             * Override this to run your specific intrigue like a animation and so on.
             */
            ReelsController.prototype.startIntrigue = function (reelIndex) {
            };
            /**
             * Override this to stop your specific intrigue like a animation and so on.
             */
            ReelsController.prototype.stopIntrigue = function () {
            };
            /**
             *
             * @param index
             */
            ReelsController.prototype.animateSpecificSymbolWithIndex = function (index) {
            };
            ReelsController.RESPIN_COMPLETE = "RespinComplete";
            ReelsController.FINISH_SHOW_WINNING = "FinishShowWinning";
            ReelsController.STOP = "Stop";
            ReelsController.INTRIGUE = "Intrigue";
            return ReelsController;
        }(ActionController));
        common.ReelsController = ReelsController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
