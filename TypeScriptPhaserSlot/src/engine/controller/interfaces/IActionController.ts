﻿module TypeScriptPhaserSlot.common {

    export interface IActionController extends IController{

        state: IState;

        init(data?: any): void;

        actionSignal: Phaser.Signal;

        start(data?: any): void;

        stop(data?: SpinResult): void;

    }
}  