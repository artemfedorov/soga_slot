﻿module TypeScriptPhaserSlot.common {

    export interface ILinesController extends IActionController {

        showLines(count:number):void;
        finishShowingSignal: Phaser.Signal
        
    }
}  