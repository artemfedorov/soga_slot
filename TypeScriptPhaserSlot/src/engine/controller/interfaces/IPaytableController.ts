﻿module TypeScriptPhaserSlot.common {

    export interface IPaytableController extends IActionController {

        show():void;
        hide():void;
        switch():void;
        goToPage(pageIndex:number | string):void;
        isShown:boolean;
        
    }
}  