﻿module TypeScriptPhaserSlot.common {

    export interface IGameController extends IController {

        game: Game;

        bindToModel(model: IModel): void;
    }
}  