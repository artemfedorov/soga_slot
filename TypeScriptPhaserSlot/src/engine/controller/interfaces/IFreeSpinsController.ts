﻿module TypeScriptPhaserSlot.common {

    export interface IFreeSpinsController extends IActionController {

        startFreeSpins(data?:any):void
        addedFreeSpins(data?:any):void
        endOfFreeSpins(data?:any):void
        leftFreeSpins(data?:any):void
        
    }
}  