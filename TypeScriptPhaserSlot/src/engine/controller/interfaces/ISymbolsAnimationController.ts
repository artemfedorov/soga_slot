﻿module TypeScriptPhaserSlot.common {

    export interface ISymbolsAnimationController extends IActionController{

        animateSymbols(data: SpinResult): void;
        animateSymbolsOfLine(data: Payline, spinResult:SpinResult): void;
        animateScatters(scatter: Scatter, spinResult: SpinResult, delayBetween?: number): void;
        stopAnimation(): void;
        //animateSpecificSymbolWithIndex(index: number): void;

    }
}  