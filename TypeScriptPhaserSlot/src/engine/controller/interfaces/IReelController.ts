﻿module TypeScriptPhaserSlot.common {

    export interface IReelController extends IActionController{

        rebuildReels(gameStateResponse: GameStateResponse):void;
        respin(data?: any): void;
        stopRespin(data?: any): void;
        animateSymbols(data: any): void;
        animateSymbolsOfLine(data: any): void;
        animateScatters(data: any): void
        stopAnimation(): void;
        urgentStop(): void;
    }
}  