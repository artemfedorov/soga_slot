﻿module TypeScriptPhaserSlot.common {

    export interface IInterfaceController extends IActionController {

        init(): void;
        changeBetText(value: number): void;
        changeLinesText(value: number): void;
        changeTotalBetText(value: number): void;
        changeWinText(value: number): void;
        resetWinText(): void;
        changeBalance(value: number): void;
        stateChanged(stateName: string): void;
        autoChanged(value: boolean): void;
        showAutoplayText(value: boolean): void;
        showPopupWindow(data:any):void;
        hidePopupWindow(data?:any):void;
        disableButtons(buttonsNames:string[]):void;

        stop(): void;
        start(): void;
        
    }
}  