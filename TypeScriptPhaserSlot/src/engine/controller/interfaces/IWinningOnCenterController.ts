﻿module TypeScriptPhaserSlot.common {

    export interface IWinningOnCenterController extends IActionController {

        showWinning(data?:any):void
        hideWinning(data?:any):void
        
    }
}  