﻿module TypeScriptPhaserSlot.common {

    export interface IEmotionsController extends IActionController {

        stopEmotion(): void
        startEmotions(emotions:EmotionConfiguration[]): void
        startEmotion(type:string, winning:number, count:number): void
        
    }
}  