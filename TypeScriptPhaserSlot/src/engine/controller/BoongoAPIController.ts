///<reference path="./BaseController.ts" />
// We can get session right here: http://test-api.playvulkan.com/get-session

module TypeScriptPhaserSlot.common {

    export class BoongoAPIController extends BaseController {

        protected _gameList_signal: Phaser.Signal;
        protected _gameState_signal: Phaser.Signal;
        protected _spin_signal: Phaser.Signal;
        protected _collect_signal: Phaser.Signal;
        protected _gamble_signal: Phaser.Signal;
        protected _exit_signal: Phaser.Signal;
        protected _bonusGame_signal: Phaser.Signal;
        protected gameRunner: any;

        constructor(url: string, name: string, debugMode: boolean = false) {
            super(name, debugMode);
            this._gameList_signal = new Phaser.Signal();
            this._gameState_signal = new Phaser.Signal();
            this._spin_signal = new Phaser.Signal();
            this._collect_signal = new Phaser.Signal();
            this._gamble_signal = new Phaser.Signal();
            this._exit_signal = new Phaser.Signal();
            this._bonusGame_signal = new Phaser.Signal();

            this.gameRunner = window['GR'];

            this.addListeners();
        }

        protected addListeners():void {
            //this.gameRunner.Events.flow.state.on(this.onGetGameState, this);
            this.gameRunner.Events.flow.round_result.on(this.onSpin, this);
        }

        protected encodeData(data: string): string {
            return data.toString();
        }

        protected decodeData(data: string): string {
            return data.toString();
        }

        protected getHash(data: string): string {
            return '';
        }

        protected getParams(data: any): string {
            return '';
        }

        protected createParams(data: any): string {
            return '';
        }

        /**
         * Server requests
         */
        public getGameList(data?: any): void {
            this.gameList_signal.dispatch();
        }

        public getGameState(gameName: string): void {
            //this.gameRunner.Events.game.start();
            if(window['windowBooongoStateData']) {
                this.onGetGameState(window['windowBooongoStateData']);
            } else {
                this.gameRunner.Events.flow.state.once(this.onGetGameState, this);
            }
        }


        protected onGetGameState(data:any): void {
            let parseData: any = data.data;
            console.log(JSON.stringify(parseData));
            this.gameRunner.Events.game.ready();
            this.gameState_signal.dispatch(new BoongoGameStateResponse(parseData));
        }

        protected onError(e: Event): void {

        }

        /**
         *
         * @param data
         */
        public spin(data: any): void {
            /*let paramsObj: object = {
                action: {
                    name: 'spin',
                    params: {
                        lines: data.lines,
                        bet_per_line: data.bet
                    }
                },
                bet: data.lines * data.bet
            };
            this.gameRunner.Events.game.ready();
            this.gameRunner.Events.game.play(paramsObj);*/
        }

        /**
         *
         * @param data
         */
        public respin(data?: any): void {
            let paramsObj: object = {
                action: {
                    name: 'respin',
                    params: {}
                }
            };
            this.gameRunner.Events.game.ready();
            this.gameRunner.Events.game.play(paramsObj);
        }

        /**
         *
         * @param data
         */
        public freespin(data: any): void {
            let paramsObj: object = {
                action: {
                    name: 'freespin',
                    params: {}
                }
            };
            this.gameRunner.Events.game.ready();
            this.gameRunner.Events.game.play(paramsObj);
        }

        /**
         *
         * @param data
         */
        protected onSpin(data:any): void {
            let parseData: any = data.data;
            console.log(JSON.stringify(parseData));

            if(parseData.context.last_action == 'freespin_init' ||
                parseData.context.last_action == 'freespin_stop') {
                return;
            }

            this.spin_signal.dispatch(new BoongoSpinResponse(parseData));
        }


        /**
         *
         * @param {string} bonusType
         */
        public bonus(bonusType: string): void {

        }

        /**
         *
         * @param data
         */
        protected onBonus(data:any): void {
            let parseData: any = data.data;
            this.spin_signal.dispatch(new BonusResponse(parseData));
        }


        //
        public collect(data?: any): void {
            //this.collect_signal.dispatch(jsonObject);
        }

        //
        public gamble(data?: any): void {
            //this.gamble_signal.dispatch(jsonObject);
        }

        //
        public exit(data?: any): void {
            //this.exit_signal.dispatch(jsonObject);
        }

        /************************************************************
         Getters and Setters
         *************************************************************/
        public get gameList_signal(): Phaser.Signal {
            return this._gameList_signal;
        }

        public get gameState_signal(): Phaser.Signal {
            return this._gameState_signal;
        }

        public get spin_signal(): Phaser.Signal {
            return this._spin_signal;
        }

        public get collect_signal(): Phaser.Signal {
            return this._collect_signal;
        }

        public get gamble_signal(): Phaser.Signal {
            return this._gamble_signal;
        }

        public get exit_signal(): Phaser.Signal {
            return this._exit_signal;
        }

        public get bonusGame_signal(): Phaser.Signal {
            return this._bonusGame_signal;
        }
    }
}