﻿///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />

module TypeScriptPhaserSlot.common {

    export class ToolbarController extends ActionController {

        protected _config: GameConfiguration;
        protected layer: Phaser.Group;
        protected view: BaseView;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, configuration: GameConfiguration) {
            super(game, name, debugMode);
            this._config = configuration;
            this.layer = layer;
            this.init();
        }

        /**
         * Initial creation
         */
        public init(): void {
            super.init();

            let exitButton:IToolbarButton = new ExitButton(this.game, 0, 20);
            exitButton.name = 'exitButton';
            exitButton.actionSignal.add(this.onFullScreen, this);

            let soundButton:IToolbarButton = new SoundButton(this.game, 0, 20);
            soundButton.name = 'soundButton';
            soundButton.actionSignal.add(this.onFullScreen, this);

            let fullScreenButton:IToolbarButton = new FullScreenButton(this.game, 0, 20);
            fullScreenButton.name = 'fullScreenButton';
            fullScreenButton.actionSignal.add(this.onFullScreen, this);

            this.layer.add(exitButton);
            this.layer.add(soundButton);
            this.layer.add(fullScreenButton);

            this.layer.fixedToCamera = true;

            this.changePositions();

            this.game.scale.onSizeChange.add(function () {
                this.changePositions();
            },this, -1);
        }

        protected changePositions():void {
            let ratio:number = this.game.camera.width / this.game.camera.height;

            this.layer.children.forEach(function (child:any):void {
                switch (child.name) {
                    case 'exitButton':
                        child.x = 768 * ratio - 50;
                        break;
                    case 'soundButton':
                        child.x = 768 * ratio - 150;
                        break;
                    case 'fullScreenButton':
                        child.x = 768 * ratio - 100;
                        break;
                }
            }, this);
        }

        protected onFullScreen(data:any):void {
            this.actionSignal.dispatch(data);
        }

    }
}