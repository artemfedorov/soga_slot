﻿///<reference path="./interfaces/IActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class BackController extends ActionController {

        private view: SimpleSprite;
        private layer: Phaser.Group;
        private pattern: any;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, pattern: any) {
            super(game, name, debugMode);
            this.layer = layer;
            this.pattern = pattern;
            this.init(pattern);
        }
        /**
        * Building reel views
        */
        public init(pattern: any): void {
            this.view = new SimpleSprite(this.game, pattern.background.x, pattern.background.y, pattern.background.name, this.debugMode);
            this.layer.add(this.view);
        }

        /**
         *
         * @param data
         */
        public changeBackground(data:any): void {

        }
    }
}