﻿///<reference path="./interfaces/IActionController.ts" />
///<reference path="./ActionController.ts" />


/**
 * The Class accumulates specified game effects controlls etc.
 * @param {Game} game
 * @param {string} name
 * @param {Phaser.Group} layer
 * @param {boolean} debugMode
 */
module TypeScriptPhaserSlot.common {

    export class EffectController extends ActionController implements IActionController {

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean) {
            super(game, name, debugMode);
        }
    }
}