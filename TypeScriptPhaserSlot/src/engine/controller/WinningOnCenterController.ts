///<reference path="./interfaces/IWinningOnCenterController.ts" />
///<reference path="./ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class WinningOnCenterController extends ActionController implements IWinningOnCenterController {

        protected layer: Phaser.Group;
        protected textField: LocaleText;
        protected tweens:Phaser.Tween[];

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean) {
            super(game, name, debugMode);
            this.layer = layer;
            this.init();
        }

        public init():void {
            super.init();

            this.tweens = [];

            this.textField = this.game.textFactory.getTextWithStyle('', "WINNING_ON_CENTER_TEXT_style");
            this.textField.anchor.set(0.5, 0.5);
            this.textField.x = 683;
            this.textField.y = 384;
            this.textField.visible = false;
            this.layer.add(this.textField);
        }

        public showWinning(winValue:number):void {
            if(winValue == 0) {
                return;
            }

            let sizeTween:Phaser.Tween = this.game.add.tween(this.textField.scale).to({x:1.2, y:1.2}, 250, Phaser.Easing.Linear.None, true, 0, -1, true);
            sizeTween.repeatDelay(500);

            let tween:Phaser.Tween = this.game.add.tween(this.textField).to({text:winValue}, 500, Phaser.Easing.Linear.None, true);
            tween.onUpdateCallback(this.onTweenUpdate, this);
            tween.onComplete.addOnce(this.onTweenComplete, this);
            this.textField.visible = true;

            this.tweens = [sizeTween, tween];
        }

        protected onTweenUpdate():void {
            //this.textField.text = (+this.textField.text).toFixed(2);
            this.textField.text = MoneyFormatController.instance.format(+(+this.textField.text).toFixed(2), true);
        }

        protected onTweenComplete():void {
            //this.textField.text = (+this.textField.text).toFixed(2);
            this.textField.text = MoneyFormatController.instance.format(+(+this.textField.text).toFixed(2), true);
        }

        public hideWinning(data?:any):void {
            this.killAllTweens();
            this.textField.scale.set(1, 1);
            this.textField.text = '';
            this.textField.visible = false;
        }

        protected killAllTweens():void {
            this.tweens.forEach(function(tween:Phaser.Tween): void {
                tween.stop(false);
                this.game.tweens.remove(tween);
            }, this);
        }
    }
}