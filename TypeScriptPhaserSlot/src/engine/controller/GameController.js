///<reference path="./interfaces/IGameController.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GameController = /** @class */ (function (_super) {
            __extends(GameController, _super);
            function GameController(game, name, debugMode, args) {
                var _this = _super.call(this, name, debugMode) || this;
                _this._configs = args;
                _this._game = game;
                return _this;
            }
            GameController.prototype.start = function () {
                this.configuration = new GameConfiguration(this.configs[0]);
                this.game.pattern = this.configs[1];
                var modelStateList = [];
                /*
                    Ordered list of states for Model State Manager
                */
                var state;
                for (var i = 0; i < this.configuration.states.length; i++) {
                    state = new TypeScriptPhaserSlot.common[this.configuration.states[i].name](this.model, this.configuration.states[i], this.debugMode);
                    modelStateList.push(state);
                }
                this.model.setStateList(modelStateList);
            };
            /**
             * Binding Game Controller to Model instance
             * @param model - instance which implements IModel
             */
            GameController.prototype.bindToModel = function (model) {
                this.model = model;
                this.model.modelSignal.add(this.modelSays, this);
                this.start();
            };
            /**
             * Listener Model State Manager states signals
             * @param state
             * @param data
             * @param isRequiredAction
             */
            GameController.prototype.modelSays = function (state, data, isRequiredAction) {
                this.currState = state;
                this._currentStateRequiredAction = isRequiredAction;
            };
            Object.defineProperty(GameController.prototype, "game", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._game;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameController.prototype, "configs", {
                get: function () {
                    return this._configs;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameController.prototype, "currentStateRequiredAction", {
                get: function () {
                    return this._currentStateRequiredAction;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameController.prototype, "currState", {
                get: function () {
                    return this._currState;
                },
                set: function (value) {
                    this._currState = value;
                },
                enumerable: true,
                configurable: true
            });
            return GameController;
        }(BaseController));
        common.GameController = GameController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
