///<reference path="./ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class SoundController extends ActionController implements ISoundController{

        private static _instance:SoundController;

        protected muted:boolean;

        protected gameConfiguration:GameConfiguration;
        protected gameStateResponse:GameStateResponse;
        protected groups:SoundGroup[];

        constructor(game: Game, name: string, debugMode: boolean, gameConfiguration: GameConfiguration, gameStateResponse: GameStateResponse) {
            if(SoundController._instance) {
                throw new Error("Error: Instantiation failed: Use SoundController.instance instead of new.");
            }

            super(game, name, debugMode);

            this.gameConfiguration = gameConfiguration;
            this.gameStateResponse = gameStateResponse;

            this.init();

            SoundController._instance = this;
        }

        /**
         *
         */
        public init(): void {
            this.muted = false;
            this.groups = [];
            this.groups.push(new SoundGroup('default'));
        }

        public playSound(key:string, groupName?:string, volume?:number, loop?:boolean): Phaser.Sound {
            let startConfig: any = {
                assetKey:key,
                key:key,
                group:groupName,
                volume:volume,
                loop:loop
            };
            let config:SoundConfiguration = new SoundConfiguration(startConfig);
            for(let i:number = 0; i < this.gameConfiguration.sounds.length; i++) {
                let soundConfig:SoundConfiguration = this.gameConfiguration.sounds[i];
                if(soundConfig.key == key) {
                    config = soundConfig;
                    break;
                }
            }

            let currentConfig:any = config;
            currentConfig.assetKey = groupName || config.key;
            currentConfig.group = groupName || config.group;
            currentConfig.volume = volume || config.volume;
            currentConfig.loop = loop || config.loop;

            let sound:Phaser.Sound = this.game.add.audio(currentConfig.assetKey, currentConfig.volume, currentConfig.loop);
            let group:SoundGroup = this.getGroup(config.group);
            if (!group) {
                group = new SoundGroup(currentConfig.group);
                group.mute = this.muted;
                this.groups.push(group);
            }

            group.addSound(sound);

            sound['group'] = currentConfig.group;
            //sound['id'] = Math.random() * Math.random() * Math.random();
            sound.onStop.addOnce(this.onSoundStop.bind(this));
            sound.play();

            if(group.mute) {
                sound.mute = group.mute;
            }

            return sound;
        }

        /**
         *
         * @param key
         * @param groupName
         * @param volume
         * @param loop
         * @returns {Phaser.Sound}
         */
        /*public playSound(key:string, groupName = 'default', volume?:number, loop?:boolean): Phaser.Sound {
            let sound:Phaser.Sound = this.game.add.audio(key, volume, loop);
            let group:SoundGroup = this.getGroup(groupName);
            if (!group) {
                group = new SoundGroup(groupName);
                group.mute = this.muted;
                this.groups.push(group);
            }

            group.addSound(sound);

            sound['group'] = groupName;
            //sound['id'] = Math.random() * Math.random() * Math.random();
            sound.onStop.addOnce(this.onSoundStop.bind(this));
            sound.play();

            if(group.mute) {
                sound.mute = group.mute;
            }

            return sound;
        }*/

        /**
         *
         * @param sound
         */
        private onSoundStop(sound:Phaser.Sound):void {
            let group:SoundGroup = this.getGroup(sound['group']);
            group.removeSound(sound.key);
        }

        /**
         *
         * @param soundName
         * @param groupName
         */
        public stopSound(soundName:string, groupName = 'default'): void {
            let sound: Phaser.Sound;
            let group:SoundGroup = this.getGroup(groupName);
            sound = group.getSounds(soundName);
            if(sound) {
                sound.stop();
            }
        }

        /**
         *
         * @param soundName
         * @param groupName
         * @returns {Phaser.Sound}
         */
        public getSound(soundName:string, groupName = 'default'): Phaser.Sound {
            let sound: Phaser.Sound;
            let group:SoundGroup = this.getGroup(groupName);
            if(group) {
                sound = group.getSounds(soundName);
                return sound
            }
            return null;
        }

        /**
         *
         * @param groupName
         * @returns {number}
         */
        private getGroupIndex(groupName:string):number {
            let index:number = -1;
            let group:SoundGroup;
            for (let i:number = 0; i < this.groups.length; i++) {
                group = this.groups[i];
                if(group.name == groupName) {
                    index = i;
                    break;
                }
            }
            return index;
        }

        /**
         *
         * @param value
         */
        public muteAll(value:boolean):void {
            this.muted = value;
            let group:SoundGroup;
            for (let i:number = 0; i < this.groups.length; i++) {
                group = this.groups[i];
                group.mute = value;
            }
        }

        /**
         *
         * @param groupName
         * @returns {SoundGroup}
         */
        public getGroup(groupName:string): SoundGroup {
            let group:SoundGroup;
            let index:number = this.getGroupIndex(groupName);
            if(index != -1) {
                group = this.groups[index];
            }
            return group;
        }

        public static get instance(): SoundController {
            return SoundController._instance;
        }
    }

    export class SoundGroup {

        private _mute:boolean;
        private _name:string;
        private _sounds:Phaser.Sound[];

        constructor(name:string) {
            this._name   = name;
            this._sounds = [];
            this._mute = false;
        }

        /**
         *
         * @param name
         * @returns {any}
         */
        public getSounds(name:string):Phaser.Sound {
            let found:Phaser.Sound[] = this.sounds.filter(function (value:Phaser.Sound):any {
                return value.key == name;
            }.bind(this));

            if(found && found.length > 0) {
                return found[0];
            }
            return null;
        }

        /**
         *
         * @param sound
         */
        public addSound(sound:Phaser.Sound):void {
            this.sounds.push(sound);
        }

        /**
         *
         */
        public stopAllSounds():void {
            let sound:Phaser.Sound = null;
            for(let i:number = 0; i < this._sounds.length; i++) {
                sound = this.sounds[i];
                sound.stop();
            }
        }

        /**
         *
         * @param soundName
         */
        public removeSound(soundName:string):void {
            let index:number = this.getSoundIndex(soundName);
            if(index != -1) {
                this.sounds.splice(index, 1);
            }
        }

        /**
         *
         * @param soundName
         * @returns {number}
         */
        private getSoundIndex(soundName:string):number {
            let index:number = -1;
            let sound:Phaser.Sound;
            for (let i:number = 0; i < this.sounds.length; i++) {
                sound = this.sounds[i];
                if(sound.key == soundName) {
                    index = i;
                    break;
                }
            }
            return index;
        }

        /**
         *
         * @returns {string}
         */
        public get name():string {
            return this._name;
        }

        /**
         *
         * @returns {Phaser.Sound[]}
         */
        public get sounds():Phaser.Sound[] {
            return this._sounds;
        }

        public get mute(): boolean {
            return this._mute;
        }

        public set mute(value: boolean) {
            let sound:Phaser.Sound = null;
            for(let i:number = 0; i < this._sounds.length; i++) {
                sound = this.sounds[i];
                sound.mute = value;
            }
            this._mute = value;
        }
    }
}