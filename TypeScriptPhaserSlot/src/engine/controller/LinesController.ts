﻿///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />

module TypeScriptPhaserSlot.common {

    export class LinesController extends ActionController implements ILinesController {

        protected _config: GameConfiguration;
        protected layer: Phaser.Group;
        protected showLineTimeout: any;
        protected view: ILinesView;
        protected _finishShowingSignal: Phaser.Signal;
        protected finishShowLines:boolean;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, configuration: GameConfiguration) {
            super(game, name, debugMode);
            this._config = configuration;
            this.layer = layer;
            this._finishShowingSignal = new Phaser.Signal();
            this.finishShowLines = false;
            this.init();
        }

        /**
         * Initial creation
         */
        public init(): void {
            super.init();
            let lineViewName: string = this._config.gameName + "LinesView";
            this.view = new TypeScriptPhaserSlot.common[lineViewName](this.game, lineViewName, this._config.paylines, this.game.pattern, this.debugMode);
            this.layer.add(this.view);
        }

        /**
         * Start showing
         */
        public start(data?: SpinResult): void {
            super.start();
            this.finishShowLines = false;
            this.showWinLine(data, 0);
        }

        /**
         *
         * @param data
         */
        public stop(data?: SpinResult): void {
            super.stop();
            this.finishShowLines = true;
            clearTimeout(this.showLineTimeout);
            this.hideLines();
        }

        /**
         *
         */
        private hideLines(): void {
            this.view.hideLines();
        }

        /**
         *
         * @param count
         */
        public showLines(count:number):void {
            this.view.showLines(count);
        }

        /**
         *
         * @param {SpinResult} data
         * @param {number} resultLineindex
         */
        private showWinLine(data: SpinResult, resultLineindex: number): void {
            this.hideLines();

            if(this.finishShowLines) {
                return;
            }

            if (resultLineindex >= data.paylines.length) {
                this.finishShowingSignal.dispatch();
                return;
            }

            let payLine: Payline = data.paylines[resultLineindex];
            this.view.showWinLine(payLine);

            this.actionSignal.dispatch(data.paylines[resultLineindex]);

            let timeout: number = this._config.paylines[payLine.index].timeout * Phaser.Timer.SECOND;
            this.showLineTimeout = setTimeout(() => {
                this.showWinLine(data, resultLineindex + 1)
            }, timeout);

            this.showLineTimeout = setTimeout(() => {
                SoundController.instance.playSound('line3', 'default', 0.4);
            }, 300);
        }


        public get finishShowingSignal(): Phaser.Signal {
            return this._finishShowingSignal;
        }

    }
}