﻿///<reference path="./interfaces/IGameController.ts" />

module TypeScriptPhaserSlot.common {

    export abstract class GameController extends BaseController implements IGameController {

        protected model: IGameModel;

        protected _game: Game;

        protected _currState: IState;

        protected _configs: any[];

        protected _currentStateRequiredAction: boolean;

        protected _configuration: GameConfiguration;


        constructor(game: Game, name: string, debugMode: boolean, args?: any[]) {
            super(name, debugMode);
            this._configs = args;
            this._game = game;
        }

        protected start() {
            this._configuration = new GameConfiguration(this.configs[0]);
            this.game.pattern = this.configs[1];

            let modelStateList = [];
            /*
                Ordered list of states for Model State Manager
            */
            let state: IState;

            for (let i: number = 0; i < this.configuration.states.length; i++) {
                state = new TypeScriptPhaserSlot.common[this.configuration.states[i].name](this.model, this.configuration.states[i], this.debugMode);
                modelStateList.push(state);
            }
            this.model.setStateList(modelStateList)
        }

        /**
         * Binding Game Controller to Model instance
         * @param model - instance which implements IModel
         */
        public bindToModel(model: IGameModel): void {
            this.model = model;
            this.model.notEnoughMoneySignal.add(this.onNotEnoughMoney, this);
            this.model.modelSignal.add(this.modelSays, this);
            this.game.model = this.model;
            this.start();
        }

        /**
         * Listener Model State Manager states signals
         * @param state
         * @param data
         * @param isRequiredAction
         */
        protected modelSays(state: IState, data: any, isRequiredAction: boolean): void {
            this.currState = state;
            this._currentStateRequiredAction = isRequiredAction;
        }

        /**
         * Just to be overriden
         */
        protected onNotEnoughMoney(): void {
            this.debuglog("No money - no cry:)");
            //"No money - no cry:)"
            //write here a code for vizualization onNotEnoughMoney for player
        }

        /**
         *
         * @param {string} controllerName
         * @returns {string}
         */
        protected getControllerName(controllerName: string): string {
            let tryName: string = this.configs[0].gameName + controllerName;
            if (TypeScriptPhaserSlot.common[tryName]) {
                return tryName;
            }
            tryName = 'BaseGame' + controllerName;
            if (TypeScriptPhaserSlot.common[tryName]) {
                return tryName;
            }
            return controllerName;
        }

        /************************************************************
         Getters and Setters
         *************************************************************/
        public get game(): Game {
            return this._game;
        }

        public get configs(): any[] {
            return this._configs;
        }

        public get configuration(): GameConfiguration {
            return this._configuration;
        }

        public get currentStateRequiredAction(): boolean {
            return this._currentStateRequiredAction;
        }

        public get currState(): IState {
            return this._currState;
        }

        public set currState(value: IState) {
            this._currState = value;
        }
    }

}