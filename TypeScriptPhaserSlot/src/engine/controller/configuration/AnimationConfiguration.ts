﻿module TypeScriptPhaserSlot.common {

    export class AnimationConfiguration {

        private _key: string;
        private _assetKey: string;
        private _scale: {x:number, y:number};
        private _pivot: {x:number, y:number};
        private _anchor: {x:number, y:number};
        private _animations: AnimationDataConfiguration[];

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): AnimationConfiguration {
            this._assetKey = rawData.assetKey;
            this._key = rawData.key;
            this._scale = rawData.scale || { x:1, y:1 };
            this._pivot = rawData.pivot || { x:1, y:1 };
            this._anchor = rawData.anchor || { x:1, y:1 };
            this._animations = [];

            for(let i:number = 0; i < rawData.animations.length; i++) {
                this._animations.push(new AnimationDataConfiguration(rawData.animations[i]));
            }

            return this;
        }

        public get assetKey(): string {
            return this._assetKey;
        }
        public get key(): string {
            return this._key;
        }
        public get scale(): {x:number, y:number} {
            return this._scale;
        }
        public get pivot(): {x:number, y:number} {
            return this._pivot;
        }
        public get anchor(): {x:number, y:number} {
            return this._anchor;
        }
        public get animations(): any[] {
            return this._animations;
        }
    }

    export class AnimationDataConfiguration {

        private _name: string;
        private _startFrame: number;
        private _endFrame: number;
        private _isLoop: boolean;
        private _frameRate: number;
        private _playOnCompleted: string;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): AnimationDataConfiguration {
            this._name = rawData.name;
            this._startFrame = rawData.startFrame || 1;
            this._endFrame = rawData.endFrame || 1;
            this._isLoop = rawData.isLoop || false;
            this._frameRate = rawData.frameRate || 24;
            this._playOnCompleted = rawData.playOnCompleted || undefined;

            return this;
        }

        public get name(): string {
            return this._name;
        }
        public get startFrame(): number {
            return this._startFrame;
        }
        public get endFrame(): number {
            return this._endFrame;
        }
        public get isLoop(): boolean {
            return this._isLoop;
        }
        public get frameRate(): number {
            return this._frameRate;
        }
        public get playOnCompleted(): string {
            return this._playOnCompleted;
        }

    }
}
