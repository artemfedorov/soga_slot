﻿module TypeScriptPhaserSlot.common {

    export class ButtonConfiguration {

        private _name: string;
        private _className: string;
        private _key: string;
        private _action: string;
        private _caption: string;
        private _overFrame: number | string ;
        private _outFrame: number | string ;
        private _downFrame: number | string ;
        private _upFrame: number | string ;
        private _disabledFrame: number | string ;
        private _textStyle: string;
        private _onDownSoundKey: string;
        private _onOverSoundKey: string;
        private _onOutSoundKey: string;
        private _onUpSoundKey: string;

        private _triggerStateFrames: number[];
        private _trigger: boolean;
        private _triggerSoundKeys: string[];

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): ButtonConfiguration {
            this._name = rawData.name;
            this._className = rawData.className;
            this._key = rawData.key;
            this._action = rawData.action ? rawData.action : "";
            this._caption = rawData.caption ? rawData.caption : "";
            this._textStyle = rawData.textStyle;
            this._disabledFrame = rawData.disabledFrame ? rawData.disabledFrame : 0;
            this._overFrame = rawData.overFrame ? rawData.overFrame : 0;
            this._outFrame = rawData.outFrame ? rawData.outFrame : 0;
            this._downFrame = rawData.downFrame ? rawData.downFrame : 0;
            this._upFrame = rawData.upFrame ? rawData.upFrame : 0;

            this._trigger = rawData.trigger ? rawData.trigger : false;
            this._triggerSoundKeys = rawData.triggerSoundKeys ? rawData.triggerSoundKeys : null;
            this._triggerStateFrames = rawData.triggerStateFrames ? rawData.triggerStateFrames : 0;

            this._onDownSoundKey = rawData.onDownSoundKey ? rawData.onDownSoundKey : null;
            this._onOverSoundKey = rawData.onOverSoundKey ? rawData.onOverSoundKey : null;
            this._onOutSoundKey = rawData.onOutSoundKey ? rawData.onOutSoundKey : null;
            this._onUpSoundKey = rawData.onUpSoundKey ? rawData.onUpSoundKey : null;
            return this;
        }

        public get name(): string {
            return this._name;
        }
        public get className(): string {
            return this._className;
        }
        public get trigger(): boolean {
            return this._trigger;
        }
        public get triggerStateFrames(): number[] {
            return this._triggerStateFrames;
        }
        public get triggerSoundKeys(): string[] {
            return this._triggerSoundKeys;
        }
        public get textStyle(): string {
            return this._textStyle;
        }
        public get key(): string {
            return this._key;
        }
        public get onDownSoundKey(): string {
            return this._onDownSoundKey;
        }
        public get onOverSoundKey(): string {
            return this._onOverSoundKey;
        }
        public get onOutSoundKey(): string {
            return this._onOutSoundKey;
        }
        public get onUpSoundKey(): string {
            return this._onUpSoundKey;
        }
        public get action(): string {
            return this._action;
        }
        public set action(value: string) {
            this._action = value;
        }
        public get caption(): string {
            return this._caption;
        }
        public get overFrame(): number | string  {
            return this._overFrame;
        }
        public get disabledFrame(): number | string  {
            return this._disabledFrame;
        }
        public get outFrame(): number | string  {
            return this._outFrame;
        }
        public get downFrame(): number | string  {
            return this._downFrame;
        }
        public get upFrame(): number | string  {
            return this._upFrame;
        }
    }
}
