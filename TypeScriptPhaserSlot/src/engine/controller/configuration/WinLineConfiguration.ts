﻿module TypeScriptPhaserSlot.common {

    export class WinLineConfiguration {

        private _index: number;
        private _keys: string[];
        private _symbols: number[];
        private _info: number[];
        private _colour: string;
        private _sounds: string[];
        private _timeout: number;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): WinLineConfiguration {
            this._index = rawData.index;
            this._keys = rawData.keys;
            this._info = rawData.info;
            this._sounds = rawData.sounds;
            this._timeout = rawData.timeout;
            return this;
        }

        public get index(): number {
            return this._index;
        }

        public get keys(): string[] {
            return this._keys;
        }

        public get info(): number[] {
            return this._info;
        }

        public get sounds(): string[] {
            return this._sounds;
        }
        public get timeout(): number {
            return this._timeout;
        }
    }
}
