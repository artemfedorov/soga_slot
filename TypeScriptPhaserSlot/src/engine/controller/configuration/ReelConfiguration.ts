﻿module TypeScriptPhaserSlot.common {

    export class ReelConfiguration {

        private _startIndex: number;
        private _stopIndex: number;
        private _startTimeout: number;
        private _stopTimeout: number;
        private _shift: number;
        private _speeds: number[];
        private _visibledSymbols: number;
        private _topInvisibleSymbolsCount: number;
        private _bottomInvisibleSymbolsCount: number;
        private _width: number;
        private _height: number;

        private _soundStopKeys: string[];


        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): ReelConfiguration {
            this._startIndex = rawData.startIndex;
            this._stopIndex = rawData.stopIndex;
            this._stopTimeout = rawData.stopTimeout;
            this._soundStopKeys = rawData.soundStopKeys;
            this._startTimeout = rawData.startTimeout;
            this._shift = rawData.shift;
            this._speeds = rawData.speeds;
            this._visibledSymbols = rawData.visibledSymbols || 3;
            this._topInvisibleSymbolsCount = rawData.hasOwnProperty('topInvisibleSymbolsCount') ? rawData.topInvisibleSymbolsCount : 2;
            this._bottomInvisibleSymbolsCount = rawData.hasOwnProperty('bottomInvisibleSymbolsCount') ? rawData.bottomInvisibleSymbolsCount : 1;
            this._width = rawData.width;
            this._height = rawData.height;

            return this;
        }

        public get soundStopKeys(): string[] {
            return this._soundStopKeys;
        }
        public get stopTimeout(): number {
            return this._stopTimeout;
        }
        public get startIndex(): number {
            return this._startIndex;
        }
        public get stopIndex(): number {
            return this._stopIndex;
        }
        public get startTimeout(): number {
            return this._startTimeout;
        }
        public get shift(): number {
            return this._shift;
        }
        public get speeds(): number[] {
            return this._speeds;
        }
        public get width(): number {
            return this._width;
        }
        public get height(): number {
            return this._height;
        }
        public get visibledSymbols(): number {
            return this._visibledSymbols;
        }
        public get topInvisibleSymbolsCount(): number {
            return this._topInvisibleSymbolsCount;
        }
        public get bottomInvisibleSymbolsCount(): number {
            return this._bottomInvisibleSymbolsCount;
        }


    }
}
