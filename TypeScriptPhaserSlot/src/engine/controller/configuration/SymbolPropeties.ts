﻿module TypeScriptPhaserSlot.common {

    export class SymbolProperties {

        private _className: string;
        private _frameRate: number;
        private _key: string;
        private _borderKeys: string[];
        private _index: number;
        private _height: number;
        private _scale: any;
        private _pivot: any;
        private _anchor: any;
        private _animationData: string | AnimationConfiguration;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): SymbolProperties {
            this._className = rawData.className;
            this._frameRate = rawData.frameRate;
            this._key = rawData.key;
            this._borderKeys = rawData.borderKeys;
            this._index = rawData.index;
            this._height = rawData.height;

            this._scale = rawData.scale || {x:1, y:1};
            this._pivot = rawData.pivot || {x:0, y:0};
            this._anchor = rawData.anchor || {x:0, y:0};

            if(rawData.animations) {
                this._animationData = rawData.animations;
            }

            return this;
        }

        public get className(): string {
            return this._className;
        }

        public get frameRate(): number {
            return this._frameRate;
        }

        public get key(): string {
            return this._key;
        }

        public get borderKeys(): string[] {
            return this._borderKeys;
        }
        
        public get index(): number {
            return this._index;
        }
        
        public get height(): number {
            return this._height;
        }

        public get scale(): any {
            return this._scale;
        }

        public get pivot(): any {
            return this._pivot;
        }

        public get anchor(): any {
            return this._anchor;
        }

        public get animationData(): string | AnimationConfiguration {
            return this._animationData;
        }
    }
}
