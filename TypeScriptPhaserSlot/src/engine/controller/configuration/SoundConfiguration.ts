﻿module TypeScriptPhaserSlot.common {

    export class SoundConfiguration {

        private _assetKey: string;
        private _key: string;
        private _group: string;
        private _volume: number;
        private _loop: boolean;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): SoundConfiguration {
            this._assetKey = rawData.assetKey;
            this._key = rawData.key;
            this._group = rawData.group == '' ? 'default' : rawData.group;
            this._volume = rawData.volume;
            this._loop = rawData.loop;

            return this;
        }

        public get assetKey(): string {
            return this._assetKey;
        }
        public get key(): string {
            return this._key;
        }
        public get group(): string {
            return this._group;
        }
        public get volume(): number {
            return this._volume;
        }
        public get loop(): boolean {
            return this._loop;
        }
    }
}
