﻿module TypeScriptPhaserSlot.common {

    export class WinningShowingSettings {

        private _delayBetweenWinningShowing: number;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): WinningShowingSettings {
            this._delayBetweenWinningShowing = rawData.delayBetweenWinningShowing || 0;

            return this;
        }

        public get delayBetweenWinningShowing(): number {
            return this._delayBetweenWinningShowing;
        }


    }
}
