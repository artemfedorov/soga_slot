﻿module TypeScriptPhaserSlot.common {

    export class WinScattersConfiguration {

        private _index: number;
        private _sounds: string[];
        private _timeout: number;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): WinScattersConfiguration {
            this._index = rawData.index;
            this._sounds = rawData.sounds;
            this._timeout = rawData.timeout;
            return this;
        }

        public get index(): number {
            return this._index;
        }

        public get sounds(): string[] {
            return this._sounds;
        }

        public get timeout(): number {
            return this._timeout;
        }
    }
}
