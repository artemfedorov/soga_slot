﻿module TypeScriptPhaserSlot.common {

    export class EmotionConfiguration {

        private _type: string;
        private _className:string;
        private _coef: number;
        private _count: number;
        private _lines: number;
        private _emotion: AnimationConfiguration;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        /**
         *
         * @param rawData
         * @returns {EmotionsConfiguration}
         */
        private parse(rawData: any): EmotionConfiguration {
            this._type = rawData.type;
            this._className = rawData.className;
            this._coef = rawData.coef;
            this._count = rawData.count;
            this._lines = rawData.lines;
            if(rawData.emotion) {
                this._emotion = new AnimationConfiguration(rawData.emotion);
            }
            return this;
        }

        get type(): string {
            return this._type;
        }
        get className(): string {
            return this._className;
        }
        get coef(): number {
            return this._coef;
        }
        get count(): number {
            return this._count;
        }
        get lines(): number {
            return this._lines;
        }
        get emotion(): AnimationConfiguration {
            return this._emotion;
        }
    }
}
