﻿module TypeScriptPhaserSlot.common {

    export class GameConfiguration {

        private _gameName: string;
        private _clientAPI: string;
        private _states: StateConfiguration[];
        private _symbols: SymbolProperties[];
        private _standartSymbolSetting: StandartSymbolSetting;
        private _rollingMechanics: RollingMechanicsConfiguration;
        private _reels: ReelConfiguration[];
        private _paylines: WinLineConfiguration[];
        private _scatters: WinScattersConfiguration[];
        private _buttons: ButtonConfiguration[];
        private _emotions: EmotionConfiguration[];
        private _winningShowingSettings: WinningShowingSettings;
        private _sounds: SoundConfiguration[];
        private _animations: AnimationConfiguration[];

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): GameConfiguration {
            this._gameName = rawData.gameName;
            this._clientAPI = rawData.clientAPI;
            this._winningShowingSettings = new WinningShowingSettings(rawData.winningShowingSettings);
            this._standartSymbolSetting = new StandartSymbolSetting(rawData.standartSymbolSetting);
            this._states = [];
            for (let i: number = 0; i < rawData.states.length; i++) {
                this._states.push(new StateConfiguration(rawData.states[i]));
            }

            this._symbols = [];
            for (let i: number = 0; i < rawData.symbols.length; i++) {
                this._symbols.push(new SymbolProperties(rawData.symbols[i]));
            }
            this._rollingMechanics = new RollingMechanicsConfiguration(rawData.rollingMechanics);
            this._reels = [];
            this._paylines = [];
            this._scatters = [];
            this._buttons = [];
            this._emotions = [];
            this._sounds = [];
            this._animations = [];

            if (rawData.reels) {
                for (let i: number = 0; i < rawData.reels.length; i++) {
                    this._reels.push(new ReelConfiguration(rawData.reels[i]));
                }
            }

            if (rawData.paylines) {
                for (let i: number = 0; i < rawData.paylines.length; i++) {
                    this._paylines.push(new WinLineConfiguration(rawData.paylines[i]));
                }
            }

            if (rawData.scatters) {
                for (let i: number = 0; i < rawData.scatters.length; i++) {
                    this._scatters.push(new WinScattersConfiguration(rawData.scatters[i]));
                }
            }

            if (rawData.buttons) {
                for (let i: number = 0; i < rawData.buttons.length; i++) {
                    this._buttons.push(new ButtonConfiguration(rawData.buttons[i]));
                }
            }

            if (rawData.emotions) {
                for (let i: number = 0; i < rawData.emotions.length; i++) {
                    this._emotions.push(new EmotionConfiguration(rawData.emotions[i]));
                }
            }

            if (rawData.sounds) {
                for (let i: number = 0; i < rawData.sounds.length; i++) {
                    this._sounds.push(new SoundConfiguration(rawData.sounds[i]));
                }
            }

            if (rawData.animations) {
                for (let i: number = 0; i < rawData.animations.length; i++) {
                    this._animations.push(new AnimationConfiguration(rawData.animations[i]));
                }
            }

            return this;
        }


        public get standartSymbolSetting(): StandartSymbolSetting {
            return this._standartSymbolSetting;
        }

        public get gameName(): string {
            return this._gameName;
        }

        public get clientAPI(): string {
            return this._clientAPI;
        }

        public get symbols(): SymbolProperties[] {
            return this._symbols;
        }

        public get rollingMechanics(): RollingMechanicsConfiguration {
            return this._rollingMechanics;
        }

        public get reels(): ReelConfiguration[] {
            return this._reels;
        }

        public get paylines(): WinLineConfiguration[] {
            return this._paylines;
        }

        public get scatters(): WinScattersConfiguration[] {
            return this._scatters;
        }

        public get states(): StateConfiguration[] {
            return this._states;
        }

        public get buttons(): ButtonConfiguration[] {
            return this._buttons;
        }

        public get emotions(): EmotionConfiguration[] {
            return this._emotions;
        }

        public get winningShowingSettings(): WinningShowingSettings {
            return this._winningShowingSettings;
        }

        public get sounds(): SoundConfiguration[] {
            return this._sounds;
        }

        public get animations(): AnimationConfiguration[] {
            return this._animations;
        }
    }

    export class StandartSymbolSetting {

        private _width: number;
        private _height: number;
        private _borders: string[];
        private _trackedSymbols: number[];
        private _trackedSymbolsMin: number;
        private _trackedSymbolsMinRequired: number;
        private _className: string;

        constructor(rawData: any) {
            this._width = rawData.width;
            this._height = rawData.height;
            this._borders = rawData.borders;
            this._className = rawData.className;
            this._trackedSymbols = rawData.trackedSymbols;
            this._trackedSymbolsMin = rawData.trackedSymbolsMin;
            this._trackedSymbolsMinRequired = rawData.trackedSymbolsMinRequired;
        }

        public get width(): number {
            return this._width;
        }

        public get trackedSymbols(): number[] {
            return this._trackedSymbols;
        }

        public get trackedSymbolsMin(): number {
            return this._trackedSymbolsMin;
        }

        public get trackedSymbolsMinRequired(): number {
            return this._trackedSymbolsMinRequired;
        }

        public get height(): number {
            return this._height;
        }

        public get borders(): string[] {
            return this._borders;
        }

        public get className(): string {
            return this._className;
        }
    }
}
