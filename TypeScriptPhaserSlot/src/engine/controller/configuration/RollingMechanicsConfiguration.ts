﻿module TypeScriptPhaserSlot.common {

    export class RollingMechanicsConfiguration {

        private _tweenToTop: any;
        private _tweenAccelaration: any;
        private _resultedTween: any;
        private _tweenUpturn: any;

        private easeMap = {

            "Power0": Phaser.Easing.Power0,
            "Power1": Phaser.Easing.Power1,
            "Power2": Phaser.Easing.power2,
            "Power3": Phaser.Easing.power3,
            "Power4": Phaser.Easing.power4,

        "Linear": Phaser.Easing.Linear.None,
        "Quad": Phaser.Easing.Quadratic.Out,
        "Cubic": Phaser.Easing.Cubic.Out,
        "Quart": Phaser.Easing.Quartic.Out,
        "Quint": Phaser.Easing.Quintic.Out,
        "Sine": Phaser.Easing.Sinusoidal.Out,
        "Expo": Phaser.Easing.Exponential.Out,
        "Circ": Phaser.Easing.Circular.Out,
        "Elastic": Phaser.Easing.Elastic.Out,
        "Back": Phaser.Easing.Back.Out,
        "Bounce": Phaser.Easing.Bounce.Out,

        "Quad.easeIn": Phaser.Easing.Quadratic.In,
        "Cubic.easeIn": Phaser.Easing.Cubic.In,
        "Quart.easeIn": Phaser.Easing.Quartic.In,
        "Quint.easeIn": Phaser.Easing.Quintic.In,
        "Sine.easeIn": Phaser.Easing.Sinusoidal.In,
        "Expo.easeIn": Phaser.Easing.Exponential.In,
        "Circ.easeIn": Phaser.Easing.Circular.In,
        "Elastic.easeIn": Phaser.Easing.Elastic.In,
        "Back.easeIn": Phaser.Easing.Back.In,
        "Bounce.easeIn": Phaser.Easing.Bounce.In,

        "Quad.easeOut": Phaser.Easing.Quadratic.Out,
        "Cubic.easeOut": Phaser.Easing.Cubic.Out,
        "Quart.easeOut": Phaser.Easing.Quartic.Out,
        "Quint.easeOut": Phaser.Easing.Quintic.Out,
        "Sine.easeOut": Phaser.Easing.Sinusoidal.Out,
        "Expo.easeOut": Phaser.Easing.Exponential.Out,
        "Circ.easeOut": Phaser.Easing.Circular.Out,
        "Elastic.easeOut": Phaser.Easing.Elastic.Out,
        "Back.easeOut": Phaser.Easing.Back.Out,
        "Bounce.easeOut": Phaser.Easing.Bounce.Out,

        "Quad.easeInOut": Phaser.Easing.Quadratic.InOut,
        "Cubic.easeInOut": Phaser.Easing.Cubic.InOut,
        "Quart.easeInOut": Phaser.Easing.Quartic.InOut,
        "Quint.easeInOut": Phaser.Easing.Quintic.InOut,
        "Sine.easeInOut": Phaser.Easing.Sinusoidal.InOut,
        "Expo.easeInOut": Phaser.Easing.Exponential.InOut,
        "Circ.easeInOut": Phaser.Easing.Circular.InOut,
        "Elastic.easeInOut": Phaser.Easing.Elastic.InOut,
        "Back.easeInOut": Phaser.Easing.Back.InOut,
        "Bounce.easeInOut": Phaser.Easing.Bounce.InOut

    };

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): RollingMechanicsConfiguration {
            this._tweenToTop = rawData.tweenToTop;
            this._tweenAccelaration = rawData.tweenAccelaration;
            this._resultedTween = rawData.resultedTween;
            this._tweenUpturn = rawData.tweenUpturn;

            return this;
        }

        public tweenOf(str: string): any {
            return this.easeMap[str];
        }
        public get tweenToTop(): any {
            return this._tweenToTop;
        }
        public get tweenAccelaration(): any {
            return this._tweenAccelaration;
        }
        public get resultedTween(): any {
            return this._resultedTween;
        }
        public get tweenUpturn(): any {
            return this._tweenUpturn;
        }
    }
}
