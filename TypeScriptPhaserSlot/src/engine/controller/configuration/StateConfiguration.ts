﻿module TypeScriptPhaserSlot.common {

    export class StateConfiguration {

        private _index: number;
        private _name: string;
        private _gotoStateName: string;
        private _isAction: boolean;
        private _isRequiredAction: boolean;

        constructor(rawData: any) {
            this.parse(rawData);
        }

        private parse(rawData: any): StateConfiguration {
            this._index = rawData.index;
            this._name = rawData.name;
            this._gotoStateName = rawData.gotoStateName;
            this._isAction = rawData.isAction;
            this._isRequiredAction = rawData.isRequiredAction;
            return this;
        }

        public get index(): number {
            return this._index;
        }

        public get name(): string {
            return this._name;
        }

        public get gotoStateName(): string {
            return this._gotoStateName;
        }

        public get isAction(): boolean {
            return this._isAction;
        }
        public get isRequiredAction(): boolean {
            return this._isRequiredAction;
        }
    }
}
