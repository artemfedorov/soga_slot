﻿module TypeScriptPhaserSlot.common {

    export class ButtonActionConstants {

        public static HELP_ACTION: string =         "btn_help";
        public static SETTING_ACTION: string =      "btn_settings";
        public static MUTE_ACTION: string =         "btn_mute";
        public static HOME_ACTION: string =         "btn_home";
        public static SPIN_ACTION: string =         "spin";
        public static STOP_ACTION: string =         "stop";
        public static COLLECT_ACTION: string =      "collect";
        public static AUTO_ACTION: string =         "auto";
        public static GAMBLE_ACTION: string =       "gamble";
        public static AUTO_TURBO_ACTION: string =   "turbo";
        public static MAXBET_ACTION: string =       "maxbet_spin";
        public static BET_INC_ACTION: string =      "bet_inc";
        public static BET_DEC_ACTION: string =      "bet_dec";
        public static LINES_INC_ACTION: string =    "lines_inc";
        public static LINES_DEC_ACTION: string =    "lines_dec";
        public static SETTINGS_ACTION: string =     "settings";
        public static GALLERY_ACTION: string =      "gallery";
        public static INFO_ACTION: string =         "info";

    }
}