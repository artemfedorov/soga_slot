///<reference path="./interfaces/IEmotionsController.ts" />
///<reference path="./ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class EmotionsController extends ActionController implements IEmotionsController{

        protected gameStateResponse: GameStateResponse;
        protected gameConfiguration: GameConfiguration;
        protected mainPattern: any;
        protected layer: Phaser.Group;
        protected emotions: IEmotion[];
        protected spinResult: SpinResult;
        protected emotionsConfigs: EmotionConfiguration[];

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, gameConfiguration: GameConfiguration, pattern: any, gameStateResponse: GameStateResponse) {
            super(game, name, debugMode);
            this.layer = layer;
            this.gameStateResponse = gameStateResponse;
            this.mainPattern = pattern;
            this.gameConfiguration = gameConfiguration;
            this.init();
        }

        /**
         *
         */
        public init(): void {
            this.emotions = [];
        }

        /**
         *
         */
        public start(data: SpinResult):void {
            this.spinResult = data;
            let emotionsConfigs:EmotionConfiguration[] = this.findEmotions(data);

            if(emotionsConfigs.length == 0) {
                this.actionSignal.dispatch("emotionsComplete");
            } else {
                this.startEmotions(emotionsConfigs);
            }
        }

        /**
         *
         */
        public stopEmotion(): void {
            let emotion: IEmotion;
            for(let i: number = 0; i < this.emotions.length; i++) {
                emotion = this.emotions[i];
                emotion.stop();
                emotion.parent.removeChild(emotion);
                this.layer.remove(emotion);
                emotion.destroy();
                emotion = null;

                let emotionsSoundGroup:SoundGroup = SoundController.instance.getGroup('emotions');
                if(emotionsSoundGroup) {
                    emotionsSoundGroup.stopAllSounds();
                }
            }
            this.emotions = [];
        }

        /**
         *
         * @param data
         * @returns {EmotionConfiguration[]}
         */
        protected findEmotions(data: SpinResult): EmotionConfiguration[] {
            let emotionsConfigs: EmotionConfiguration[] = [];

            let tempEmotionsConfigsArr: EmotionConfiguration[] = this.gameConfiguration.emotions.concat();

            tempEmotionsConfigsArr.sort(this.sortFunction);

            let emotionData:EmotionConfiguration;

            for(let i:number = 0; i < tempEmotionsConfigsArr.length; i++) {
                emotionData = tempEmotionsConfigsArr[i];
                if(emotionData.lines <= data.paylines.length) {
                    emotionsConfigs.push(emotionData);
                    break;
                }
            }

            for(let i:number = 0; i < tempEmotionsConfigsArr.length; i++) {
                emotionData = tempEmotionsConfigsArr[i];
                if(emotionData.coef * this.game.state.getCurrentState()['model'].totalBet <= data.payout) {
                    emotionsConfigs.push(emotionData);
                    break;
                }
            }

            // TODO: refactor this function
            for(let j:number = 0; j < data.paylines.length; j++) {
                let arr:EmotionConfiguration[] = tempEmotionsConfigsArr.filter(function (obj:EmotionConfiguration) {
                    if(obj.count && obj.count == data.paylines[j].icon_count) {
                        tempEmotionsConfigsArr.splice(tempEmotionsConfigsArr.indexOf(obj), 1);
                        return true;
                    }
                    return false;
                }.bind(this));
                if(arr) {
                    emotionsConfigs = emotionsConfigs.concat(arr);
                }
            }

            return emotionsConfigs;
        }

        /**
         *
         * @param a
         * @param b
         * @returns {number}
         */
        protected sortFunction(a:EmotionConfiguration, b:EmotionConfiguration):number {
            return (b.coef || 0) - (a.coef || 0);
        }

        /**
         *
         * @param emotionsConfigs
         */
        public startEmotions(emotionsConfigs:EmotionConfiguration[]): void {
            this.stopEmotion();

            this.emotionsConfigs = emotionsConfigs;

            this.animateEmotion(this.emotionsConfigs.shift());
            this.actionSignal.dispatch("startShowedEmotions");
        }

        /**
         *
         * @param winning
         * @param count
         * @param type
         */
        public startEmotion(type:string, winning:number, count:number): void {
            this.stopEmotion();

            let emotionData: EmotionConfiguration;
            for(let i:number = 0; i < this.gameConfiguration.emotions.length; i++) {
                emotionData = this.gameConfiguration.emotions[i];
                if(emotionData.type == type) {
                    if(emotionData.coef && emotionData.coef <= winning) {
                        this.animateEmotion(emotionData);
                    } else if(emotionData.count && emotionData.count >= count) {
                        this.animateEmotion(emotionData);
                    }
                }
            }
        }

        /**
         *
         * @param emotionData
         */
        protected animateEmotion(emotionData:EmotionConfiguration): void {
            let emotion: IEmotion = new TypeScriptPhaserSlot.common[emotionData.className](this.game, emotionData);
            emotion.actionSignal.addOnce(this.onAnimationAction, this);

            //animation.x = this.pattern.reels[reelIndex].x;
            //animation.y = this.pattern.reels[reelIndex]["icon" + symbolIndex].y;
            this.layer.add(emotion);

            this.emotions.push(emotion);

            emotion.start();
        }

        /**
         *
         * @param data
         */
        protected onAnimationAction(data: any): void {
            if(data == AnimatedSprite.ALL_ANIMATIONS_COMPLETE) {
                if(this.emotionsConfigs.length > 0) {
                    this.stopEmotion();
                    this.animateEmotion(this.emotionsConfigs.shift());
                } else {
                    this.actionSignal.dispatch("stopShowedEmotions");
                    this.actionSignal.dispatch("emotionsComplete");
                    this.stopEmotion();
                }
            }
        }
    }
}