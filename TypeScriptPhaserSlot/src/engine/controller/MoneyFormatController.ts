///<reference path="./ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class MoneyFormatController extends ActionController{

        private static _instance:MoneyFormatController;

        protected gameConfiguration:GameConfiguration;
        protected gameStateResponse:GameStateResponse;

        constructor(game: Game, name: string, debugMode: boolean, gameConfiguration: GameConfiguration, gameStateResponse: GameStateResponse) {
            if(MoneyFormatController._instance) {
                throw new Error("Error: Instantiation failed: Use MoneyFormatController.instance instead of new.");
            }

            super(game, name, debugMode);

            this.gameConfiguration = gameConfiguration;
            this.gameStateResponse = gameStateResponse;

            this.init();

            MoneyFormatController._instance = this;
        }

        /**
         *
         */
        public init(): void {

        }

        public format(value:number, withoutSymbol = false): string {
            if(window['GR']) {
                return window['GR'].CurrencyFormatter.format(value, withoutSymbol);
            }
            //return (+value.toFixed(10)).toString();
            return (+value.toFixed(10)).toFixed(2);
        }

        public static get instance(): MoneyFormatController {
            return MoneyFormatController._instance;
        }
    }
}