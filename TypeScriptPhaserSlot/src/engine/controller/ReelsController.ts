﻿///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />

module TypeScriptPhaserSlot.common {

    export class ReelsController extends ActionController implements IReelController {

        public static RESPIN_COMPLETE: string = "RespinComplete";
        public static FINISH_SHOW_WINNING: string = "FinishShowWinning";
        public static STOP: string = "Stop";
        public static INTRIGUE: string = "Intrigue";
        public static FINISH_REMOVE_SYMBOLS: string = "FinishRemoveSymbols";


        protected isPendingRespinComplete: boolean = false;
        protected stopSpinData: SpinResult;
        protected justTouchedCounter: number;
        protected stopCounter: number;
        protected mapReelFreespins: number[][];
        protected mapReel: number[][];
        protected gameStateResponse: GameStateResponse;
        protected symbolProperties: SymbolProperties[];
        protected gameConfiguration: GameConfiguration;
        protected reelViews: Array<IReelView>;
        protected mainPattern: any;
        protected pattern: any;
        protected layer: Phaser.Group;
        protected borderLayer: Phaser.Group;
        protected respinData: any;
        protected trackedSymbols: number[];
        protected trackedSymbolsCalculation: number[];
        protected additionalSymbolsNumbers: number[];
        protected addedSymbols: number[];
        protected spinTimeout: any;
        protected isStopping: boolean;
        protected _symbolsEnabled: boolean = true;
        protected reelsOrderStoping: number[];

        /*
         *Flag shows is stopping requires urgent/fast stop.
         * */
        protected isUrgentStop: boolean;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfiguration: GameConfiguration, pattern: any, gameStateResponse: GameStateResponse) {
            super(game, name, debugMode);
            this.layer = layer;
            this.gameStateResponse = gameStateResponse;
            this.mapReel = gameStateResponse.reelMap;
            this.mapReelFreespins = gameStateResponse.reelMapFreespins;
            this.symbolProperties = symbolProperties;
            this.mainPattern = pattern;
            this.addedSymbols = [];
            this.additionalSymbolsNumbers = [];
            this.pattern = pattern.reels;
            this.gameConfiguration = gameConfiguration;
            this.trackedSymbolsCalculation = [];
            this.trackedSymbols = this.gameConfiguration.standartSymbolSetting.trackedSymbols;
            this.reelsOrderStoping = [];
            for(let i = 0; i < this.gameConfiguration.reels.length; i++) {
                this.reelsOrderStoping[this.gameConfiguration.reels[i].stopIndex] = i;
            }
            this.init(gameStateResponse);

        }

        /**
         * Building reel views
         */
        public init(gameStateResponse: GameStateResponse): void {
            this.reelViews = [];
            //Creating the reels
            this.buildReels(gameStateResponse);
        }

        public rebuildReels(gameStateResponse: GameStateResponse) {
            this.reelViews.forEach(function (reel:IReelView) {
                reel.destroy(true, false);
            }, this);
            this.reelViews = [];
            Game.pool = [];
            this.buildReels(gameStateResponse);
            /*this.reelViews.forEach(function(reel:IReelView):void {
                reel.visible = true;
                reel.symbols.forEach(function (symbol:ISymbol):void {
                    symbol.visible = true;
                }, this);
            }, this);*/
        }

        /**
         * Called by GameController once Respin State comes
         */
        public respin(data?: any): void {
            this.respinData = data;
            //please override this gently
        }

        public stopRespin(data?: any): void {
            this.respinData = data;
            //please override this gently
        }

        /**
         *
         */
        public start(data?: any): void {
            this.isStopping = false;
            this.isUrgentStop = false;
            this.stopSpinData = null;
            this.justTouchedCounter = 0;
            this.stopCounter = 0;
            let t: number = 200 + Number(this.gameConfiguration.rollingMechanics.tweenToTop.time) * Phaser.Timer.SECOND;
            setTimeout(() => {SoundController.instance.playSound('start_spin_sound', 'default', 0.1, true);
            }, t);
            this.spin(0);
        }

        /**
         *
         * @param {number} reelIndex
         */
        protected spin(reelIndex: number): void {
            let delay: number;
            this.isUrgentStop = false;
            this.trackedSymbolsCalculation.length = 0;
            this.reelViews[reelIndex].start();
            if (reelIndex + 1 < this.reelViews.length) {
                delay = this.gameConfiguration.reels[reelIndex].startTimeout * Phaser.Timer.SECOND;
                if (delay == 0 || reelIndex == 0) {
                    this.spin(reelIndex + 1);
                } else {
                    setTimeout(() => {
                        this.spin(reelIndex + 1)
                    }, delay);
                }
            }
        }

        /**
         * Stop showing
         */
        public stop(data: SpinResult): void {
            let timeout: number = this.isUrgentStop ? 0 : 1000;
            this.stopSpinData = data;

            this.alignReels();
            this.handleTrackedSymbols();
            this.spinTimeout = setTimeout(() => {
                this.reelStop(data, 0)
            }, timeout);
        }



        protected alignReels(): void {
            let maxLengthReelIndex: number = 0;
            let amount: number;
            for (let i: number = 0; i < this.reelViews.length; i++) {
                maxLengthReelIndex = this.reelViews[i].symbols.length > this.reelViews[maxLengthReelIndex].symbols.length ? i : maxLengthReelIndex;
            }

            amount = this.reelViews[maxLengthReelIndex].symbols.length;

            for (let i: number = 0; i < this.reelViews.length; i++) {
                this.debuglog("total symbols in reel#" + i + " = " + this.reelViews[i].symbols.length);
                let addSymbols: number = amount - this.reelViews[i].symbols.length;
                this.additionalSymbolsNumbers[i] = addSymbols;
            }
        }

        /**
         *
         * @param data
         * @param reelIndex
         */
        protected reelStop(data: SpinResult, reelIndex: number): void {
            this.isStopping = true;
            let timeout: number = this.isUrgentStop ? 0 : this.gameConfiguration.reels[reelIndex].stopTimeout;

            this.reelViews[this.reelsOrderStoping[reelIndex]].stop(data.reels[this.reelsOrderStoping[reelIndex]].concat(), this.additionalSymbolsNumbers[reelIndex]);
            if (reelIndex + 1 < this.reelViews.length) {
                setTimeout(() => {
                    this.reelStop(data, reelIndex + 1)
                }, timeout);
            }
        }

        /**
         *
         */
        public urgentStop(): void {
            this.isUrgentStop = true;
            if (this.spinTimeout) {
                clearTimeout(this.spinTimeout);
                if (!this.isStopping && this.stopSpinData) {
                    this.reelStop(this.stopSpinData, 0);
                }
            }
        }

        /**
         * Callback stopping the reel
         * @param data
         */
        public onReelStoped(data?: any): void {
            this.state.finishWork();
        }

        /**
         *
         * @param {GameStateResponse} gameStateResponse
         */
        protected buildReels(gameStateResponse: GameStateResponse): void {
            this.layer.x = this.pattern.x;
            this.layer.y = this.pattern.y;
            this.borderLayer = new Phaser.Group(this.game);
            let reelsAmount: number = this.gameConfiguration.reels.length;
            let reelView: IReelView;
            for (let i: number = 0; i < reelsAmount; i++) {
                reelView = this.buildReel(i, this.gameConfiguration.gameName + String(i), this.debugMode, this.symbolProperties,
                    this.gameConfiguration, this.pattern, gameStateResponse.reels[i].concat(), this.borderLayer);
                this.layer.add(reelView);
                this.reelViews.push(reelView);
            }
            this.layer.add(this.borderLayer);
        }

        /**
         * JUST TO BE OVERRIDEN
         * DO NOT CALL SUPER!
         * @param index
         * @param name
         * @param debugMode
         * @param symbolProperties
         * @param gameConfiguration
         * @param pattern
         * @param dataReel
         * @param borderLayer
         * @returns {IReelView}
         */
        protected buildReel(index: number, name: string, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfiguration: GameConfiguration, pattern: any, dataReel: number[], borderLayer?: Phaser.Group): IReelView {
            let reelView: IReelView;
            reelView.justTouchedStopSignal.add(this.onJustTouchedReelStop, this);
            reelView.actionSignal.add(this.onReelFinnalyStopped, this);
            return reelView;
        }


        /**
         *
         */
        protected handleTrackedSymbols(): void {
            let symbolIndex: number;
            for (let reelIndex: number = 0; reelIndex < this.stopSpinData.reels.length; reelIndex++) {
                for (let i: number = 0; i < this.stopSpinData.reels[reelIndex].length; i++) {
                    symbolIndex = this.stopSpinData.reels[reelIndex][i];
                    if (this.trackedSymbols.indexOf(symbolIndex) > -1) {
                        if (!this.trackedSymbolsCalculation[symbolIndex]) {
                            this.trackedSymbolsCalculation[symbolIndex] = 0;
                        }
                        this.trackedSymbolsCalculation[symbolIndex]++;
                        if (this.trackedSymbolsCalculation[symbolIndex] == this.gameConfiguration.standartSymbolSetting.trackedSymbolsMin &&
                            this.gameConfiguration.reels.length - (reelIndex + 1) > 0) {
                            for (let j: number = reelIndex + 1; j < this.gameConfiguration.reels.length; j++) {
                                this.reelViews[j].isIntrigued = true;
                            }
                        }
                    }
                }
            }
        }

        /**
         * Signals when particular reel touched the ground:) before ending.
         * @param {number} reelIndex
         */
        protected onJustTouchedReelStop(reelIndex: number): void {
            if (reelIndex + 1 < this.reelViews.length) {
                if (this.reelViews[reelIndex + 1].isIntrigued) {
                    this.startIntrigue(reelIndex + 1);
                }
            } else {
                this.stopIntrigue(reelIndex);
            }
            this.justTouchedCounter++;

            if (this.justTouchedCounter == this.gameConfiguration.reels.length) {
                let stopSound: Phaser.Sound = SoundController.instance.getSound("start_spin_sound", 'default');
                if(stopSound) {
                    stopSound.stop();
                }
            }
        }


        /**
         *
         * @param reelIndex
         */
        protected onReelFinnalyStopped(reelIndex: number): void {
            if (this.isPendingRespinComplete) {
                this.actionSignal.dispatch(ReelsController.RESPIN_COMPLETE);
                this.isPendingRespinComplete = false;
                return;
            }

            this.stopCounter++;

            if (this.stopCounter == this.gameConfiguration.reels.length) {
                this.stopIntrigue();

                this.actionSignal.dispatch(ReelsController.STOP);
            }
        }



        public animateSymbols(data: any): void {
            this.animateSymbolsOfLine(data.paylines[0]);
        }

        /**
         *
         * @param line
         */
        public animateSymbolsOfLine(line: Payline): void {
            this.stopAnimation(true);

            if(this._symbolsEnabled) {
                this.hideShowNoWinSymbols(false);
            }

            let index: number;
            for (let j: number = line.offset; j < line.offset + line.icon_count; j++) {
                index = line.line[j];
                let symbol:ISymbol = this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount];
                symbol.hide();
            }
        }

        public animateScatters(scatter: Scatter): void {
            this.stopAnimation(true);

            if(this._symbolsEnabled) {
                this.hideShowNoWinSymbols(false);
            }

            let indexes: number[];
            let symbolIndex: number;
            for (let j: number = 0; j < scatter.indexes.length; j++) {
                indexes = scatter.indexes[j];
                let symbol:ISymbol = this.reelViews[indexes[0]].symbols[indexes[1] + this.reelViews[indexes[0]].reelConfig.topInvisibleSymbolsCount];
                symbol.hide();
            }
        }

        public stopAnimation(tween?:boolean): void {
            if(!tween) {
                this.hideShowNoWinSymbols(true);
            }

            for(let i:number = 0; i < this.reelViews.length; i++) {
                for(let j:number = 0; j < this.reelViews[i].symbols.length; j++) {
                    let symbol:ISymbol = this.reelViews[i].symbols[j];
                    if(symbol) {
                        this.reelViews[i].symbols[j].show();
                    }
                }
            }
        }

        /**
         *
         * @param show
         */
        protected hideShowNoWinSymbols(show:boolean): void {
            this._symbolsEnabled = show;
            for(let k:number = 0; k < this.reelViews.length; k++) {
                for(let l:number = 0; l < this.reelViews[k].symbols.length; l++) {
                    let item: ISymbol = this.reelViews[k].symbols[l];
                    if(item) {
                        if(show) {
                            item.activate();
                        } else {
                            item.deactivate();
                        }
                    }
                }
            }
        }

        /**
         * Override this to run your specific intrigue like a animation and so on.
         */
        protected startIntrigue(reelIndex: number): void {

        }

        /**
         * Override this to stop your specific intrigue like a animation and so on.
         */
        protected stopIntrigue(reelIndex?: number): void {

        }

        /**
         *
         * @param index
         */
        protected animateSpecificSymbolWithIndex(index: number): void {

        }
    }
}