///<reference path="./interfaces/IFreeSpinsController.ts" />
///<reference path="./ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class FreeSpinsController extends ActionController implements IFreeSpinsController{

        protected gameStateResponse: GameStateResponse;
        protected gameConfiguration: GameConfiguration;
        protected mainPattern: any;
        protected layer: Phaser.Group;
        protected animations: AnimatedSprite[];
        protected spinResult: SpinResult;
        protected emotions: EmotionConfiguration[];

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, gameConfiguration: GameConfiguration, pattern: any, gameStateResponse: GameStateResponse) {
            super(game, name, debugMode);
            this.layer = layer;
            this.gameStateResponse = gameStateResponse;
            this.mainPattern = pattern;
            this.gameConfiguration = gameConfiguration;
            this.init();
        }

        public init():void {
            super.init();
        }

        public startFreeSpins(data?:any):void {

        }

        public addedFreeSpins(data?:any):void {

        }

        public endOfFreeSpins(data?:any):void {

        }

        public leftFreeSpins(data?:any):void {

        }
    }
}