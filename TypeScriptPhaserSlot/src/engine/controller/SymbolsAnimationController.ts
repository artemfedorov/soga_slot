///<reference path="./ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class SymbolsAnimationController extends ActionController implements ISymbolsAnimationController{

        protected gameStateResponse: GameStateResponse;
        protected symbolProperties: SymbolProperties[];
        protected gameConfiguration: GameConfiguration;
        protected mainPattern: any;
        protected pattern: any;
        protected layer: Phaser.Group;
        protected animations: AnimatedSprite[];
        protected startCount:number;
        protected spinResult: SpinResult;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, symbolProperties: SymbolProperties[], gameConfiguration: GameConfiguration, pattern: any, gameStateResponse: GameStateResponse) {
            super(game, name, debugMode);
            this.layer = layer;
            this.gameStateResponse = gameStateResponse;
            this.symbolProperties = symbolProperties;
            this.mainPattern = pattern;
            this.pattern = pattern.reels;
            this.gameConfiguration = gameConfiguration;
            this.init();
        }

        /**
         *
         */
        public init(): void {
            this.animations = [];
            this.startCount = 0;

            this.layer.x = this.pattern.x;
            this.layer.y = this.pattern.y;

            let mask = new Phaser.Graphics(this.game, 0, 0);
            mask.beginFill(0xffffff, 0.5);
            let w:number = 171;
            let h:number = 171;
            mask.drawRect(-(w / 2), -(h / 2), w * 5 + w, h * 3 + h);

            //TODO: вернуть маску если нужно
            //this.layer.add(mask);
            //this.layer.mask = mask;
        }

        /**
         *
         */
        public stopAnimation(): void {
            let animation: AnimatedSprite;
            for(let i: number = 0; i < this.animations.length; i++) {
                animation = this.animations[i];
                animation.stop();
                animation.parent.removeChild(animation);
                this.layer.remove(animation);
                animation.animations.destroy();
                animation.destroy();
                animation = null;
            }
            this.animations = [];
            this.startCount = 0;
        }

        /**
         *
         * @param data
         */
        public animateSymbols(data: SpinResult): void {
            this.animateSymbolsOfLine(data.paylines[0], data);
        }

        /**
         *
         * @param line
         * @param spinResult
         */
        public animateSymbolsOfLine(line: Payline, spinResult: SpinResult): void {
            this.stopAnimation();

            this.spinResult = spinResult;
            this.correctSpinResult();

            let index: number;
            let symbolIndex: number;

            console.log('line.offset', line.offset, 'line.icon_count', line.icon_count);

            for (let j: number = line.offset; j < line.offset + line.icon_count; j++) {
                index = line.line[j];
                symbolIndex = this.spinResult.reels[j][index];

                this.animatSymbol(j, index, symbolIndex);
                this.startCount++;
            }
        }

        public animateScatters(scatter: Scatter, spinResult: SpinResult, delayBetween?: number): void {
            this.stopAnimation();
            this.spinResult = spinResult;
            this.correctSpinResult();

            let indexes: number[];
            let symbolIndex: number;
            for (let j: number = 0; j < scatter.indexes.length; j++) {
                indexes = scatter.indexes[j];
                symbolIndex = this.spinResult.reels[indexes[0]][indexes[1]];
                this.animatSymbol(indexes[0], indexes[1], symbolIndex, delayBetween * j);

                this.startCount++;
            }
        }

        /**
         *
         */
        protected correctSpinResult():void {

        }

        /**
         *
         * @param symbolData
         * @returns {SimpleSprite}
         */
        protected createAnimation(symbolData: any): AnimatedSprite {
            let currentSymbolProperties: SymbolProperties = null;
            this.symbolProperties.forEach(function (item: SymbolProperties) {
                if(item.index == symbolData) {
                    currentSymbolProperties = item;
                }
            }.bind(this));

            let animation: AnimatedSprite = new AnimatedSprite(this.game, 0, 0, currentSymbolProperties.animationData, this.debugMode);
            return animation;
        }

        /**
         *
         * @param reelIndex
         * @param symbolIndex
         * @param symbolData
         */
        protected animatSymbol(reelIndex: number, symbolIndex: number, symbolData:any, delayBetween?: number): void {
            let animation: AnimatedSprite = this.createAnimation(symbolData);
            let delay: number = delayBetween || 0;
            animation.x = this.pattern.reels[reelIndex].x;
            animation.y = this.pattern.reels[reelIndex]["icon" + symbolIndex].y;

            animation.actionSignal.addOnce(this.onAnimationAction, this);

            this.layer.add(animation);

            this.animations.push(animation);
            setTimeout(function() {
                animation.start();
            }, delayBetween);
        }

        protected onAnimationAction(data: any): void {
            if(data == AnimatedSprite.ANIMATION_FIRST_LOOP_COMPLETE) {
                this.startCount--;
                if(this.startCount == 0) {
                    //this.actionSignal.dispatch(ReelsController.FINISH_SHOW_WINNING);
                }
            }
        }
    }
}