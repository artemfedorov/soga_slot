﻿///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />

module TypeScriptPhaserSlot.common {

    export class PaytableController extends ActionController implements IPaytableController {

        protected _config: GameConfiguration;
        protected layer: Phaser.Group;
        protected view: BaseView;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, configuration: GameConfiguration) {
            super(game, name, debugMode);
            this._config = configuration;
            this.layer = layer;
            this.init();
        }

        /**
         * Initial creation
         */
        public init(): void {
            super.init();
            let viewName: string = this.getClassName("PaytableView");
            this.view = new TypeScriptPhaserSlot.common[viewName](this.game, viewName, this.game.pattern, this.debugMode);
            this.layer.add(this.view);

            this.hide();
        }

        /**
         *
         * @param className
         * @returns {string}
         */
        protected getClassName(className:string):string {
            if(TypeScriptPhaserSlot.common[this._config.gameName + className]) {
                return this._config.gameName + className;
            }
            return className;
        }

        /**
         *
         */
        public show():void {
            this.view['show']();
        }

        /**
         *
         */
        public hide():void {
            this.view['hide']();
        }

        /**
         *
         */
        public switch():void {
            if(this.view.visible) {
                this.hide();
            } else {
                this.show();
            }
        }

        /**
         *
         * @param pageIndex
         */
        public goToPage(pageIndex:number | string):void {
            this.view['goToPage'](pageIndex);
        }

        /**
         *
         * @returns {boolean}
         */
        public get isShown():boolean {
            return this.view.visible;
        }

    }
}