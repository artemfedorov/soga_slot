﻿///<reference path="./BaseController.ts" />
///<reference path="./interfaces/IActionController.ts" />

// Class which provides the work by Start-Stop proccess
module TypeScriptPhaserSlot.common {

    export abstract class ActionController extends BaseController implements IActionController {

        protected _state: IState;
        protected _action_signal: Phaser.Signal;
        private _game: Game;

        constructor(game: Game, name: string, debugMode: boolean) {
            super(name, debugMode);
            this._action_signal = new Phaser.Signal();
            this._game = game;
        }

        /**
         * Initialize
         */
        public init(data?: any): void {
            
        }

        /**
         * Start action
         */
        public start(data?: any): void {

        }

        /**
         * Stop action
         */
        public stop(data?: any): void {

        }

        /************************************************************
            Getters and Setters
        *************************************************************/
        public get game(): Game {
            return this._game;
        }

        public get actionSignal(): Phaser.Signal {
            return this._action_signal;
        }

        public get state(): IState {
            return this._state;
        }
    }
}