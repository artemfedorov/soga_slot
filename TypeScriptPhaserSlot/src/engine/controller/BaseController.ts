﻿///<reference path="./interfaces/IController.ts" />

module TypeScriptPhaserSlot.common {

    export abstract class BaseController implements IController {

        private _debugMode: boolean;
        private _name: string;

        constructor(name: string, debugMode: boolean = false) {
            this._name = name;
            this.debugMode = debugMode;
        }

        /**
         * Specific console logger
         * @param message
         */
        protected debuglog(message: any): void {
            console.log(this.name, ":", message);
        }

        /************************************************************
            Getters and Setters
        *************************************************************/
        public get debugMode(): boolean {
            return this._debugMode;
        }

        public set debugMode(value: boolean) {
            this._debugMode = value;
        }

        public get name(): string {
            return this._name;
        }
    }
}