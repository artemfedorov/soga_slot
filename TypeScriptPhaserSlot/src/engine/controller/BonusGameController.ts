﻿///<reference path="./interfaces/IActionController.ts" />
module TypeScriptPhaserSlot.common {

    export class BonusGameController extends ActionController {

        protected pattern: any;
        protected layer: Phaser.Group;


        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, gameConfiguration: GameConfiguration, pattern: any, gameStateResponse: GameStateResponse) {
            super(game, name, debugMode);
            this.pattern = pattern;
            this.layer = layer;
            this.layer.visible = false;
        }

        /**
        * Initial creation
        */
        public init(): void {
            super.init();
        }

        /**
         * Start showing
         */
        public start(data?: any): void {
            this.init();
            this.actionSignal.dispatch();

            this.layer.visible = true;
        }

        /**
         * Stop showing
         */
        public stop(data?: any): void {
            super.stop();

            this.layer.visible = false;
           
        }
    }
}