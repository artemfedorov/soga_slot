﻿// Example of any particular ActionController

///<reference path="./interfaces/IActionController.ts" />
///<reference path="./ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class InterfaceController extends ActionController implements IInterfaceController {

        protected _config: ButtonConfiguration[];
        protected buttonsList: IButton[];
        protected spaceBtn: Phaser.Key;
        protected layer: Phaser.Group;
        protected pattern: any;
        protected isAuto: boolean;
        protected currentState: string;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, configuration: ButtonConfiguration[], pattern: any) {
            super(game, name, debugMode);
            this.layer = layer;
            this.isAuto = false;
            this.pattern = pattern;
            this._config = configuration;
            this.layer.x = this.pattern.x;
            this.layer.y = this.pattern.y;

            this.init();
        }

        /**
        * Initial creation
        */
        public init(): void {
            this.buttonsList = [];
            for (let i: number = 0; i < this._config.length; i++) {
                this.createButton(this._config[i]);
            }

            let stateData: any = this.game.buttonsStates.states.default;
            for (let i: number = 0; i < stateData.length; i++) {
                this.setButtonByState(stateData[i]);
            } 

            this.spaceBtn = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            this.spaceBtn.onDown.add(this.onSpinPressed, this);
        }

        /**
         * 
         * @param value
         */
        public changeBetText(value: number): void {

        }

        /**
         *
         * @param value
         */
        public changeLinesText(value: number): void {

        }

        /**
         *
         * @param value
         */
        public changeTotalBetText(value: number): void {

        }

        /**
         * 
         * @param value
         */
        public changeWinText(value: number): void {

        }

        public resetWinText(): void {

        }

        /**
         *
         * @param value
         */
        public changeBalance(value: number): void {

        }

        public autoChanged(value: boolean): void {
            this.stateChanged();
        }

        /**
         * Called by GameController
         * Listener model changing
         * @param {string} stateName
         */
        public stateChanged(stateName?: string): void {
            this.isAuto = this.game.state.getCurrentState()['model'].isAutoPlay;
            this.currentState = stateName || this.currentState;

            /*let stateData: any = this.game.buttonsStates.states.default;
            for (let i: number = 0; i < stateData.length; i++) {
                this.setButtonByState(stateData[i]);
            }*/

            let stateData:any = this.game.buttonsStates.states[this.currentState];
            if (stateData) {
                for (let i: number = 0; i < stateData.length; i++) {
                    this.setButtonByState(stateData[i]);
                }
            }
        }

        /**
         * 
         * @param $buttonData
         */
        protected setButtonByState($buttonData: any): void {
            let button: IButton = this.getButtonByName($buttonData.name);
            if (!button) {
                return;
            }
            button.visible = $buttonData.visible;
            button.enabled = $buttonData.enabled;

            if ($buttonData.actions) {
                button.actions = $buttonData.actions;
            }

            /*if (this.isAuto) {
                button.enabled = $buttonData.enabledInAutoplay;
            }*/

            if (this.isAuto) {
                if ($buttonData.visibleInAutoplay != undefined) {
                    button.visible = $buttonData.visibleInAutoplay;
                }

                if ($buttonData.enabledInAutoplay != undefined) {
                    button.enabled = $buttonData.enabledInAutoplay;
                }
            }

            if (this.game.state.getCurrentState()['model'].isFreespins) {
                if (button.name != 'startSpin_btn' && button.name != 'stopSpin_btn') {
                    button.enabled = false;
                }
            }
        }

        /**
         * Release buttons handler
         * @param {BaseButton} button
         */
        protected callBack(button: IButton): void {
            this.debuglog(button.action + " pressed");
            this.actionSignal.dispatch(button.action);
        }


        /**
         * Creation all buttons
         * @param {ButtonConfiguration} data
         */
        protected createButton(data: ButtonConfiguration): void {
            let patternData: any = this.pattern[data.name];
            let btn: IButton = new TypeScriptPhaserSlot.common[data.className](this.game, data, patternData.x, patternData.y, this.callBack, this, true) as IButton;
            this.buttonsList.push(btn);
            //btn.anchor.set(0.5, 0.5);

            switch (data.action) {
                case ButtonActionConstants.BET_DEC_ACTION:
                    break;
                case ButtonActionConstants.BET_INC_ACTION:
                    break;
                case ButtonActionConstants.SPIN_ACTION:
                    break;
                case ButtonActionConstants.STOP_ACTION:
                    break;
                case ButtonActionConstants.MAXBET_ACTION:
                    break;
            }
            this.layer.add(btn);
        }

        /**
         * 
         * @param {number} frameIndex
         */
        protected forceButtonFrame(frameIndex: number): void {

        }


        /**
         * 
         * @param {BaseButton} btn
         */
        protected disableButton(btn: IButton|string): void {            
            if (typeof btn === "string") {
                btn = this.getButtonByName(btn);
            }

            if (!btn) return;

            btn.frame = btn.data.disabledFrame;
            btn.inputEnabled = false;

            btn.enabled = false;
        }

        /**
         * 
         * @param {BaseButton} btn
         */
        protected enableButton(btn: IButton|string): void {            
            if (typeof btn === "string") {
                btn = this.getButtonByName(btn);
            }

            if (!btn) return;

            btn.frame = btn.data.upFrame;
            btn.inputEnabled = true;
        }

        /**
         * 
         * @param {string} action
         */
        protected hideButton(action: string): void {
            let btn: IButton = this.getButtonByName(action);
            if (btn) {
                btn.visible = false;
            }
        }


        /**
         * 
         * @param {string} action
         */
        protected showButton(action: string): void {
            let btn: IButton = this.getButtonByName(action);
            if (btn) {
                btn.visible = true;
            }
        }

        public stop(): void
        {
            super.stop();
        }

        public start(): void
        {
            super.start();
        }

        public showAutoplayText(autoplay:boolean): void {
            
        }

        public showPopupWindow(data:any):void {

        }

        public hidePopupWindow(data:any):void {

        }

        /**
         * 
         * @param {string} action
         * @returns
         */
        protected getButtonByAction(action: string): IButton {
            let btn: IButton;
            for (let i: number = 0; i < this.buttonsList.length; i++) {
                btn = this.buttonsList[i];
                if (btn.action == action) {
                    return btn;
                }
            }
            return null;
        }

        /**
         * 
         * @param $name
         */
        protected getButtonByName($name: string): IButton {
            let btn: IButton;
            for (let i: number = 0; i < this.buttonsList.length; i++) {
                btn = this.buttonsList[i];
                if (btn.name == $name) {
                    return btn;
                }
            }
            return null;
        }

        /**
         * 
         */
        protected onSpinPressed(): void {
            this.actionSignal.dispatch("spin");
        }

        public disableButtons(buttonsNames:string[]):void {
            buttonsNames.forEach(function (buttonName:string) {
                this.disableButton(buttonName);
            }, this);
        }
    }
}