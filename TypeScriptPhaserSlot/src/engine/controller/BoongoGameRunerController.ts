///<reference path="./ActionController.ts" />

module TypeScriptPhaserSlot.common {

    export class BoongoGameRunerController extends ActionController {

        protected gameRunner:any;

        constructor(game: Game, name: string, debugMode:boolean) {
            super(game, name, debugMode);

            this.init();
        }

        public init():void {
            this.gameRunner = window['GR'];

            super.init();
            this.addGameRunerListens();
            this.checkGameMode();
        }

        public checkGameMode():void {
            if(window['windowBooongoStateData'] && window['windowBooongoStateData'].data.mode) {
                this.actionSignal.dispatch({event:'gameMode', data:window['windowBooongoStateData'].data});
            }
        }

        /**
         *
         * @param {string} event
         * @param data
         */
        public sayToGameRuner(event:string, data?:any):void {
            if(!this.enable) {
                console.log('>>>GR is not found');
                return;
            }
            console.log('>>>GR', event);
            switch (event) {
                case 'start':
                    this.gameRunner.Events.game.start();
                    break;
                case 'progress':
                    this.gameRunner.Events.game.progress(data);
                    break;
                case 'loaded':
                    this.gameRunner.Events.game.loaded();
                    break;
                case 'ready':
                    this.gameRunner.Events.game.ready();
                    break;
                case 'play':
                    this.gameRunner.Events.game.play(data);
                    break;
                case 'autogameStart':
                    this.gameRunner.Events.game.autogame_start(data);
                    break;
                case 'autogameStop':
                    this.gameRunner.Events.game.autogame_stop();
                    break;
                case 'freespin_init':
                    this.gameRunner.Events.game.play({action: {name: 'freespin_init', params: {} } });
                    break;
                case 'freespin':
                    this.gameRunner.Events.game.play({action: {name: 'freespin', params: {} } });
                    break;
                case 'freespin_stop':
                    this.gameRunner.Events.game.play({action: {name: 'freespin_stop', params: {} } });
                    break;
                case 'balance_changed':
                    this.gameRunner.Events.game.balance_changed(data);
                    break;
                case 'sound_changed':
                    this.gameRunner.Events.game.sound_changed({ is_enabled : data });
                    break;
                case 'bigwin_start':
                    this.gameRunner.Events.game.bigwin_start({win: data });
                    break;
                case 'bigwin_finish':
                    this.gameRunner.Events.game.bigwin_finish();
                    break;
            }
        }

        /**
         *
         */
        protected addGameRunerListens():void {
            if(!this.enable) {
                console.log('>>>GR is not found');
                return;
            }
            console.log('>>>GR added listeners');

            this.gameRunner.Events.game.error.on(function(data:any):void{
                console.log(">>>GR", data['name']);
                console.log(JSON.stringify(data['data']));
                this.actionSignal.dispatch({event:'error', data:data['data']});
            }, this);

            this.gameRunner.Events.flow.state.on(function(data:any):void{
                console.log(">>>GR", data['name']);
                console.log(JSON.stringify(data['data']));
                this.actionSignal.dispatch({event:'state', data:data['data']});
            }, this);

            this.gameRunner.Events.flow.available_actions.on(function(data:any):void{
                console.log(">>>GR", data['name']);
                console.log(JSON.stringify(data['data']));
                this.actionSignal.dispatch({event:'available_actions', data:data['data']});

                if(data['data'].mode == 'stream') {
                    this.actionSignal.dispatch({event:'stream', data:data['data']});
                }
                if(data['data'].mode == 'replay') {
                    this.actionSignal.dispatch({event:'replay', data:data['data']});
                }
            }, this);

            this.gameRunner.Events.flow.round_start.on(function(data:any):void{
                console.log(">>>GR", data['name']);
                console.log(JSON.stringify(data['data']));
                this.actionSignal.dispatch({event:'round_start', data:data['data']});
            }, this);

            this.gameRunner.Events.flow.round_result.on(function(data:any):void{
                console.log(">>>GR", data['name']);
                console.log(JSON.stringify(data['data']));
                this.actionSignal.dispatch({event:'round_result', data:data['data']});
            }, this);

            this.gameRunner.Events.flow.restart.on(function(data:any):void{
                console.log(">>>GR", data['name']);
                console.log(JSON.stringify(data['data']));
                this.actionSignal.dispatch({event:'restart', data:data['data']});
            }, this);

            this.gameRunner.Events.flow.balance.on(function(data:any):void{
                console.log(">>>GR", data['name']);
                console.log(JSON.stringify(data['data']));
                this.actionSignal.dispatch({event:'balance', data:data['data']});
            }, this);

            this.gameRunner.UI.Events.paytable.on(function(data:any):void{
                console.log(">>>GR", 'paytable');
                this.actionSignal.dispatch({event:'paytable', data:data['data']});
            }, this);

            this.gameRunner.UI.Events.spin.on(function(data:any):void{
                console.log(">>>GR", 'onSpinButton');
                this.actionSignal.dispatch({event:'onSpinButton', data:data['data']});
            }, this);
        }

        public get enable():boolean {
            return (this.gameRunner !== null && this.gameRunner !== undefined);
        }
    }
}