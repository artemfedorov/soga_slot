﻿///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />

module TypeScriptPhaserSlot.common {

    export class ScattersController extends ActionController {

        private _config: GameConfiguration;
        private showLineTimeout: any;
        private _finishShowingSignal: Phaser.Signal;

        constructor(game: Game, name: string, layer: Phaser.Group, debugMode: boolean, configuration: GameConfiguration) {
            super(game, name, debugMode);
            this._config = configuration;
            this._finishShowingSignal = new Phaser.Signal();
            this.init();
        }

        /**
         * Initial creation
         */
        public init(): void {
            super.init();
        }

        /**
         * Start showing
         */
        public start(data?: SpinResult): void {
            this.showScatter(data, 0);
        }

        /**
         *
         * @param data
         */
        public stop(data?: SpinResult): void {
            super.stop();
            clearTimeout(this.showLineTimeout);
            //this.finishShowingSignal.dispatch();
        }

        /**
         *
         * @param {SpinResult} data
         * @param {number} resultLineindex
         */
        private showScatter(data: SpinResult, resultLineindex: number): void {
            if (resultLineindex >= data.scatters.length) {
                this.finishShowingSignal.dispatch();
                return;
            }
            let scatter: Scatter = data.scatters[resultLineindex];
            //let scatterConfigData:WinScattersConfiguration = this._config.scatters[scatter.icon];
            let scatterConfigData:WinScattersConfiguration = this._config.scatters.filter(function (element:WinScattersConfiguration) {
                return element.index == scatter.icon;
            }, this)[0];
            this.actionSignal.dispatch(scatter);

            let scatterSoundName:string = scatterConfigData.sounds[scatter.icon_count] || scatterConfigData.sounds[0];
            SoundController.instance.playSound(scatterSoundName);

            let timeout: number = scatterConfigData.timeout * Phaser.Timer.SECOND;
            this.showLineTimeout = setTimeout(() => {
                this.showScatter(data, resultLineindex + 1)
            }, timeout + this._config.winningShowingSettings.delayBetweenWinningShowing);
        }


        public get finishShowingSignal(): Phaser.Signal {
            return this._finishShowingSignal;
        }

    }
}