/**
 * Created by rocket on 25.07.2017.
 */
window.BOOONGO.game = {
    use: ['autoresize,16:9', 'protocol', 'ui', 'preloader', 'custom_paytable'],
    grapi: '2.1',

    init: function(element, options) {
        GR.Loader.get('game.js').then(function () {
            window.gameConfig = {
                session_id: 'e282c8f33d878ad461ea7702a2a8ea265abd6271',
                game_code: 'the_witch',
                additional_info: ''
            };


            GR.debug = 1;
            new TypeScriptPhaserSlot.common.Game(Phaser.CANVAS, element, options);
        }.bind(this));
    }
};