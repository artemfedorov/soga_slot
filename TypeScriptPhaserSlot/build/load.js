///<reference path='Game.ts'/>
//var path = window.location.href.replace(/(index.php|index.html)/, '');
//var gameName = document.getElementById('gameScript').getAttribute('gameName');
var path = './';
var gameName = window.gameConfig.game_code;

//html_sogaproducts/

var scriptTag = document.createElement('script');
scriptTag.src = path + gameName + '/' + 'game.js';
scriptTag.type='text/javascript';
document.getElementsByTagName('head')[0].appendChild(scriptTag);

window.onload = function () {
    var browser;
    var renderType;

    function myFunction() {
        if ((navigator.userAgent.indexOf('Opera') !== -1 || navigator.userAgent.indexOf('OPR')) !== -1) {
            browser = 'Opera';
        }
        else if (navigator.userAgent.indexOf('Chrome') !== -1) {
            browser = 'Chrome';
        }
        else if (navigator.userAgent.indexOf('Safari') !== -1) {
            browser = 'Safari';
        }
        else if (navigator.userAgent.indexOf('Firefox') !== -1) {
            browser = 'Firefox';
        }
        else if ((navigator.userAgent.indexOf('MSIE') !== -1)) //IF IE > 10
        {
            browser = 'IE';
        }
        else {
            browser = 'unknown';
        }
        renderType = browser === 'Chrome' ? Phaser.WEBGL : Phaser.CANVAS;
    }

    myFunction();
    new TypeScriptPhaserSlot.common.Game(renderType, document.getElementById('content'), {game_path: path + gameName + '/'});
};