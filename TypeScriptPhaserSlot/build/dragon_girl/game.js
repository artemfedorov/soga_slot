var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///<reference path="./interfaces/IController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseController = /** @class */ (function () {
            function BaseController(name, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                this._name = name;
                this.debugMode = debugMode;
            }
            /**
             * Specific console logger
             * @param message
             */
            BaseController.prototype.debuglog = function (message) {
                console.log(this.name, ":", message);
            };
            Object.defineProperty(BaseController.prototype, "debugMode", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._debugMode;
                },
                set: function (value) {
                    this._debugMode = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseController.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            return BaseController;
        }());
        common.BaseController = BaseController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseController.ts" />
///<reference path="./interfaces/IActionController.ts" />
// Class which provides the work by Start-Stop proccess
var TypeScriptPhaserSlot;
///<reference path="./BaseController.ts" />
///<reference path="./interfaces/IActionController.ts" />
// Class which provides the work by Start-Stop proccess
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ActionController = /** @class */ (function (_super) {
            __extends(ActionController, _super);
            function ActionController(game, name, debugMode) {
                var _this = _super.call(this, name, debugMode) || this;
                _this._action_signal = new Phaser.Signal();
                _this._game = game;
                return _this;
            }
            /**
             * Initialize
             */
            ActionController.prototype.init = function (data) {
            };
            /**
             * Start action
             */
            ActionController.prototype.start = function (data) {
            };
            /**
             * Stop action
             */
            ActionController.prototype.stop = function (data) {
            };
            Object.defineProperty(ActionController.prototype, "game", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._game;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ActionController.prototype, "actionSignal", {
                get: function () {
                    return this._action_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ActionController.prototype, "state", {
                get: function () {
                    return this._state;
                },
                enumerable: true,
                configurable: true
            });
            return ActionController;
        }(common.BaseController));
        common.ActionController = ActionController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BackController = /** @class */ (function (_super) {
            __extends(BackController, _super);
            function BackController(game, name, layer, debugMode, pattern) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.layer = layer;
                _this.pattern = pattern;
                _this.init(pattern);
                return _this;
            }
            /**
            * Building reel views
            */
            BackController.prototype.init = function (pattern) {
                this.view = new common.SimpleSprite(this.game, pattern.background.x, pattern.background.y, pattern.background.name, this.debugMode);
                this.layer.add(this.view);
            };
            /**
             *
             * @param data
             */
            BackController.prototype.changeBackground = function (data) {
            };
            return BackController;
        }(common.ActionController));
        common.BackController = BackController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BonusGameController = /** @class */ (function (_super) {
            __extends(BonusGameController, _super);
            function BonusGameController(game, name, layer, debugMode, gameConfiguration, pattern, gameStateResponse) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.pattern = pattern;
                _this.layer = layer;
                _this.layer.visible = false;
                return _this;
            }
            /**
            * Initial creation
            */
            BonusGameController.prototype.init = function () {
                _super.prototype.init.call(this);
            };
            /**
             * Start showing
             */
            BonusGameController.prototype.start = function (data) {
                this.init();
                this.actionSignal.dispatch();
                this.layer.visible = true;
            };
            /**
             * Stop showing
             */
            BonusGameController.prototype.stop = function (data) {
                _super.prototype.stop.call(this);
                this.layer.visible = false;
            };
            return BonusGameController;
        }(common.ActionController));
        common.BonusGameController = BonusGameController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseController.ts" />
// We can get session right here: http://test-api.playvulkan.com/get-session
var TypeScriptPhaserSlot;
///<reference path="./BaseController.ts" />
// We can get session right here: http://test-api.playvulkan.com/get-session
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BoongoAPIController = /** @class */ (function (_super) {
            __extends(BoongoAPIController, _super);
            function BoongoAPIController(url, name, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, name, debugMode) || this;
                _this._gameList_signal = new Phaser.Signal();
                _this._gameState_signal = new Phaser.Signal();
                _this._spin_signal = new Phaser.Signal();
                _this._collect_signal = new Phaser.Signal();
                _this._gamble_signal = new Phaser.Signal();
                _this._exit_signal = new Phaser.Signal();
                _this._bonusGame_signal = new Phaser.Signal();
                _this.gameRunner = window['GR'];
                _this.addListeners();
                return _this;
            }
            BoongoAPIController.prototype.addListeners = function () {
                //this.gameRunner.Events.flow.state.on(this.onGetGameState, this);
                this.gameRunner.Events.flow.round_result.on(this.onSpin, this);
            };
            BoongoAPIController.prototype.encodeData = function (data) {
                return data.toString();
            };
            BoongoAPIController.prototype.decodeData = function (data) {
                return data.toString();
            };
            BoongoAPIController.prototype.getHash = function (data) {
                return '';
            };
            BoongoAPIController.prototype.getParams = function (data) {
                return '';
            };
            BoongoAPIController.prototype.createParams = function (data) {
                return '';
            };
            /**
             * Server requests
             */
            BoongoAPIController.prototype.getGameList = function (data) {
                this.gameList_signal.dispatch();
            };
            BoongoAPIController.prototype.getGameState = function (gameName) {
                //this.gameRunner.Events.game.start();
                if (window['windowBooongoStateData']) {
                    this.onGetGameState(window['windowBooongoStateData']);
                }
                else {
                    this.gameRunner.Events.flow.state.once(this.onGetGameState, this);
                }
            };
            BoongoAPIController.prototype.onGetGameState = function (data) {
                var parseData = data.data;
                console.log(JSON.stringify(parseData));
                this.gameRunner.Events.game.ready();
                this.gameState_signal.dispatch(new common.BoongoGameStateResponse(parseData));
            };
            BoongoAPIController.prototype.onError = function (e) {
            };
            /**
             *
             * @param data
             */
            BoongoAPIController.prototype.spin = function (data) {
                /*let paramsObj: object = {
                    action: {
                        name: 'spin',
                        params: {
                            lines: data.lines,
                            bet_per_line: data.bet
                        }
                    },
                    bet: data.lines * data.bet
                };
                this.gameRunner.Events.game.ready();
                this.gameRunner.Events.game.play(paramsObj);*/
            };
            /**
             *
             * @param data
             */
            BoongoAPIController.prototype.respin = function (data) {
                var paramsObj = {
                    action: {
                        name: 'respin',
                        params: {}
                    }
                };
                this.gameRunner.Events.game.ready();
                this.gameRunner.Events.game.play(paramsObj);
            };
            /**
             *
             * @param data
             */
            BoongoAPIController.prototype.freespin = function (data) {
                var paramsObj = {
                    action: {
                        name: 'freespin',
                        params: {}
                    }
                };
                this.gameRunner.Events.game.ready();
                this.gameRunner.Events.game.play(paramsObj);
            };
            /**
             *
             * @param data
             */
            BoongoAPIController.prototype.onSpin = function (data) {
                var parseData = data.data;
                console.log(JSON.stringify(parseData));
                if (parseData.context.last_action == 'freespin_init' ||
                    parseData.context.last_action == 'freespin_stop') {
                    return;
                }
                this.spin_signal.dispatch(new common.BoongoSpinResponse(parseData));
            };
            /**
             *
             * @param {string} bonusType
             */
            BoongoAPIController.prototype.bonus = function (bonusType) {
            };
            /**
             *
             * @param data
             */
            BoongoAPIController.prototype.onBonus = function (data) {
                var parseData = data.data;
                this.spin_signal.dispatch(new common.BonusResponse(parseData));
            };
            //
            BoongoAPIController.prototype.collect = function (data) {
                //this.collect_signal.dispatch(jsonObject);
            };
            //
            BoongoAPIController.prototype.gamble = function (data) {
                //this.gamble_signal.dispatch(jsonObject);
            };
            //
            BoongoAPIController.prototype.exit = function (data) {
                //this.exit_signal.dispatch(jsonObject);
            };
            Object.defineProperty(BoongoAPIController.prototype, "gameList_signal", {
                /************************************************************
                 Getters and Setters
                 *************************************************************/
                get: function () {
                    return this._gameList_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoAPIController.prototype, "gameState_signal", {
                get: function () {
                    return this._gameState_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoAPIController.prototype, "spin_signal", {
                get: function () {
                    return this._spin_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoAPIController.prototype, "collect_signal", {
                get: function () {
                    return this._collect_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoAPIController.prototype, "gamble_signal", {
                get: function () {
                    return this._gamble_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoAPIController.prototype, "exit_signal", {
                get: function () {
                    return this._exit_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoAPIController.prototype, "bonusGame_signal", {
                get: function () {
                    return this._bonusGame_signal;
                },
                enumerable: true,
                configurable: true
            });
            return BoongoAPIController;
        }(common.BaseController));
        common.BoongoAPIController = BoongoAPIController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./ActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./ActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BoongoGameRunerController = /** @class */ (function (_super) {
            __extends(BoongoGameRunerController, _super);
            function BoongoGameRunerController(game, name, debugMode) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.init();
                return _this;
            }
            BoongoGameRunerController.prototype.init = function () {
                this.gameRunner = window['GR'];
                _super.prototype.init.call(this);
                this.addGameRunerListens();
                this.checkGameMode();
            };
            BoongoGameRunerController.prototype.checkGameMode = function () {
                if (window['windowBooongoStateData'] && window['windowBooongoStateData'].data.mode) {
                    this.actionSignal.dispatch({ event: 'gameMode', data: window['windowBooongoStateData'].data });
                }
            };
            /**
             *
             * @param {string} event
             * @param data
             */
            BoongoGameRunerController.prototype.sayToGameRuner = function (event, data) {
                if (!this.enable) {
                    console.log('>>>GR is not found');
                    return;
                }
                console.log('>>>GR', event);
                switch (event) {
                    case 'start':
                        this.gameRunner.Events.game.start();
                        break;
                    case 'progress':
                        this.gameRunner.Events.game.progress(data);
                        break;
                    case 'loaded':
                        this.gameRunner.Events.game.loaded();
                        break;
                    case 'ready':
                        this.gameRunner.Events.game.ready();
                        break;
                    case 'play':
                        this.gameRunner.Events.game.play(data);
                        break;
                    case 'autogameStart':
                        this.gameRunner.Events.game.autogame_start(data);
                        break;
                    case 'autogameStop':
                        this.gameRunner.Events.game.autogame_stop();
                        break;
                    case 'freespin_init':
                        this.gameRunner.Events.game.play({ action: { name: 'freespin_init', params: {} } });
                        break;
                    case 'freespin':
                        this.gameRunner.Events.game.play({ action: { name: 'freespin', params: {} } });
                        break;
                    case 'freespin_stop':
                        this.gameRunner.Events.game.play({ action: { name: 'freespin_stop', params: {} } });
                        break;
                    case 'balance_changed':
                        this.gameRunner.Events.game.balance_changed(data);
                        break;
                    case 'sound_changed':
                        this.gameRunner.Events.game.sound_changed({ is_enabled: data });
                        break;
                    case 'bigwin_start':
                        this.gameRunner.Events.game.bigwin_start({ win: data });
                        break;
                    case 'bigwin_finish':
                        this.gameRunner.Events.game.bigwin_finish();
                        break;
                }
            };
            /**
             *
             */
            BoongoGameRunerController.prototype.addGameRunerListens = function () {
                if (!this.enable) {
                    console.log('>>>GR is not found');
                    return;
                }
                console.log('>>>GR added listeners');
                this.gameRunner.Events.game.error.on(function (data) {
                    console.log(">>>GR", data['name']);
                    console.log(JSON.stringify(data['data']));
                    this.actionSignal.dispatch({ event: 'error', data: data['data'] });
                }, this);
                this.gameRunner.Events.flow.state.on(function (data) {
                    console.log(">>>GR", data['name']);
                    console.log(JSON.stringify(data['data']));
                    this.actionSignal.dispatch({ event: 'state', data: data['data'] });
                }, this);
                this.gameRunner.Events.flow.available_actions.on(function (data) {
                    console.log(">>>GR", data['name']);
                    console.log(JSON.stringify(data['data']));
                    this.actionSignal.dispatch({ event: 'available_actions', data: data['data'] });
                    if (data['data'].mode == 'stream') {
                        this.actionSignal.dispatch({ event: 'stream', data: data['data'] });
                    }
                    if (data['data'].mode == 'replay') {
                        this.actionSignal.dispatch({ event: 'replay', data: data['data'] });
                    }
                }, this);
                this.gameRunner.Events.flow.round_start.on(function (data) {
                    console.log(">>>GR", data['name']);
                    console.log(JSON.stringify(data['data']));
                    this.actionSignal.dispatch({ event: 'round_start', data: data['data'] });
                }, this);
                this.gameRunner.Events.flow.round_result.on(function (data) {
                    console.log(">>>GR", data['name']);
                    console.log(JSON.stringify(data['data']));
                    this.actionSignal.dispatch({ event: 'round_result', data: data['data'] });
                }, this);
                this.gameRunner.Events.flow.restart.on(function (data) {
                    console.log(">>>GR", data['name']);
                    console.log(JSON.stringify(data['data']));
                    this.actionSignal.dispatch({ event: 'restart', data: data['data'] });
                }, this);
                this.gameRunner.Events.flow.balance.on(function (data) {
                    console.log(">>>GR", data['name']);
                    console.log(JSON.stringify(data['data']));
                    this.actionSignal.dispatch({ event: 'balance', data: data['data'] });
                }, this);
                this.gameRunner.UI.Events.paytable.on(function (data) {
                    console.log(">>>GR", 'paytable');
                    this.actionSignal.dispatch({ event: 'paytable', data: data['data'] });
                }, this);
                this.gameRunner.UI.Events.spin.on(function (data) {
                    console.log(">>>GR", 'onSpinButton');
                    this.actionSignal.dispatch({ event: 'onSpinButton', data: data['data'] });
                }, this);
            };
            Object.defineProperty(BoongoGameRunerController.prototype, "enable", {
                get: function () {
                    return (this.gameRunner !== null && this.gameRunner !== undefined);
                },
                enumerable: true,
                configurable: true
            });
            return BoongoGameRunerController;
        }(common.ActionController));
        common.BoongoGameRunerController = BoongoGameRunerController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ButtonActionConstants = /** @class */ (function () {
            function ButtonActionConstants() {
            }
            ButtonActionConstants.HELP_ACTION = "btn_help";
            ButtonActionConstants.SETTING_ACTION = "btn_settings";
            ButtonActionConstants.MUTE_ACTION = "btn_mute";
            ButtonActionConstants.HOME_ACTION = "btn_home";
            ButtonActionConstants.SPIN_ACTION = "spin";
            ButtonActionConstants.STOP_ACTION = "stop";
            ButtonActionConstants.COLLECT_ACTION = "collect";
            ButtonActionConstants.AUTO_ACTION = "auto";
            ButtonActionConstants.GAMBLE_ACTION = "gamble";
            ButtonActionConstants.AUTO_TURBO_ACTION = "turbo";
            ButtonActionConstants.MAXBET_ACTION = "maxbet_spin";
            ButtonActionConstants.BET_INC_ACTION = "bet_inc";
            ButtonActionConstants.BET_DEC_ACTION = "bet_dec";
            ButtonActionConstants.LINES_INC_ACTION = "lines_inc";
            ButtonActionConstants.LINES_DEC_ACTION = "lines_dec";
            ButtonActionConstants.SETTINGS_ACTION = "settings";
            ButtonActionConstants.GALLERY_ACTION = "gallery";
            ButtonActionConstants.INFO_ACTION = "info";
            return ButtonActionConstants;
        }());
        common.ButtonActionConstants = ButtonActionConstants;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./ActionController.ts" />
/**
 * The Class accumulates specified game effects controlls etc.
 * @param {Game} game
 * @param {string} name
 * @param {Phaser.Group} layer
 * @param {boolean} debugMode
 */
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./ActionController.ts" />
/**
 * The Class accumulates specified game effects controlls etc.
 * @param {Game} game
 * @param {string} name
 * @param {Phaser.Group} layer
 * @param {boolean} debugMode
 */
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var EffectController = /** @class */ (function (_super) {
            __extends(EffectController, _super);
            function EffectController(game, name, layer, debugMode) {
                return _super.call(this, game, name, debugMode) || this;
            }
            return EffectController;
        }(common.ActionController));
        common.EffectController = EffectController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IEmotionsController.ts" />
///<reference path="./ActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IEmotionsController.ts" />
///<reference path="./ActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var EmotionsController = /** @class */ (function (_super) {
            __extends(EmotionsController, _super);
            function EmotionsController(game, name, layer, debugMode, gameConfiguration, pattern, gameStateResponse) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.layer = layer;
                _this.gameStateResponse = gameStateResponse;
                _this.mainPattern = pattern;
                _this.gameConfiguration = gameConfiguration;
                _this.init();
                return _this;
            }
            /**
             *
             */
            EmotionsController.prototype.init = function () {
                this.emotions = [];
            };
            /**
             *
             */
            EmotionsController.prototype.start = function (data) {
                this.spinResult = data;
                var emotionsConfigs = this.findEmotions(data);
                if (emotionsConfigs.length == 0) {
                    this.actionSignal.dispatch("emotionsComplete");
                }
                else {
                    this.startEmotions(emotionsConfigs);
                }
            };
            /**
             *
             */
            EmotionsController.prototype.stopEmotion = function () {
                var emotion;
                for (var i = 0; i < this.emotions.length; i++) {
                    emotion = this.emotions[i];
                    emotion.stop();
                    emotion.parent.removeChild(emotion);
                    this.layer.remove(emotion);
                    emotion.destroy();
                    emotion = null;
                    var emotionsSoundGroup = common.SoundController.instance.getGroup('emotions');
                    if (emotionsSoundGroup) {
                        emotionsSoundGroup.stopAllSounds();
                    }
                }
                this.emotions = [];
            };
            /**
             *
             * @param data
             * @returns {EmotionConfiguration[]}
             */
            EmotionsController.prototype.findEmotions = function (data) {
                var emotionsConfigs = [];
                var tempEmotionsConfigsArr = this.gameConfiguration.emotions.concat();
                tempEmotionsConfigsArr.sort(this.sortFunction);
                var emotionData;
                for (var i = 0; i < tempEmotionsConfigsArr.length; i++) {
                    emotionData = tempEmotionsConfigsArr[i];
                    if (emotionData.lines <= data.paylines.length) {
                        emotionsConfigs.push(emotionData);
                        break;
                    }
                }
                for (var i = 0; i < tempEmotionsConfigsArr.length; i++) {
                    emotionData = tempEmotionsConfigsArr[i];
                    if (emotionData.coef * this.game.state.getCurrentState()['model'].totalBet <= data.payout) {
                        emotionsConfigs.push(emotionData);
                        break;
                    }
                }
                var _loop_1 = function (j) {
                    var arr = tempEmotionsConfigsArr.filter(function (obj) {
                        if (obj.count && obj.count == data.paylines[j].icon_count) {
                            tempEmotionsConfigsArr.splice(tempEmotionsConfigsArr.indexOf(obj), 1);
                            return true;
                        }
                        return false;
                    }.bind(this_1));
                    if (arr) {
                        emotionsConfigs = emotionsConfigs.concat(arr);
                    }
                };
                var this_1 = this;
                // TODO: refactor this function
                for (var j = 0; j < data.paylines.length; j++) {
                    _loop_1(j);
                }
                return emotionsConfigs;
            };
            /**
             *
             * @param a
             * @param b
             * @returns {number}
             */
            EmotionsController.prototype.sortFunction = function (a, b) {
                return (b.coef || 0) - (a.coef || 0);
            };
            /**
             *
             * @param emotionsConfigs
             */
            EmotionsController.prototype.startEmotions = function (emotionsConfigs) {
                this.stopEmotion();
                this.emotionsConfigs = emotionsConfigs;
                this.animateEmotion(this.emotionsConfigs.shift());
                this.actionSignal.dispatch("startShowedEmotions");
            };
            /**
             *
             * @param winning
             * @param count
             * @param type
             */
            EmotionsController.prototype.startEmotion = function (type, winning, count) {
                this.stopEmotion();
                var emotionData;
                for (var i = 0; i < this.gameConfiguration.emotions.length; i++) {
                    emotionData = this.gameConfiguration.emotions[i];
                    if (emotionData.type == type) {
                        if (emotionData.coef && emotionData.coef <= winning) {
                            this.animateEmotion(emotionData);
                        }
                        else if (emotionData.count && emotionData.count >= count) {
                            this.animateEmotion(emotionData);
                        }
                    }
                }
            };
            /**
             *
             * @param emotionData
             */
            EmotionsController.prototype.animateEmotion = function (emotionData) {
                var emotion = new TypeScriptPhaserSlot.common[emotionData.className](this.game, emotionData);
                emotion.actionSignal.addOnce(this.onAnimationAction, this);
                //animation.x = this.pattern.reels[reelIndex].x;
                //animation.y = this.pattern.reels[reelIndex]["icon" + symbolIndex].y;
                this.layer.add(emotion);
                this.emotions.push(emotion);
                emotion.start();
            };
            /**
             *
             * @param data
             */
            EmotionsController.prototype.onAnimationAction = function (data) {
                if (data == common.AnimatedSprite.ALL_ANIMATIONS_COMPLETE) {
                    if (this.emotionsConfigs.length > 0) {
                        this.stopEmotion();
                        this.animateEmotion(this.emotionsConfigs.shift());
                    }
                    else {
                        this.actionSignal.dispatch("stopShowedEmotions");
                        this.actionSignal.dispatch("emotionsComplete");
                        this.stopEmotion();
                    }
                }
            };
            return EmotionsController;
        }(common.ActionController));
        common.EmotionsController = EmotionsController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IFreeSpinsController.ts" />
///<reference path="./ActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IFreeSpinsController.ts" />
///<reference path="./ActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var FreeSpinsController = /** @class */ (function (_super) {
            __extends(FreeSpinsController, _super);
            function FreeSpinsController(game, name, layer, debugMode, gameConfiguration, pattern, gameStateResponse) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.layer = layer;
                _this.gameStateResponse = gameStateResponse;
                _this.mainPattern = pattern;
                _this.gameConfiguration = gameConfiguration;
                _this.init();
                return _this;
            }
            FreeSpinsController.prototype.init = function () {
                _super.prototype.init.call(this);
            };
            FreeSpinsController.prototype.startFreeSpins = function (data) {
            };
            FreeSpinsController.prototype.addedFreeSpins = function (data) {
            };
            FreeSpinsController.prototype.endOfFreeSpins = function (data) {
            };
            FreeSpinsController.prototype.leftFreeSpins = function (data) {
            };
            return FreeSpinsController;
        }(common.ActionController));
        common.FreeSpinsController = FreeSpinsController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IGameController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IGameController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GameController = /** @class */ (function (_super) {
            __extends(GameController, _super);
            function GameController(game, name, debugMode, args) {
                var _this = _super.call(this, name, debugMode) || this;
                _this._configs = args;
                _this._game = game;
                return _this;
            }
            GameController.prototype.start = function () {
                this._configuration = new common.GameConfiguration(this.configs[0]);
                this.game.pattern = this.configs[1];
                var modelStateList = [];
                /*
                    Ordered list of states for Model State Manager
                */
                var state;
                for (var i = 0; i < this.configuration.states.length; i++) {
                    state = new TypeScriptPhaserSlot.common[this.configuration.states[i].name](this.model, this.configuration.states[i], this.debugMode);
                    modelStateList.push(state);
                }
                this.model.setStateList(modelStateList);
            };
            /**
             * Binding Game Controller to Model instance
             * @param model - instance which implements IModel
             */
            GameController.prototype.bindToModel = function (model) {
                this.model = model;
                this.model.notEnoughMoneySignal.add(this.onNotEnoughMoney, this);
                this.model.modelSignal.add(this.modelSays, this);
                this.game.model = this.model;
                this.start();
            };
            /**
             * Listener Model State Manager states signals
             * @param state
             * @param data
             * @param isRequiredAction
             */
            GameController.prototype.modelSays = function (state, data, isRequiredAction) {
                this.currState = state;
                this._currentStateRequiredAction = isRequiredAction;
            };
            /**
             * Just to be overriden
             */
            GameController.prototype.onNotEnoughMoney = function () {
                this.debuglog("No money - no cry:)");
                //"No money - no cry:)"
                //write here a code for vizualization onNotEnoughMoney for player
            };
            /**
             *
             * @param {string} controllerName
             * @returns {string}
             */
            GameController.prototype.getControllerName = function (controllerName) {
                var tryName = this.configs[0].gameName + controllerName;
                if (TypeScriptPhaserSlot.common[tryName]) {
                    return tryName;
                }
                tryName = 'BaseGame' + controllerName;
                if (TypeScriptPhaserSlot.common[tryName]) {
                    return tryName;
                }
                return controllerName;
            };
            Object.defineProperty(GameController.prototype, "game", {
                /************************************************************
                 Getters and Setters
                 *************************************************************/
                get: function () {
                    return this._game;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameController.prototype, "configs", {
                get: function () {
                    return this._configs;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameController.prototype, "configuration", {
                get: function () {
                    return this._configuration;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameController.prototype, "currentStateRequiredAction", {
                get: function () {
                    return this._currentStateRequiredAction;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameController.prototype, "currState", {
                get: function () {
                    return this._currState;
                },
                set: function (value) {
                    this._currState = value;
                },
                enumerable: true,
                configurable: true
            });
            return GameController;
        }(common.BaseController));
        common.GameController = GameController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
// Example of any particular ActionController
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./ActionController.ts" />
var TypeScriptPhaserSlot;
// Example of any particular ActionController
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./ActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var InterfaceController = /** @class */ (function (_super) {
            __extends(InterfaceController, _super);
            function InterfaceController(game, name, layer, debugMode, configuration, pattern) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.layer = layer;
                _this.isAuto = false;
                _this.pattern = pattern;
                _this._config = configuration;
                _this.layer.x = _this.pattern.x;
                _this.layer.y = _this.pattern.y;
                _this.init();
                return _this;
            }
            /**
            * Initial creation
            */
            InterfaceController.prototype.init = function () {
                this.buttonsList = [];
                for (var i = 0; i < this._config.length; i++) {
                    this.createButton(this._config[i]);
                }
                var stateData = this.game.buttonsStates.states.default;
                for (var i = 0; i < stateData.length; i++) {
                    this.setButtonByState(stateData[i]);
                }
                this.spaceBtn = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
                this.spaceBtn.onDown.add(this.onSpinPressed, this);
            };
            /**
             *
             * @param value
             */
            InterfaceController.prototype.changeBetText = function (value) {
            };
            /**
             *
             * @param value
             */
            InterfaceController.prototype.changeLinesText = function (value) {
            };
            /**
             *
             * @param value
             */
            InterfaceController.prototype.changeTotalBetText = function (value) {
            };
            /**
             *
             * @param value
             */
            InterfaceController.prototype.changeWinText = function (value) {
            };
            InterfaceController.prototype.resetWinText = function () {
            };
            /**
             *
             * @param value
             */
            InterfaceController.prototype.changeBalance = function (value) {
            };
            InterfaceController.prototype.autoChanged = function (value) {
                this.stateChanged();
            };
            /**
             * Called by GameController
             * Listener model changing
             * @param {string} stateName
             */
            InterfaceController.prototype.stateChanged = function (stateName) {
                this.isAuto = this.game.state.getCurrentState()['model'].isAutoPlay;
                this.currentState = stateName || this.currentState;
                /*let stateData: any = this.game.buttonsStates.states.default;
                for (let i: number = 0; i < stateData.length; i++) {
                    this.setButtonByState(stateData[i]);
                }*/
                var stateData = this.game.buttonsStates.states[this.currentState];
                if (stateData) {
                    for (var i = 0; i < stateData.length; i++) {
                        this.setButtonByState(stateData[i]);
                    }
                }
            };
            /**
             *
             * @param $buttonData
             */
            InterfaceController.prototype.setButtonByState = function ($buttonData) {
                var button = this.getButtonByName($buttonData.name);
                if (!button) {
                    return;
                }
                button.visible = $buttonData.visible;
                button.enabled = $buttonData.enabled;
                if ($buttonData.actions) {
                    button.actions = $buttonData.actions;
                }
                /*if (this.isAuto) {
                    button.enabled = $buttonData.enabledInAutoplay;
                }*/
                if (this.isAuto) {
                    if ($buttonData.visibleInAutoplay != undefined) {
                        button.visible = $buttonData.visibleInAutoplay;
                    }
                    if ($buttonData.enabledInAutoplay != undefined) {
                        button.enabled = $buttonData.enabledInAutoplay;
                    }
                }
                if (this.game.state.getCurrentState()['model'].isFreespins) {
                    if (button.name != 'startSpin_btn' && button.name != 'stopSpin_btn') {
                        button.enabled = false;
                    }
                }
            };
            /**
             * Release buttons handler
             * @param {BaseButton} button
             */
            InterfaceController.prototype.callBack = function (button) {
                this.debuglog(button.action + " pressed");
                this.actionSignal.dispatch(button.action);
            };
            /**
             * Creation all buttons
             * @param {ButtonConfiguration} data
             */
            InterfaceController.prototype.createButton = function (data) {
                var patternData = this.pattern[data.name];
                var btn = new TypeScriptPhaserSlot.common[data.className](this.game, data, patternData.x, patternData.y, this.callBack, this, true);
                this.buttonsList.push(btn);
                //btn.anchor.set(0.5, 0.5);
                switch (data.action) {
                    case common.ButtonActionConstants.BET_DEC_ACTION:
                        break;
                    case common.ButtonActionConstants.BET_INC_ACTION:
                        break;
                    case common.ButtonActionConstants.SPIN_ACTION:
                        break;
                    case common.ButtonActionConstants.STOP_ACTION:
                        break;
                    case common.ButtonActionConstants.MAXBET_ACTION:
                        break;
                }
                this.layer.add(btn);
            };
            /**
             *
             * @param {number} frameIndex
             */
            InterfaceController.prototype.forceButtonFrame = function (frameIndex) {
            };
            /**
             *
             * @param {BaseButton} btn
             */
            InterfaceController.prototype.disableButton = function (btn) {
                if (typeof btn === "string") {
                    btn = this.getButtonByName(btn);
                }
                if (!btn)
                    return;
                btn.frame = btn.data.disabledFrame;
                btn.inputEnabled = false;
                btn.enabled = false;
            };
            /**
             *
             * @param {BaseButton} btn
             */
            InterfaceController.prototype.enableButton = function (btn) {
                if (typeof btn === "string") {
                    btn = this.getButtonByName(btn);
                }
                if (!btn)
                    return;
                btn.frame = btn.data.upFrame;
                btn.inputEnabled = true;
            };
            /**
             *
             * @param {string} action
             */
            InterfaceController.prototype.hideButton = function (action) {
                var btn = this.getButtonByName(action);
                if (btn) {
                    btn.visible = false;
                }
            };
            /**
             *
             * @param {string} action
             */
            InterfaceController.prototype.showButton = function (action) {
                var btn = this.getButtonByName(action);
                if (btn) {
                    btn.visible = true;
                }
            };
            InterfaceController.prototype.stop = function () {
                _super.prototype.stop.call(this);
            };
            InterfaceController.prototype.start = function () {
                _super.prototype.start.call(this);
            };
            InterfaceController.prototype.showAutoplayText = function (autoplay) {
            };
            InterfaceController.prototype.showPopupWindow = function (data) {
            };
            InterfaceController.prototype.hidePopupWindow = function (data) {
            };
            /**
             *
             * @param {string} action
             * @returns
             */
            InterfaceController.prototype.getButtonByAction = function (action) {
                var btn;
                for (var i = 0; i < this.buttonsList.length; i++) {
                    btn = this.buttonsList[i];
                    if (btn.action == action) {
                        return btn;
                    }
                }
                return null;
            };
            /**
             *
             * @param $name
             */
            InterfaceController.prototype.getButtonByName = function ($name) {
                var btn;
                for (var i = 0; i < this.buttonsList.length; i++) {
                    btn = this.buttonsList[i];
                    if (btn.name == $name) {
                        return btn;
                    }
                }
                return null;
            };
            /**
             *
             */
            InterfaceController.prototype.onSpinPressed = function () {
                this.actionSignal.dispatch("spin");
            };
            InterfaceController.prototype.disableButtons = function (buttonsNames) {
                buttonsNames.forEach(function (buttonName) {
                    this.disableButton(buttonName);
                }, this);
            };
            return InterfaceController;
        }(common.ActionController));
        common.InterfaceController = InterfaceController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var LinesController = /** @class */ (function (_super) {
            __extends(LinesController, _super);
            function LinesController(game, name, layer, debugMode, configuration) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this._config = configuration;
                _this.layer = layer;
                _this._finishShowingSignal = new Phaser.Signal();
                _this.finishShowLines = false;
                _this.init();
                return _this;
            }
            /**
             * Initial creation
             */
            LinesController.prototype.init = function () {
                _super.prototype.init.call(this);
                var lineViewName = this._config.gameName + "LinesView";
                this.view = new TypeScriptPhaserSlot.common[lineViewName](this.game, lineViewName, this._config.paylines, this.game.pattern, this.debugMode);
                this.layer.add(this.view);
            };
            /**
             * Start showing
             */
            LinesController.prototype.start = function (data) {
                _super.prototype.start.call(this);
                this.finishShowLines = false;
                this.showWinLine(data, 0);
            };
            /**
             *
             * @param data
             */
            LinesController.prototype.stop = function (data) {
                _super.prototype.stop.call(this);
                this.finishShowLines = true;
                clearTimeout(this.showLineTimeout);
                this.hideLines();
            };
            /**
             *
             */
            LinesController.prototype.hideLines = function () {
                this.view.hideLines();
            };
            /**
             *
             * @param count
             */
            LinesController.prototype.showLines = function (count) {
                this.view.showLines(count);
            };
            /**
             *
             * @param {SpinResult} data
             * @param {number} resultLineindex
             */
            LinesController.prototype.showWinLine = function (data, resultLineindex) {
                var _this = this;
                this.hideLines();
                if (this.finishShowLines) {
                    return;
                }
                if (resultLineindex >= data.paylines.length) {
                    this.finishShowingSignal.dispatch();
                    return;
                }
                var payLine = data.paylines[resultLineindex];
                this.view.showWinLine(payLine);
                this.actionSignal.dispatch(data.paylines[resultLineindex]);
                var timeout = this._config.paylines[payLine.index].timeout * Phaser.Timer.SECOND;
                this.showLineTimeout = setTimeout(function () {
                    _this.showWinLine(data, resultLineindex + 1);
                }, timeout);
                this.showLineTimeout = setTimeout(function () {
                    common.SoundController.instance.playSound('line3', 'default', 0.4);
                }, 300);
            };
            Object.defineProperty(LinesController.prototype, "finishShowingSignal", {
                get: function () {
                    return this._finishShowingSignal;
                },
                enumerable: true,
                configurable: true
            });
            return LinesController;
        }(common.ActionController));
        common.LinesController = LinesController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./ActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./ActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var MoneyFormatController = /** @class */ (function (_super) {
            __extends(MoneyFormatController, _super);
            function MoneyFormatController(game, name, debugMode, gameConfiguration, gameStateResponse) {
                var _this = this;
                if (MoneyFormatController._instance) {
                    throw new Error("Error: Instantiation failed: Use MoneyFormatController.instance instead of new.");
                }
                _this = _super.call(this, game, name, debugMode) || this;
                _this.gameConfiguration = gameConfiguration;
                _this.gameStateResponse = gameStateResponse;
                _this.init();
                MoneyFormatController._instance = _this;
                return _this;
            }
            /**
             *
             */
            MoneyFormatController.prototype.init = function () {
            };
            MoneyFormatController.prototype.format = function (value, withoutSymbol) {
                if (withoutSymbol === void 0) { withoutSymbol = false; }
                if (window['GR']) {
                    return window['GR'].CurrencyFormatter.format(value, withoutSymbol);
                }
                //return (+value.toFixed(10)).toString();
                return (+value.toFixed(10)).toFixed(2);
            };
            Object.defineProperty(MoneyFormatController, "instance", {
                get: function () {
                    return MoneyFormatController._instance;
                },
                enumerable: true,
                configurable: true
            });
            return MoneyFormatController;
        }(common.ActionController));
        common.MoneyFormatController = MoneyFormatController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var PaytableController = /** @class */ (function (_super) {
            __extends(PaytableController, _super);
            function PaytableController(game, name, layer, debugMode, configuration) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this._config = configuration;
                _this.layer = layer;
                _this.init();
                return _this;
            }
            /**
             * Initial creation
             */
            PaytableController.prototype.init = function () {
                _super.prototype.init.call(this);
                var viewName = this.getClassName("PaytableView");
                this.view = new TypeScriptPhaserSlot.common[viewName](this.game, viewName, this.game.pattern, this.debugMode);
                this.layer.add(this.view);
                this.hide();
            };
            /**
             *
             * @param className
             * @returns {string}
             */
            PaytableController.prototype.getClassName = function (className) {
                if (TypeScriptPhaserSlot.common[this._config.gameName + className]) {
                    return this._config.gameName + className;
                }
                return className;
            };
            /**
             *
             */
            PaytableController.prototype.show = function () {
                this.view['show']();
            };
            /**
             *
             */
            PaytableController.prototype.hide = function () {
                this.view['hide']();
            };
            /**
             *
             */
            PaytableController.prototype.switch = function () {
                if (this.view.visible) {
                    this.hide();
                }
                else {
                    this.show();
                }
            };
            /**
             *
             * @param pageIndex
             */
            PaytableController.prototype.goToPage = function (pageIndex) {
                this.view['goToPage'](pageIndex);
            };
            Object.defineProperty(PaytableController.prototype, "isShown", {
                /**
                 *
                 * @returns {boolean}
                 */
                get: function () {
                    return this.view.visible;
                },
                enumerable: true,
                configurable: true
            });
            return PaytableController;
        }(common.ActionController));
        common.PaytableController = PaytableController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ReelsController = /** @class */ (function (_super) {
            __extends(ReelsController, _super);
            function ReelsController(game, name, layer, debugMode, symbolProperties, gameConfiguration, pattern, gameStateResponse) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.isPendingRespinComplete = false;
                _this._symbolsEnabled = true;
                _this.layer = layer;
                _this.gameStateResponse = gameStateResponse;
                _this.mapReel = gameStateResponse.reelMap;
                _this.mapReelFreespins = gameStateResponse.reelMapFreespins;
                _this.symbolProperties = symbolProperties;
                _this.mainPattern = pattern;
                _this.addedSymbols = [];
                _this.additionalSymbolsNumbers = [];
                _this.pattern = pattern.reels;
                _this.gameConfiguration = gameConfiguration;
                _this.trackedSymbolsCalculation = [];
                _this.trackedSymbols = _this.gameConfiguration.standartSymbolSetting.trackedSymbols;
                _this.reelsOrderStoping = [];
                for (var i = 0; i < _this.gameConfiguration.reels.length; i++) {
                    _this.reelsOrderStoping[_this.gameConfiguration.reels[i].stopIndex] = i;
                }
                _this.init(gameStateResponse);
                return _this;
            }
            /**
             * Building reel views
             */
            ReelsController.prototype.init = function (gameStateResponse) {
                this.reelViews = [];
                //Creating the reels
                this.buildReels(gameStateResponse);
            };
            /**
             * Called by GameController once Respin State comes
             */
            ReelsController.prototype.respin = function (data) {
                this.respinData = data;
                //please override this gently
            };
            ReelsController.prototype.stopRespin = function (data) {
                this.respinData = data;
                //please override this gently
            };
            /**
             *
             */
            ReelsController.prototype.start = function (data) {
                this.isStopping = false;
                this.isUrgentStop = false;
                this.stopSpinData = null;
                this.justTouchedCounter = 0;
                this.stopCounter = 0;
                var t = 200 + Number(this.gameConfiguration.rollingMechanics.tweenToTop.time) * Phaser.Timer.SECOND;
                setTimeout(function () {
                    common.SoundController.instance.playSound('start_spin_sound', 'default', 0.1, true);
                }, t);
                this.spin(0);
            };
            /**
             *
             * @param {number} reelIndex
             */
            ReelsController.prototype.spin = function (reelIndex) {
                var _this = this;
                var delay;
                this.isUrgentStop = false;
                this.trackedSymbolsCalculation.length = 0;
                this.reelViews[reelIndex].start();
                if (reelIndex + 1 < this.reelViews.length) {
                    delay = this.gameConfiguration.reels[reelIndex].startTimeout * Phaser.Timer.SECOND;
                    if (delay == 0 || reelIndex == 0) {
                        this.spin(reelIndex + 1);
                    }
                    else {
                        setTimeout(function () {
                            _this.spin(reelIndex + 1);
                        }, delay);
                    }
                }
            };
            /**
             * Stop showing
             */
            ReelsController.prototype.stop = function (data) {
                var _this = this;
                var timeout = this.isUrgentStop ? 0 : 1000;
                this.stopSpinData = data;
                this.alignReels();
                this.handleTrackedSymbols();
                this.spinTimeout = setTimeout(function () {
                    _this.reelStop(data, 0);
                }, timeout);
            };
            ReelsController.prototype.alignReels = function () {
                var maxLengthReelIndex = 0;
                var amount;
                for (var i = 0; i < this.reelViews.length; i++) {
                    maxLengthReelIndex = this.reelViews[i].symbols.length > this.reelViews[maxLengthReelIndex].symbols.length ? i : maxLengthReelIndex;
                }
                amount = this.reelViews[maxLengthReelIndex].symbols.length;
                for (var i = 0; i < this.reelViews.length; i++) {
                    this.debuglog("total symbols in reel#" + i + " = " + this.reelViews[i].symbols.length);
                    var addSymbols = amount - this.reelViews[i].symbols.length;
                    this.additionalSymbolsNumbers[i] = addSymbols;
                }
            };
            /**
             *
             * @param data
             * @param reelIndex
             */
            ReelsController.prototype.reelStop = function (data, reelIndex) {
                var _this = this;
                this.isStopping = true;
                var timeout = this.isUrgentStop ? 0 : this.gameConfiguration.reels[reelIndex].stopTimeout;
                this.reelViews[this.reelsOrderStoping[reelIndex]].stop(data.reels[this.reelsOrderStoping[reelIndex]].concat(), this.additionalSymbolsNumbers[reelIndex]);
                if (reelIndex + 1 < this.reelViews.length) {
                    setTimeout(function () {
                        _this.reelStop(data, reelIndex + 1);
                    }, timeout);
                }
            };
            /**
             *
             */
            ReelsController.prototype.urgentStop = function () {
                this.isUrgentStop = true;
                if (this.spinTimeout) {
                    clearTimeout(this.spinTimeout);
                    if (!this.isStopping && this.stopSpinData) {
                        this.reelStop(this.stopSpinData, 0);
                    }
                }
            };
            /**
             * Callback stopping the reel
             * @param data
             */
            ReelsController.prototype.onReelStoped = function (data) {
                this.state.finishWork();
            };
            /**
             *
             * @param {GameStateResponse} gameStateResponse
             */
            ReelsController.prototype.buildReels = function (gameStateResponse) {
                this.layer.x = this.pattern.x;
                this.layer.y = this.pattern.y;
                this.borderLayer = new Phaser.Group(this.game);
                var reelsAmount = this.gameConfiguration.reels.length;
                var reelView;
                for (var i = 0; i < reelsAmount; i++) {
                    reelView = this.buildReel(i, this.gameConfiguration.gameName + String(i), this.debugMode, this.symbolProperties, this.gameConfiguration, this.pattern, gameStateResponse.reels[i].concat(), this.borderLayer);
                    this.layer.add(reelView);
                    this.reelViews.push(reelView);
                }
                this.layer.add(this.borderLayer);
            };
            /**
             * JUST TO BE OVERRIDEN
             * DO NOT CALL SUPER!
             * @param index
             * @param name
             * @param debugMode
             * @param symbolProperties
             * @param gameConfiguration
             * @param pattern
             * @param dataReel
             * @param borderLayer
             * @returns {IReelView}
             */
            ReelsController.prototype.buildReel = function (index, name, debugMode, symbolProperties, gameConfiguration, pattern, dataReel, borderLayer) {
                var reelView;
                reelView.justTouchedStopSignal.add(this.onJustTouchedReelStop, this);
                reelView.actionSignal.add(this.onReelFinnalyStopped, this);
                return reelView;
            };
            /**
             *
             */
            ReelsController.prototype.handleTrackedSymbols = function () {
                var symbolIndex;
                for (var reelIndex = 0; reelIndex < this.stopSpinData.reels.length; reelIndex++) {
                    for (var i = 0; i < this.stopSpinData.reels[reelIndex].length; i++) {
                        symbolIndex = this.stopSpinData.reels[reelIndex][i];
                        if (this.trackedSymbols.indexOf(symbolIndex) > -1) {
                            if (!this.trackedSymbolsCalculation[symbolIndex]) {
                                this.trackedSymbolsCalculation[symbolIndex] = 0;
                            }
                            this.trackedSymbolsCalculation[symbolIndex]++;
                            if (this.trackedSymbolsCalculation[symbolIndex] == this.gameConfiguration.standartSymbolSetting.trackedSymbolsMin &&
                                this.gameConfiguration.reels.length - (reelIndex + 1) > 0) {
                                for (var j = reelIndex + 1; j < this.gameConfiguration.reels.length; j++) {
                                    this.reelViews[j].isIntrigued = true;
                                }
                            }
                        }
                    }
                }
            };
            /**
             * Signals when particular reel touched the ground:) before ending.
             * @param {number} reelIndex
             */
            ReelsController.prototype.onJustTouchedReelStop = function (reelIndex) {
                if (reelIndex + 1 < this.reelViews.length) {
                    if (this.reelViews[reelIndex + 1].isIntrigued) {
                        this.startIntrigue(reelIndex + 1);
                    }
                }
                else {
                    this.stopIntrigue(reelIndex);
                }
                this.justTouchedCounter++;
                if (this.justTouchedCounter == this.gameConfiguration.reels.length) {
                    var stopSound = common.SoundController.instance.getSound("start_spin_sound", 'default');
                    if (stopSound) {
                        stopSound.stop();
                    }
                }
            };
            /**
             *
             * @param reelIndex
             */
            ReelsController.prototype.onReelFinnalyStopped = function (reelIndex) {
                if (this.isPendingRespinComplete) {
                    this.actionSignal.dispatch(ReelsController.RESPIN_COMPLETE);
                    this.isPendingRespinComplete = false;
                    return;
                }
                this.stopCounter++;
                if (this.stopCounter == this.gameConfiguration.reels.length) {
                    this.stopIntrigue();
                    this.actionSignal.dispatch(ReelsController.STOP);
                }
            };
            ReelsController.prototype.animateSymbols = function (data) {
                this.animateSymbolsOfLine(data.paylines[0]);
            };
            /**
             *
             * @param line
             */
            ReelsController.prototype.animateSymbolsOfLine = function (line) {
                this.stopAnimation(true);
                if (this._symbolsEnabled) {
                    this.hideShowNoWinSymbols(false);
                }
                var index;
                for (var j = line.offset; j < line.offset + line.icon_count; j++) {
                    index = line.line[j];
                    var symbol = this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount];
                    symbol.hide();
                }
            };
            ReelsController.prototype.animateScatters = function (scatter) {
                this.stopAnimation(true);
                if (this._symbolsEnabled) {
                    this.hideShowNoWinSymbols(false);
                }
                var indexes;
                var symbolIndex;
                for (var j = 0; j < scatter.indexes.length; j++) {
                    indexes = scatter.indexes[j];
                    var symbol = this.reelViews[indexes[0]].symbols[indexes[1] + this.reelViews[indexes[0]].reelConfig.topInvisibleSymbolsCount];
                    symbol.hide();
                }
            };
            ReelsController.prototype.stopAnimation = function (tween) {
                if (!tween) {
                    this.hideShowNoWinSymbols(true);
                }
                for (var i = 0; i < this.reelViews.length; i++) {
                    for (var j = 0; j < this.reelViews[i].symbols.length; j++) {
                        var symbol = this.reelViews[i].symbols[j];
                        if (symbol) {
                            this.reelViews[i].symbols[j].show();
                        }
                    }
                }
            };
            /**
             *
             * @param show
             */
            ReelsController.prototype.hideShowNoWinSymbols = function (show) {
                this._symbolsEnabled = show;
                for (var k = 0; k < this.reelViews.length; k++) {
                    for (var l = 0; l < this.reelViews[k].symbols.length; l++) {
                        var item = this.reelViews[k].symbols[l];
                        if (item) {
                            if (show) {
                                item.activate();
                            }
                            else {
                                item.deactivate();
                            }
                        }
                    }
                }
            };
            /**
             * Override this to run your specific intrigue like a animation and so on.
             */
            ReelsController.prototype.startIntrigue = function (reelIndex) {
            };
            /**
             * Override this to stop your specific intrigue like a animation and so on.
             */
            ReelsController.prototype.stopIntrigue = function (reelIndex) {
            };
            /**
             *
             * @param index
             */
            ReelsController.prototype.animateSpecificSymbolWithIndex = function (index) {
            };
            ReelsController.RESPIN_COMPLETE = "RespinComplete";
            ReelsController.FINISH_SHOW_WINNING = "FinishShowWinning";
            ReelsController.STOP = "Stop";
            ReelsController.INTRIGUE = "Intrigue";
            ReelsController.FINISH_REMOVE_SYMBOLS = "FinishRemoveSymbols";
            return ReelsController;
        }(common.ActionController));
        common.ReelsController = ReelsController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ScattersController = /** @class */ (function (_super) {
            __extends(ScattersController, _super);
            function ScattersController(game, name, layer, debugMode, configuration) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this._config = configuration;
                _this._finishShowingSignal = new Phaser.Signal();
                _this.init();
                return _this;
            }
            /**
             * Initial creation
             */
            ScattersController.prototype.init = function () {
                _super.prototype.init.call(this);
            };
            /**
             * Start showing
             */
            ScattersController.prototype.start = function (data) {
                this.showScatter(data, 0);
            };
            /**
             *
             * @param data
             */
            ScattersController.prototype.stop = function (data) {
                _super.prototype.stop.call(this);
                clearTimeout(this.showLineTimeout);
                //this.finishShowingSignal.dispatch();
            };
            /**
             *
             * @param {SpinResult} data
             * @param {number} resultLineindex
             */
            ScattersController.prototype.showScatter = function (data, resultLineindex) {
                var _this = this;
                if (resultLineindex >= data.scatters.length) {
                    this.finishShowingSignal.dispatch();
                    return;
                }
                var scatter = data.scatters[resultLineindex];
                //let scatterConfigData:WinScattersConfiguration = this._config.scatters[scatter.icon];
                var scatterConfigData = this._config.scatters.filter(function (element) {
                    return element.index == scatter.icon;
                }, this)[0];
                this.actionSignal.dispatch(scatter);
                var scatterSoundName = scatterConfigData.sounds[scatter.icon_count] || scatterConfigData.sounds[0];
                common.SoundController.instance.playSound(scatterSoundName);
                var timeout = scatterConfigData.timeout * Phaser.Timer.SECOND;
                this.showLineTimeout = setTimeout(function () {
                    _this.showScatter(data, resultLineindex + 1);
                }, timeout + this._config.winningShowingSettings.delayBetweenWinningShowing);
            };
            Object.defineProperty(ScattersController.prototype, "finishShowingSignal", {
                get: function () {
                    return this._finishShowingSignal;
                },
                enumerable: true,
                configurable: true
            });
            return ScattersController;
        }(common.ActionController));
        common.ScattersController = ScattersController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./ActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./ActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var SoundController = /** @class */ (function (_super) {
            __extends(SoundController, _super);
            function SoundController(game, name, debugMode, gameConfiguration, gameStateResponse) {
                var _this = this;
                if (SoundController._instance) {
                    throw new Error("Error: Instantiation failed: Use SoundController.instance instead of new.");
                }
                _this = _super.call(this, game, name, debugMode) || this;
                _this.gameConfiguration = gameConfiguration;
                _this.gameStateResponse = gameStateResponse;
                _this.init();
                SoundController._instance = _this;
                return _this;
            }
            /**
             *
             */
            SoundController.prototype.init = function () {
                this.muted = false;
                this.groups = [];
                this.groups.push(new SoundGroup('default'));
            };
            SoundController.prototype.playSound = function (key, groupName, volume, loop) {
                var startConfig = {
                    assetKey: key,
                    key: key,
                    group: groupName,
                    volume: volume,
                    loop: loop
                };
                var config = new common.SoundConfiguration(startConfig);
                for (var i = 0; i < this.gameConfiguration.sounds.length; i++) {
                    var soundConfig = this.gameConfiguration.sounds[i];
                    if (soundConfig.key == key) {
                        config = soundConfig;
                        break;
                    }
                }
                var currentConfig = config;
                currentConfig.assetKey = groupName || config.key;
                currentConfig.group = groupName || config.group;
                currentConfig.volume = volume || config.volume;
                currentConfig.loop = loop || config.loop;
                var sound = this.game.add.audio(currentConfig.assetKey, currentConfig.volume, currentConfig.loop);
                var group = this.getGroup(config.group);
                if (!group) {
                    group = new SoundGroup(currentConfig.group);
                    group.mute = this.muted;
                    this.groups.push(group);
                }
                group.addSound(sound);
                sound['group'] = currentConfig.group;
                //sound['id'] = Math.random() * Math.random() * Math.random();
                sound.onStop.addOnce(this.onSoundStop.bind(this));
                sound.play();
                if (group.mute) {
                    sound.mute = group.mute;
                }
                return sound;
            };
            /**
             *
             * @param key
             * @param groupName
             * @param volume
             * @param loop
             * @returns {Phaser.Sound}
             */
            /*public playSound(key:string, groupName = 'default', volume?:number, loop?:boolean): Phaser.Sound {
                let sound:Phaser.Sound = this.game.add.audio(key, volume, loop);
                let group:SoundGroup = this.getGroup(groupName);
                if (!group) {
                    group = new SoundGroup(groupName);
                    group.mute = this.muted;
                    this.groups.push(group);
                }
    
                group.addSound(sound);
    
                sound['group'] = groupName;
                //sound['id'] = Math.random() * Math.random() * Math.random();
                sound.onStop.addOnce(this.onSoundStop.bind(this));
                sound.play();
    
                if(group.mute) {
                    sound.mute = group.mute;
                }
    
                return sound;
            }*/
            /**
             *
             * @param sound
             */
            SoundController.prototype.onSoundStop = function (sound) {
                var group = this.getGroup(sound['group']);
                group.removeSound(sound.key);
            };
            /**
             *
             * @param soundName
             * @param groupName
             */
            SoundController.prototype.stopSound = function (soundName, groupName) {
                if (groupName === void 0) { groupName = 'default'; }
                var sound;
                var group = this.getGroup(groupName);
                sound = group.getSounds(soundName);
                if (sound) {
                    sound.stop();
                }
            };
            /**
             *
             * @param soundName
             * @param groupName
             * @returns {Phaser.Sound}
             */
            SoundController.prototype.getSound = function (soundName, groupName) {
                if (groupName === void 0) { groupName = 'default'; }
                var sound;
                var group = this.getGroup(groupName);
                if (group) {
                    sound = group.getSounds(soundName);
                    return sound;
                }
                return null;
            };
            /**
             *
             * @param groupName
             * @returns {number}
             */
            SoundController.prototype.getGroupIndex = function (groupName) {
                var index = -1;
                var group;
                for (var i = 0; i < this.groups.length; i++) {
                    group = this.groups[i];
                    if (group.name == groupName) {
                        index = i;
                        break;
                    }
                }
                return index;
            };
            /**
             *
             * @param value
             */
            SoundController.prototype.muteAll = function (value) {
                this.muted = value;
                var group;
                for (var i = 0; i < this.groups.length; i++) {
                    group = this.groups[i];
                    group.mute = value;
                }
            };
            /**
             *
             * @param groupName
             * @returns {SoundGroup}
             */
            SoundController.prototype.getGroup = function (groupName) {
                var group;
                var index = this.getGroupIndex(groupName);
                if (index != -1) {
                    group = this.groups[index];
                }
                return group;
            };
            Object.defineProperty(SoundController, "instance", {
                get: function () {
                    return SoundController._instance;
                },
                enumerable: true,
                configurable: true
            });
            return SoundController;
        }(common.ActionController));
        common.SoundController = SoundController;
        var SoundGroup = /** @class */ (function () {
            function SoundGroup(name) {
                this._name = name;
                this._sounds = [];
                this._mute = false;
            }
            /**
             *
             * @param name
             * @returns {any}
             */
            SoundGroup.prototype.getSounds = function (name) {
                var found = this.sounds.filter(function (value) {
                    return value.key == name;
                }.bind(this));
                if (found && found.length > 0) {
                    return found[0];
                }
                return null;
            };
            /**
             *
             * @param sound
             */
            SoundGroup.prototype.addSound = function (sound) {
                this.sounds.push(sound);
            };
            /**
             *
             */
            SoundGroup.prototype.stopAllSounds = function () {
                var sound = null;
                for (var i = 0; i < this._sounds.length; i++) {
                    sound = this.sounds[i];
                    sound.stop();
                }
            };
            /**
             *
             * @param soundName
             */
            SoundGroup.prototype.removeSound = function (soundName) {
                var index = this.getSoundIndex(soundName);
                if (index != -1) {
                    this.sounds.splice(index, 1);
                }
            };
            /**
             *
             * @param soundName
             * @returns {number}
             */
            SoundGroup.prototype.getSoundIndex = function (soundName) {
                var index = -1;
                var sound;
                for (var i = 0; i < this.sounds.length; i++) {
                    sound = this.sounds[i];
                    if (sound.key == soundName) {
                        index = i;
                        break;
                    }
                }
                return index;
            };
            Object.defineProperty(SoundGroup.prototype, "name", {
                /**
                 *
                 * @returns {string}
                 */
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SoundGroup.prototype, "sounds", {
                /**
                 *
                 * @returns {Phaser.Sound[]}
                 */
                get: function () {
                    return this._sounds;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SoundGroup.prototype, "mute", {
                get: function () {
                    return this._mute;
                },
                set: function (value) {
                    var sound = null;
                    for (var i = 0; i < this._sounds.length; i++) {
                        sound = this.sounds[i];
                        sound.mute = value;
                    }
                    this._mute = value;
                },
                enumerable: true,
                configurable: true
            });
            return SoundGroup;
        }());
        common.SoundGroup = SoundGroup;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./ActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./ActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var SymbolsAnimationController = /** @class */ (function (_super) {
            __extends(SymbolsAnimationController, _super);
            function SymbolsAnimationController(game, name, layer, debugMode, symbolProperties, gameConfiguration, pattern, gameStateResponse) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.layer = layer;
                _this.gameStateResponse = gameStateResponse;
                _this.symbolProperties = symbolProperties;
                _this.mainPattern = pattern;
                _this.pattern = pattern.reels;
                _this.gameConfiguration = gameConfiguration;
                _this.init();
                return _this;
            }
            /**
             *
             */
            SymbolsAnimationController.prototype.init = function () {
                this.animations = [];
                this.startCount = 0;
                this.layer.x = this.pattern.x;
                this.layer.y = this.pattern.y;
                var mask = new Phaser.Graphics(this.game, 0, 0);
                mask.beginFill(0xffffff, 0.5);
                var w = 171;
                var h = 171;
                mask.drawRect(-(w / 2), -(h / 2), w * 5 + w, h * 3 + h);
                //TODO: вернуть маску если нужно
                //this.layer.add(mask);
                //this.layer.mask = mask;
            };
            /**
             *
             */
            SymbolsAnimationController.prototype.stopAnimation = function () {
                var animation;
                for (var i = 0; i < this.animations.length; i++) {
                    animation = this.animations[i];
                    animation.stop();
                    animation.parent.removeChild(animation);
                    this.layer.remove(animation);
                    animation.animations.destroy();
                    animation.destroy();
                    animation = null;
                }
                this.animations = [];
                this.startCount = 0;
            };
            /**
             *
             * @param data
             */
            SymbolsAnimationController.prototype.animateSymbols = function (data) {
                this.animateSymbolsOfLine(data.paylines[0], data);
            };
            /**
             *
             * @param line
             * @param spinResult
             */
            SymbolsAnimationController.prototype.animateSymbolsOfLine = function (line, spinResult) {
                this.stopAnimation();
                this.spinResult = spinResult;
                this.correctSpinResult();
                var index;
                var symbolIndex;
                console.log('line.offset', line.offset, 'line.icon_count', line.icon_count);
                for (var j = line.offset; j < line.offset + line.icon_count; j++) {
                    index = line.line[j];
                    symbolIndex = this.spinResult.reels[j][index];
                    this.animatSymbol(j, index, symbolIndex);
                    this.startCount++;
                }
            };
            SymbolsAnimationController.prototype.animateScatters = function (scatter, spinResult, delayBetween) {
                this.stopAnimation();
                this.spinResult = spinResult;
                this.correctSpinResult();
                var indexes;
                var symbolIndex;
                for (var j = 0; j < scatter.indexes.length; j++) {
                    indexes = scatter.indexes[j];
                    symbolIndex = this.spinResult.reels[indexes[0]][indexes[1]];
                    this.animatSymbol(indexes[0], indexes[1], symbolIndex, delayBetween * j);
                    this.startCount++;
                }
            };
            /**
             *
             */
            SymbolsAnimationController.prototype.correctSpinResult = function () {
            };
            /**
             *
             * @param symbolData
             * @returns {SimpleSprite}
             */
            SymbolsAnimationController.prototype.createAnimation = function (symbolData) {
                var currentSymbolProperties = null;
                this.symbolProperties.forEach(function (item) {
                    if (item.index == symbolData) {
                        currentSymbolProperties = item;
                    }
                }.bind(this));
                var animation = new common.AnimatedSprite(this.game, 0, 0, currentSymbolProperties.animationData, this.debugMode);
                return animation;
            };
            /**
             *
             * @param reelIndex
             * @param symbolIndex
             * @param symbolData
             */
            SymbolsAnimationController.prototype.animatSymbol = function (reelIndex, symbolIndex, symbolData, delayBetween) {
                var animation = this.createAnimation(symbolData);
                var delay = delayBetween || 0;
                animation.x = this.pattern.reels[reelIndex].x;
                animation.y = this.pattern.reels[reelIndex]["icon" + symbolIndex].y;
                animation.actionSignal.addOnce(this.onAnimationAction, this);
                this.layer.add(animation);
                this.animations.push(animation);
                setTimeout(function () {
                    animation.start();
                }, delayBetween);
            };
            SymbolsAnimationController.prototype.onAnimationAction = function (data) {
                if (data == common.AnimatedSprite.ANIMATION_FIRST_LOOP_COMPLETE) {
                    this.startCount--;
                    if (this.startCount == 0) {
                        //this.actionSignal.dispatch(ReelsController.FINISH_SHOW_WINNING);
                    }
                }
            };
            return SymbolsAnimationController;
        }(common.ActionController));
        common.SymbolsAnimationController = SymbolsAnimationController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IActionController.ts" />
///<reference path="./GameController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ToolbarController = /** @class */ (function (_super) {
            __extends(ToolbarController, _super);
            function ToolbarController(game, name, layer, debugMode, configuration) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this._config = configuration;
                _this.layer = layer;
                _this.init();
                return _this;
            }
            /**
             * Initial creation
             */
            ToolbarController.prototype.init = function () {
                _super.prototype.init.call(this);
                var exitButton = new common.ExitButton(this.game, 0, 20);
                exitButton.name = 'exitButton';
                exitButton.actionSignal.add(this.onFullScreen, this);
                var soundButton = new common.SoundButton(this.game, 0, 20);
                soundButton.name = 'soundButton';
                soundButton.actionSignal.add(this.onFullScreen, this);
                var fullScreenButton = new common.FullScreenButton(this.game, 0, 20);
                fullScreenButton.name = 'fullScreenButton';
                fullScreenButton.actionSignal.add(this.onFullScreen, this);
                this.layer.add(exitButton);
                this.layer.add(soundButton);
                this.layer.add(fullScreenButton);
                this.layer.fixedToCamera = true;
                this.changePositions();
                this.game.scale.onSizeChange.add(function () {
                    this.changePositions();
                }, this, -1);
            };
            ToolbarController.prototype.changePositions = function () {
                var ratio = this.game.camera.width / this.game.camera.height;
                this.layer.children.forEach(function (child) {
                    switch (child.name) {
                        case 'exitButton':
                            child.x = 768 * ratio - 50;
                            break;
                        case 'soundButton':
                            child.x = 768 * ratio - 150;
                            break;
                        case 'fullScreenButton':
                            child.x = 768 * ratio - 100;
                            break;
                    }
                }, this);
            };
            ToolbarController.prototype.onFullScreen = function (data) {
                this.actionSignal.dispatch(data);
            };
            return ToolbarController;
        }(common.ActionController));
        common.ToolbarController = ToolbarController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./interfaces/IWinningOnCenterController.ts" />
///<reference path="./ActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./interfaces/IWinningOnCenterController.ts" />
///<reference path="./ActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var WinningOnCenterController = /** @class */ (function (_super) {
            __extends(WinningOnCenterController, _super);
            function WinningOnCenterController(game, name, layer, debugMode) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.layer = layer;
                _this.init();
                return _this;
            }
            WinningOnCenterController.prototype.init = function () {
                _super.prototype.init.call(this);
                this.tweens = [];
                this.textField = this.game.textFactory.getTextWithStyle('', "WINNING_ON_CENTER_TEXT_style");
                this.textField.anchor.set(0.5, 0.5);
                this.textField.x = 683;
                this.textField.y = 384;
                this.textField.visible = false;
                this.layer.add(this.textField);
            };
            WinningOnCenterController.prototype.showWinning = function (winValue) {
                if (winValue == 0) {
                    return;
                }
                var sizeTween = this.game.add.tween(this.textField.scale).to({ x: 1.2, y: 1.2 }, 250, Phaser.Easing.Linear.None, true, 0, -1, true);
                sizeTween.repeatDelay(500);
                var tween = this.game.add.tween(this.textField).to({ text: winValue }, 500, Phaser.Easing.Linear.None, true);
                tween.onUpdateCallback(this.onTweenUpdate, this);
                tween.onComplete.addOnce(this.onTweenComplete, this);
                this.textField.visible = true;
                this.tweens = [sizeTween, tween];
            };
            WinningOnCenterController.prototype.onTweenUpdate = function () {
                //this.textField.text = (+this.textField.text).toFixed(2);
                this.textField.text = common.MoneyFormatController.instance.format(+(+this.textField.text).toFixed(2), true);
            };
            WinningOnCenterController.prototype.onTweenComplete = function () {
                //this.textField.text = (+this.textField.text).toFixed(2);
                this.textField.text = common.MoneyFormatController.instance.format(+(+this.textField.text).toFixed(2), true);
            };
            WinningOnCenterController.prototype.hideWinning = function (data) {
                this.killAllTweens();
                this.textField.scale.set(1, 1);
                this.textField.text = '';
                this.textField.visible = false;
            };
            WinningOnCenterController.prototype.killAllTweens = function () {
                this.tweens.forEach(function (tween) {
                    tween.stop(false);
                    this.game.tweens.remove(tween);
                }, this);
            };
            return WinningOnCenterController;
        }(common.ActionController));
        common.WinningOnCenterController = WinningOnCenterController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var AnimationConfiguration = /** @class */ (function () {
            function AnimationConfiguration(rawData) {
                this.parse(rawData);
            }
            AnimationConfiguration.prototype.parse = function (rawData) {
                this._assetKey = rawData.assetKey;
                this._key = rawData.key;
                this._scale = rawData.scale || { x: 1, y: 1 };
                this._pivot = rawData.pivot || { x: 1, y: 1 };
                this._anchor = rawData.anchor || { x: 1, y: 1 };
                this._animations = [];
                for (var i = 0; i < rawData.animations.length; i++) {
                    this._animations.push(new AnimationDataConfiguration(rawData.animations[i]));
                }
                return this;
            };
            Object.defineProperty(AnimationConfiguration.prototype, "assetKey", {
                get: function () {
                    return this._assetKey;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimationConfiguration.prototype, "key", {
                get: function () {
                    return this._key;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimationConfiguration.prototype, "scale", {
                get: function () {
                    return this._scale;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimationConfiguration.prototype, "pivot", {
                get: function () {
                    return this._pivot;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimationConfiguration.prototype, "anchor", {
                get: function () {
                    return this._anchor;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimationConfiguration.prototype, "animations", {
                get: function () {
                    return this._animations;
                },
                enumerable: true,
                configurable: true
            });
            return AnimationConfiguration;
        }());
        common.AnimationConfiguration = AnimationConfiguration;
        var AnimationDataConfiguration = /** @class */ (function () {
            function AnimationDataConfiguration(rawData) {
                this.parse(rawData);
            }
            AnimationDataConfiguration.prototype.parse = function (rawData) {
                this._name = rawData.name;
                this._startFrame = rawData.startFrame || 1;
                this._endFrame = rawData.endFrame || 1;
                this._isLoop = rawData.isLoop || false;
                this._frameRate = rawData.frameRate || 24;
                this._playOnCompleted = rawData.playOnCompleted || undefined;
                return this;
            };
            Object.defineProperty(AnimationDataConfiguration.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimationDataConfiguration.prototype, "startFrame", {
                get: function () {
                    return this._startFrame;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimationDataConfiguration.prototype, "endFrame", {
                get: function () {
                    return this._endFrame;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimationDataConfiguration.prototype, "isLoop", {
                get: function () {
                    return this._isLoop;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimationDataConfiguration.prototype, "frameRate", {
                get: function () {
                    return this._frameRate;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimationDataConfiguration.prototype, "playOnCompleted", {
                get: function () {
                    return this._playOnCompleted;
                },
                enumerable: true,
                configurable: true
            });
            return AnimationDataConfiguration;
        }());
        common.AnimationDataConfiguration = AnimationDataConfiguration;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ButtonConfiguration = /** @class */ (function () {
            function ButtonConfiguration(rawData) {
                this.parse(rawData);
            }
            ButtonConfiguration.prototype.parse = function (rawData) {
                this._name = rawData.name;
                this._className = rawData.className;
                this._key = rawData.key;
                this._action = rawData.action ? rawData.action : "";
                this._caption = rawData.caption ? rawData.caption : "";
                this._textStyle = rawData.textStyle;
                this._disabledFrame = rawData.disabledFrame ? rawData.disabledFrame : 0;
                this._overFrame = rawData.overFrame ? rawData.overFrame : 0;
                this._outFrame = rawData.outFrame ? rawData.outFrame : 0;
                this._downFrame = rawData.downFrame ? rawData.downFrame : 0;
                this._upFrame = rawData.upFrame ? rawData.upFrame : 0;
                this._trigger = rawData.trigger ? rawData.trigger : false;
                this._triggerSoundKeys = rawData.triggerSoundKeys ? rawData.triggerSoundKeys : null;
                this._triggerStateFrames = rawData.triggerStateFrames ? rawData.triggerStateFrames : 0;
                this._onDownSoundKey = rawData.onDownSoundKey ? rawData.onDownSoundKey : null;
                this._onOverSoundKey = rawData.onOverSoundKey ? rawData.onOverSoundKey : null;
                this._onOutSoundKey = rawData.onOutSoundKey ? rawData.onOutSoundKey : null;
                this._onUpSoundKey = rawData.onUpSoundKey ? rawData.onUpSoundKey : null;
                return this;
            };
            Object.defineProperty(ButtonConfiguration.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "className", {
                get: function () {
                    return this._className;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "trigger", {
                get: function () {
                    return this._trigger;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "triggerStateFrames", {
                get: function () {
                    return this._triggerStateFrames;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "triggerSoundKeys", {
                get: function () {
                    return this._triggerSoundKeys;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "textStyle", {
                get: function () {
                    return this._textStyle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "key", {
                get: function () {
                    return this._key;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "onDownSoundKey", {
                get: function () {
                    return this._onDownSoundKey;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "onOverSoundKey", {
                get: function () {
                    return this._onOverSoundKey;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "onOutSoundKey", {
                get: function () {
                    return this._onOutSoundKey;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "onUpSoundKey", {
                get: function () {
                    return this._onUpSoundKey;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "action", {
                get: function () {
                    return this._action;
                },
                set: function (value) {
                    this._action = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "caption", {
                get: function () {
                    return this._caption;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "overFrame", {
                get: function () {
                    return this._overFrame;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "disabledFrame", {
                get: function () {
                    return this._disabledFrame;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "outFrame", {
                get: function () {
                    return this._outFrame;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "downFrame", {
                get: function () {
                    return this._downFrame;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ButtonConfiguration.prototype, "upFrame", {
                get: function () {
                    return this._upFrame;
                },
                enumerable: true,
                configurable: true
            });
            return ButtonConfiguration;
        }());
        common.ButtonConfiguration = ButtonConfiguration;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var EmotionConfiguration = /** @class */ (function () {
            function EmotionConfiguration(rawData) {
                this.parse(rawData);
            }
            /**
             *
             * @param rawData
             * @returns {EmotionsConfiguration}
             */
            EmotionConfiguration.prototype.parse = function (rawData) {
                this._type = rawData.type;
                this._className = rawData.className;
                this._coef = rawData.coef;
                this._count = rawData.count;
                this._lines = rawData.lines;
                if (rawData.emotion) {
                    this._emotion = new common.AnimationConfiguration(rawData.emotion);
                }
                return this;
            };
            Object.defineProperty(EmotionConfiguration.prototype, "type", {
                get: function () {
                    return this._type;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(EmotionConfiguration.prototype, "className", {
                get: function () {
                    return this._className;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(EmotionConfiguration.prototype, "coef", {
                get: function () {
                    return this._coef;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(EmotionConfiguration.prototype, "count", {
                get: function () {
                    return this._count;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(EmotionConfiguration.prototype, "lines", {
                get: function () {
                    return this._lines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(EmotionConfiguration.prototype, "emotion", {
                get: function () {
                    return this._emotion;
                },
                enumerable: true,
                configurable: true
            });
            return EmotionConfiguration;
        }());
        common.EmotionConfiguration = EmotionConfiguration;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GameConfiguration = /** @class */ (function () {
            function GameConfiguration(rawData) {
                this.parse(rawData);
            }
            GameConfiguration.prototype.parse = function (rawData) {
                this._gameName = rawData.gameName;
                this._clientAPI = rawData.clientAPI;
                this._winningShowingSettings = new common.WinningShowingSettings(rawData.winningShowingSettings);
                this._standartSymbolSetting = new StandartSymbolSetting(rawData.standartSymbolSetting);
                this._states = [];
                for (var i = 0; i < rawData.states.length; i++) {
                    this._states.push(new common.StateConfiguration(rawData.states[i]));
                }
                this._symbols = [];
                for (var i = 0; i < rawData.symbols.length; i++) {
                    this._symbols.push(new common.SymbolProperties(rawData.symbols[i]));
                }
                this._rollingMechanics = new common.RollingMechanicsConfiguration(rawData.rollingMechanics);
                this._reels = [];
                this._paylines = [];
                this._scatters = [];
                this._buttons = [];
                this._emotions = [];
                this._sounds = [];
                this._animations = [];
                if (rawData.reels) {
                    for (var i = 0; i < rawData.reels.length; i++) {
                        this._reels.push(new common.ReelConfiguration(rawData.reels[i]));
                    }
                }
                if (rawData.paylines) {
                    for (var i = 0; i < rawData.paylines.length; i++) {
                        this._paylines.push(new common.WinLineConfiguration(rawData.paylines[i]));
                    }
                }
                if (rawData.scatters) {
                    for (var i = 0; i < rawData.scatters.length; i++) {
                        this._scatters.push(new common.WinScattersConfiguration(rawData.scatters[i]));
                    }
                }
                if (rawData.buttons) {
                    for (var i = 0; i < rawData.buttons.length; i++) {
                        this._buttons.push(new common.ButtonConfiguration(rawData.buttons[i]));
                    }
                }
                if (rawData.emotions) {
                    for (var i = 0; i < rawData.emotions.length; i++) {
                        this._emotions.push(new common.EmotionConfiguration(rawData.emotions[i]));
                    }
                }
                if (rawData.sounds) {
                    for (var i = 0; i < rawData.sounds.length; i++) {
                        this._sounds.push(new common.SoundConfiguration(rawData.sounds[i]));
                    }
                }
                if (rawData.animations) {
                    for (var i = 0; i < rawData.animations.length; i++) {
                        this._animations.push(new common.AnimationConfiguration(rawData.animations[i]));
                    }
                }
                return this;
            };
            Object.defineProperty(GameConfiguration.prototype, "standartSymbolSetting", {
                get: function () {
                    return this._standartSymbolSetting;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "gameName", {
                get: function () {
                    return this._gameName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "clientAPI", {
                get: function () {
                    return this._clientAPI;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "symbols", {
                get: function () {
                    return this._symbols;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "rollingMechanics", {
                get: function () {
                    return this._rollingMechanics;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "reels", {
                get: function () {
                    return this._reels;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "paylines", {
                get: function () {
                    return this._paylines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "scatters", {
                get: function () {
                    return this._scatters;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "states", {
                get: function () {
                    return this._states;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "buttons", {
                get: function () {
                    return this._buttons;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "emotions", {
                get: function () {
                    return this._emotions;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "winningShowingSettings", {
                get: function () {
                    return this._winningShowingSettings;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "sounds", {
                get: function () {
                    return this._sounds;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameConfiguration.prototype, "animations", {
                get: function () {
                    return this._animations;
                },
                enumerable: true,
                configurable: true
            });
            return GameConfiguration;
        }());
        common.GameConfiguration = GameConfiguration;
        var StandartSymbolSetting = /** @class */ (function () {
            function StandartSymbolSetting(rawData) {
                this._width = rawData.width;
                this._height = rawData.height;
                this._borders = rawData.borders;
                this._className = rawData.className;
                this._trackedSymbols = rawData.trackedSymbols;
                this._trackedSymbolsMin = rawData.trackedSymbolsMin;
                this._trackedSymbolsMinRequired = rawData.trackedSymbolsMinRequired;
            }
            Object.defineProperty(StandartSymbolSetting.prototype, "width", {
                get: function () {
                    return this._width;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StandartSymbolSetting.prototype, "trackedSymbols", {
                get: function () {
                    return this._trackedSymbols;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StandartSymbolSetting.prototype, "trackedSymbolsMin", {
                get: function () {
                    return this._trackedSymbolsMin;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StandartSymbolSetting.prototype, "trackedSymbolsMinRequired", {
                get: function () {
                    return this._trackedSymbolsMinRequired;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StandartSymbolSetting.prototype, "height", {
                get: function () {
                    return this._height;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StandartSymbolSetting.prototype, "borders", {
                get: function () {
                    return this._borders;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StandartSymbolSetting.prototype, "className", {
                get: function () {
                    return this._className;
                },
                enumerable: true,
                configurable: true
            });
            return StandartSymbolSetting;
        }());
        common.StandartSymbolSetting = StandartSymbolSetting;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ReelConfiguration = /** @class */ (function () {
            function ReelConfiguration(rawData) {
                this.parse(rawData);
            }
            ReelConfiguration.prototype.parse = function (rawData) {
                this._startIndex = rawData.startIndex;
                this._stopIndex = rawData.stopIndex;
                this._stopTimeout = rawData.stopTimeout;
                this._soundStopKeys = rawData.soundStopKeys;
                this._startTimeout = rawData.startTimeout;
                this._shift = rawData.shift;
                this._speeds = rawData.speeds;
                this._visibledSymbols = rawData.visibledSymbols || 3;
                this._topInvisibleSymbolsCount = rawData.hasOwnProperty('topInvisibleSymbolsCount') ? rawData.topInvisibleSymbolsCount : 2;
                this._bottomInvisibleSymbolsCount = rawData.hasOwnProperty('bottomInvisibleSymbolsCount') ? rawData.bottomInvisibleSymbolsCount : 1;
                this._width = rawData.width;
                this._height = rawData.height;
                return this;
            };
            Object.defineProperty(ReelConfiguration.prototype, "soundStopKeys", {
                get: function () {
                    return this._soundStopKeys;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "stopTimeout", {
                get: function () {
                    return this._stopTimeout;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "startIndex", {
                get: function () {
                    return this._startIndex;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "stopIndex", {
                get: function () {
                    return this._stopIndex;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "startTimeout", {
                get: function () {
                    return this._startTimeout;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "shift", {
                get: function () {
                    return this._shift;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "speeds", {
                get: function () {
                    return this._speeds;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "width", {
                get: function () {
                    return this._width;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "height", {
                get: function () {
                    return this._height;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "visibledSymbols", {
                get: function () {
                    return this._visibledSymbols;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "topInvisibleSymbolsCount", {
                get: function () {
                    return this._topInvisibleSymbolsCount;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelConfiguration.prototype, "bottomInvisibleSymbolsCount", {
                get: function () {
                    return this._bottomInvisibleSymbolsCount;
                },
                enumerable: true,
                configurable: true
            });
            return ReelConfiguration;
        }());
        common.ReelConfiguration = ReelConfiguration;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var RollingMechanicsConfiguration = /** @class */ (function () {
            function RollingMechanicsConfiguration(rawData) {
                this.easeMap = {
                    "Power0": Phaser.Easing.Power0,
                    "Power1": Phaser.Easing.Power1,
                    "Power2": Phaser.Easing.power2,
                    "Power3": Phaser.Easing.power3,
                    "Power4": Phaser.Easing.power4,
                    "Linear": Phaser.Easing.Linear.None,
                    "Quad": Phaser.Easing.Quadratic.Out,
                    "Cubic": Phaser.Easing.Cubic.Out,
                    "Quart": Phaser.Easing.Quartic.Out,
                    "Quint": Phaser.Easing.Quintic.Out,
                    "Sine": Phaser.Easing.Sinusoidal.Out,
                    "Expo": Phaser.Easing.Exponential.Out,
                    "Circ": Phaser.Easing.Circular.Out,
                    "Elastic": Phaser.Easing.Elastic.Out,
                    "Back": Phaser.Easing.Back.Out,
                    "Bounce": Phaser.Easing.Bounce.Out,
                    "Quad.easeIn": Phaser.Easing.Quadratic.In,
                    "Cubic.easeIn": Phaser.Easing.Cubic.In,
                    "Quart.easeIn": Phaser.Easing.Quartic.In,
                    "Quint.easeIn": Phaser.Easing.Quintic.In,
                    "Sine.easeIn": Phaser.Easing.Sinusoidal.In,
                    "Expo.easeIn": Phaser.Easing.Exponential.In,
                    "Circ.easeIn": Phaser.Easing.Circular.In,
                    "Elastic.easeIn": Phaser.Easing.Elastic.In,
                    "Back.easeIn": Phaser.Easing.Back.In,
                    "Bounce.easeIn": Phaser.Easing.Bounce.In,
                    "Quad.easeOut": Phaser.Easing.Quadratic.Out,
                    "Cubic.easeOut": Phaser.Easing.Cubic.Out,
                    "Quart.easeOut": Phaser.Easing.Quartic.Out,
                    "Quint.easeOut": Phaser.Easing.Quintic.Out,
                    "Sine.easeOut": Phaser.Easing.Sinusoidal.Out,
                    "Expo.easeOut": Phaser.Easing.Exponential.Out,
                    "Circ.easeOut": Phaser.Easing.Circular.Out,
                    "Elastic.easeOut": Phaser.Easing.Elastic.Out,
                    "Back.easeOut": Phaser.Easing.Back.Out,
                    "Bounce.easeOut": Phaser.Easing.Bounce.Out,
                    "Quad.easeInOut": Phaser.Easing.Quadratic.InOut,
                    "Cubic.easeInOut": Phaser.Easing.Cubic.InOut,
                    "Quart.easeInOut": Phaser.Easing.Quartic.InOut,
                    "Quint.easeInOut": Phaser.Easing.Quintic.InOut,
                    "Sine.easeInOut": Phaser.Easing.Sinusoidal.InOut,
                    "Expo.easeInOut": Phaser.Easing.Exponential.InOut,
                    "Circ.easeInOut": Phaser.Easing.Circular.InOut,
                    "Elastic.easeInOut": Phaser.Easing.Elastic.InOut,
                    "Back.easeInOut": Phaser.Easing.Back.InOut,
                    "Bounce.easeInOut": Phaser.Easing.Bounce.InOut
                };
                this.parse(rawData);
            }
            RollingMechanicsConfiguration.prototype.parse = function (rawData) {
                this._tweenToTop = rawData.tweenToTop;
                this._tweenAccelaration = rawData.tweenAccelaration;
                this._resultedTween = rawData.resultedTween;
                this._tweenUpturn = rawData.tweenUpturn;
                return this;
            };
            RollingMechanicsConfiguration.prototype.tweenOf = function (str) {
                return this.easeMap[str];
            };
            Object.defineProperty(RollingMechanicsConfiguration.prototype, "tweenToTop", {
                get: function () {
                    return this._tweenToTop;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(RollingMechanicsConfiguration.prototype, "tweenAccelaration", {
                get: function () {
                    return this._tweenAccelaration;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(RollingMechanicsConfiguration.prototype, "resultedTween", {
                get: function () {
                    return this._resultedTween;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(RollingMechanicsConfiguration.prototype, "tweenUpturn", {
                get: function () {
                    return this._tweenUpturn;
                },
                enumerable: true,
                configurable: true
            });
            return RollingMechanicsConfiguration;
        }());
        common.RollingMechanicsConfiguration = RollingMechanicsConfiguration;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var SoundConfiguration = /** @class */ (function () {
            function SoundConfiguration(rawData) {
                this.parse(rawData);
            }
            SoundConfiguration.prototype.parse = function (rawData) {
                this._assetKey = rawData.assetKey;
                this._key = rawData.key;
                this._group = rawData.group == '' ? 'default' : rawData.group;
                this._volume = rawData.volume;
                this._loop = rawData.loop;
                return this;
            };
            Object.defineProperty(SoundConfiguration.prototype, "assetKey", {
                get: function () {
                    return this._assetKey;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SoundConfiguration.prototype, "key", {
                get: function () {
                    return this._key;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SoundConfiguration.prototype, "group", {
                get: function () {
                    return this._group;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SoundConfiguration.prototype, "volume", {
                get: function () {
                    return this._volume;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SoundConfiguration.prototype, "loop", {
                get: function () {
                    return this._loop;
                },
                enumerable: true,
                configurable: true
            });
            return SoundConfiguration;
        }());
        common.SoundConfiguration = SoundConfiguration;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var StateConfiguration = /** @class */ (function () {
            function StateConfiguration(rawData) {
                this.parse(rawData);
            }
            StateConfiguration.prototype.parse = function (rawData) {
                this._index = rawData.index;
                this._name = rawData.name;
                this._gotoStateName = rawData.gotoStateName;
                this._isAction = rawData.isAction;
                this._isRequiredAction = rawData.isRequiredAction;
                return this;
            };
            Object.defineProperty(StateConfiguration.prototype, "index", {
                get: function () {
                    return this._index;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StateConfiguration.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StateConfiguration.prototype, "gotoStateName", {
                get: function () {
                    return this._gotoStateName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StateConfiguration.prototype, "isAction", {
                get: function () {
                    return this._isAction;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(StateConfiguration.prototype, "isRequiredAction", {
                get: function () {
                    return this._isRequiredAction;
                },
                enumerable: true,
                configurable: true
            });
            return StateConfiguration;
        }());
        common.StateConfiguration = StateConfiguration;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var SymbolProperties = /** @class */ (function () {
            function SymbolProperties(rawData) {
                this.parse(rawData);
            }
            SymbolProperties.prototype.parse = function (rawData) {
                this._className = rawData.className;
                this._frameRate = rawData.frameRate;
                this._key = rawData.key;
                this._borderKeys = rawData.borderKeys;
                this._index = rawData.index;
                this._height = rawData.height;
                this._scale = rawData.scale || { x: 1, y: 1 };
                this._pivot = rawData.pivot || { x: 0, y: 0 };
                this._anchor = rawData.anchor || { x: 0, y: 0 };
                if (rawData.animations) {
                    this._animationData = rawData.animations;
                }
                return this;
            };
            Object.defineProperty(SymbolProperties.prototype, "className", {
                get: function () {
                    return this._className;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SymbolProperties.prototype, "frameRate", {
                get: function () {
                    return this._frameRate;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SymbolProperties.prototype, "key", {
                get: function () {
                    return this._key;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SymbolProperties.prototype, "borderKeys", {
                get: function () {
                    return this._borderKeys;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SymbolProperties.prototype, "index", {
                get: function () {
                    return this._index;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SymbolProperties.prototype, "height", {
                get: function () {
                    return this._height;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SymbolProperties.prototype, "scale", {
                get: function () {
                    return this._scale;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SymbolProperties.prototype, "pivot", {
                get: function () {
                    return this._pivot;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SymbolProperties.prototype, "anchor", {
                get: function () {
                    return this._anchor;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SymbolProperties.prototype, "animationData", {
                get: function () {
                    return this._animationData;
                },
                enumerable: true,
                configurable: true
            });
            return SymbolProperties;
        }());
        common.SymbolProperties = SymbolProperties;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var WinLineConfiguration = /** @class */ (function () {
            function WinLineConfiguration(rawData) {
                this.parse(rawData);
            }
            WinLineConfiguration.prototype.parse = function (rawData) {
                this._index = rawData.index;
                this._keys = rawData.keys;
                this._info = rawData.info;
                this._sounds = rawData.sounds;
                this._timeout = rawData.timeout;
                return this;
            };
            Object.defineProperty(WinLineConfiguration.prototype, "index", {
                get: function () {
                    return this._index;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(WinLineConfiguration.prototype, "keys", {
                get: function () {
                    return this._keys;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(WinLineConfiguration.prototype, "info", {
                get: function () {
                    return this._info;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(WinLineConfiguration.prototype, "sounds", {
                get: function () {
                    return this._sounds;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(WinLineConfiguration.prototype, "timeout", {
                get: function () {
                    return this._timeout;
                },
                enumerable: true,
                configurable: true
            });
            return WinLineConfiguration;
        }());
        common.WinLineConfiguration = WinLineConfiguration;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var WinningShowingSettings = /** @class */ (function () {
            function WinningShowingSettings(rawData) {
                this.parse(rawData);
            }
            WinningShowingSettings.prototype.parse = function (rawData) {
                this._delayBetweenWinningShowing = rawData.delayBetweenWinningShowing || 0;
                return this;
            };
            Object.defineProperty(WinningShowingSettings.prototype, "delayBetweenWinningShowing", {
                get: function () {
                    return this._delayBetweenWinningShowing;
                },
                enumerable: true,
                configurable: true
            });
            return WinningShowingSettings;
        }());
        common.WinningShowingSettings = WinningShowingSettings;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var WinScattersConfiguration = /** @class */ (function () {
            function WinScattersConfiguration(rawData) {
                this.parse(rawData);
            }
            WinScattersConfiguration.prototype.parse = function (rawData) {
                this._index = rawData.index;
                this._sounds = rawData.sounds;
                this._timeout = rawData.timeout;
                return this;
            };
            Object.defineProperty(WinScattersConfiguration.prototype, "index", {
                get: function () {
                    return this._index;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(WinScattersConfiguration.prototype, "sounds", {
                get: function () {
                    return this._sounds;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(WinScattersConfiguration.prototype, "timeout", {
                get: function () {
                    return this._timeout;
                },
                enumerable: true,
                configurable: true
            });
            return WinScattersConfiguration;
        }());
        common.WinScattersConfiguration = WinScattersConfiguration;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../libs/types/crypto-js.d.ts" />
///<reference path="../../libs/phaserTypes/phaser.d.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../libs/types/crypto-js.d.ts" />
///<reference path="../../libs/phaserTypes/phaser.d.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var Game = /** @class */ (function (_super) {
            __extends(Game, _super);
            function Game(browser, parent, options) {
                var _this = 
                //super(parent.offsetWidth || 1024, parent.offsetHeight || 768, Phaser.AUTO/*browser*/, parent || 'content', 'BootStage', false, false);
                _super.call(this, parent.offsetWidth || 1024, parent.offsetHeight || 768, Phaser.CANVAS, parent || 'content', 'BootStage', true, true) || this;
                _this.stateIndex = 0;
                _this.stateList = ['BootStage', 'PreloaderStage', 'GameStage'];
                console.log(parent);
                console.log(parent.offsetWidth);
                console.log(parent.offsetHeight);
                console.log('ver87');
                Game.pool = [];
                Game.bootOptions = options || { game_path: '' };
                _this.state.add("BootStage", common.BootStage, false);
                _this.state.add('PreloaderStage', common.PreloaderStage, false);
                _this.state.add('GameStage', common.GameStage, false);
                _this.startNextState();
                return _this;
            }
            Game.prototype.startNextState = function (args, clearWorld) {
                if (clearWorld === undefined) {
                    clearWorld = true;
                }
                this._currentState = this.stateList[this.stateIndex];
                this.state.start(this._currentState, clearWorld, false, args);
                this.stateIndex++;
                console.log(this.currentState);
            };
            Object.defineProperty(Game.prototype, "textFactory", {
                get: function () {
                    return this._textFactory;
                },
                set: function (value) {
                    this._textFactory = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game.prototype, "currentState", {
                get: function () {
                    return this._currentState;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game.prototype, "pattern", {
                get: function () {
                    return this._pattern;
                },
                set: function (value) {
                    this._pattern = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game.prototype, "buttonsStates", {
                get: function () {
                    return this._buttonsStates;
                },
                set: function (value) {
                    this._buttonsStates = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game.prototype, "model", {
                get: function () {
                    return this._model;
                },
                set: function (value) {
                    this._model = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game, "pool", {
                get: function () {
                    return this._pool;
                },
                set: function (value) {
                    this._pool = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Game, "bootOptions", {
                get: function () {
                    return this._bootOptions;
                },
                set: function (value) {
                    this._bootOptions = value;
                },
                enumerable: true,
                configurable: true
            });
            return Game;
        }(Phaser.Game));
        common.Game = Game;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
//console.log = function() {} 
//  Base abstract class inherited Phaser.State
//  to have an abillity to add any common properties or methods.
var TypeScriptPhaserSlot;
//  Base abstract class inherited Phaser.State
//  to have an abillity to add any common properties or methods.
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseStage = /** @class */ (function (_super) {
            __extends(BaseStage, _super);
            function BaseStage(game) {
                var _this = _super.call(this) || this;
                _this._name = name;
                _this.debugMode = false;
                _this.game = game;
                return _this;
            }
            BaseStage.prototype.preload = function () {
            };
            BaseStage.prototype.create = function () {
            };
            /**
             * Specific console logger
             * @param message
             */
            BaseStage.prototype.debuglog = function (message) {
                console.log(name, ":", message);
            };
            Object.defineProperty(BaseStage.prototype, "debugMode", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._debugMode;
                },
                set: function (value) {
                    this._debugMode = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseStage.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            return BaseStage;
        }(Phaser.State));
        common.BaseStage = BaseStage;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BootStage = /** @class */ (function (_super) {
            __extends(BootStage, _super);
            function BootStage() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            BootStage.prototype.init = function () {
                this.game.canvas.setAttribute('id', 'gameCanvas');
                this.game.tweens.frameBased = true;
                this.game.renderer.renderSession.roundPixels = false;
                this.game.forceSingleUpdate = true;
                this.game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
                this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.RESIZE;
                this.game.scale.pageAlignHorizontally = true;
                this.game.scale.pageAlignVertically = true;
                this.game.stage.disableVisibilityChange = true;
                this.game.scale.refresh();
                if (!window['GR']) {
                    this.game.scale.enterIncorrectOrientation.add(handleIncorrect, this);
                    this.game.scale.leaveIncorrectOrientation.add(handleCorrect, this);
                    this.game.scale.forceOrientation(true, false);
                    this.game.scale.forceLandscape = true;
                    this.game.scale.onSizeChange.add(correctSize, this);
                    if (this.game.scale.isLandscape) {
                        handleCorrect.call(this);
                    }
                    else {
                        handleIncorrect.call(this);
                    }
                }
                /*this.game.onBlur.add(function (data:any) {
                    this.game.paused = true;
                }, this);
                this.game.onFocus.add(function (data:any) {
                    this.game.paused = false;
                }, this);*/
                function correctSize(scaleManager, screenWidth, screenHeight) {
                    //if(screenWidth < screenHeight) {
                    if (!this.game.scale.isLandscape) {
                        handleIncorrect.call(this);
                    }
                    else {
                        handleCorrect.call(this);
                    }
                }
                /**
                 *
                 */
                function handleIncorrect() {
                    if (!this.game.device.desktop) {
                        console.log('handleIncorrect');
                        var image = document.getElementById("turn");
                        if (image) {
                            image.style.display = "block";
                        }
                        this.game.scale.stopFullScreen();
                        this.game.paused = true;
                    }
                }
                /**
                 *
                 */
                function handleCorrect() {
                    if (!this.game.device.desktop) {
                        console.log('handleCorrect');
                        var image = document.getElementById("turn");
                        if (image) {
                            image.style.display = "none";
                        }
                        this.game.paused = false;
                    }
                }
                this.gameName = "PrincessFrog";
            };
            BootStage.prototype.preload = function () {
                this.game.load.baseURL = common.Game.bootOptions.game_path;
                this.game.load.crossOrigin = true;
                //this.game.load.image('preloader_bg', "assets/preloader/preloader_bg.jpg", false);
                //this.game.load.image('preloader_i1', "assets/preloader/image_1.png", false);
                //this.game.load.image('preloader_i2', "assets/preloader/image_2.png", false);
                //this.game.load.json('preloaderConfig', "preloader.json", false);
                //this.game.load.atlas('preloader_progressbar', "assets/preloader/progerssbar.png", "assets/preloader/progerssbar.json");
                this.game.load.pack("assets", "/assets/preloader_asset.json");
                this.game.load.json('preloaderConfig', "preloader.json", false);
                this.game.load.start();
            };
            BootStage.prototype.create = function () {
                if (!window['GR']) {
                    if (this.game.scale.isLandscape) {
                        this.game.startNextState();
                    }
                    else {
                        this.game.scale.leaveIncorrectOrientation.addOnce(function () {
                            console.log('startNextState');
                            this.game.startNextState();
                        }, this);
                        //this.game.scale.leaveIncorrectOrientation.addOnce(this.game.startNextState, this.game, -1);
                    }
                }
                else {
                    this.game.startNextState();
                }
            };
            return BootStage;
        }(common.BaseStage));
        common.BootStage = BootStage;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseStage.ts" />
// Game Phaser State
// Start of the Game
var TypeScriptPhaserSlot;
///<reference path="./BaseStage.ts" />
// Game Phaser State
// Start of the Game
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GameStage = /** @class */ (function (_super) {
            __extends(GameStage, _super);
            function GameStage() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
             * Getting started arguments like a settings or pre-defined configs
             * @param args
             */
            GameStage.prototype.init = function (args) {
                /*this.game.tweens.frameBased = true;
                this.game.renderer.renderSession.roundPixels = true;
                this.game.forceSingleUpdate = true;
    
                this.game.scale.setGameSize(2732, 1536);
    
                if(this.game.device.desktop) {
                    this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
                    this.game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
    
                    this.game.scale.setMinMax(3, 4, window.screen.width, window.screen.height);
                    this.game.scale.setUserScale(0.5, 0.5, 0, 0);
    
                    this.game.scale.refresh();
                } else {
                    this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
                    this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    
                    this.game.scale.refresh();
                }
    
                this.game.scale.pageAlignHorizontally = true;
                this.game.scale.pageAlignVertically = true;
    
                this.game.stage.disableVisibilityChange = true;
                this.game.scale.refresh();
    
                this.game.scale.forceOrientation(true, false);
                this.game.scale.enterIncorrectOrientation.add(handleIncorrect, this);
                this.game.scale.leaveIncorrectOrientation.add(handleCorrect, this);
    
                this.game.scale.forceLandscape = true;
    
                if(this.game.scale.isLandscape) {
                    handleCorrect.call(this);
                } else {
                    handleIncorrect.call(this);
                }
    
                function handleIncorrect(){
                    if(!this.game.device.desktop){
                        document.getElementById("turn").style.display="block";
                    }
                }
    
                function handleCorrect(){
                    if(!this.game.device.desktop){
                        document.getElementById("turn").style.display="none";
                    }
                }*/
                /*this.game.scale.setResizeCallback(function () {
                    //window.innerWidth
                    this.game.scale.setMinMax(window.innerWidth, window.innerWidth / 1.33);
                    this.game.scale.refresh();
                }, this);*/
                this.configs = args;
            };
            /**
             * Place to put the ordered list of states to the Model instance
             */
            GameStage.prototype.preload = function () {
                // Game Model instance
                this._model = new common.GameModel(this.debugMode);
                // Game controller instance
                // will create rest of the controllers like:
                // <IAPIController>, <IGameController> including controllers
                // to work with server-side.
                var gameControllerName = String(this.configs[0].gameName + "GameController");
                this._gameController = new TypeScriptPhaserSlot.common[gameControllerName](this.game, gameControllerName, this.debugMode, this.configs);
            };
            /**
             * Getting binding main game controller class to the Model instance
             */
            GameStage.prototype.create = function () {
                this.game.canvas.getContext('2d').webkitImageSmoothingEnabled = true;
                this.game.canvas.getContext('2d').oImageSmoothingEnabled = true;
                this.game.canvas.getContext('2d').mozImageSmoothingEnabled = true;
                this.game.canvas.getContext('2d').imageSmoothingEnabled = true;
                this.gameController.bindToModel(this.model);
                this.game.model = this.model;
            };
            Object.defineProperty(GameStage.prototype, "model", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._model;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStage.prototype, "gameController", {
                get: function () {
                    return this._gameController;
                },
                enumerable: true,
                configurable: true
            });
            return GameStage;
        }(common.BaseStage));
        common.GameStage = GameStage;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseStage.ts" />
var TypeScriptPhaserSlot;
///<reference path="./BaseStage.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var PreloaderStage = /** @class */ (function (_super) {
            __extends(PreloaderStage, _super);
            function PreloaderStage() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            PreloaderStage.prototype.init = function (gameName) {
                //this.gameName = "PrincessFrog";//gameName
                //this.gameName = "Detective";//gameName
                //this.gameName = "Spinners";//gameName
                if (window['GR']) {
                    window['GR'].Events.game.start();
                    window['GR'].Events.flow.state.on(function (data) {
                        window['windowBooongoStateData'] = data;
                    }, this);
                }
                var preloaderConfig = this.game.cache.getJSON('preloaderConfig');
                this.gameName = preloaderConfig.gameName;
                var preloaderViewName = this.getClassName('PreloaderView');
                this.preloader = new TypeScriptPhaserSlot.common[preloaderViewName](this.game, preloaderViewName, null, this.debugMode);
                this.game.add.existing(this.preloader);
            };
            PreloaderStage.prototype.preload = function () {
                this.game.load.baseURL = common.Game.bootOptions.game_path;
                this.game.load.crossOrigin = true;
                //this.game.load.script('particlestorm', 'plugins/particle-storm.min.js');
                this.game.load.json('strings', "/assets/strings.json" + '?' + Math.random(), false);
                this.game.load.json('styles', "/assets/styles.json" + '?' + Math.random(), false);
                this.game.load.json('settings', "/assets/game.json" + '?' + Math.random(), false);
                this.game.load.json('buttonsStates', (this.game.device.desktop ? "/assets/buttonsStates.json" : "/assets/buttonsStates_mobile.json") + '?' + Math.random());
                this.game.load.json('pattern', "/assets/pattern.json" + '?' + Math.random());
                this.game.load.pack("assets", "/assets/asset.json" + '?' + Math.random());
                this.game.load.onFileComplete.add(this.onLoadingFileComplete, this);
                this.game.load.onLoadComplete.add(this.onLoadingComplete, this);
                this.game.load.start();
                window.addEventListener('resize', function () {
                    this.game.scale.setGameSize(this.game.canvas.parentElement.offsetWidth, this.game.canvas.parentElement.offsetHeight);
                }.bind(this));
                this.game.scale.setGameSize(this.game.canvas.parentElement.offsetWidth, this.game.canvas.parentElement.offsetHeight);
                this.game.camera.width = this.game.canvas.parentElement.offsetWidth;
                this.game.camera.height = this.game.canvas.parentElement.offsetHeight;
                this.game.scale.onSizeChange.add(this.correctSize, this);
                this.correctSize(this.game.scale, this.game.scale.width, this.game.scale.height);
            };
            /**
             *
             * @param progress
             * @param cacheKey
             * @param success
             * @param totalLoaded
             * @param totalFiles
             */
            PreloaderStage.prototype.onLoadingFileComplete = function (progress, cacheKey, success, totalLoaded, totalFiles) {
                this.preloader.onLoadingUpdate(progress, cacheKey, success, totalLoaded, totalFiles);
            };
            /**
             *
             */
            PreloaderStage.prototype.onLoadingComplete = function () {
                this.preloader.onLoadingComplete();
            };
            /**
             *
             * @param className
             * @returns {string}
             */
            PreloaderStage.prototype.getClassName = function (className) {
                if (TypeScriptPhaserSlot.common[this.gameName + className]) {
                    return this.gameName + className;
                }
                return className;
            };
            PreloaderStage.prototype.create = function () {
                if (window['GR']) {
                    window['GR'].Events.game.loaded();
                }
                var settings = this.game.cache.getJSON('settings');
                var pattern = this.game.cache.getJSON('pattern');
                var strings = this.game.cache.getJSON('strings');
                var stylesData = this.game.cache.getJSON('styles');
                var buttonsStates = this.game.cache.getJSON('buttonsStates');
                var styles = [];
                for (var i = 0; i < stylesData.styles.length; i++) {
                    styles.push(new common.TextStyle(stylesData.styles[i]));
                }
                this.game.textFactory = new common.TextFactory(this.game, styles, strings, "en");
                this.game.buttonsStates = buttonsStates;
                //this.game.camera.fade(0x0, 2000);
                /*setTimeout(function () {
                 this.game.startNextState([settings, pattern]);
                 }.bind(this), 3000);*/
                this.game.startNextState([settings, pattern]);
                this.game.scale.onSizeChange.remove(this.correctSize, this);
            };
            PreloaderStage.prototype.correctSize = function (scaleManager, screenWidth, screenHeight) {
                if (screenWidth < screenHeight) {
                    return;
                }
                if (!this.game.world.children || this.game.world.children.length == 0) {
                    return;
                }
                var w = 1366;
                var h = 768;
                var g = this.game.world.getChildAt(0);
                console.log('Game WIDTH', this.game.width);
                console.log('Game HEIGHT', this.game.height);
                console.log('Camera WIDTH', this.game.camera.width);
                console.log('Camera HEIGHT', this.game.camera.height);
                if (this.game.camera.height != h) {
                    var scale = this.game.camera.height / h;
                    g.scale.set(scale, scale);
                }
                var boundsXoffset = g.width < this.game.camera.width ? (this.game.camera.width - g.width) / 2 * -1 : 0;
                this.game.world.resize(g.width, g.height);
                this.game.world.setBounds(boundsXoffset, 0, g.width, g.height);
                var cameraX = (g.width - this.game.camera.width) / 2;
                this.game.camera.x = cameraX;
            };
            return PreloaderStage;
        }(common.BaseStage));
        common.PreloaderStage = PreloaderStage;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        /**
         * Base abstract Model created in the aspect of the concept MVC.
          The Model gets control the main states of the Game by State Manager like:
          Spin State, Collect State and so on.
          Also the Model can manage the whole list of resources and saves logic data.
          The Model sends signals and gets callbacks when works are done.
         */
        var BaseModel = /** @class */ (function () {
            /**
             * Model constructor
             * @param stateList: Model should get the ordered list of actual states to manage them.
             */
            function BaseModel(debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                this._notEnoughMoney = false;
                this.debugMode = debugMode;
            }
            /**
             * State manager list
             * @param stateList
             */
            BaseModel.prototype.setStateList = function (stateList) {
                this._stateList = stateList;
            };
            /**
             * Binding all signals from list of the states which designed for the interaction with Model
               e.g. finishWorkSignal - is Phaser.Signal which is going be dispatched when the current state is done.
               and Model have to try set next state up.
             */
            BaseModel.prototype.bindingStatesSignals = function () {
                for (var i = 0; i < this.stateList.length; i++) {
                    this.stateList[i].startWorkSignal.add(this.acceptState, this);
                    this.stateList[i].finishWorkSignal.add(this.nextState, this);
                    this.stateList[i].cancelSignal.add(this.nextState, this);
                }
            };
            /**
             * Called when current state allows to be run
             * Acceptation state and doing according work
             * Model dispatches about that to anywhere
             * @param state
             * @param data
             * @param isRequiredAction
             */
            BaseModel.prototype.acceptState = function (state, data, isRequiredAction) {
                this.debugLog("starts work " + state.name);
                this._currentState = state;
                this._currentStateIndex = this.getStateIndexByName(this._currentState.name);
                this.modelSignal.dispatch(state, data, isRequiredAction);
            };
            /**
             * Check and set next state up.
             */
            BaseModel.prototype.nextState = function (isFinished) {
                if (this.debugMode)
                    console.log("----------------------------------------------------------------------------------");
                if (this.notEnoughMoney) {
                    this._notEnoughMoneySignal.dispatch();
                    this._newState = this.getStateByName(this._newState.gotoStateName);
                    this._newStateIndex = this.getStateIndexByName(this._newState.name);
                    this._newState.tryWork();
                    this.debugLog("goto to state " + this._newState.name);
                    this.notEnoughMoney = false;
                    return;
                }
                if (isFinished && this._newState && this._newState.gotoStateName && this._newState.checkConditionForGotoState()) {
                    this._newState = this.getStateByName(this._newState.gotoStateName);
                    this._newStateIndex = this.getStateIndexByName(this._newState.name);
                    this.debugLog("goto to state " + this._newState.name);
                    this._newState.tryWork();
                    return;
                }
                if (this.stateList.length - 1 >= this._newStateIndex + 1) {
                    this._newStateIndex++;
                }
                else {
                    this._newStateIndex = 0;
                }
                this._newState = this._stateList[this._newStateIndex];
                this.debugLog("switching to " + this._newState.name);
                this._newState.tryWork();
            };
            /**
             * Specific console logger
             * @param message
             */
            BaseModel.prototype.debugLog = function (message) {
                console.log("MODEL:", message);
            };
            /**
             *
             */
            BaseModel.prototype.goToState = function (name) {
                this._newState = this.getStateByName(name);
                this._newStateIndex = this.getStateIndexByName(name);
                this._newState.tryWork();
            };
            /**
             * Search state <IState> by name in the state list
             * @param {string} stateName
             * @returns
             */
            BaseModel.prototype.getStateByName = function (stateName) {
                for (var i = 0; i < this.stateList.length; i++) {
                    if (this.stateList[i].name == stateName) {
                        return this.stateList[i];
                    }
                }
                // this.debugLog("getStateByName " + stateName + " no found");
                return null;
            };
            BaseModel.prototype.getStateIndexByName = function (stateName) {
                for (var i = 0; i < this.stateList.length; i++) {
                    if (this.stateList[i].name == stateName) {
                        return i;
                    }
                }
                return null;
            };
            Object.defineProperty(BaseModel.prototype, "currentStateIndex", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._currentStateIndex;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseModel.prototype, "newStateIndex", {
                get: function () {
                    return this._newStateIndex;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseModel.prototype, "currentState", {
                get: function () {
                    return this._currentState;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseModel.prototype, "newState", {
                get: function () {
                    return this._newState;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseModel.prototype, "stateList", {
                get: function () {
                    return this._stateList;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseModel.prototype, "notEnoughMoney", {
                get: function () {
                    return this._notEnoughMoney;
                },
                set: function (value) {
                    this._notEnoughMoney = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseModel.prototype, "debugMode", {
                get: function () {
                    return this._debugMode;
                },
                set: function (value) {
                    this._debugMode = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseModel.prototype, "modelSignal", {
                get: function () {
                    return this._modelSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseModel.prototype, "notEnoughMoneySignal", {
                get: function () {
                    return this._notEnoughMoneySignal;
                },
                enumerable: true,
                configurable: true
            });
            return BaseModel;
        }());
        common.BaseModel = BaseModel;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseModel.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseModel.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GameModel = /** @class */ (function (_super) {
            __extends(GameModel, _super);
            /**
             * debugMode
             * @param debugMode
             */
            function GameModel(debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, debugMode) || this;
                _this._isAutoPlay = false;
                _this._spinsNumber = 0;
                _this._notEnoughMoneySignal = new Phaser.Signal();
                _this._modelSignal = new Phaser.Signal();
                _this._betChangedSignal = new Phaser.Signal();
                _this._linesChangedSignal = new Phaser.Signal();
                _this._autoChangedSignal = new Phaser.Signal();
                _this._denomination = 1;
                _this._multiplier = 1;
                window['model'] = _this;
                return _this;
            }
            /**
             * Get spin response
             */
            GameModel.prototype.onSpinResponse = function (data) {
                this._bonusResponse = data.bonusResponce || null;
                this._freeSpinsAdded = data.state.freespins > this._freespins ? data.state.freespins - (this._freespins - 1) : 0;
                if (this._freespins == 0) {
                    this._freeSpinsAdded = 0;
                }
                this._freespins = data.state.freespins;
                this._freeSpinsTotal = 0;
                this._hasBonus = data.state.hasBonus;
                if (data.state.coins != null && data.state.coins != undefined) {
                    this._coins = data.state.coins;
                }
                this._currResultIndex = 0;
                this._spinResponse = data;
                this._win = data.win;
                this._totalWin = data.totalWin;
                this._multiplier = data.multiplier;
                this._bonusTotalWin = data.state.bonusTotalWin;
                this._spinsNumber++;
                this._rawData = data.rawData;
                var bet = data['betPerLine'] || this._allowedBets[this.currBetIndex];
                this.correctBets(bet);
                this.debugLog("spin #" + this._spinsNumber);
            };
            /**
             * Get collect response (optional)
             */
            GameModel.prototype.onCollectResponse = function (data) {
            };
            /**
             * Get bonus game response (optional)
             */
            /*public onBonusGameResponse(data: BonusResponse): void {
                this._bonusResponse = data;
    
                this._freespins = data.freespins || this._freespins;
                //this._hasBonus = data.hasBonus || this._hasBonus;
            }*/
            GameModel.prototype.onBonusGameResponse = function (data) {
                this._hasBonus = true;
                this._bonusResponse = data;
                this._totalWin = data.totalWin;
                this._coins = data.coins || this._coins;
                if (data.multiplier) {
                    this._multiplier = data.multiplier;
                }
                this._freespins = data.freespins || this._freespins;
            };
            GameModel.prototype.onBalanceChanged = function (data) {
                this._coins = data;
            };
            /**
             *
             * @param value
             */
            GameModel.prototype.auto = function (value) {
                if (value != undefined) {
                    this._isAutoPlay = value;
                }
                else {
                    this._isAutoPlay = !this._isAutoPlay;
                }
                if (this._isAutoPlay && (this.currentState.name == common.StatesConstants.IDLE_STATE || this.currentState.name == common.StatesConstants.STOP_SHOW_WINNING_STATE)) {
                    if (this.currentState.name == common.StatesConstants.IDLE_STATE && this.coins < this.totalBet && this.freespins == 0) {
                        console.log('');
                    }
                    else {
                        this.currentState.finishWork();
                    }
                }
                this.autoChangedSignal.dispatch(this._isAutoPlay);
            };
            /** Once received. Get server game state information before running the Game State Mananger
              * Must be initiated by instance of GameController
              */
            GameModel.prototype.onGetGameState = function (data) {
                this._freespins = data.freespins;
                if (this._freespins > 0) {
                    this._isFreespins = true;
                }
                else {
                    this._isFreespins = false;
                }
                this._initialBets = data.bets.concat();
                this._allowedBets = data.bets;
                this._allowedLines = data.lines;
                this._hasBonus = data.hasBonus;
                this._currBetIndex = this._allowedBets.indexOf(data.initialBet);
                this._currLinesIndex = this._allowedLines.indexOf(data.initialLines);
                this._coins = data.coins;
                this._currentStateIndex = -1;
                this._denomination = data.denomination || 1;
                /*this.allowedBets.sort(function(a, b) {
                    return a - b;
                });
    
                if(this._currBetIndex == -1) {
                    this._currBetIndex = 0;
                }*/
                if (this._currLinesIndex == -1) {
                    this._currLinesIndex = 0;
                }
                this.correctBets(data.initialBet);
                if (data.spinRespince) {
                    this.onSpinResponse(data.spinRespince);
                }
                if (data.bonusRespince) {
                    this.onBonusGameResponse(data.bonusRespince);
                }
                if (data.freespinResponse) {
                    this.onSpinResponse(data.freespinResponse);
                    this._isFreespins = this._freespins > 0;
                }
                this.bindingStatesSignals();
                this.nextState(true);
                this.setBetIndex(this.currBetIndex);
                this.setLinesIndex(this.currLinesIndex);
            };
            GameModel.prototype.correctBets = function (bet) {
                this._allowedBets = null;
                this._allowedBets = this._initialBets.concat();
                if (this._allowedBets.indexOf(bet) == -1) {
                    this._allowedBets.push(bet);
                }
                this.allowedBets.sort(function (a, b) {
                    return a - b;
                });
                this._currBetIndex = this._allowedBets.indexOf(bet);
                console.log(this._currBetIndex);
            };
            /**
             *
             * @param {number} bet
             */
            GameModel.prototype.setBet = function (bet) {
                var betIndex = this._allowedBets.indexOf(bet);
                if (betIndex != -1) {
                    this.setBetIndex(betIndex);
                }
            };
            /**
             * Change next allowed bet
             */
            GameModel.prototype.nextBet = function () {
                var betIndex = this._currBetIndex;
                if (betIndex + 1 < this._allowedBets.length) {
                    betIndex++;
                }
                else {
                    betIndex = 0;
                }
                this.setBetIndex(betIndex);
            };
            /**
             * Change previous allowed bet
             */
            GameModel.prototype.prevBet = function () {
                var betIndex = this._currBetIndex;
                if (betIndex - 1 >= 0) {
                    betIndex--;
                }
                else {
                    betIndex = this._allowedBets.length - 1;
                }
                this.setBetIndex(betIndex);
            };
            /**
             *
             * @param index
             */
            GameModel.prototype.setBetIndex = function (index) {
                this._currBetIndex = index;
                this.betChangedSignal.dispatch(this._allowedBets[this.currBetIndex] * this._denomination);
            };
            /**
             *
             * @param {number} lines
             */
            GameModel.prototype.setLines = function (lines) {
                var lineIndex = this._allowedLines.indexOf(lines);
                if (lineIndex != -1) {
                    this.setLinesIndex(lineIndex);
                }
            };
            /**
             * Change next allowed lines
             */
            GameModel.prototype.nextLine = function () {
                var lineIndex = this._currLinesIndex;
                if (lineIndex + 1 < this._allowedLines.length) {
                    lineIndex++;
                }
                else {
                    lineIndex = 0;
                }
                this.setLinesIndex(lineIndex);
            };
            /**
             * Change previous allowed lines
             */
            GameModel.prototype.prevLine = function () {
                var lineIndex = this._currLinesIndex;
                if (lineIndex - 1 >= 0) {
                    lineIndex--;
                }
                else {
                    lineIndex = this._allowedLines.length - 1;
                }
                this.setLinesIndex(lineIndex);
            };
            /**
             *
             * @param index
             */
            GameModel.prototype.setLinesIndex = function (index) {
                this._currLinesIndex = index;
                this.linesChangedSignal.dispatch(this._allowedLines[this.currLinesIndex]);
            };
            /**
             *
             * @param {boolean} changeLines
             * @param {boolean} changeBets
             */
            GameModel.prototype.maxBet = function (changeLines, changeBets) {
                if (changeLines === void 0) { changeLines = true; }
                if (changeBets === void 0) { changeBets = true; }
                var lines = 0;
                var bet = 0;
                for (var i = this.allowedLines.length - 1; i >= (changeLines ? 0 : this.allowedLines.length - 1); i--) {
                    lines = this.allowedLines[i];
                    for (var j = this.allowedBets.length - 1; j >= (changeBets ? 0 : this.allowedLines.length - 1); j--) {
                        bet = this.allowedBets[j];
                        if (lines * bet <= this.coins) {
                            this.setLinesIndex(i);
                            this.setBetIndex(j);
                            return;
                        }
                    }
                }
            };
            Object.defineProperty(GameModel.prototype, "bonusResponse", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._bonusResponse;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "spinResponse", {
                get: function () {
                    return this._spinResponse;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "cashBackCoins", {
                get: function () {
                    return this._cashBackCoins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "hasBonus", {
                get: function () {
                    return this._hasBonus;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "isGamble", {
                get: function () {
                    return this._isGamble;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "isAutoPlay", {
                get: function () {
                    return this._isAutoPlay;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "isFreespins", {
                get: function () {
                    return this._isFreespins;
                },
                set: function (value) {
                    this._isFreespins = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "totalWin", {
                get: function () {
                    return this._totalWin;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "bonusTotalWin", {
                get: function () {
                    return this._bonusTotalWin;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "win", {
                get: function () {
                    return this._win;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "spinsNumber", {
                get: function () {
                    return this._spinsNumber;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "currBetIndex", {
                get: function () {
                    return this._currBetIndex;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "currLinesIndex", {
                get: function () {
                    return this._currLinesIndex;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "allowedBets", {
                get: function () {
                    return this._allowedBets;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "allowedLines", {
                get: function () {
                    return this._allowedLines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "currResultIndex", {
                get: function () {
                    return this._currResultIndex;
                },
                set: function (value) {
                    this._currResultIndex = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "results", {
                get: function () {
                    if (this._spinResponse && this._spinResponse.results) {
                        return this._spinResponse.results;
                    }
                    return null;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "result", {
                get: function () {
                    if (this.results && this._currResultIndex < this.results.length) {
                        return this.results[this._currResultIndex];
                    }
                    return null;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "freespins", {
                get: function () {
                    return this._freespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "freeSpinsAdded", {
                get: function () {
                    return this._freeSpinsAdded;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "freeSpinsTotal", {
                get: function () {
                    return this._freeSpinsTotal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "coins", {
                get: function () {
                    return this._coins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "activeLines", {
                get: function () {
                    return this._activeLines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "totalLines", {
                get: function () {
                    return this._totalLines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "betChangedSignal", {
                get: function () {
                    return this._betChangedSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "linesChangedSignal", {
                get: function () {
                    return this._linesChangedSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "autoChangedSignal", {
                get: function () {
                    return this._autoChangedSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "totalBet", {
                get: function () {
                    return this._allowedBets[this.currBetIndex] * this._allowedLines[this.currLinesIndex] * this._denomination;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "betPerLine", {
                get: function () {
                    return this._allowedBets[this.currBetIndex] * this._denomination;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "denomination", {
                get: function () {
                    return this._denomination;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "multiplier", {
                get: function () {
                    return this._multiplier;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameModel.prototype, "rawData", {
                get: function () {
                    return this._rawData;
                },
                enumerable: true,
                configurable: true
            });
            return GameModel;
        }(common.BaseModel));
        common.GameModel = GameModel;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
//  Base abstract State created in the aspect of the concept "State Manager" 
//  and designed for Model (MVC);
//  Inhereted class should be incapsuled state of the game where could be exists
//  all conditions to make a decision to turning on/off according state. 
//  Example of possible states: Spin State, Collect State and so on.
var TypeScriptPhaserSlot;
//  Base abstract State created in the aspect of the concept "State Manager" 
//  and designed for Model (MVC);
//  Inhereted class should be incapsuled state of the game where could be exists
//  all conditions to make a decision to turning on/off according state. 
//  Example of possible states: Spin State, Collect State and so on.
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseState = /** @class */ (function () {
            /**
             *
             * @param {TypeScriptPhaserSlot.common.IGameModel} model
             * @param {TypeScriptPhaserSlot.common.StateConfiguration} stateConfiguration
             * @param {boolean} debugMode
             */
            function BaseState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                this._model = model;
                this._gotoStateName = stateConfiguration.gotoStateName;
                this._isRequiredAction = stateConfiguration.isRequiredAction;
                this._isAction = stateConfiguration.isAction;
                this.debugMode = debugMode;
                this._cancelSignal = new Phaser.Signal();
                this._startWorkSignal = new Phaser.Signal();
                this._finishWorkSignal = new Phaser.Signal();
            }
            /**
             *
             * @param data
             * @param {boolean} isRequiredAction
             */
            BaseState.prototype.doWork = function (data, isRequiredAction) {
                this.debuglog(" - do work. is RequiredAction: " + isRequiredAction);
                this.startWorkSignal.dispatch(this, data, isRequiredAction);
            };
            /**
             * Called when work is done or canceled conditions
             * However we can do a check before finishing work by checking some data (optional)
             */
            BaseState.prototype.finishWork = function () {
                this.debuglog("- finish work");
                this.finishWorkSignal.dispatch(true);
            };
            /**
             *
             */
            BaseState.prototype.cancelState = function () {
                this.debuglog("- CANCELED");
                this.cancelSignal.dispatch(false);
            };
            /**
             *
             * @param data: All Data could be checked to get continious
             */
            BaseState.prototype.tryWork = function () {
                var resultData = this.checkData();
                if (resultData != undefined && resultData != null && resultData != false) {
                    this.debuglog("- conditions PASSED");
                }
                else {
                    this.debuglog("- conditions FAIL");
                }
                if (!this.isAction) {
                    this.debuglog("- NO action");
                }
                if (resultData != undefined && resultData != null && resultData != false) {
                    if (!this.isAction) {
                        this.finishWork();
                    }
                    else {
                        this.doWork(resultData, this.isRequiredAction);
                    }
                }
                else {
                    this.cancelState();
                }
            };
            /**
             * Checks all conditions to make a dicision is current state is completely done
             * @param data
             */
            BaseState.prototype.checkData = function (data) {
                //overriden class instance should return TRUE or FALSE depending on checking conditions 
                return true;
            };
            /**
             * Check a condition to use GotoState
             * @returns
             */
            BaseState.prototype.checkConditionForGotoState = function () {
                //overriden class instance should return TRUE or FALSE depending on checking conditions 
                return false;
            };
            /**
             * Specific console logger
             * @param message
             */
            BaseState.prototype.debuglog = function (message) {
                if (this.debugMode)
                    console.log(this.name, message);
            };
            Object.defineProperty(BaseState.prototype, "gotoStateName", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._gotoStateName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseState.prototype, "name", {
                get: function () {
                    return this._name;
                },
                set: function (name) {
                    this._name = name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseState.prototype, "model", {
                get: function () {
                    return this._model;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseState.prototype, "isAction", {
                get: function () {
                    return this._isAction;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseState.prototype, "isRequiredAction", {
                get: function () {
                    return this._isRequiredAction;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseState.prototype, "debugMode", {
                get: function () {
                    return this._debugMode;
                },
                set: function (value) {
                    this._debugMode = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseState.prototype, "cancelSignal", {
                get: function () {
                    return this._cancelSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseState.prototype, "finishWorkSignal", {
                get: function () {
                    return this._finishWorkSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseState.prototype, "startWorkSignal", {
                get: function () {
                    return this._startWorkSignal;
                },
                enumerable: true,
                configurable: true
            });
            return BaseState;
        }());
        common.BaseState = BaseState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var AddFreespinsState = /** @class */ (function (_super) {
            __extends(AddFreespinsState, _super);
            function AddFreespinsState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.ADD_FREESPINS_STATE;
                return _this;
            }
            AddFreespinsState.prototype.checkData = function () {
                if (this.model.freeSpinsAdded > 0 && this.model.isFreespins) {
                    return true;
                }
                return null;
            };
            return AddFreespinsState;
        }(common.BaseState));
        common.AddFreespinsState = AddFreespinsState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var CheckBalanceState = /** @class */ (function (_super) {
            __extends(CheckBalanceState, _super);
            function CheckBalanceState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.CHECK_BALANCE_STATE;
                return _this;
            }
            CheckBalanceState.prototype.checkData = function () {
                if (this.model.coins >= this.model.totalBet || this.model.isFreespins) {
                    this.model.notEnoughMoney = false;
                    return null;
                }
                this.model.notEnoughMoney = true;
                return true;
            };
            return CheckBalanceState;
        }(common.BaseState));
        common.CheckBalanceState = CheckBalanceState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var CollectState = /** @class */ (function (_super) {
            __extends(CollectState, _super);
            function CollectState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.COLLECT_STATE;
                return _this;
            }
            CollectState.prototype.checkData = function () {
                if (this.model.totalWin > 0) {
                    return true;
                }
                return null;
            };
            return CollectState;
        }(common.BaseState));
        common.CollectState = CollectState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var EmotionsState = /** @class */ (function (_super) {
            __extends(EmotionsState, _super);
            function EmotionsState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.EMOTIONS_STATE;
                return _this;
            }
            EmotionsState.prototype.checkData = function () {
                if (this.model.win > 0) {
                    return this.model.spinResponse.results[this.model.currResultIndex];
                }
                return null;
            };
            return EmotionsState;
        }(common.BaseState));
        common.EmotionsState = EmotionsState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="./BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var FinishBonusGameState = /** @class */ (function (_super) {
            __extends(FinishBonusGameState, _super);
            function FinishBonusGameState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.FINISH_BONUS_GAME_STATE;
                return _this;
            }
            FinishBonusGameState.prototype.checkData = function () {
                if (this.model.hasBonus && this.model.bonusResponse) {
                    return true;
                }
                return null;
            };
            return FinishBonusGameState;
        }(common.BaseState));
        common.FinishBonusGameState = FinishBonusGameState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var FinishFreespinsState = /** @class */ (function (_super) {
            __extends(FinishFreespinsState, _super);
            function FinishFreespinsState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.FINISH_FREESPINS_STATE;
                return _this;
            }
            FinishFreespinsState.prototype.checkData = function () {
                if (this.model.freespins == 0 && this.model.isFreespins) {
                    this.model.isFreespins = false;
                    return true;
                }
                else
                    return null;
            };
            return FinishFreespinsState;
        }(common.BaseState));
        common.FinishFreespinsState = FinishFreespinsState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GambleState = /** @class */ (function (_super) {
            __extends(GambleState, _super);
            function GambleState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.GAMBLE_STATE;
                return _this;
            }
            GambleState.prototype.checkData = function () {
                if (this.model.isGamble) {
                    return true;
                }
                return null;
            };
            return GambleState;
        }(common.BaseState));
        common.GambleState = GambleState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GetCollectResponseState = /** @class */ (function (_super) {
            __extends(GetCollectResponseState, _super);
            function GetCollectResponseState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.GET_COLLECT_RESPONSE_STATE;
                return _this;
            }
            GetCollectResponseState.prototype.checkData = function () {
                if (this.model.totalWin > 0) {
                    this.debuglog("totalWin " + this.model.totalWin);
                    return true;
                }
            };
            return GetCollectResponseState;
        }(common.BaseState));
        common.GetCollectResponseState = GetCollectResponseState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GetGambleResponseState = /** @class */ (function (_super) {
            __extends(GetGambleResponseState, _super);
            function GetGambleResponseState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.GET_GAMBLE_RESPONSE_STATE;
                return _this;
            }
            GetGambleResponseState.prototype.checkData = function () {
                return this.model.isGamble;
            };
            return GetGambleResponseState;
        }(common.BaseState));
        common.GetGambleResponseState = GetGambleResponseState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="./BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GetRespinResponseState = /** @class */ (function (_super) {
            __extends(GetRespinResponseState, _super);
            function GetRespinResponseState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.GET_RESPIN_RESPONSE_STATE;
                return _this;
            }
            GetRespinResponseState.prototype.checkData = function () {
                _super.prototype.checkData.call(this);
                return this.model.currentState.name == common.StatesConstants.START_RESPIN_STATE;
            };
            return GetRespinResponseState;
        }(common.BaseState));
        common.GetRespinResponseState = GetRespinResponseState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="./BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GetSpinResponseState = /** @class */ (function (_super) {
            __extends(GetSpinResponseState, _super);
            function GetSpinResponseState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.GET_SPIN_RESPONSE_STATE;
                return _this;
            }
            GetSpinResponseState.prototype.checkData = function () {
                _super.prototype.checkData.call(this);
                var data = {
                    bet: this.model.allowedBets[this.model.currBetIndex] * this.model.denomination,
                    lines: this.model.allowedLines[this.model.currLinesIndex],
                    denomination: this.model.denomination
                };
                return data;
            };
            return GetSpinResponseState;
        }(common.BaseState));
        common.GetSpinResponseState = GetSpinResponseState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var IdleState = /** @class */ (function (_super) {
            __extends(IdleState, _super);
            function IdleState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.first = false;
                _this.name = common.StatesConstants.IDLE_STATE;
                return _this;
            }
            IdleState.prototype.checkData = function () {
                return true;
            };
            IdleState.prototype.doWork = function (data, isRequiredAction) {
                _super.prototype.doWork.call(this, data, isRequiredAction);
                if (this.first) {
                    return;
                }
                this.first = true;
                if (this._model.result && (this._model.result.paylines.length > 0 || this._model.result.scatters.length > 0)) {
                    this._model.goToState(common.StatesConstants.START_SHOW_WINNING_STATE);
                }
            };
            return IdleState;
        }(common.BaseState));
        common.IdleState = IdleState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="./BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var PreSpinState = /** @class */ (function (_super) {
            __extends(PreSpinState, _super);
            function PreSpinState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.PRESPIN_STATE;
                return _this;
            }
            PreSpinState.prototype.checkData = function () {
                if (true) {
                    return true;
                }
            };
            return PreSpinState;
        }(common.BaseState));
        common.PreSpinState = PreSpinState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="./BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var StartBonusGameState = /** @class */ (function (_super) {
            __extends(StartBonusGameState, _super);
            function StartBonusGameState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.START_BONUS_GAME_STATE;
                return _this;
            }
            StartBonusGameState.prototype.checkData = function () {
                if (this.model.hasBonus /* && !this.model.bonusResponse*/) {
                    return this.model.bonusResponse || {};
                }
                return null;
            };
            return StartBonusGameState;
        }(common.BaseState));
        common.StartBonusGameState = StartBonusGameState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var StartFreespinsState = /** @class */ (function (_super) {
            __extends(StartFreespinsState, _super);
            function StartFreespinsState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.START_FREESPINS_STATE;
                return _this;
            }
            StartFreespinsState.prototype.checkData = function () {
                if (this.model.freespins > 0 && !this.model.isFreespins) {
                    this.model.isFreespins = true;
                    return true;
                }
                return null;
            };
            return StartFreespinsState;
        }(common.BaseState));
        common.StartFreespinsState = StartFreespinsState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var StartRespinState = /** @class */ (function (_super) {
            __extends(StartRespinState, _super);
            function StartRespinState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.START_RESPIN_STATE;
                return _this;
            }
            StartRespinState.prototype.checkData = function () {
                /*if (this.model.totalLines > 0) {
                    return this.model.spinResponse.results[this.model.currResultIndex];
                }*/
                return this.model.totalWin > 0;
            };
            StartRespinState.prototype.checkConditionForGotoState = function () {
                return true;
            };
            return StartRespinState;
        }(common.BaseState));
        common.StartRespinState = StartRespinState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var StartShowWinningState = /** @class */ (function (_super) {
            __extends(StartShowWinningState, _super);
            function StartShowWinningState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.START_SHOW_WINNING_STATE;
                return _this;
            }
            StartShowWinningState.prototype.checkData = function () {
                //if (this.model.win > 0) {
                if (this.model.spinResponse.results[this.model.currResultIndex].paylines.length > 0 || this.model.spinResponse.results[this.model.currResultIndex].scatters.length > 0) {
                    return this.model.spinResponse.results[this.model.currResultIndex];
                }
                return null;
            };
            return StartShowWinningState;
        }(common.BaseState));
        common.StartShowWinningState = StartShowWinningState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="./BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var StartSpinState = /** @class */ (function (_super) {
            __extends(StartSpinState, _super);
            function StartSpinState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.START_SPIN_STATE;
                return _this;
            }
            StartSpinState.prototype.checkData = function () {
                return true;
            };
            return StartSpinState;
        }(common.BaseState));
        common.StartSpinState = StartSpinState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var StatesConstants = /** @class */ (function () {
            function StatesConstants() {
            }
            StatesConstants.IDLE_STATE = "IDLE_STATE";
            StatesConstants.PRESPIN_STATE = "PRESPIN_STATE";
            StatesConstants.GET_SPIN_RESPONSE_STATE = "GET_SPIN_RESPONSE_STATE";
            StatesConstants.START_SPIN_STATE = "START_SPIN_STATE";
            StatesConstants.SPIN_STATE = "SPIN_STATE";
            StatesConstants.GET_RESPIN_RESPONSE_STATE = "GET_RESPIN_RESPONSE_STATE";
            StatesConstants.START_RESPIN_STATE = "START_RESPIN_STATE";
            StatesConstants.STOP_RESPIN_STATE = "STOP_RESPIN_STATE";
            StatesConstants.STOP_SPIN_STATE = "STOP_SPIN_STATE";
            StatesConstants.START_FREESPINS_STATE = "START_FREESPINS_STATE";
            StatesConstants.FINISH_FREESPINS_STATE = "FINISH_FREESPINS_STATE";
            StatesConstants.ADD_FREESPINS_STATE = "ADD_FREESPINS_STATE";
            StatesConstants.GET_COLLECT_RESPONSE_STATE = "GET_COLLECT_RESPONSE_STATE";
            StatesConstants.COLLECT_STATE = "COLLECT_STATE";
            StatesConstants.START_SHOW_WINNING_STATE = "START_SHOW_WINNING_STATE";
            StatesConstants.STOP_SHOW_WINNING_STATE = "STOP_SHOW_WINNING_STATE";
            StatesConstants.GAMBLE_STATE = "GAMBLE_STATE";
            StatesConstants.GET_GAMBLE_RESPONSE_STATE = "GET_GAMBLE_RESPONSE_STATE";
            StatesConstants.CHECK_BALANCE_STATE = "CHECK_BALANCE_STATE";
            StatesConstants.EMOTIONS_STATE = "EMOTIONS_STATE";
            StatesConstants.PRINCESS_FROG_TRANSFORM_PRINCESS_STATE = "PRINCESS_FROG_TRANSFORM_PRINCESS_STATE";
            StatesConstants.REMOVE_SYMBOLS_STATE = "REMOVE_SYMBOLS_STATE";
            StatesConstants.RESPIN_STATE = "RESPIN_STATE";
            StatesConstants.START_BONUS_GAME_STATE = "START_BONUS_GAME_STATE";
            StatesConstants.SHOW_WINNING_BONUS_GAME_STATE = "SHOW_WINNING_BONUS_GAME_STATE";
            StatesConstants.IDLE_BONUS_GAME_STATE = "IDLE_BONUS_GAME_STATE";
            StatesConstants.FINISH_BONUS_GAME_STATE = "FINISH_BONUS_GAME_STATE";
            return StatesConstants;
        }());
        common.StatesConstants = StatesConstants;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var StopRespinState = /** @class */ (function (_super) {
            __extends(StopRespinState, _super);
            function StopRespinState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.STOP_RESPIN_STATE;
                return _this;
            }
            StopRespinState.prototype.checkData = function () {
                //return this.model.spinResponse.results[this.model.currResultIndex];
                if (this.model.currentState.name == common.StatesConstants.GET_RESPIN_RESPONSE_STATE) {
                    return this.model.spinResponse.results[this.model.currResultIndex];
                }
                return null;
            };
            StopRespinState.prototype.checkConditionForGotoState = function () {
                return true;
                /*if (this.model.spinResponse.results[this.model.currResultIndex].paylines.length > 0) {
                    return true;
                } else {
                    return false;
                }*/
            };
            return StopRespinState;
        }(common.BaseState));
        common.StopRespinState = StopRespinState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var StopShowWinningState = /** @class */ (function (_super) {
            __extends(StopShowWinningState, _super);
            function StopShowWinningState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.STOP_SHOW_WINNING_STATE;
                return _this;
            }
            StopShowWinningState.prototype.checkData = function () {
                if (this.model.result.paylines.length > 0 || this.model.result.scatters.length > 0) {
                    return this.model.result.payout;
                }
                return null;
                /*if (this.model.totalWin > 0) {
                    return this.model.result.payout;
                } else {
                    return null;
                }*/
            };
            return StopShowWinningState;
        }(common.BaseState));
        common.StopShowWinningState = StopShowWinningState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="./BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var StopSpinState = /** @class */ (function (_super) {
            __extends(StopSpinState, _super);
            function StopSpinState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.STOP_SPIN_STATE;
                return _this;
            }
            StopSpinState.prototype.checkData = function () {
                return this.model.spinResponse.results[this.model.currResultIndex];
            };
            return StopSpinState;
        }(common.BaseState));
        common.StopSpinState = StopSpinState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../controller/BaseController.ts" />
var TypeScriptPhaserSlot;
///<reference path="../controller/BaseController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ApplicationProxy = /** @class */ (function (_super) {
            __extends(ApplicationProxy, _super);
            function ApplicationProxy(controller, name, debugMode) {
                var _this = _super.call(this, name, debugMode) || this;
                _this._spinResponseSignal = new Phaser.Signal();
                _this._getGameStateSignal = new Phaser.Signal();
                _this._bonusGameResponseSignal = new Phaser.Signal();
                _this.apiController = controller;
                _this.bindToAPIController();
                return _this;
            }
            /**
             * APIcontroller requests
             */
            /**
             * Initial information to start the particular game
             * @data? any
             */
            ApplicationProxy.prototype.getGameState = function (gameName) {
                this.apiController.getGameState(gameName);
            };
            ApplicationProxy.prototype.onGameState = function (data) {
                this.debuglog('onGameState' + data);
                this.getGameStateSignal.dispatch(data);
            };
            /**
             * Spin request to apicontroller
             * @param data
             */
            ApplicationProxy.prototype.spin = function (data) {
                this.apiController.spin(data);
            };
            ApplicationProxy.prototype.respin = function () {
                this.apiController.respin();
            };
            /**
             * Freespin request to apicontroller
             * @param coins: any actual information which goes to server like:
             *  (coins, points)
             */
            ApplicationProxy.prototype.freespin = function (coins) {
                this.apiController.freespin(coins);
            };
            ApplicationProxy.prototype.onSpin = function (data) {
                this.debuglog('onSpin');
                this._spinResponseSignal.dispatch(data);
            };
            /**
             * After getting any winning sends the request to collect up the current winning
             * @param data
             */
            ApplicationProxy.prototype.collect = function (data) {
                this.apiController.collect(data);
            };
            ApplicationProxy.prototype.onCollect = function (data) {
                this.debuglog('onCollect');
                this._spinResponseSignal.dispatch(data);
            };
            /**
            * Sends player choice information
            * @param data
            */
            ApplicationProxy.prototype.gamble = function (data) {
                this.apiController.collect(data);
            };
            ApplicationProxy.prototype.onGamble = function (data) {
                this.debuglog('onGamble');
                this._spinResponseSignal.dispatch(data);
            };
            /**
             * Sends bonus game information
             * @param data
             */
            ApplicationProxy.prototype.bonus = function (data) {
                this.apiController.bonus(data);
            };
            ApplicationProxy.prototype.onBonus = function (data) {
                this.debuglog('onBonus');
                this._bonusGameResponseSignal.dispatch(data);
            };
            /**
             * get exit
             * @param data
             */
            ApplicationProxy.prototype.exit = function (data) {
                this.apiController.collect(data);
            };
            ApplicationProxy.prototype.onExit = function (data) {
                this.debuglog('onExit');
                this._spinResponseSignal.dispatch(data);
            };
            ApplicationProxy.prototype.bindToAPIController = function () {
                this.apiController.bonusGame_signal.add(this.onBonus, this);
                this.apiController.gameState_signal.add(this.onGameState, this);
                this.apiController.spin_signal.add(this.onSpin, this);
                this.apiController.collect_signal.add(this.onCollect, this);
                this.apiController.gamble_signal.add(this.onGamble, this);
                this.apiController.exit_signal.add(this.onExit, this);
            };
            Object.defineProperty(ApplicationProxy.prototype, "spinResponseSignal", {
                /************************************************************
                   Getters and Setters
               *************************************************************/
                get: function () {
                    return this._spinResponseSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ApplicationProxy.prototype, "bonusGameResponseSignal", {
                get: function () {
                    return this._bonusGameResponseSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ApplicationProxy.prototype, "getGameStateSignal", {
                get: function () {
                    return this._getGameStateSignal;
                },
                enumerable: true,
                configurable: true
            });
            return ApplicationProxy;
        }(common.BaseController));
        common.ApplicationProxy = ApplicationProxy;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BonusResponse = /** @class */ (function () {
            function BonusResponse(rawData) {
                this.parse(rawData);
            }
            BonusResponse.prototype.parse = function (rawData) {
                this._data = rawData;
                this._hasBonus = rawData.state == 1 || rawData.state == 2;
                this._totalWin = rawData.winnings ? rawData.winnings.total : rawData.winning;
                this._coins = rawData.balance;
                this._multiplier = 1;
                if (rawData.bonuses_summary) {
                    this._freespins = rawData.bonuses_summary.freespins_remainder || 0;
                }
                if (rawData.additional_info) {
                    this._multiplier = rawData.additional_info.multiplier || 1;
                }
                return this;
            };
            Object.defineProperty(BonusResponse.prototype, "data", {
                get: function () {
                    return this._data;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BonusResponse.prototype, "hasBonus", {
                get: function () {
                    return this._hasBonus;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BonusResponse.prototype, "freespins", {
                get: function () {
                    return this._freespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BonusResponse.prototype, "totalWin", {
                get: function () {
                    return this._totalWin;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BonusResponse.prototype, "coins", {
                get: function () {
                    return this._coins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BonusResponse.prototype, "multiplier", {
                get: function () {
                    return this._multiplier;
                },
                enumerable: true,
                configurable: true
            });
            return BonusResponse;
        }());
        common.BonusResponse = BonusResponse;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BoongoGameStateResponse = /** @class */ (function () {
            function BoongoGameStateResponse(rawData) {
                this.parse(rawData);
            }
            BoongoGameStateResponse.prototype.parse = function (rawData) {
                var souce = rawData.context.spins || rawData.context.freespins;
                this._initialBet = souce.bet_per_line;
                this._initialLines = souce.lines;
                this._coins = rawData.user.balance;
                this._denomination = rawData.user.denominator;
                this._reels = souce.board;
                //this._freespins = rawData.response.state.freespins;
                //this._points = rawData.response.state.points;
                //this._fsTotal = rawData.response.state.fsTotal;
                //this._fsMult = rawData.response.state.fsMult;
                //this._bonusType = rawData.response.state.bonusType;
                //this._bonusGame = rawData.response.state.bonusGame;
                //this._giftspins = rawData.response.state.giftspins;
                this._bets = rawData.settings.bets;
                this._lines = /*rawData.settings.lines*/ [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
                //this._lineCount = rawData.response.lineCount;
                //this._lineCountFreespins = rawData.response.lineCountFreespins;
                //this._paylines = rawData.response.paylineMap;
                //this._paylinesFreespins = rawData.response.paylineMapFreespins;
                //this._paytable = rawData.response.paytable;
                this._userCoints = rawData.user.balance;
                if (souce) {
                    this._reels = souce.board;
                    this._spinRespince = new common.BoongoSpinResponse(rawData);
                    this._freespins = 0;
                    if (rawData.context.freespins) {
                        this._freespins = rawData.context.freespins.rounds_left;
                    }
                }
                return this;
            };
            Object.defineProperty(BoongoGameStateResponse.prototype, "status", {
                get: function () {
                    return this._status;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "initialBet", {
                get: function () {
                    return this._initialBet;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "initialLines", {
                get: function () {
                    return this._initialLines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "coins", {
                get: function () {
                    return this._coins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "line", {
                get: function () {
                    return this._line;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "bet", {
                get: function () {
                    return this._bet;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "hasBonus", {
                get: function () {
                    return this._hasBonus;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "bStage", {
                get: function () {
                    return this._bStage;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "freespins", {
                get: function () {
                    return this._freespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "points", {
                get: function () {
                    return this._points;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "fsTotal", {
                get: function () {
                    return this._fsTotal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "fsMult", {
                get: function () {
                    return this._fsMult;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "bonusType", {
                get: function () {
                    return this.bonusType;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "bonusGame", {
                get: function () {
                    return this._bonusGame;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "giftspins", {
                get: function () {
                    return this._giftspins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "jackpot0", {
                get: function () {
                    return this._jackpot0;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "jackpot1", {
                get: function () {
                    return this._jackpot1;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "betsPreselect", {
                get: function () {
                    return this._betsPreselect;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "lineCount", {
                get: function () {
                    return this._lineCount;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "lineCountFreespins", {
                get: function () {
                    return this._lineCountFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "bets", {
                get: function () {
                    return this._bets;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "lines", {
                get: function () {
                    return this._lines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "reels", {
                get: function () {
                    return this._reels;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "paytable", {
                get: function () {
                    return this._paytable;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "userCoints", {
                get: function () {
                    return this._userCoints;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "paylines", {
                get: function () {
                    return this._paylines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "paylinesFreespins", {
                get: function () {
                    return this._paylinesFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "reelMap", {
                get: function () {
                    return this._reelMap;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "reelMapFreespins", {
                get: function () {
                    return this._reelMapFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "multipliers", {
                get: function () {
                    return this._multipliers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "multipliersFreespins", {
                get: function () {
                    return this._multipliersFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "commands", {
                get: function () {
                    return this._commands;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "serverInfo", {
                //public get response(): any {
                //    return this._response;
                //}
                get: function () {
                    return this._serverInfo;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "spinRespince", {
                get: function () {
                    return this._spinRespince;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoGameStateResponse.prototype, "denomination", {
                get: function () {
                    return this._denomination;
                },
                enumerable: true,
                configurable: true
            });
            return BoongoGameStateResponse;
        }());
        common.BoongoGameStateResponse = BoongoGameStateResponse;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BoongoSpinResponse = /** @class */ (function () {
            function BoongoSpinResponse(rawData) {
                this.parse(rawData);
            }
            BoongoSpinResponse.prototype.parse = function (rawData) {
                //this._status = rawData.status;
                //this._commands = rawData.commands;
                //this._jackpot = new Jackpot(rawData.response.jackpot);
                this._rawData = rawData;
                var source = rawData.context.spins || rawData.context.freespins;
                this._results = [];
                this._results.push(new common.BoongoSpinResult(rawData));
                this._win = source.round_win;
                this._totalWin = source.round_win;
                var fakeStateData = {};
                //fakeStateData.freespins = rawData.bonuses[0].amount || 0;
                //fakeStateData.freespins = rawData.context.freespins || 0;
                fakeStateData.freespins = 0;
                if (rawData.context.freespins) {
                    fakeStateData.freespins = rawData.context.freespins.rounds_left;
                }
                else if (source.winscatters[0]) {
                    fakeStateData.freespins = source.winscatters[0].freespins;
                }
                fakeStateData.points = 0;
                fakeStateData.hasBonus = false;
                fakeStateData.coins = rawData.user.balance;
                fakeStateData.total = source.total_win || source.round_win;
                this._state = new common.State(fakeStateData);
                //this._user = new User(rawData.response.user);
                //this._serverInfo = new ServerInfo(rawData.serverInfo);
                this._betPerLine = source.bet_per_line;
                return this;
                /*this._status = rawData.status;
                this._commands = rawData.commands;
                this._jackpot = new Jackpot(rawData.response.jackpot);
                
                this._results = [];
                for (let i: number = 0; i < rawData.response.result.length; i++) {
                    this._results.push(new SpinResult(rawData.response.result[i]));
                }
                this._win = rawData.response.win;
                this._totalWin = rawData.response.totalWin;
                this._state = new State(rawData.response.state);
                this._user = new User(rawData.response.user);
                this._serverInfo = new ServerInfo(rawData.serverInfo);
                return this;*/
            };
            Object.defineProperty(BoongoSpinResponse.prototype, "status", {
                get: function () {
                    return this._status;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoSpinResponse.prototype, "commands", {
                get: function () {
                    return this._commands;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoSpinResponse.prototype, "jackpot", {
                get: function () {
                    return this._jackpot;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoSpinResponse.prototype, "results", {
                get: function () {
                    return this._results;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoSpinResponse.prototype, "win", {
                get: function () {
                    return this._win;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoSpinResponse.prototype, "totalWin", {
                get: function () {
                    return this._totalWin;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoSpinResponse.prototype, "state", {
                get: function () {
                    return this._state;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoSpinResponse.prototype, "user", {
                get: function () {
                    return this._user;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoSpinResponse.prototype, "serverInfo", {
                get: function () {
                    return this._serverInfo;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoSpinResponse.prototype, "rawData", {
                get: function () {
                    return this._rawData;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BoongoSpinResponse.prototype, "betPerLine", {
                get: function () {
                    return this._betPerLine;
                },
                enumerable: true,
                configurable: true
            });
            return BoongoSpinResponse;
        }());
        common.BoongoSpinResponse = BoongoSpinResponse;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var GameStateResponse = /** @class */ (function () {
            function GameStateResponse(rawData) {
                this.parse(rawData);
            }
            GameStateResponse.prototype.parse = function (rawData) {
                this._status = rawData.status;
                this._initialBet = rawData.response.initialBet;
                this._coins = rawData.response.user.coins;
                this._lines = rawData.response.state.lines;
                this._hasBonus = rawData.response.state.hasBonus;
                this._bStage = rawData.response.state.bStage;
                this._reels = rawData.response.state.reels;
                this._freespins = rawData.response.state.freespins;
                this._points = rawData.response.state.points;
                this._fsTotal = rawData.response.state.fsTotal;
                this._fsMult = rawData.response.state.fsMult;
                this._bonusType = rawData.response.state.bonusType;
                this._bonusGame = rawData.response.state.bonusGame;
                this._giftspins = rawData.response.state.giftspins;
                this._jackpot0 = rawData.response.state.jackpot0;
                this._jackpot1 = rawData.response.state.jackpot1;
                this._bets = rawData.response.bets;
                this._betsPreselect = rawData.response.betsPreselect;
                this._lineCount = rawData.response.lineCount;
                this._lineCountFreespins = rawData.response.lineCountFreespins;
                this._paylines = rawData.response.paylineMap;
                this._paylinesFreespins = rawData.response.paylineMapFreespins;
                this._paytable = rawData.response.paytable;
                this._userCoints = rawData.response.user.coins;
                this._reelMap = rawData.response.reelMap;
                this._reelMapFreespins = rawData.response.reelMapFreespins;
                this._multipliers = rawData.response.multipliers;
                this._multipliersFreespins = rawData.response.multipliersFreespins;
                this._commands = rawData.commands;
                this._serverInfo = rawData.serverInfo;
                return this;
            };
            Object.defineProperty(GameStateResponse.prototype, "status", {
                get: function () {
                    return this._status;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "initialBet", {
                get: function () {
                    return this._initialBet;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "initialLines", {
                get: function () {
                    return this._initialLines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "coins", {
                get: function () {
                    return this._coins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "hasBonus", {
                get: function () {
                    return this._hasBonus;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "bStage", {
                get: function () {
                    return this._bStage;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "freespins", {
                get: function () {
                    return this._freespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "points", {
                get: function () {
                    return this._points;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "fsTotal", {
                get: function () {
                    return this._fsTotal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "fsMult", {
                get: function () {
                    return this._fsMult;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "bonusType", {
                get: function () {
                    return this.bonusType;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "bonusGame", {
                get: function () {
                    return this._bonusGame;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "giftspins", {
                get: function () {
                    return this._giftspins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "jackpot0", {
                get: function () {
                    return this._jackpot0;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "jackpot1", {
                get: function () {
                    return this._jackpot1;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "betsPreselect", {
                get: function () {
                    return this._betsPreselect;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "lineCount", {
                get: function () {
                    return this._lineCount;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "lineCountFreespins", {
                get: function () {
                    return this._lineCountFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "bets", {
                get: function () {
                    return this._bets;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "lines", {
                get: function () {
                    return this._lines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "reels", {
                get: function () {
                    return this._reels;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "paytable", {
                get: function () {
                    return this._paytable;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "userCoints", {
                get: function () {
                    return this._userCoints;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "paylines", {
                get: function () {
                    return this._paylines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "paylinesFreespins", {
                get: function () {
                    return this._paylinesFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "reelMap", {
                get: function () {
                    return this._reelMap;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "reelMapFreespins", {
                get: function () {
                    return this._reelMapFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "multipliers", {
                get: function () {
                    return this._multipliers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "multipliersFreespins", {
                get: function () {
                    return this._multipliersFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "commands", {
                get: function () {
                    return this._commands;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "serverInfo", {
                //public get response(): any {
                //    return this._response;
                //}
                get: function () {
                    return this._serverInfo;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "spinRespince", {
                get: function () {
                    return this._spinRespince;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "freespinResponse", {
                get: function () {
                    return this._freespinResponse;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "bonusRespince", {
                get: function () {
                    return this._bonusRespince;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameStateResponse.prototype, "denomination", {
                get: function () {
                    return this._denomination;
                },
                enumerable: true,
                configurable: true
            });
            return GameStateResponse;
        }());
        common.GameStateResponse = GameStateResponse;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var SpinResponse = /** @class */ (function () {
            function SpinResponse(rawData) {
                this.parse(rawData);
            }
            SpinResponse.prototype.parse = function (rawData) {
                this._status = rawData.status;
                this._commands = rawData.commands;
                this._jackpot = new Jackpot(rawData.response.jackpot);
                this._results = [];
                for (var i = 0; i < rawData.response.result.length; i++) {
                    this._results.push(new common.SpinResult(rawData.response.result[i]));
                }
                this._win = rawData.response.win;
                this._totalWin = rawData.response.totalWin;
                this._multiplier = 1;
                this._state = new State(rawData.response.state);
                this._user = new User(rawData.response.user);
                this._serverInfo = new ServerInfo(rawData.serverInfo);
                this._rawData = rawData;
                return this;
            };
            Object.defineProperty(SpinResponse.prototype, "status", {
                get: function () {
                    return this._status;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "commands", {
                get: function () {
                    return this._commands;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "jackpot", {
                get: function () {
                    return this._jackpot;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "results", {
                get: function () {
                    return this._results;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "win", {
                get: function () {
                    return this._win;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "totalWin", {
                get: function () {
                    return this._totalWin;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "multiplier", {
                get: function () {
                    return this._multiplier;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "state", {
                get: function () {
                    return this._state;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "user", {
                get: function () {
                    return this._user;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "serverInfo", {
                get: function () {
                    return this._serverInfo;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "rawData", {
                get: function () {
                    return this._rawData;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResponse.prototype, "bonusResponce", {
                get: function () {
                    return this._bonusResponce;
                },
                enumerable: true,
                configurable: true
            });
            return SpinResponse;
        }());
        common.SpinResponse = SpinResponse;
        var User = /** @class */ (function () {
            function User(rawData) {
                this.parse(rawData);
            }
            User.prototype.parse = function (rawData) {
                this._coins = rawData.coins;
                this._points = rawData.points;
                this._actCS = rawData.actCS;
                this._pWinSpins = rawData.pWinSpins;
                return this;
            };
            Object.defineProperty(User.prototype, "coins", {
                get: function () {
                    return this._coins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(User.prototype, "points", {
                get: function () {
                    return this._points;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(User.prototype, "actCS", {
                get: function () {
                    return this._actCS;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(User.prototype, "pWinSpins", {
                get: function () {
                    return this._pWinSpins;
                },
                enumerable: true,
                configurable: true
            });
            return User;
        }());
        common.User = User;
        var State = /** @class */ (function () {
            function State(rawData) {
                this.parse(rawData);
            }
            State.prototype.parse = function (rawData) {
                console.log(rawData);
                this._freespins = rawData.freespins;
                this._bonusTotalWin = rawData.total;
                this._points = rawData.points;
                this._hasBonus = rawData.hasBonus;
                this._coins = rawData.coins;
                return this;
            };
            Object.defineProperty(State.prototype, "coins", {
                get: function () {
                    return this._coins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(State.prototype, "freespins", {
                get: function () {
                    return this._freespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(State.prototype, "bonusTotalWin", {
                get: function () {
                    return this._bonusTotalWin;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(State.prototype, "points", {
                get: function () {
                    return this._points;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(State.prototype, "hasBonus", {
                get: function () {
                    return this._hasBonus;
                },
                enumerable: true,
                configurable: true
            });
            return State;
        }());
        common.State = State;
        var ServerInfo = /** @class */ (function () {
            function ServerInfo(rawData) {
                this.parse(rawData);
            }
            ServerInfo.prototype.parse = function (rawData) {
                this._time = rawData.time;
                return this;
            };
            Object.defineProperty(ServerInfo.prototype, "time", {
                get: function () {
                    return this._time;
                },
                enumerable: true,
                configurable: true
            });
            return ServerInfo;
        }());
        common.ServerInfo = ServerInfo;
        var Jackpot = /** @class */ (function () {
            function Jackpot(rawData) {
                this.parse(rawData);
            }
            Jackpot.prototype.parse = function (rawData) {
                this._winners = [];
                for (var i = 0; i < rawData.winners.length; i++) {
                    this._winners.push(new Winner(rawData.winners[i]));
                }
                return this;
            };
            Object.defineProperty(Jackpot.prototype, "winners", {
                get: function () {
                    return this._winners;
                },
                enumerable: true,
                configurable: true
            });
            return Jackpot;
        }());
        common.Jackpot = Jackpot;
        var Bonus = /** @class */ (function () {
            function Bonus(rawData) {
                this.parse(rawData);
            }
            Bonus.prototype.parse = function (rawData) {
                return this;
            };
            return Bonus;
        }());
        common.Bonus = Bonus;
        var Winner = /** @class */ (function () {
            function Winner(rawData) {
                this.parse(rawData);
            }
            Winner.prototype.parse = function (rawData) {
                this._id = rawData.id;
                this._type = rawData.type;
                this._slotId = rawData.slotId;
                this._userId = rawData.userId;
                this._winAmount = rawData.winAmount;
                this._name = rawData.name;
                return this;
            };
            Object.defineProperty(Winner.prototype, "id", {
                get: function () {
                    return this._id;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Winner.prototype, "type", {
                get: function () {
                    return this._type;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Winner.prototype, "slotId", {
                get: function () {
                    return this._slotId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Winner.prototype, "userId", {
                get: function () {
                    return this._userId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Winner.prototype, "winAmount", {
                get: function () {
                    return this._winAmount;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Winner.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            return Winner;
        }());
        common.Winner = Winner;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var SpinResult = /** @class */ (function () {
            function SpinResult(rawData) {
                this.parse(rawData);
            }
            SpinResult.prototype.parse = function (rawData) {
                this._freespin_icons = rawData.freespin_icons;
                this._bet = rawData.bet;
                this._payout = rawData.payout;
                this._reels = rawData.reels;
                this._mult = rawData.mult;
                this._paylines = [];
                this._rawData = rawData;
                for (var i = 0; i < rawData.paylines.length; i++) {
                    this._paylines.push(new Payline(rawData.paylines[i]));
                }
                return this;
            };
            Object.defineProperty(SpinResult.prototype, "freespin_icons", {
                get: function () {
                    return this._freespin_icons;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResult.prototype, "bet", {
                get: function () {
                    return this._bet;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResult.prototype, "payout", {
                get: function () {
                    return this._payout;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResult.prototype, "paylines", {
                get: function () {
                    return this._paylines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResult.prototype, "scatters", {
                get: function () {
                    return this._scatters;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResult.prototype, "reels", {
                get: function () {
                    return this._reels;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResult.prototype, "mult", {
                get: function () {
                    return this._mult;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SpinResult.prototype, "rawData", {
                get: function () {
                    return this._rawData;
                },
                enumerable: true,
                configurable: true
            });
            return SpinResult;
        }());
        common.SpinResult = SpinResult;
        var Payline = /** @class */ (function () {
            function Payline(rawData) {
                this.parse(rawData);
            }
            Payline.prototype.parse = function (rawData) {
                this._index = rawData.index;
                this._offset = rawData.offset;
                this._direction = rawData.direction;
                this._line = rawData.line;
                this._icon = rawData.icon;
                this._icon_count = rawData.icon_count;
                this._coin_multiplier = rawData.coin_multiplier;
                this._sum = 0;
                return this;
            };
            Object.defineProperty(Payline.prototype, "index", {
                get: function () {
                    return this._index;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Payline.prototype, "offset", {
                get: function () {
                    return this._offset;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Payline.prototype, "direction", {
                get: function () {
                    return this._direction;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Payline.prototype, "line", {
                get: function () {
                    return this._line;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Payline.prototype, "icon", {
                get: function () {
                    return this._icon;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Payline.prototype, "icon_count", {
                get: function () {
                    return this._icon_count;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Payline.prototype, "coin_multiplier", {
                get: function () {
                    return this._coin_multiplier;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Payline.prototype, "sum", {
                get: function () {
                    return this._sum;
                },
                enumerable: true,
                configurable: true
            });
            return Payline;
        }());
        common.Payline = Payline;
        var Scatter = /** @class */ (function () {
            function Scatter(rawData) {
                this.parse(rawData);
            }
            Scatter.prototype.parse = function (rawData) {
                this._icon = rawData.icon || 1;
                this._icon_count = rawData.icon_count;
                this._coin_multiplier = rawData.coin_multiplier;
                this._indexes = rawData.indexes;
                this._sum = rawData.sum;
                return this;
            };
            Object.defineProperty(Scatter.prototype, "icon", {
                get: function () {
                    return this._icon;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Scatter.prototype, "icon_count", {
                get: function () {
                    return this._icon_count;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Scatter.prototype, "coin_multiplier", {
                get: function () {
                    return this._coin_multiplier;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Scatter.prototype, "indexes", {
                get: function () {
                    return this._indexes;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Scatter.prototype, "sum", {
                get: function () {
                    return this._sum;
                },
                enumerable: true,
                configurable: true
            });
            return Scatter;
        }());
        common.Scatter = Scatter;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./SpinResult.ts" />
var TypeScriptPhaserSlot;
///<reference path="./SpinResult.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BoongoSpinResult = /** @class */ (function (_super) {
            __extends(BoongoSpinResult, _super);
            function BoongoSpinResult() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            BoongoSpinResult.prototype.parse = function (rawData) {
                //this._freespin_icons = rawData.freespin_icons;
                //this._bet = rawData.bet;
                var souce = rawData.context.spins || rawData.context.freespins;
                this._payout = souce.round_win;
                this._reels = souce.board;
                //this._mult = rawData.mult;
                this._paylines = [];
                this._scatters = [];
                for (var i = 0; i < souce.winlines.length; i++) {
                    var winItem = souce.winlines[i];
                    this._paylines.push(new BoongoPayline(winItem));
                }
                for (var j = 0; j < souce.winscatters.length; j++) {
                    var winItem = souce.winscatters[j];
                    this._scatters.push(new BoongoScatter(winItem));
                    /*let scatters:any = souce.winscatters[0];
                    for (let k: number = 0; k < scatters.occurrences; k++) {
                        let obj: any = {
                            icon: 11,
                            icon_count: 3,
                            coin_multiplier: 1,
                            indexes: [[0, 1], [1, 1], [2, 1],]
                        };
                        this._scatters.push(new Scatter(obj));
                    }*/
                }
                /*this._freespin_icons = rawData.freespin_icons;
                this._bet = rawData.bet;
                this._payout = rawData.payout;
                this._reels = rawData.reels;
                this._mult = rawData.mult;
                this._paylines = [];
                for (let i: number = 0; i < rawData.paylines.length; i++) {
                    this._paylines.push(new Payline(rawData.paylines[i]));
                }*/
                return this;
            };
            return BoongoSpinResult;
        }(common.SpinResult));
        common.BoongoSpinResult = BoongoSpinResult;
        var BoongoPayline = /** @class */ (function (_super) {
            __extends(BoongoPayline, _super);
            function BoongoPayline() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            BoongoPayline.prototype.parse = function (rawData) {
                this._index = rawData.line - 1;
                this._offset = 0;
                this._direction = "left";
                this._line = [];
                for (var i = 0; i < 5; i++) {
                    var dataForPush = 0;
                    if (rawData.positions[i]) {
                        dataForPush = rawData.positions[i][1];
                    }
                    this._line.push(dataForPush);
                }
                this._icon = rawData.symbol;
                this._icon_count = rawData.occurrences;
                this._coin_multiplier = rawData.multiplier;
                this._sum = rawData.amount;
                return this;
            };
            return BoongoPayline;
        }(common.Payline));
        common.BoongoPayline = BoongoPayline;
        var BoongoScatter = /** @class */ (function (_super) {
            __extends(BoongoScatter, _super);
            function BoongoScatter() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            BoongoScatter.prototype.parse = function (rawData) {
                this._icon = rawData.symbol;
                this._icon_count = rawData.occurrences;
                this._coin_multiplier = rawData.coin_multiplier;
                this._indexes = rawData.positions;
                return this;
            };
            return BoongoScatter;
        }(common.Scatter));
        common.BoongoScatter = BoongoScatter;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var LocaleText = /** @class */ (function (_super) {
            __extends(LocaleText, _super);
            function LocaleText(game, x, y, text, style, identifier) {
                var _this = _super.call(this, game, x, y, text, style) || this;
                _this.identifier = identifier;
                return _this;
            }
            return LocaleText;
        }(Phaser.Text));
        common.LocaleText = LocaleText;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
// SHA-256 (+ HMAC and PBKDF2) for JavaScript.
//
// Written in 2014-2016 by Dmitry Chestnykh.
// Public domain, no warranty.
//
// Functions (accept and return Uint8Arrays):
//
//   sha256(message) -> hash
//   sha256.hmac(key, message) -> mac
//   sha256.pbkdf2(password, salt, rounds, dkLen) -> dk
//
//  Classes:
//
//   new sha256.Hash()
//   new sha256.HMAC(key)
//
var TypeScriptPhaserSlot;
// SHA-256 (+ HMAC and PBKDF2) for JavaScript.
//
// Written in 2014-2016 by Dmitry Chestnykh.
// Public domain, no warranty.
//
// Functions (accept and return Uint8Arrays):
//
//   sha256(message) -> hash
//   sha256.hmac(key, message) -> mac
//   sha256.pbkdf2(password, salt, rounds, dkLen) -> dk
//
//  Classes:
//
//   new sha256.Hash()
//   new sha256.HMAC(key)
//
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        common.digestLength = 32;
        common.blockSize = 64;
        // SHA-256 constants
        var K = new Uint32Array([
            0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b,
            0x59f111f1, 0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01,
            0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7,
            0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
            0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152,
            0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
            0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc,
            0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
            0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819,
            0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116, 0x1e376c08,
            0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f,
            0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
            0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
        ]);
        function hashBlocks(w, v, p, pos, len) {
            var a, b, c, d, e, f, g, h, u, i, j, t1, t2;
            while (len >= 64) {
                a = v[0];
                b = v[1];
                c = v[2];
                d = v[3];
                e = v[4];
                f = v[5];
                g = v[6];
                h = v[7];
                for (i = 0; i < 16; i++) {
                    j = pos + i * 4;
                    w[i] = (((p[j] & 0xff) << 24) | ((p[j + 1] & 0xff) << 16) |
                        ((p[j + 2] & 0xff) << 8) | (p[j + 3] & 0xff));
                }
                for (i = 16; i < 64; i++) {
                    u = w[i - 2];
                    t1 = (u >>> 17 | u << (32 - 17)) ^ (u >>> 19 | u << (32 - 19)) ^ (u >>> 10);
                    u = w[i - 15];
                    t2 = (u >>> 7 | u << (32 - 7)) ^ (u >>> 18 | u << (32 - 18)) ^ (u >>> 3);
                    w[i] = (t1 + w[i - 7] | 0) + (t2 + w[i - 16] | 0);
                }
                for (i = 0; i < 64; i++) {
                    t1 = (((((e >>> 6 | e << (32 - 6)) ^ (e >>> 11 | e << (32 - 11)) ^
                        (e >>> 25 | e << (32 - 25))) + ((e & f) ^ (~e & g))) | 0) +
                        ((h + ((K[i] + w[i]) | 0)) | 0)) | 0;
                    t2 = (((a >>> 2 | a << (32 - 2)) ^ (a >>> 13 | a << (32 - 13)) ^
                        (a >>> 22 | a << (32 - 22))) + ((a & b) ^ (a & c) ^ (b & c))) | 0;
                    h = g;
                    g = f;
                    f = e;
                    e = (d + t1) | 0;
                    d = c;
                    c = b;
                    b = a;
                    a = (t1 + t2) | 0;
                }
                v[0] += a;
                v[1] += b;
                v[2] += c;
                v[3] += d;
                v[4] += e;
                v[5] += f;
                v[6] += g;
                v[7] += h;
                pos += 64;
                len -= 64;
            }
            return pos;
        }
        // Hash implements SHA256 hash algorithm.
        var Hash = /** @class */ (function () {
            function Hash() {
                this.digestLength = common.digestLength;
                this.blockSize = common.blockSize;
                // Note: Int32Array is used instead of Uint32Array for performance reasons.
                this.state = new Int32Array(8); // hash state
                this.temp = new Int32Array(64); // temporary state
                this.buffer = new Uint8Array(128); // buffer for data to hash
                this.bufferLength = 0; // number of bytes in buffer
                this.bytesHashed = 0; // number of total bytes hashed
                this.finished = false; // indicates whether the hash was finalized
                this.reset();
            }
            // Resets hash state making it possible
            // to re-use this instance to hash other data.
            Hash.prototype.reset = function () {
                this.state[0] = 0x6a09e667;
                this.state[1] = 0xbb67ae85;
                this.state[2] = 0x3c6ef372;
                this.state[3] = 0xa54ff53a;
                this.state[4] = 0x510e527f;
                this.state[5] = 0x9b05688c;
                this.state[6] = 0x1f83d9ab;
                this.state[7] = 0x5be0cd19;
                this.bufferLength = 0;
                this.bytesHashed = 0;
                this.finished = false;
                return this;
            };
            // Cleans internal buffers and re-initializes hash state.
            Hash.prototype.clean = function () {
                for (var i = 0; i < this.buffer.length; i++) {
                    this.buffer[i] = 0;
                }
                for (var i = 0; i < this.temp.length; i++) {
                    this.temp[i] = 0;
                }
                this.reset();
            };
            // Updates hash state with the given data.
            //
            // Optionally, length of the data can be specified to hash
            // fewer bytes than data.length.
            //
            // Throws error when trying to update already finalized hash:
            // instance must be reset to use it again.
            Hash.prototype.update = function (data, dataLength) {
                if (dataLength === void 0) { dataLength = data.length; }
                if (this.finished) {
                    throw new Error("SHA256: can't update because hash was finished.");
                }
                var dataPos = 0;
                this.bytesHashed += dataLength;
                if (this.bufferLength > 0) {
                    while (this.bufferLength < 64 && dataLength > 0) {
                        this.buffer[this.bufferLength++] = data[dataPos++];
                        dataLength--;
                    }
                    if (this.bufferLength === 64) {
                        hashBlocks(this.temp, this.state, this.buffer, 0, 64);
                        this.bufferLength = 0;
                    }
                }
                if (dataLength >= 64) {
                    dataPos = hashBlocks(this.temp, this.state, data, dataPos, dataLength);
                    dataLength %= 64;
                }
                while (dataLength > 0) {
                    this.buffer[this.bufferLength++] = data[dataPos++];
                    dataLength--;
                }
                return this;
            };
            // Finalizes hash state and puts hash into out.
            //
            // If hash was already finalized, puts the same value.
            Hash.prototype.finish = function (out) {
                if (!this.finished) {
                    var bytesHashed = this.bytesHashed;
                    var left = this.bufferLength;
                    var bitLenHi = (bytesHashed / 0x20000000) | 0;
                    var bitLenLo = bytesHashed << 3;
                    var padLength = (bytesHashed % 64 < 56) ? 64 : 128;
                    this.buffer[left] = 0x80;
                    for (var i = left + 1; i < padLength - 8; i++) {
                        this.buffer[i] = 0;
                    }
                    this.buffer[padLength - 8] = (bitLenHi >>> 24) & 0xff;
                    this.buffer[padLength - 7] = (bitLenHi >>> 16) & 0xff;
                    this.buffer[padLength - 6] = (bitLenHi >>> 8) & 0xff;
                    this.buffer[padLength - 5] = (bitLenHi >>> 0) & 0xff;
                    this.buffer[padLength - 4] = (bitLenLo >>> 24) & 0xff;
                    this.buffer[padLength - 3] = (bitLenLo >>> 16) & 0xff;
                    this.buffer[padLength - 2] = (bitLenLo >>> 8) & 0xff;
                    this.buffer[padLength - 1] = (bitLenLo >>> 0) & 0xff;
                    hashBlocks(this.temp, this.state, this.buffer, 0, padLength);
                    this.finished = true;
                }
                for (var i = 0; i < 8; i++) {
                    out[i * 4 + 0] = (this.state[i] >>> 24) & 0xff;
                    out[i * 4 + 1] = (this.state[i] >>> 16) & 0xff;
                    out[i * 4 + 2] = (this.state[i] >>> 8) & 0xff;
                    out[i * 4 + 3] = (this.state[i] >>> 0) & 0xff;
                }
                return this;
            };
            // Returns the final hash digest.
            Hash.prototype.digest = function () {
                var out = new Uint8Array(this.digestLength);
                this.finish(out);
                return out;
            };
            // Internal function for use in HMAC for optimization.
            Hash.prototype._saveState = function (out) {
                for (var i = 0; i < this.state.length; i++) {
                    out[i] = this.state[i];
                }
            };
            // Internal function for use in HMAC for optimization.
            Hash.prototype._restoreState = function (from, bytesHashed) {
                for (var i = 0; i < this.state.length; i++) {
                    this.state[i] = from[i];
                }
                this.bytesHashed = bytesHashed;
                this.finished = false;
                this.bufferLength = 0;
            };
            return Hash;
        }());
        common.Hash = Hash;
        // HMAC implements HMAC-SHA256 message authentication algorithm.
        var HMAC = /** @class */ (function () {
            function HMAC(key) {
                this.inner = new Hash();
                this.outer = new Hash();
                this.blockSize = this.inner.blockSize;
                this.digestLength = this.inner.digestLength;
                var pad = new Uint8Array(this.blockSize);
                if (key.length > this.blockSize) {
                    (new Hash()).update(key).finish(pad).clean();
                }
                else {
                    for (var i = 0; i < key.length; i++) {
                        pad[i] = key[i];
                    }
                }
                for (var i = 0; i < pad.length; i++) {
                    pad[i] ^= 0x36;
                }
                this.inner.update(pad);
                for (var i = 0; i < pad.length; i++) {
                    pad[i] ^= 0x36 ^ 0x5c;
                }
                this.outer.update(pad);
                this.istate = new Uint32Array(8);
                this.ostate = new Uint32Array(8);
                this.inner._saveState(this.istate);
                this.outer._saveState(this.ostate);
                for (var i = 0; i < pad.length; i++) {
                    pad[i] = 0;
                }
            }
            // Returns HMAC state to the state initialized with key
            // to make it possible to run HMAC over the other data with the same
            // key without creating a new instance.
            HMAC.prototype.reset = function () {
                this.inner._restoreState(this.istate, this.inner.blockSize);
                this.outer._restoreState(this.ostate, this.outer.blockSize);
                return this;
            };
            // Cleans HMAC state.
            HMAC.prototype.clean = function () {
                for (var i = 0; i < this.istate.length; i++) {
                    this.ostate[i] = this.istate[i] = 0;
                }
                this.inner.clean();
                this.outer.clean();
            };
            // Updates state with provided data.
            HMAC.prototype.update = function (data) {
                this.inner.update(data);
                return this;
            };
            // Finalizes HMAC and puts the result in out.
            HMAC.prototype.finish = function (out) {
                if (this.outer.finished) {
                    this.outer.finish(out);
                }
                else {
                    this.inner.finish(out);
                    this.outer.update(out, this.digestLength).finish(out);
                }
                return this;
            };
            // Returns message authentication code.
            HMAC.prototype.digest = function () {
                var out = new Uint8Array(this.digestLength);
                this.finish(out);
                return out;
            };
            return HMAC;
        }());
        common.HMAC = HMAC;
        // Returns SHA256 hash of data.
        function hash(data) {
            var h = (new Hash()).update(data);
            var digest = h.digest();
            h.clean();
            return digest;
        }
        common.hash = hash;
        // Function hash is both available as module.hash and as default export.
        //export default hash;
        // Returns HMAC-SHA256 of data under the key.
        function hmac(key, data) {
            var h = (new HMAC(key)).update(data);
            var digest = h.digest();
            h.clean();
            return digest;
        }
        common.hmac = hmac;
        // Derives a key from password and salt using PBKDF2-HMAC-SHA256
        // with the given number of iterations.
        //
        // The number of bytes returned is equal to dkLen.
        //
        // (For better security, avoid dkLen greater than hash length - 32 bytes).
        function pbkdf2(password, salt, iterations, dkLen) {
            var prf = new HMAC(password);
            var len = prf.digestLength;
            var ctr = new Uint8Array(4);
            var t = new Uint8Array(len);
            var u = new Uint8Array(len);
            var dk = new Uint8Array(dkLen);
            for (var i = 0; i * len < dkLen; i++) {
                var c = i + 1;
                ctr[0] = (c >>> 24) & 0xff;
                ctr[1] = (c >>> 16) & 0xff;
                ctr[2] = (c >>> 8) & 0xff;
                ctr[3] = (c >>> 0) & 0xff;
                prf.reset();
                prf.update(salt);
                prf.update(ctr);
                prf.finish(u);
                for (var j = 0; j < len; j++) {
                    t[j] = u[j];
                }
                for (var j = 2; j <= iterations; j++) {
                    prf.reset();
                    prf.update(u).finish(u);
                    for (var k = 0; k < len; k++) {
                        t[k] ^= u[k];
                    }
                }
                for (var j = 0; j < len && i * len + j < dkLen; j++) {
                    dk[i * len + j] = t[j];
                }
            }
            for (var i = 0; i < len; i++) {
                t[i] = u[i] = 0;
            }
            for (var i = 0; i < 4; i++) {
                ctr[i] = 0;
            }
            prf.clean();
            return dk;
        }
        common.pbkdf2 = pbkdf2;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var TextFactory = /** @class */ (function () {
            function TextFactory(game, styles, strings, initLocale) {
                this.game = game;
                this.styles = styles;
                this.strings = strings;
                this.localeTextArray = [];
                this._currLocale = initLocale;
                this._actionSignal = new Phaser.Signal();
            }
            TextFactory.prototype.getStyleByName = function (name) {
                for (var i = 0; i < this.styles.length; i++) {
                    if (this.styles[i].name == name) {
                        return this.styles[i];
                    }
                }
                return null;
            };
            /**
             *
             * @param {string} identifier
             * @param {string | TextStyle} style?
             * @returns
             */
            TextFactory.prototype.getTextWithStyle = function (identifier, style) {
                var ts;
                var text;
                if (style) {
                    ts = (typeof style === "string") ? this.getStyleByName(style) : style;
                }
                else {
                    var sn = this.strings[identifier] ? this.strings[identifier][this._currLocale]["styleName"] : "defaultStyle";
                    ts = this.getStyleByName(sn);
                }
                if (typeof identifier === "string") {
                    text = this.strings[identifier] ? this.strings[identifier][this._currLocale].text : "";
                }
                if (typeof identifier === "number") {
                    text = String(identifier);
                }
                var pt = new common.LocaleText(this.game, 0, 0, text, ts.style, identifier);
                pt.name = ts.name;
                pt.shadowFill = ts.shadowFill;
                pt.shadowStroke = ts.shadowStroke;
                pt.strokeColors = ts.strokeColors;
                if (ts.gradientFromColour != "" && ts.gradientToColour != "") {
                    var grd = pt.context.createLinearGradient(0, 0, 0, pt.canvas.height);
                    grd.addColorStop(0, ts.gradientFromColour);
                    grd.addColorStop(1, ts.gradientToColour);
                    pt.fill = grd;
                }
                this.localeTextArray.push(pt);
                return pt;
            };
            TextFactory.prototype.getStringByIdentifier = function (identifier) {
                var text = '';
                if (this.strings[identifier]) {
                    if (this.strings[identifier][this._currLocale]) {
                        text = this.strings[identifier][this._currLocale].text;
                    }
                    else {
                        text = 'NO LOCALE';
                    }
                }
                //let text: string = this.strings[identifier] ? this.strings[identifier][this._currLocale].text : '';
                return text;
            };
            TextFactory.prototype.changeLocaleAndRefresh = function (locale) {
                this._currLocale = locale;
                for (var i = 0; i < this.localeTextArray.length; i++) {
                    this.localeTextArray[i].text = this.getStringByIdentifier(this.localeTextArray[i].identifier);
                }
                this._actionSignal.dispatch({ action: 'changeLocale', data: this._currLocale });
            };
            Object.defineProperty(TextFactory.prototype, "currLocale", {
                get: function () {
                    return this._currLocale;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TextFactory.prototype, "actionSignal", {
                get: function () {
                    return this._actionSignal;
                },
                enumerable: true,
                configurable: true
            });
            return TextFactory;
        }());
        common.TextFactory = TextFactory;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var TextStyle = /** @class */ (function () {
            function TextStyle(rawData) {
                this._style = new Object();
                this._style.font = rawData.font;
                this._style.fill = rawData.fill;
                this._style.align = rawData.align;
                this._style.stroke = rawData.stroke;
                this._style.strokeThickness = rawData.strokeThickness;
                this._style.wordWrap = rawData.wordWrap;
                this._style.wordWrapWidth = rawData.wordWrapWidth;
                this._style.maxLines = rawData.maxLines;
                this._style.shadowOffsetX = rawData.shadowOffsetX;
                this._style.shadowOffsetY = rawData.shadowOffsetY;
                this._style.shadowColor = rawData.shadowColor;
                this._style.shadowBlur = rawData.shadowBlur;
                this._style.tab = rawData.tab;
                this._style.tabs = rawData.tabs;
                this._style.fontSize = rawData.fontSize;
                this._style.fontStyle = rawData.fontStyle;
                this._style.fontVariant = rawData.fontVariant;
                this._style.fontWeight = rawData.fontWeight;
                this._style.backgroundColor = rawData.backgroundColor;
                this._style.boundsAlignH = rawData.boundsAlignH;
                this._style.boundsAlignV = rawData.boundsAlignV;
                this._name = rawData.styleName;
                this._gradientFromColour = rawData.gradientFromColour;
                this._gradientToColour = rawData.gradientToColour;
                this._shadowFill = rawData.shadowFill;
                this._shadowStroke = rawData.shadowStroke;
                this._strokeColors = rawData.strokeColors;
                this._tint = rawData.tint;
            }
            Object.defineProperty(TextStyle.prototype, "style", {
                get: function () {
                    return this._style;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TextStyle.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TextStyle.prototype, "gradientFromColour", {
                get: function () {
                    return this._gradientFromColour;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TextStyle.prototype, "gradientToColour", {
                get: function () {
                    return this._gradientToColour;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TextStyle.prototype, "shadowFill", {
                get: function () {
                    return this._shadowFill;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TextStyle.prototype, "shadowStroke", {
                get: function () {
                    return this._shadowStroke;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TextStyle.prototype, "strokeColors", {
                get: function () {
                    return this._strokeColors;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TextStyle.prototype, "tint", {
                get: function () {
                    return this._tint;
                },
                enumerable: true,
                configurable: true
            });
            return TextStyle;
        }());
        common.TextStyle = TextStyle;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var AnimatedSprite = /** @class */ (function (_super) {
            __extends(AnimatedSprite, _super);
            function AnimatedSprite(game, x, y, config, debugMode) {
                var _this = this;
                var configuration;
                if (typeof config == 'string') {
                    configuration = (function () {
                        var newConfig;
                        var gameConfiguration = game.state.getCurrentState()['gameController'].configuration;
                        for (var i = 0; i < gameConfiguration.animations.length; i++) {
                            var animConfig = gameConfiguration.animations[i];
                            if (animConfig.key == config) {
                                newConfig = animConfig;
                                break;
                            }
                        }
                        return newConfig;
                    })();
                }
                else {
                    configuration = config;
                }
                _this = _super.call(this, game, x, y, configuration.assetKey || configuration.key) || this;
                _this._name = name;
                _this._config = configuration;
                _this._debugMode = debugMode;
                _this.init();
                return _this;
            }
            AnimatedSprite.prototype.getAnimationConfig = function (configName) {
            };
            /**
             *
             */
            AnimatedSprite.prototype.init = function () {
                this._actions = new Phaser.Signal();
                this.getAnimations();
                this.correctParams();
            };
            /**
             *
             */
            AnimatedSprite.prototype.correctParams = function () {
                this.scale.set(this._config.scale.x, this._config.scale.y);
                this.pivot.set(this._config.pivot.x, this._config.pivot.y);
                this.anchor.set(this._config.anchor.x, this._config.anchor.y);
            };
            /**
             *
             */
            AnimatedSprite.prototype.getAnimations = function () {
                var animationData;
                var selectedFrames;
                for (var i = 0; i < this._config.animations.length; i++) {
                    animationData = this._config.animations[i];
                    selectedFrames = [];
                    var animationName = animationData.name;
                    var startFrame = animationData.startFrame;
                    var endFrame = animationData.endFrame;
                    if (startFrame < endFrame) {
                        for (var i_1 = startFrame; i_1 < endFrame + 1; i_1++) {
                            selectedFrames.push(i_1);
                        }
                    }
                    else {
                        for (var i_2 = endFrame; i_2 > startFrame; i_2--) {
                            selectedFrames.push(i_2);
                        }
                    }
                    this.animations.add(animationName, selectedFrames, animationData.frameRate, animationData.isLoop, true);
                    this.events.onAnimationLoop.addOnce(this.onAnimationFirstLoopedCallback, this);
                    this.events.onAnimationLoop.add(this.onAnimationLoopedCallback, this);
                    this.events.onAnimationComplete.add(this.onAnimationCompleteCallback, this, 1, animationData.playOnCompleted);
                }
            };
            /**
             *
             * @param sprite
             * @param animation
             */
            AnimatedSprite.prototype.onAnimationFirstLoopedCallback = function (sprite, animation) {
                this._actions.dispatch(AnimatedSprite.ANIMATION_FIRST_LOOP_COMPLETE);
            };
            /**
             *
             * @param sprite
             * @param animation
             */
            AnimatedSprite.prototype.onAnimationLoopedCallback = function (sprite, animation) {
                this._actions.dispatch(AnimatedSprite.ANIMATION_LOOPED);
            };
            /**
             *
             * @param sprite
             * @param animation
             * @param nextAnimationName
             */
            AnimatedSprite.prototype.onAnimationCompleteCallback = function (sprite, animation, nextAnimationName) {
                if (nextAnimationName) {
                    this._actions.dispatch(AnimatedSprite.ANIMATION_COMPLETE);
                    sprite.animations.play(nextAnimationName);
                }
                else {
                    this._actions.dispatch(AnimatedSprite.ALL_ANIMATIONS_COMPLETE);
                }
            };
            AnimatedSprite.prototype.start = function (animationName) {
                this.animations.play(animationName || 'start');
                this.events.onAnimationLoop.remove(this.onAnimationFirstLoopedCallback, this);
                this.events.onAnimationLoop.addOnce(this.onAnimationFirstLoopedCallback, this);
            };
            AnimatedSprite.prototype.stop = function (resetFrame) {
                if (resetFrame === void 0) { resetFrame = true; }
                this.animations.currentAnim.stop(resetFrame, false);
                this.animations.stop();
            };
            AnimatedSprite.prototype.changeKey = function (key) {
                this.key = key;
                this.loadTexture(key, 0);
            };
            AnimatedSprite.prototype.destroy = function () {
                _super.prototype.destroy.call(this, true);
            };
            Object.defineProperty(AnimatedSprite.prototype, "debugMode", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._debugMode;
                },
                set: function (value) {
                    this._debugMode = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimatedSprite.prototype, "name", {
                get: function () {
                    return this._name;
                },
                set: function (value) {
                    this._name = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimatedSprite.prototype, "config", {
                get: function () {
                    return this._config;
                },
                set: function (value) {
                    this._config = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimatedSprite.prototype, "actionSignal", {
                get: function () {
                    return this._actions;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AnimatedSprite.prototype, "userData", {
                get: function () {
                    return this._userData;
                },
                set: function (value) {
                    this._userData = value;
                },
                enumerable: true,
                configurable: true
            });
            AnimatedSprite.ANIMATION_FIRST_LOOP_COMPLETE = 'animationFirstLoopComplete';
            AnimatedSprite.ANIMATION_LOOPED = 'animationLooped';
            AnimatedSprite.ANIMATION_COMPLETE = 'animationComplete';
            AnimatedSprite.ALL_ANIMATIONS_COMPLETE = 'allAnimationsComplete';
            return AnimatedSprite;
        }(Phaser.Sprite));
        common.AnimatedSprite = AnimatedSprite;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseView = /** @class */ (function (_super) {
            __extends(BaseView, _super);
            function BaseView(game, name, debugMode) {
                var _this = _super.call(this, game, null) || this;
                _this._name = name;
                _this.debugMode = debugMode;
                return _this;
            }
            BaseView.prototype.setPosition = function (x, y) {
                this.x = x;
                this.y = y;
            };
            /**
             * Specific console logger
             * @param message
             */
            BaseView.prototype.debuglog = function (message) {
                console.log(this.name, ":", message);
            };
            Object.defineProperty(BaseView.prototype, "debugMode", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._debugMode;
                },
                set: function (value) {
                    this._debugMode = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseView.prototype, "name", {
                get: function () {
                    return this._name;
                },
                enumerable: true,
                configurable: true
            });
            return BaseView;
        }(Phaser.Group));
        common.BaseView = BaseView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var SimpleSprite = /** @class */ (function (_super) {
            __extends(SimpleSprite, _super);
            function SimpleSprite(game, x, y, key, debugMode, frame) {
                var _this = _super.call(this, game, x, y, key, frame) || this;
                if (typeof frame == 'number') {
                    _this.frame = frame;
                }
                else if (typeof frame == 'string') {
                    _this.frameName = frame;
                }
                _this._name = name;
                _this.debugMode = debugMode;
                _this.init();
                return _this;
            }
            SimpleSprite.prototype.init = function () {
            };
            SimpleSprite.prototype.start = function (data) {
                //start do any work 
            };
            SimpleSprite.prototype.stop = function (data) {
                //stop doing current work
            };
            SimpleSprite.prototype.changeKey = function (key) {
                this.key = key;
                this.loadTexture(key, 0);
            };
            Object.defineProperty(SimpleSprite.prototype, "debugMode", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._debugMode;
                },
                set: function (value) {
                    this._debugMode = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SimpleSprite.prototype, "name", {
                get: function () {
                    return this._name;
                },
                set: function (value) {
                    this._name = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(SimpleSprite.prototype, "userData", {
                get: function () {
                    return this._userData;
                },
                set: function (value) {
                    this._userData = value;
                },
                enumerable: true,
                configurable: true
            });
            return SimpleSprite;
        }(Phaser.Sprite));
        common.SimpleSprite = SimpleSprite;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
/**
 * Created by rocket on 04.07.2017.
 */
var TypeScriptPhaserSlot;
/**
 * Created by rocket on 04.07.2017.
 */
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseEmotion = /** @class */ (function (_super) {
            __extends(BaseEmotion, _super);
            function BaseEmotion(game, config) {
                var _this = _super.call(this, game) || this;
                _this._tweens = [];
                _this.config = config;
                _this._actionSignal = new Phaser.Signal();
                _this.init();
                return _this;
            }
            /**
             * This is an example how to we can init smth :
             */
            BaseEmotion.prototype.init = function () {
                //this.animation = new AnimatedSprite(this.game, 0, 0, this.config.emotion);
                //this.animation.actionSignal.addOnce(this.onAnimationsComplete, this);
                //this.add(this.animation);
            };
            /**
             *
             */
            BaseEmotion.prototype.start = function () {
                this.animate();
            };
            /**
             *
             */
            BaseEmotion.prototype.stop = function () {
                for (var i = 0; i < this._tweens.length; i++) {
                    this._tweens[i].stop(false);
                }
                //this.animation.stop();
                //this.animation.animations.destroy();
                //this.animation.destroy();
                //this.animation = null;
            };
            /**
             *
             */
            BaseEmotion.prototype.animate = function () {
                //this.animation.start();
            };
            /**
             * Just finished all stuff
             */
            BaseEmotion.prototype.onAnimationsComplete = function (data) {
            };
            Object.defineProperty(BaseEmotion.prototype, "actionSignal", {
                /**
                 *
                 * @returns {Phaser.Signal}
                 */
                get: function () {
                    return this._actionSignal;
                },
                enumerable: true,
                configurable: true
            });
            return BaseEmotion;
        }(Phaser.Group));
        common.BaseEmotion = BaseEmotion;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/view/interface/interfaces/IButton.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/view/interface/interfaces/IButton.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseButton = /** @class */ (function (_super) {
            __extends(BaseButton, _super);
            function BaseButton(game, data, x, y, callback, callbackContext, debugMode) {
                var _this = _super.call(this, game, x, y, data.key, null, null, data.overFrame, data.outFrame, data.downFrame, data.upFrame) || this;
                _this._name = data.name;
                _this._action = data.action;
                _this._caption = data.caption;
                _this._textStyleName = data.textStyle;
                _this._callback = callback;
                _this._callbackContext = callbackContext;
                _this._data = data;
                /*this.onDownSound = new Phaser.Sound(this.game, data.onDownSoundKey);
                this.onOverSound = new Phaser.Sound(this.game, data.onOverSoundKey);
                this.onOutSound = new Phaser.Sound(this.game, data.onOutSoundKey);
                this.onUpSound = new Phaser.Sound(this.game, data.onUpSoundKey);*/
                _this._doubleClickTimer = game.time.create(false);
                _this._longDownTimer = game.time.create(false);
                if (_this._caption != "") {
                    _this._label = _this.game.textFactory.getTextWithStyle(_this._caption, _this._textStyleName);
                    _this._label.anchor.set(0.5, 0.5);
                    _this.addChild(_this._label);
                }
                _this.onInputOver.add(_this.onMouseOver, _this);
                _this.onInputOut.add(_this.onMouseOut, _this);
                _this.onInputDown.add(_this.onMouseDown, _this);
                _this.onInputUp.add(_this.onMouseUp, _this);
                _this.debugMode = debugMode;
                return _this;
            }
            BaseButton.prototype.onDoubleTimer = function () {
                this.onButtonEvents(BaseButton.MOUSE_CLICK);
                this._doubleClickTimer.stop();
            };
            BaseButton.prototype.onLongTimer = function () {
                this.onButtonEvents(BaseButton.MOUSE_LONG_DOWN);
                this._longDownTimer.stop();
            };
            /**
             *
             */
            BaseButton.prototype.onMouseOver = function () {
                this.onButtonEvents(BaseButton.MOUSE_OVER);
            };
            /**
             *
             */
            BaseButton.prototype.onMouseOut = function () {
                this.onButtonEvents(BaseButton.MOUSE_OUT);
                this._longDownTimer.stop();
                this._doubleClickTimer.stop();
            };
            /**
             *
             */
            BaseButton.prototype.onMouseDown = function () {
                this.onButtonEvents(BaseButton.MOUSE_DOWN);
                this._longDownTimer.add(BaseButton.LONG_CLICK_DELAY, this.onLongTimer, this);
                this._longDownTimer.start();
            };
            /**
             *
             */
            BaseButton.prototype.onMouseUp = function () {
                this.onButtonEvents(BaseButton.MOUSE_UP);
                this._longDownTimer.stop(false);
                if (this._doubleClickTimer.running) {
                    this.onButtonEvents(BaseButton.MOUSE_DOUBLE_CLICK);
                    this._doubleClickTimer.stop();
                }
                else {
                    this._doubleClickTimer.add(BaseButton.DOUBLE_CLICK_DELAY, this.onDoubleTimer, this);
                    this._doubleClickTimer.start();
                }
            };
            /**
             * Обработка действий кнопки
             * @param $handlerType
             */
            BaseButton.prototype.onButtonEvents = function ($handlerType) {
                if (!this._actions || !this._actions.hasOwnProperty($handlerType)) {
                    return;
                }
                var actionData = this._actions[$handlerType];
                this._action = actionData.action;
                this._callback.call(this._callbackContext, this);
                if (actionData.visibleAfter != undefined) {
                    this.visible = actionData.visibleAfter;
                }
                if (actionData.enabledAfter != undefined) {
                    this.enabled = actionData.enabledAfter;
                }
            };
            /**
             * Handle changing locale e.g. "pl", "en" etc.
             * @param {string} locale
             */
            BaseButton.prototype.onLocaleChanged = function (locale) {
                this._caption = locale[this.action];
                this._label.text = this._caption;
            };
            /**
             * Specific console logger
             * @param message
             */
            BaseButton.prototype.debuglog = function (message) {
                console.log(this.name, ":", message);
            };
            Object.defineProperty(BaseButton.prototype, "debugMode", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._debugMode;
                },
                set: function (value) {
                    this._debugMode = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseButton.prototype, "action", {
                get: function () {
                    return this._action;
                },
                set: function ($value) {
                    this._action = $value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseButton.prototype, "actions", {
                get: function () {
                    return this._actions;
                },
                set: function ($value) {
                    this._actions = $value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseButton.prototype, "enabled", {
                get: function () {
                    return this._enabled;
                },
                set: function ($value) {
                    this._enabled = $value;
                    //this.frame = this._enabled ? this.data.upFrame : this.data.disabledFrame;
                    var newFrame = this._enabled ? this.data.upFrame : this.data.disabledFrame;
                    switch (typeof newFrame) {
                        case 'string':
                            this.frameName = newFrame.toString();
                            break;
                        case 'number':
                            this.frame = newFrame;
                            break;
                    }
                    this.inputEnabled = this._enabled;
                    this.freezeFrames = !this._enabled;
                    //this.input.useHandCursor = this._enabled;
                    //this.buttonMode = this._enabled;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseButton.prototype, "name", {
                get: function () {
                    return this._name;
                },
                set: function ($value) {
                    this._name = $value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseButton.prototype, "data", {
                get: function () {
                    return this._data;
                },
                set: function ($value) {
                    this._data = $value;
                },
                enumerable: true,
                configurable: true
            });
            BaseButton.DOUBLE_CLICK_DELAY = 200;
            BaseButton.LONG_CLICK_DELAY = 1000;
            BaseButton.MOUSE_DOUBLE_CLICK = "mouseDoubleClick";
            BaseButton.MOUSE_LONG_DOWN = "mouseLongDown";
            BaseButton.MOUSE_CLICK = "mouseClick";
            BaseButton.MOUSE_OVER = "mouseOver";
            BaseButton.MOUSE_OUT = "mouseOut";
            BaseButton.MOUSE_DOWN = "mouseDown";
            BaseButton.MOUSE_UP = "mouseUp";
            return BaseButton;
        }(Phaser.Button));
        common.BaseButton = BaseButton;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/view/interface/interfaces/IButton.ts" />
///<reference path="../../../engine/view/interface/BaseButton.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/view/interface/interfaces/IButton.ts" />
///<reference path="../../../engine/view/interface/BaseButton.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var TriggerButton = /** @class */ (function (_super) {
            __extends(TriggerButton, _super);
            function TriggerButton(game, data, x, y, callback, callbackContext, debugMode) {
                var _this = _super.call(this, game, data, x, y, callback, callbackContext, debugMode) || this;
                if (data.trigger) {
                    _this.currentTriggerState = 0;
                    _this.isTrigger = data.trigger;
                    _this.triggerStateFrames = data.triggerStateFrames;
                    _this.freezeFrames = _this.isTrigger;
                    _this.triggerSounds = [];
                    for (var i = 0; i < data.triggerSoundKeys.length; i++) {
                        _this.triggerSounds[i] = new Phaser.Sound(_this.game, data.triggerSoundKeys[i]);
                    }
                }
                return _this;
            }
            /**
             *
             */
            TriggerButton.prototype.playTriggerStateSound = function () {
                if (this.triggerSounds.length > this.currentTriggerState) {
                    this.triggerSounds[this.currentTriggerState].play();
                }
            };
            /**
             *
             */
            TriggerButton.prototype.switchToNextState = function () {
                if (this.isTrigger) {
                    if (this.currentTriggerState + 1 < this.triggerStateFrames.length) {
                        this.currentTriggerState++;
                        this.frame = this.triggerStateFrames[this.currentTriggerState];
                    }
                    else {
                        this.currentTriggerState = 0;
                    }
                }
            };
            /**
             *
             * @param {number} index
             */
            TriggerButton.prototype.forceTriggerState = function (index) {
                if (this.isTrigger) {
                    this.currentTriggerState = index;
                    this.triggerStateFrames[this.currentTriggerState];
                    this.frame = this.triggerStateFrames[this.currentTriggerState];
                }
            };
            return TriggerButton;
        }(common.BaseButton));
        common.TriggerButton = TriggerButton;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../SimpleSprite.ts" />
var TypeScriptPhaserSlot;
///<reference path="../SimpleSprite.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var LineSprite = /** @class */ (function (_super) {
            __extends(LineSprite, _super);
            function LineSprite(game, x, y, key, config, pattern, debugMode, frame) {
                var _this = _super.call(this, game, x, y, key, debugMode, frame) || this;
                _this.pattern = pattern;
                _this.config = config;
                return _this;
            }
            LineSprite.prototype.start = function (line) {
                this.visible = true;
                this.startBlinking();
            };
            LineSprite.prototype.stop = function (data) {
                this.visible = false;
            };
            LineSprite.prototype.startBlinking = function () {
                this.alpha = 1;
                var blinkTween = this.game.add.tween(this).to({ alpha: 0.2 }, (this.config.timeout / 2) * Phaser.Timer.SECOND, Phaser.Easing.Cubic.In, true, 0, 999);
            };
            return LineSprite;
        }(common.SimpleSprite));
        common.LineSprite = LineSprite;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/view/BaseView.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/view/BaseView.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var LinesView = /** @class */ (function (_super) {
            __extends(LinesView, _super);
            function LinesView(game, name, configuration, pattern, debugMode) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.game = game;
                _this.pattern = pattern;
                _this._config = configuration;
                _this.init();
                return _this;
            }
            LinesView.prototype.init = function () {
                this.createLines();
                this.createLineNumbers();
                this.hideLines();
            };
            /**
             * Creation all lines
             */
            LinesView.prototype.createLines = function () {
            };
            /**
             * Creation all line numbers
             */
            LinesView.prototype.createLineNumbers = function () {
            };
            /**
             *
             */
            LinesView.prototype.hideLines = function (animated) {
            };
            /**
             *
             * @param count
             */
            LinesView.prototype.showLines = function (count) {
            };
            /**
             *
             * @param {PayLine} line
             */
            LinesView.prototype.showWinLine = function (line) {
            };
            return LinesView;
        }(common.BaseView));
        common.LinesView = LinesView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../BaseView.ts" />
var TypeScriptPhaserSlot;
///<reference path="../BaseView.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var PaytableView = /** @class */ (function (_super) {
            __extends(PaytableView, _super);
            function PaytableView(game, name, pattern, debugMode) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.game = game;
                _this.pattern = pattern.paytable;
                _this.init();
                return _this;
            }
            /**
             *
             */
            PaytableView.prototype.init = function () {
                this.values = {
                    "0": "40\n10\n5",
                    "1": "50\n10\n5",
                    "2": "75\n15\n5",
                    "3": "100\n25\n10",
                    "4": "125\n30\n15",
                    "5": "200\n50\n15",
                    "6": "250\n50\n25",
                    "7": "300\n100\n50",
                    "8": "300\n100\n50",
                    "9": "300\n100\n50"
                };
                this.createBackground();
                this.createLogo();
                this.construct(this.pattern, this);
                this.createPages();
                this.createButtons();
                this.interactive = false;
            };
            PaytableView.prototype.createBackground = function () {
                var background = new common.SimpleSprite(this.game, 0, 0, 'background_paytable', this.debugMode);
                this.add(background);
            };
            PaytableView.prototype.createLogo = function () {
                var locale = this.game.textFactory.currLocale;
                var gameLogoPatternData = this.getPatternData(this.pattern, 'game_logo');
                if (!gameLogoPatternData) {
                    gameLogoPatternData = { x: 0, y: 0 };
                }
                this.logo = new common.SimpleSprite(this.game, gameLogoPatternData.x, gameLogoPatternData.y, 'game_logo', this.debugMode, 'logo_' + locale);
                this.logo.anchor.set(0.5, 0.5);
                if (this.logo.key != '__missing') {
                    this.add(this.logo);
                }
                this.game.textFactory.actionSignal.add(this.onLocaleChanged, this);
            };
            PaytableView.prototype.onLocaleChanged = function (data) {
                this.logo.frameName = 'logo_' + data.data;
            };
            PaytableView.prototype.createButtons = function () {
                var buttonActions = {
                    mouseUp: {
                        action: '',
                        params: null,
                        sound: null,
                        enabledAfter: undefined
                    }
                };
                var nextButtonConfigurationParams = {
                    name: 'paytableNext_btn',
                    className: 'BaseButton',
                    key: 'paytable',
                    caption: '',
                    textStyle: 'style1',
                    overFrame: 'infoNext_btn/over',
                    outFrame: 'infoNext_btn/enabled',
                    downFrame: 'infoNext_btn/down',
                    upFrame: 'infoNext_btn/enabled',
                    disabledFrame: 'infoNext_btn/disabled',
                    onDownSoundKey: 'clicksound',
                    onOverSoundKey: '',
                    onOutSoundKey: '',
                    onUpSoundKey: ''
                };
                var nextButtonConfiguration = new common.ButtonConfiguration(nextButtonConfigurationParams);
                var nextButtonPatternData = this.getPatternData(this.pattern, 'paytableNext_btn');
                var nextButton = new common.BaseButton(this.game, nextButtonConfiguration, nextButtonPatternData.x, nextButtonPatternData.y, this.onNextButtonClick, this);
                nextButton.actions = buttonActions;
                nextButton.enabled = true;
                this.add(nextButton);
                var prevButtonConfigurationParams = {
                    name: 'paytablePrev_btn',
                    className: 'BaseButton',
                    key: 'paytable',
                    caption: '',
                    textStyle: 'style1',
                    overFrame: 'infoPrew_btn/over',
                    outFrame: 'infoPrew_btn/enabled',
                    downFrame: 'infoPrew_btn/down',
                    upFrame: 'infoPrew_btn/enabled',
                    disabledFrame: 'infoPrew_btn/disabled',
                    onDownSoundKey: 'clicksound',
                    onOverSoundKey: '',
                    onOutSoundKey: '',
                    onUpSoundKey: ''
                };
                var prevButtonConfiguration = new common.ButtonConfiguration(prevButtonConfigurationParams);
                var prevButtonPatternData = this.getPatternData(this.pattern, 'paytablePrev_btn');
                var prevButton = new common.BaseButton(this.game, prevButtonConfiguration, prevButtonPatternData.x, prevButtonPatternData.y, this.onPrevButtonClick, this);
                prevButton.actions = buttonActions;
                prevButton.enabled = true;
                this.add(prevButton);
            };
            PaytableView.prototype.onNextButtonClick = function () {
                this.goToPage('increase');
            };
            PaytableView.prototype.onPrevButtonClick = function () {
                this.goToPage('decrease');
            };
            /**
             *
             */
            PaytableView.prototype.createPages = function () {
                this.pages = [];
                var page;
                var pagesCount = this.pattern.child.filter(function (child) {
                    return child.name.indexOf('page') != -1;
                }.bind(this)).length;
                for (var i = 0; i < pagesCount; i++) {
                    page = this.getByName('page' + (i + 1));
                    this.pages.push(page);
                    this.removeChild(page);
                }
                console.log(this.pages);
            };
            /**
             *
             * @param data
             * @returns {Phaser.Sprite}
             */
            PaytableView.prototype.construct = function (data, parent) {
                var child;
                var type = 'sprite';
                if (data.name.indexOf('_image') > -1) {
                    type = 'image';
                }
                if (data.name.indexOf('_text') > -1) {
                    type = 'text';
                }
                if (data.name.indexOf('_textKey') > -1) {
                    type = 'textKey';
                }
                switch (type) {
                    case 'image':
                        var name_1 = data.name;
                        name_1 = name_1.replace('_image', '');
                        var splitted = name_1.split('$');
                        child = new common.SimpleSprite(this.game, 0, 0, splitted[0], this.debugMode);
                        child.frameName = splitted[1];
                        break;
                    case 'text':
                        child = this.game.textFactory.getTextWithStyle('', 'PAYTABLE_VALUE_TEXT_style');
                        //child.text = '1000\n100\n10';
                        var symbolId = data.name.split('_')[1];
                        //let sss:any = this.game.state.getCurrentState()['model'].denomination;
                        var text_1 = '';
                        this.values[symbolId].split('\n').forEach(function (element) {
                            text_1 += (+element * this.game.state.getCurrentState()['model'].denomination).toFixed(2) + '\n';
                        }, this);
                        child.text = String(text_1);
                        break;
                    case 'textKey':
                        var key = data.name;
                        key = key.replace('_textKey', '');
                        child = this.game.textFactory.getTextWithStyle(key);
                        break;
                    case 'sprite':
                        child = parent || new Phaser.Group(this.game);
                        break;
                }
                if (data.hasOwnProperty('child')) {
                    var newChild = void 0;
                    for (var i = 0; i < data.child.length; i++) {
                        newChild = this.construct(data.child[i]);
                        child['add'](newChild);
                    }
                }
                child.name = data.name;
                child.x = data.x;
                child.y = data.y;
                child.scale.x = data.scaleX;
                child.scale.y = data.scaleY;
                switch (type) {
                    case 'text':
                    case 'label':
                    case 'textKey':
                        this.setTextPosition(child, data);
                        break;
                }
                return child;
            };
            PaytableView.prototype.setTextPosition = function (textField, pattern) {
                switch (pattern.align) {
                    case 'left':
                        textField.align = 'left';
                        textField.x = pattern.x;
                        textField.y = pattern.y;
                        textField.anchor.set(0, 0);
                        break;
                    case 'right':
                        textField.align = 'right';
                        textField.x = pattern.x + pattern.width;
                        textField.y = pattern.y;
                        textField.anchor.set(1, 0);
                        break;
                    case 'center':
                        textField.align = 'center';
                        textField.x = pattern.x + pattern.width / 2;
                        textField.y = pattern.y;
                        textField.anchor.set(0.5, 0);
                        break;
                }
            };
            /**
             *
             * @param pageIndex
             */
            PaytableView.prototype.goToPage = function (pageIndex) {
                var newIndex;
                var currentPageIndex = this.pages.indexOf(this.currentPage);
                switch (pageIndex) {
                    case 'increase':
                        newIndex = currentPageIndex + 1;
                        if (newIndex == this.pages.length) {
                            newIndex = 0;
                        }
                        break;
                    case 'decrease':
                        newIndex = currentPageIndex - 1;
                        if (newIndex == -1) {
                            newIndex = this.pages.length - 1;
                        }
                        break;
                    default:
                        newIndex = pageIndex;
                }
                if (this.currentPage) {
                    this.removeChild(this.currentPage);
                }
                this.currentPage = this.pages[newIndex];
                this.add(this.currentPage);
            };
            PaytableView.prototype.getPatternData = function (parent, childName) {
                for (var i = 0; i < parent.child.length; i++) {
                    var childPattern = parent.child[i];
                    if (childPattern.name == childName) {
                        return childPattern;
                    }
                }
                return null;
            };
            /**
             *
             */
            PaytableView.prototype.show = function () {
                this.goToPage(0);
                this.visible = true;
            };
            /**
             *
             */
            PaytableView.prototype.hide = function () {
                this.visible = false;
            };
            return PaytableView;
        }(common.BaseView));
        common.PaytableView = PaytableView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
/**
 * Created by rocket on 21.07.2017.
 */
var TypeScriptPhaserSlot;
/**
 * Created by rocket on 21.07.2017.
 */
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var PreloaderView = /** @class */ (function (_super) {
            __extends(PreloaderView, _super);
            function PreloaderView(game, name, pattern, debugMode) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.game = game;
                _this.pattern = pattern;
                _this.init();
                return _this;
            }
            PreloaderView.prototype.init = function () {
                var preloader = new Phaser.Group(this.game);
                for (var i = 0; i < 6; i++) {
                    var circle = new Phaser.Graphics(this.game);
                    circle.beginFill(0xffffff, 1);
                    circle.drawCircle(i * 40, 0, 25);
                    circle.endFill();
                    preloader.add(circle);
                }
                /*let preloaderBackground:Phaser.Graphics = new Phaser.Graphics(this.game, 0, 0);
                preloaderBackground.beginFill(0xCCCCCC, 1);
                preloaderBackground.drawRect(0, 0, 1000, 500);
                preloaderBackground.endFill();
                preloaderBackground.lineStyle(3, 0x0, 1);
                preloaderBackground.moveTo(0,0);
                preloaderBackground.lineTo(1000,0);
                preloaderBackground.lineTo(1000,500);
                preloaderBackground.lineTo(0,500);
                preloaderBackground.lineTo(0,0);
                preloader.add(preloaderBackground);*/
                preloader.y = (768 - preloader.height) / 2;
                this.add(preloader);
            };
            PreloaderView.prototype.onLoadingUpdate = function (progress, cacheKey, success, totalLoaded, totalFiles) {
                var oneFilePercent = 100 / totalFiles;
                var onePercent = oneFilePercent / 100;
                var loadedPercents = (oneFilePercent * totalLoaded) + (onePercent * progress);
                console.log('===>', loadedPercents);
            };
            PreloaderView.prototype.onLoadingComplete = function () {
            };
            return PreloaderView;
        }(common.BaseView));
        common.PreloaderView = PreloaderView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../SimpleSprite.ts" />
var TypeScriptPhaserSlot;
///<reference path="../SimpleSprite.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseReelSymbol = /** @class */ (function (_super) {
            __extends(BaseReelSymbol, _super);
            function BaseReelSymbol(game, x, y, symbolProperties, debugMode) {
                var _this = _super.call(this, game) || this;
                _this.x = x;
                _this.y = y;
                _this.debugMode = debugMode;
                _this._currentSymbolProperties = symbolProperties;
                _this._borderKey = symbolProperties.borderKeys[0] ? symbolProperties.borderKeys[0] : "";
                _this.init();
                return _this;
            }
            BaseReelSymbol.prototype.init = function () {
                this.symbolImage = new common.SimpleSprite(this.game, 0, 0, 'symbols', this.debugMode, this.currentSymbolProperties.key);
                //this.symbolImage.anchor.setTo(0.5, 0.5);
                this.add(this.symbolImage);
            };
            /**
             *
             * @param data
             */
            BaseReelSymbol.prototype.start = function (data) {
            };
            /**
             *
             * @param {any} data
             */
            BaseReelSymbol.prototype.stop = function (data) {
            };
            /**
             *
             */
            BaseReelSymbol.prototype.activate = function () {
            };
            /**
             *
             */
            BaseReelSymbol.prototype.deactivate = function () {
            };
            /**
             *
             */
            BaseReelSymbol.prototype.blur = function () {
            };
            /**
             *
             */
            BaseReelSymbol.prototype.unblur = function () {
            };
            /**
             *
             * @param game
             * @param symbolProperties
             */
            BaseReelSymbol.prototype.changeKey = function (game, symbolProperties) {
                this.ownIndex = symbolProperties.index;
                this.symbolImage.frameName = symbolProperties.key;
                this.game = game;
                this._borderKey = symbolProperties.borderKeys[0];
                this._currentSymbolProperties = symbolProperties;
            };
            BaseReelSymbol.prototype.shadow = function () {
            };
            BaseReelSymbol.prototype.show = function () {
                this.visible = true;
            };
            BaseReelSymbol.prototype.hide = function () {
                this.visible = false;
            };
            Object.defineProperty(BaseReelSymbol.prototype, "ownIndex", {
                /************************************************************
                    Getters and Setters
                *************************************************************/
                get: function () {
                    return this._ownIndex;
                },
                set: function (value) {
                    this._ownIndex = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelSymbol.prototype, "currentSymbolProperties", {
                get: function () {
                    return this._currentSymbolProperties;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelSymbol.prototype, "borderKey", {
                get: function () {
                    return this._borderKey;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelSymbol.prototype, "Key", {
                get: function () {
                    return this.symbolImage.key;
                },
                enumerable: true,
                configurable: true
            });
            return BaseReelSymbol;
        }(Phaser.Group));
        common.BaseReelSymbol = BaseReelSymbol;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../BaseView.ts" />
var TypeScriptPhaserSlot;
///<reference path="../BaseView.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseReelView = /** @class */ (function (_super) {
            __extends(BaseReelView, _super);
            function BaseReelView(game, index, name, debugMode, symbolProperties, gameConfig, pattern, data, mapReel, mapReelFreespins, borderLayer) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this._isIntrigued = false;
                _this.isReelStopped = false;
                _this.isReelRolling = false;
                _this.delta = 0;
                _this.initData = data;
                _this.index = Number(index);
                _this.gameConfig = gameConfig;
                _this.symbolClassName = _this.gameConfig.standartSymbolSetting.className;
                _this._justTouchedStopSignal = new Phaser.Signal();
                _this._upturnSignal = new Phaser.Signal();
                _this._actionSignal = new Phaser.Signal();
                _this._respineSignal = new Phaser.Signal();
                _this.rollingMechanics = gameConfig.rollingMechanics;
                _this.speeds = gameConfig.reels[index].speeds;
                _this.changeSpeed(0);
                _this.borderLayer = borderLayer;
                _this.sortLayers();
                _this.symbolProperties = symbolProperties;
                _this._reelConfig = gameConfig.reels[index];
                _this.pattern = pattern;
                _this.createBorders();
                /*let mask = new Phaser.Graphics(this.game, 0, 0);
                mask.beginFill(0xffffff);
                mask.drawRect(0, 0, gameConfig.reels[index].width, gameConfig.reels[index].height);
                this.add(mask);
                this.mask = mask;*/
                _this._currentShift = 0;
                _this.tweenedObject = {};
                return _this;
            }
            BaseReelView.prototype.init = function () {
                this._symbols = [];
                //this.currentShift = 0;
                for (var i = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                    this.initData.unshift(this.getNextKey());
                }
                for (var j = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                    this.initData.push(this.getNextKey());
                }
                this.addSymbols(this.initData);
                for (var k = 0; k < this._symbols.length; k++) {
                    this._symbols[k].y -= this.symbolHeight * this._reelConfig.topInvisibleSymbolsCount;
                }
            };
            /**
             * Sort and initialize layers
             */
            BaseReelView.prototype.sortLayers = function () {
                this.bottomLayer = new Phaser.Group(this.game);
                this.optionalLayer = new Phaser.Group(this.game);
                this.topLayer = new Phaser.Group(this.game);
                this.add(this.bottomLayer);
                this.add(this.optionalLayer);
                this.add(this.topLayer);
            };
            BaseReelView.prototype.createBorders = function () {
                /*let border: SimpleSprite;
                this.borders = [];
                let borders: string[] = this.gameConfig.standartSymbolSetting.borders;
                for (let i: number = 0; i < borders.length; i++) {
                    border = new SimpleSprite(this.game, 0, 0, borders[i]);
                    this.addChild(border);
                    border.visible = false;
                    this.borderLayer.add(border);
                    border.animations.add('play');
                    this.borders.push(border);
                }*/
            };
            /**
             *
             * @param item
             */
            BaseReelView.prototype.poolIn = function (item) {
                item.visible = false;
                common.Game.pool.push(item);
            };
            /**
             *
             * @param x
             * @param y
             * @param properties
             * @returns {ISymbol}
             */
            BaseReelView.prototype.createSymbol = function (x, y, properties) {
                var sym = this.poolOut(this.game, properties);
                if (this.isReelRolling) {
                    sym.blur();
                }
                else {
                    sym.unblur();
                }
                sym.x = x;
                sym.y = y;
                return sym;
            };
            /**
             * Creation of symbols definded with <symbolsPerReel> variable
             */
            BaseReelView.prototype.addSymbols = function (symbolIndexes, onTop) {
                var concatSymbol;
                var addedSymbols = [];
                var newSymbol;
                var sp;
                var symbolX = 0;
                var symbolY = 0;
                if (!onTop) {
                    for (var i = 0; i < symbolIndexes.length; i++) {
                        concatSymbol = this._symbols[this._symbols.length - 1];
                        sp = this.getSymbolPropertiesByIndex(symbolIndexes[i]);
                        if (concatSymbol) {
                            symbolY = concatSymbol.y + this.symbolHeight;
                        }
                        newSymbol = this.createSymbol(symbolX, symbolY, sp);
                        newSymbol.ownIndex = symbolIndexes[i];
                        newSymbol.x = symbolX;
                        newSymbol.y = symbolY;
                        this.bottomLayer.add(newSymbol);
                        this._symbols.push(newSymbol);
                        addedSymbols.push(newSymbol);
                    }
                }
                else {
                    for (var i = symbolIndexes.length - 1; i >= 0; i--) {
                        concatSymbol = this._symbols[0];
                        sp = this.getSymbolPropertiesByIndex(symbolIndexes[i]);
                        if (concatSymbol) {
                            symbolY = concatSymbol.y - this.symbolHeight;
                        }
                        newSymbol = this.createSymbol(symbolX, symbolY, sp);
                        newSymbol.ownIndex = symbolIndexes[i];
                        newSymbol.x = symbolX;
                        newSymbol.y = symbolY;
                        this.bottomLayer.add(newSymbol);
                        this._symbols.unshift(newSymbol);
                        addedSymbols.unshift(newSymbol);
                    }
                }
                return addedSymbols;
            };
            /**
             *
             * @param index
             * @returns {any}
             */
            BaseReelView.prototype.getSymbolPropertiesByIndex = function (index) {
                for (var i = 0; i < this.symbolProperties.length; i++) {
                    if (this.symbolProperties[i].index == index)
                        return this.symbolProperties[i];
                }
                //TODO: вернуть как было
                //return null;
                return this.symbolProperties[0];
            };
            /**
             *
             * @param game
             * @param sp
             * @returns {ISymbol}
             */
            BaseReelView.prototype.poolOut = function (game, sp) {
                var icon;
                var sss = common.Game.pool;
                // console.log(sss);
                if (common.Game.pool.length == 0) {
                    if (!sp) {
                        //console.log();
                    }
                    var className = sp.className;
                    if (!TypeScriptPhaserSlot.common || !TypeScriptPhaserSlot.common.hasOwnProperty(className)) {
                        //console.log();
                    }
                    icon = new TypeScriptPhaserSlot.common[className](game, 0, 0, sp);
                    this.poolIn(icon);
                }
                icon = common.Game.pool.pop();
                icon.changeKey(game, sp);
                if (this.isReelRolling) {
                    icon.blur();
                }
                else {
                    icon.unblur();
                }
                icon.visible = true;
                return icon;
            };
            /**
             *
             * @param length
             * @param minNumber
             * @param maxNumber
             */
            BaseReelView.prototype.generateSymbolsBand = function (length, minNumber, maxNumber) {
                for (var i = 0; i < length; i++) {
                    this.mapReel.push(this.getRandomKey(minNumber, maxNumber - minNumber));
                }
            };
            /**
             * Switched to freespins reel
             */
            BaseReelView.prototype.setFreespinsMap = function () {
                if (!this.mapReelFreespins)
                    return;
                this.currentMap = this.mapReelFreespins;
                this.currentShift = this.mapReelFreespinsShift;
            };
            /**
             * Switched to regular reel
             */
            BaseReelView.prototype.setRegularMap = function () {
                if (!this.mapReel)
                    return;
                this.currentMap = this.mapReel;
                this.currentShift = this.mapReelShift;
            };
            /**
             *
             * @param key
             * @param symbolIndex
             */
            BaseReelView.prototype.animateBorderByKey = function (key, symbolIndex) {
                for (var i = 0; i < this.borders.length; i++) {
                    if (this.borders[i].key == key) {
                        this.borders[i].visible = true;
                        this.borders[i].animations.play('play', 24);
                        this.borders[i].events.onAnimationComplete.addOnce(this.onBorderAnimationComplete, this);
                        this.borders[i].x = this.pattern.reels[this.index].x;
                        this.borders[i].y = this.pattern.reels[this.index]["icon" + symbolIndex].y;
                    }
                }
            };
            BaseReelView.prototype.onBorderAnimationComplete = function () {
                for (var i = 0; i < this.borders.length; i++) {
                    this.borders[i].visible = false;
                }
            };
            /**
             * function called by ReelsController to begin rolling
             * @param {any} data
             */
            BaseReelView.prototype.start = function (data) {
                this.topVisibledLimit = 0;
                this.isIntrigued = false;
                this.isUrgentStop = false;
                this.resultData = null;
                this.isReelStopped = false;
                this.isReelRolling = true;
            };
            /**
             * Call this before start() just to change speed rolling one-time
             * @param index
             */
            BaseReelView.prototype.changeSpeed = function (index) {
                this.speed = this.speeds[index];
            };
            /**
             * First stage of rolling.
             */
            BaseReelView.prototype.tweenToTop = function () {
                this.delta = 0;
                var d = Number(this.rollingMechanics.tweenToTop.pixels);
                if (d > 0) {
                    this.tweenedObject.delta = 0;
                    var t = Number(this.rollingMechanics.tweenToTop.time) * Phaser.Timer.SECOND;
                    var tween = this.game.add.tween(this.tweenedObject).to({ delta: -d }, t, this.rollingMechanics.tweenOf(this.rollingMechanics.tweenToTop.easingFunction));
                    tween.onComplete.addOnce(this.tweenAccelaration, this);
                    tween.onUpdateCallback(this.onTweenUpdate, this);
                    tween.start();
                }
                else {
                    this.tweenAccelaration();
                }
            };
            BaseReelView.prototype.onTweenUpdate = function ($first) {
                var delta = $first.target.delta - this.delta;
                this.delta = $first.target.delta;
                for (var i = 0; i < this._symbols.length; i++) {
                    this._symbols[i].y += delta;
                }
            };
            /**
             * Second stage of rolling.
             */
            BaseReelView.prototype.tweenAccelaration = function () {
                this.onTweenUpdate(arguments[1]);
                this.delta = 0;
                var d = Number(this.rollingMechanics.tweenAccelaration.pixels);
                var t = (d / ((this.speed / 4) * 60)) * Phaser.Timer.SECOND;
                this.tweenedObject.delta = 0;
                var tween = this.game.add.tween(this.tweenedObject).to({ delta: d }, t, this.rollingMechanics.tweenOf(this.rollingMechanics.tweenAccelaration.easingFunction));
                tween.onComplete.addOnce(this.onCompleteTweenAccelaration, this);
                tween.onUpdateCallback(this.onTweenUpdate, this);
                tween.start();
            };
            BaseReelView.prototype.onCompleteTweenAccelaration = function () {
                this.updater = this.staticRolling;
                //this.staticTween();
                this.symbols.forEach(function (symbol) {
                    symbol.blur();
                }, this);
            };
            /**
             * Third stage of rolling.
             */
            BaseReelView.prototype.staticRolling = function () {
                for (var i = 0; i < this._symbols.length; i++) {
                    this._symbols[i].y += this.speed;
                }
                var lastSymbol = this._symbols[this._symbols.length - 1];
                if (lastSymbol.y > this.bottomRemovedLimit) {
                    this.poolIn(lastSymbol);
                    this.symbols.pop();
                    this.addNewSymbolsInStaticRolling();
                }
            };
            /**
             * This function adding next symbol on the top during static speed rolling
             * However it could be overriden to change behaviour e.g you have to check twiced or trippled
             * stacked icons.
             */
            BaseReelView.prototype.addNewSymbolsInStaticRolling = function () {
                var nextKeys = this.getNextKeys(1);
                this.addSymbols(nextKeys, true);
            };
            /**
             * Fourth stage of rolling.
             */
            BaseReelView.prototype.resultedTween = function () {
                this.delta = 0;
                var d = Math.abs(this.topVisibledLimit - this._symbols[this._reelConfig.topInvisibleSymbolsCount].y) - this._reelConfig.shift;
                var t = (d / (this.speed * 60)) * Phaser.Timer.SECOND;
                this.tweenedObject.delta = 0;
                var tween = this.game.add.tween(this.tweenedObject).to({ delta: d }, t, Phaser.Easing.Linear.None);
                tween.onComplete.addOnce(this.tweenDecelaration, this);
                tween.onUpdateCallback(this.onTweenUpdate, this);
                tween.start();
            };
            /**
             * Fifth stage of rolling.
             */
            BaseReelView.prototype.tweenDecelaration = function () {
                this.onTweenUpdate(arguments[1]);
                this.delta = 0;
                this.justTouchedStopSignal.dispatch(this.index);
                common.SoundController.instance.playSound('reel_' + this.index + '_stopped_sound', 'default', 2);
                var d = this._reelConfig.shift * 2;
                var t = (d / ((this.speed / 5) * 60)) * Phaser.Timer.SECOND;
                this.tweenedObject.delta = 0;
                var tween = this.game.add.tween(this.tweenedObject).to({ delta: d }, t, Phaser.Easing.Quadratic.Out);
                tween.onComplete.addOnce(this.tweenUpturn, this);
                tween.onUpdateCallback(this.onTweenUpdate, this);
                tween.start();
                this.symbols.forEach(function (symbol) {
                    symbol.unblur();
                }, this);
            };
            /**
             * sixth stage of rolling.
             */
            BaseReelView.prototype.tweenUpturn = function () {
                this.onTweenUpdate(arguments[1]);
                this.upturnSignal.dispatch(this.index);
                this.delta = 0;
                var d = this._reelConfig.shift;
                var t = this.rollingMechanics.tweenUpturn.time * Phaser.Timer.SECOND;
                this.tweenedObject.delta = 0;
                var tween = this.game.add.tween(this.tweenedObject).to({ delta: -d }, t, this.rollingMechanics.tweenOf(this.rollingMechanics.tweenUpturn.easingFunction));
                tween.onUpdateCallback(this.onTweenUpdate, this);
                tween.onComplete.addOnce(this.onCompleteTweenUpturn, this);
                tween.start();
            };
            /**
             *
             */
            BaseReelView.prototype.onCompleteTweenUpturn = function () {
                var maxLength = this._reelConfig.topInvisibleSymbolsCount +
                    this._reelConfig.visibledSymbols +
                    this._reelConfig.bottomInvisibleSymbolsCount;
                if (this._symbols.length >= maxLength) {
                    for (var i = maxLength; i < this._symbols.length; i++) {
                        this.poolIn(this._symbols[i]);
                    }
                    this._symbols.splice(maxLength);
                }
                var startIndex = this.reelConfig.topInvisibleSymbolsCount + this.reelConfig.visibledSymbols + this.reelConfig.bottomInvisibleSymbolsCount - 1;
                for (var k = startIndex; k < this._symbols.length; k++) {
                    this._symbols[k].destroy();
                }
                this._symbols.splice(startIndex);
                this.isReelRolling = false;
                this.actionSignal.dispatch(this.index);
                this.changeSpeed(0);
            };
            /**
             *
             * @param data
             * @param additionalSymbols
             */
            BaseReelView.prototype.stop = function (data, additionalSymbols) {
                var _this = this;
                var intriguedPause = this.isIntrigued ? 1000 * this.index : 0;
                setTimeout(function () {
                    _this.realStop(data, additionalSymbols);
                }, intriguedPause);
            };
            /**
             *
             * @param data
             * @param additionalSymbols
             */
            BaseReelView.prototype.realStop = function (data, additionalSymbols) {
                this.updater = null;
                this.game.tweens.removeFrom(this.tweenedObject);
                this.resultData = data;
                this.currentShift = 0;
                for (var i = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                    this.resultData.unshift(this.getNextKey());
                }
                for (var j = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                    this.resultData.push(this.getNextKey());
                }
                for (var j = 0; j < additionalSymbols; j++) {
                    this.resultData.push(this.getNextKey());
                }
                this.addSymbols(this.resultData, true);
                this.resultedTween();
            };
            /**
             *  It is actual only if mapping is there
             *  Getting the next indexes from the current reel (Optional)
             * @returns
             */
            BaseReelView.prototype.getNextKeyFromBand = function () {
                var key = this.currentMap[this.currentShift++];
                return key;
            };
            /**
             * Optional.
             */
            BaseReelView.prototype.respin = function (data) {
            };
            BaseReelView.prototype.stopRespin = function (data) {
            };
            /**
             * Getting the next symbol index from the band
             * @returns
             */
            BaseReelView.prototype.getNextKey = function () {
                return this.getNextKeyFromBand();
            };
            /**
             * Getting the next indexes from the current band
             * @param {number} keyAmount - how many indexes do you need to get
             * @returns
             */
            BaseReelView.prototype.getNextKeys = function (keyAmount) {
                var res = [];
                for (var i = 0; i < keyAmount; i++) {
                    res.push(this.getNextKey());
                }
                return res;
            };
            /**
             *  Getting the next index randomly
             * @returns
             */
            BaseReelView.prototype.getRandomKey = function (minId, maxId) {
                var index = minId + Math.ceil(Math.random() * (maxId - minId));
                return this.symbolProperties[index].index;
            };
            /**
             *
             */
            BaseReelView.prototype.update = function () {
                if (this.updater) {
                    this.updater();
                }
            };
            /**
             *
             */
            BaseReelView.prototype.emptyFunction = function () {
            };
            BaseReelView.prototype.destroy = function (destroyChildren, soft) {
                /*while(this.symbols.length > 0) {
                    let symbol:ISymbol = this.symbols.shift();
                    symbol.parent.removeChild(symbol);
                    this.poolIn(symbol);
                }*/
                _super.prototype.destroy.call(this, destroyChildren, soft);
                this._actionSignal = null;
                this.removeAll(true, false, true);
            };
            /**
             * Checks if there any one symbol with Index in visible limit
             * @param index
             * @returns {boolean}
             */
            BaseReelView.prototype.checkForSymbol = function (index) {
                for (var i = this._reelConfig.topInvisibleSymbolsCount; i < this._reelConfig.visibledSymbols; i++) {
                    if (this.symbols[i].ownIndex == index)
                        return true;
                }
                return false;
            };
            Object.defineProperty(BaseReelView.prototype, "currentShift", {
                /************************************************************
                 Getters and Setters
                 *************************************************************/
                get: function () {
                    return this._currentShift;
                },
                set: function (value) {
                    var newValue = value;
                    if (this.currentMap.length > 0 && value >= this.currentMap.length) {
                        newValue = value - this.currentMap.length;
                    }
                    this._currentShift = newValue;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "index", {
                get: function () {
                    return this._index;
                },
                set: function (value) {
                    this._index = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "isIntrigued", {
                get: function () {
                    return this._isIntrigued;
                },
                set: function (value) {
                    this._isIntrigued = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "symbols", {
                get: function () {
                    return this._symbols;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "resultData", {
                get: function () {
                    return this._resultData;
                },
                set: function (value) {
                    this._resultData = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "actionSignal", {
                get: function () {
                    return this._actionSignal;
                },
                set: function (value) {
                    this._actionSignal = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "respineSignal", {
                get: function () {
                    return this._respineSignal;
                },
                set: function (value) {
                    this._respineSignal = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "justTouchedStopSignal", {
                get: function () {
                    return this._justTouchedStopSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "upturnSignal", {
                get: function () {
                    return this._upturnSignal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseReelView.prototype, "reelConfig", {
                get: function () {
                    return this._reelConfig;
                },
                enumerable: true,
                configurable: true
            });
            return BaseReelView;
        }(common.BaseView));
        common.BaseReelView = BaseReelView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ReelSymbolGroup = /** @class */ (function (_super) {
            __extends(ReelSymbolGroup, _super);
            function ReelSymbolGroup(game, index, symbolProperties, symbolsGap, symbolClassName, debugMode) {
                var _this = _super.call(this, game, "ReelSymbolGroup" + String(index), debugMode) || this;
                _this.index = index;
                _this._symbolClassName = symbolClassName;
                _this.symbolProperties = symbolProperties;
                _this.symbolsGap = symbolsGap;
                return _this;
            }
            /**
            * Creation of symbols definded with <symbolsPerReel> variable
            */
            ReelSymbolGroup.prototype.addSymbols = function (symbolIndexes) {
                this._symbols = [];
                var heightPrev = 0;
                var newSymbol;
                var sp;
                var symbolX = 0;
                var symbolY = 0;
                for (var i = 0; i < symbolIndexes.length; i++) {
                    sp = this.getSymbolPropertiesByIndex(symbolIndexes[i]);
                    symbolY = (this.symbolsGap + heightPrev);
                    heightPrev += sp.height;
                    newSymbol = this.createSymbol(symbolX, symbolY, sp);
                    newSymbol.ownIndex = symbolIndexes[i];
                    newSymbol.x = symbolX;
                    newSymbol.y = symbolY;
                    this.add(newSymbol);
                    this.symbols.push(newSymbol);
                }
            };
            /**
             *
             * @param item
             */
            ReelSymbolGroup.prototype.poolIn = function (item) {
                item.visible = false;
                common.Game.pool.push(item);
            };
            /**
             *
             * @param game
             * @param sp
             * @returns {ISymbol}
             */
            ReelSymbolGroup.prototype.poolOut = function (game, sp) {
                var icon;
                if (common.Game.pool.length == 0) {
                    var className = sp.className || this.SymbolClassName;
                    icon = new TypeScriptPhaserSlot.common[className](game, 0, 0, sp);
                    this.poolIn(icon);
                }
                icon = common.Game.pool.pop();
                icon.changeKey(game, sp);
                icon.visible = true;
                return icon;
            };
            /**
             *
             * @param x
             * @param y
             * @param properties
             * @returns {ISymbol}
             */
            ReelSymbolGroup.prototype.createSymbol = function (x, y, properties) {
                var sym = this.poolOut(this.game, properties);
                sym.x = x;
                sym.y = y;
                return sym;
            };
            /**
             *
             */
            ReelSymbolGroup.prototype.stopAllAnimation = function () {
                for (var i = 0; i < this._symbols.length; i++) {
                    this._symbols[i].stop();
                }
            };
            /**
             *
             * @param index
             * @param count
             * @param connectSymbols
             */
            ReelSymbolGroup.prototype.removeSymbolsFromIndex = function (index, count, connectSymbols) {
                var finalIndex = count ? index + count : this._symbols.length;
                for (var i = index; i < finalIndex; i++) {
                    this.poolIn(this._symbols[i]);
                }
                count = count ? count : this._symbols.length;
                this._symbols.splice(index, count);
                if (connectSymbols) {
                    this.connectSymbols();
                }
            };
            /**
             *
             */
            ReelSymbolGroup.prototype.connectSymbols = function () {
                var symbol;
                for (var i = 0; i < this._symbols.length; i++) {
                    symbol = this._symbols[i];
                    symbol.y = symbol.height * i;
                }
            };
            /**
             *
             * @param index
             */
            ReelSymbolGroup.prototype.animateSymbolByIndex = function (index, data) {
                this._symbols[index].start(data);
            };
            /**
             *
             * @param index
             * @returns {any}
             */
            ReelSymbolGroup.prototype.getSymbolPropertiesByIndex = function (index) {
                for (var i = 0; i < this.symbolProperties.length; i++) {
                    if (this.symbolProperties[i].index == index)
                        return this.symbolProperties[i];
                }
                return null;
            };
            /**
             *
             * @param destroyChildren
             * @param soft
             */
            ReelSymbolGroup.prototype.destroy = function (destroyChildren, soft) {
                for (var i = 0; i < this.symbols.length; i++) {
                    this.poolIn(this.symbols[i]);
                }
                this._symbols = null;
                _super.prototype.destroy.call(this, destroyChildren, soft);
            };
            Object.defineProperty(ReelSymbolGroup.prototype, "SymbolClassName", {
                get: function () {
                    return this._symbolClassName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ReelSymbolGroup.prototype, "symbols", {
                get: function () {
                    return this._symbols;
                },
                enumerable: true,
                configurable: true
            });
            ReelSymbolGroup.prototype.getTopSymbol = function () {
                return this.symbols[0];
            };
            ReelSymbolGroup.prototype.getBottomSymbol = function () {
                return this.symbols[this.symbols.length - 1];
            };
            Object.defineProperty(ReelSymbolGroup.prototype, "symbolsCount", {
                get: function () {
                    return this.symbols.length;
                },
                enumerable: true,
                configurable: true
            });
            return ReelSymbolGroup;
        }(common.BaseView));
        common.ReelSymbolGroup = ReelSymbolGroup;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
/**
 * Created by rocket on 30.06.2017.
 */
var TypeScriptPhaserSlot;
/**
 * Created by rocket on 30.06.2017.
 */
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var ExitButton = /** @class */ (function (_super) {
            __extends(ExitButton, _super);
            function ExitButton(game, x, y) {
                var _this = _super.call(this, game, x, y, 'toolbar') || this;
                _this.inputEnabled = true;
                _this.stateIndex = 0;
                _this.statesFrames = ['exit_1'];
                _this.states = ['exit'];
                _this.frameName = 'toolbar/' + _this.statesFrames[_this.stateIndex];
                _this._actionSignal = new Phaser.Signal();
                _this.alpha = 0.7;
                //this.events.onInputDown.add(this.onDown, this);
                _this.events.onInputUp.add(_this.onDown, _this);
                return _this;
            }
            ExitButton.prototype.onDown = function () {
                this.stateIndex++;
                if (this.stateIndex == this.states.length) {
                    this.stateIndex = 0;
                }
                this.frameName = 'toolbar/' + this.statesFrames[this.stateIndex];
                this.actionSignal.dispatch(this.states[this.stateIndex]);
            };
            Object.defineProperty(ExitButton.prototype, "actionSignal", {
                get: function () {
                    return this._actionSignal;
                },
                enumerable: true,
                configurable: true
            });
            return ExitButton;
        }(common.SimpleSprite));
        common.ExitButton = ExitButton;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
/**
 * Created by rocket on 30.06.2017.
 */
var TypeScriptPhaserSlot;
/**
 * Created by rocket on 30.06.2017.
 */
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var FullScreenButton = /** @class */ (function (_super) {
            __extends(FullScreenButton, _super);
            function FullScreenButton(game, x, y) {
                var _this = _super.call(this, game, x, y, 'toolbar') || this;
                _this.inputEnabled = true;
                _this.stateIndex = 0;
                _this.statesFrames = ['full_screen_1', 'full_screen_2'];
                _this.states = ['switch_fullscreen', 'switch_fullscreen'];
                _this.frameName = 'toolbar/' + _this.statesFrames[_this.stateIndex];
                _this._actionSignal = new Phaser.Signal();
                _this.alpha = 0.7;
                //this.events.onInputDown.add(this.onDown, this);
                _this.events.onInputUp.add(_this.onDown, _this);
                return _this;
            }
            FullScreenButton.prototype.onDown = function () {
                this.stateIndex++;
                if (this.stateIndex == this.states.length) {
                    this.stateIndex = 0;
                }
                this.frameName = 'toolbar/' + this.statesFrames[this.stateIndex];
                if (!this.game.device.desktop) {
                    console.log('MOBILE FULLSCREEN');
                    if (this.game.scale.isFullScreen) {
                        this.game.scale.stopFullScreen();
                    }
                    else {
                        this.game.scale.startFullScreen(true);
                    }
                }
                else {
                    this.actionSignal.dispatch(this.states[this.stateIndex]);
                }
            };
            Object.defineProperty(FullScreenButton.prototype, "actionSignal", {
                get: function () {
                    return this._actionSignal;
                },
                enumerable: true,
                configurable: true
            });
            return FullScreenButton;
        }(common.SimpleSprite));
        common.FullScreenButton = FullScreenButton;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
/**
 * Created by rocket on 30.06.2017.
 */
var TypeScriptPhaserSlot;
/**
 * Created by rocket on 30.06.2017.
 */
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var SoundButton = /** @class */ (function (_super) {
            __extends(SoundButton, _super);
            function SoundButton(game, x, y) {
                var _this = _super.call(this, game, x, y, 'toolbar') || this;
                _this.inputEnabled = true;
                _this.stateIndex = 0;
                _this.statesFrames = ['sound_1', 'sound_2', 'sound_3'];
                _this.states = ['all_on', 'music_off', 'all_off'];
                _this.frameName = 'toolbar/' + _this.statesFrames[_this.stateIndex];
                _this._actionSignal = new Phaser.Signal();
                _this.alpha = 0.7;
                _this.events.onInputDown.add(_this.onDown, _this);
                return _this;
            }
            SoundButton.prototype.onDown = function () {
                this.stateIndex++;
                if (this.stateIndex == this.states.length) {
                    this.stateIndex = 0;
                }
                this.frameName = 'toolbar/' + this.statesFrames[this.stateIndex];
                this.actionSignal.dispatch(this.states[this.stateIndex]);
            };
            Object.defineProperty(SoundButton.prototype, "actionSignal", {
                get: function () {
                    return this._actionSignal;
                },
                enumerable: true,
                configurable: true
            });
            return SoundButton;
        }(common.SimpleSprite));
        common.SoundButton = SoundButton;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/controller/ActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/controller/ActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseGameBackController = /** @class */ (function (_super) {
            __extends(BaseGameBackController, _super);
            function BaseGameBackController(game, name, layer, debugMode, pattern) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.layer = layer;
                _this.pattern = pattern;
                _this.init(pattern);
                return _this;
            }
            /**
             *
             * @param pattern
             */
            BaseGameBackController.prototype.init = function (pattern) {
                this.view = new common.SimpleSprite(this.game, pattern.background.x, pattern.background.y, pattern.background.name, this.debugMode);
                this.layer.add(this.view);
            };
            /**
             *
             * @param data
             */
            BaseGameBackController.prototype.changeBackground = function (data) {
            };
            return BaseGameBackController;
        }(common.ActionController));
        common.BaseGameBackController = BaseGameBackController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/controller/ActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/controller/ActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseGameDecorationsController = /** @class */ (function (_super) {
            __extends(BaseGameDecorationsController, _super);
            function BaseGameDecorationsController(game, name, layer, debugMode, pattern) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.layer = layer;
                _this.pattern = pattern;
                _this.init(pattern);
                return _this;
            }
            /**
            * Building reel views
            */
            BaseGameDecorationsController.prototype.init = function (pattern) {
                this.frame = new common.SimpleSprite(this.game, pattern.usual_frame.x, pattern.usual_frame.y, pattern.usual_frame.name, this.debugMode);
                this.layer.add(this.frame);
                this.createLogo();
            };
            /**
             *
             */
            BaseGameDecorationsController.prototype.createLogo = function () {
            };
            /**
             *
             * @param data
             */
            BaseGameDecorationsController.prototype.changeFrame = function (data) {
            };
            return BaseGameDecorationsController;
        }(common.ActionController));
        common.BaseGameDecorationsController = BaseGameDecorationsController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/controller/GameController.ts" />
///<reference path="../../../engine/controller/configuration/GameConfiguration.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/controller/GameController.ts" />
///<reference path="../../../engine/controller/configuration/GameConfiguration.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseGameGameController = /** @class */ (function (_super) {
            __extends(BaseGameGameController, _super);
            function BaseGameGameController() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            BaseGameGameController.prototype.start = function () {
                _super.prototype.start.call(this);
                this.model.betChangedSignal.add(this.onBetChangedHandler, this);
                this.model.linesChangedSignal.add(this.onLinesChangedHandler, this);
                this.model.autoChangedSignal.add(this.onAutoChangedHandler, this);
                /*
                 Creation outside Api Controller to comunicate to third-party server API
                 */
                var apiControllerName = window['GR'] ? 'BoongoAPIController' : String(this.configs[0].gameName + 'APIController');
                var apiController = new TypeScriptPhaserSlot.common[apiControllerName](this.url, apiControllerName, this.debugMode);
                /*
                 Class which provides translation between side-APIController and internal API Standart and sends the data to Model instance
                 */
                this._appProxy = new common.ApplicationProxy(apiController, "ApplicationProxy", this.debugMode);
                this._appProxy.spinResponseSignal.add(this.onSpinResponse, this);
                this._appProxy.getGameStateSignal.add(this.onGetGameState, this);
                this._appProxy.bonusGameResponseSignal.add(this.onBonusResponse, this);
                this.appProxy.getGameState(this.configuration.gameName);
                this.game.scale.onSizeChange.add(this.correctSize, this);
                this.game.input.mouse.enabled = this.game.device.desktop || false;
            };
            BaseGameGameController.prototype.correctSize = function (scaleManager, screenWidth, screenHeight) {
                if (screenWidth < screenHeight) {
                    return;
                }
                if (!this.game.world.children || this.game.world.children.length == 0) {
                    return;
                }
                var w = 1366;
                var h = 768;
                var g = this.game.world.getChildAt(0);
                var removed = [];
                this.game.world.children[0]['forEach'](function (child, index) {
                    if (child.x + child.width > 1366) {
                        console.log('removed:', child.name);
                        removed.push({ parent: child.parent, index: child.parent.getChildIndex(child), child: child });
                        child.parent.removeChild(child);
                    }
                }, this);
                //this.game.canvas.width = screenWidth;
                //this.game.canvas.height = screenHeight;
                console.log('screenWidth', screenWidth);
                console.log('screenHeight', screenHeight);
                this.game.camera.setSize(screenWidth, screenHeight);
                if (this.game.camera.height != h) {
                    var scale = this.game.camera.height / h;
                    console.log('gameScale', scale);
                    g.scale.set(scale, scale);
                    this.game.world.getChildAt(1).scale.set(scale, scale);
                    this.game.world.getChildAt(2).scale.set(scale, scale);
                }
                var boundsXoffset = g.width < this.game.camera.width ? (this.game.camera.width - g.width) / 2 * -1 : 0;
                this.game.world.resize(g.width, g.height);
                this.game.world.setBounds(boundsXoffset, 0, g.width, g.height);
                var cameraX = (g.width - this.game.camera.width) / 2;
                this.game.camera.x = cameraX;
                this.game.stage.smoothed = true;
                this.game.antialias = true;
                removed.forEach(function (element) {
                    element.parent.addChildAt(element.child, element.index);
                }, this);
                removed = [];
            };
            /**
             *
             * @param value
             */
            BaseGameGameController.prototype.onBetChangedHandler = function (value) {
                this._interfaceController.changeBetText(value);
                this._interfaceController.changeTotalBetText(this.model.totalBet);
            };
            /**
             *
             * @param value
             */
            BaseGameGameController.prototype.onLinesChangedHandler = function (value) {
                this._interfaceController.changeLinesText(value);
                this._interfaceController.changeTotalBetText(this.model.totalBet);
            };
            /**
             *
             * @param {number} value
             */
            BaseGameGameController.prototype.onAutoChangedHandler = function (value) {
                common.SoundController.instance.playSound(this.model.isAutoPlay ? 'auto' : 'auto');
                if (this.model.isAutoPlay) {
                    this._boongoGameRunnerController.sayToGameRuner('autogameStart', { rounds: 999 });
                }
                else {
                    this._boongoGameRunnerController.sayToGameRuner('autogameStop');
                }
                this._interfaceController['showAutoplayText'](this.model.isAutoPlay);
                this._interfaceController.autoChanged(this.model.isAutoPlay);
                if (this.model.coins < this.model.totalBet && this.model.freespins == 0) {
                    if (this.model.isAutoPlay) {
                        this.onInterfaceControllerAction(common.ButtonActionConstants.AUTO_ACTION);
                    }
                }
            };
            /**
             *
             * @param {IState} state
             * @param {any} data
             * @param {boolean} isRequiredAction
             */
            BaseGameGameController.prototype.modelSays = function (state, data, isRequiredAction) {
                _super.prototype.modelSays.call(this, state, data, isRequiredAction);
                if (state) {
                    return;
                }
                //let isRequired: boolean = !isRequiredAction || this.model.isAutoPlay ? false : true;
                var isRequired = !(!isRequiredAction || this.model.isAutoPlay);
                if (!isRequiredAction) {
                    this.debuglog("do work for " + state.name);
                }
                this._interfaceController.stateChanged(state.name);
                if (state.name == common.StatesConstants.IDLE_STATE) {
                    this._interfaceController.changeBalance(this.model.coins);
                    this._boongoGameRunnerController.sayToGameRuner('ready');
                }
                if (!isRequired)
                    switch (state.name) {
                        case common.StatesConstants.IDLE_STATE:
                            this._linesController.stop();
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            this._interfaceController.changeBalance(this.model.coins);
                            if (this.model.isAutoPlay && this.model.coins < this.model.totalBet && this.model.freespins == 0) {
                                this.model.auto(false);
                                this.onInterfaceControllerAction(common.ButtonActionConstants.AUTO_ACTION);
                                break;
                            }
                            state.finishWork();
                            break;
                        case common.StatesConstants.PRESPIN_STATE:
                            state.finishWork();
                            break;
                        case common.StatesConstants.GET_SPIN_RESPONSE_STATE:
                            if (this.model.isFreespins) {
                                this.appProxy.freespin(1);
                            }
                            else {
                                this.appProxy.spin(data);
                            }
                            break;
                        case common.StatesConstants.START_SPIN_STATE:
                            this._linesController.stop();
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            this._interfaceController.resetWinText();
                            this._reelsController.start();
                            if (this.model.isFreespins) {
                                this._freeSpinsController.leftFreeSpins(this.model.freespins);
                            }
                            this._interfaceController.changeBalance(this.model.coins - (this.model.isFreespins ? 0 : this.model.totalBet));
                            state.finishWork();
                            break;
                        case common.StatesConstants.START_RESPIN_STATE:
                            this._linesController.stop();
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            this._reelsController.respin();
                            //this._reelsController.respin(data);
                            state.finishWork();
                            break;
                        case common.StatesConstants.GET_RESPIN_RESPONSE_STATE:
                            this.appProxy.respin();
                            break;
                        case common.StatesConstants.STOP_RESPIN_STATE:
                            this._reelsController.stopRespin(data);
                            break;
                        case common.StatesConstants.STOP_SPIN_STATE:
                            this._reelsController.stop(data);
                            break;
                        case common.StatesConstants.PRINCESS_FROG_TRANSFORM_PRINCESS_STATE:
                            var result = this._reelsController['transformPrincess']();
                            if (result) {
                                setTimeout(function () {
                                    state.finishWork();
                                }.bind(this), 1000);
                            }
                            else {
                                state.finishWork();
                            }
                            break;
                        case common.StatesConstants.EMOTIONS_STATE:
                            this._emotionsController.start(data);
                            break;
                        case common.StatesConstants.START_SHOW_WINNING_STATE:
                            this.lastSpinResult = data;
                            this._linesController.start(data);
                            this._emotionsController.stopEmotion();
                            //this._reelsController.animateSymbols(data);
                            //this._symbolsAnimationsController.animateSymbols(data);
                            break;
                        case common.StatesConstants.STOP_SHOW_WINNING_STATE:
                            this._interfaceController.resetWinText();
                            this._interfaceController.changeBalance(this.model.coins);
                            this._winningOnCenterController.hideWinning();
                            state.finishWork();
                            common.SoundController.instance.playSound('ching', 'default', 0.3);
                            break;
                        case common.StatesConstants.COLLECT_STATE:
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            state.finishWork();
                            break;
                        case common.StatesConstants.START_FREESPINS_STATE:
                            this._boongoGameRunnerController.sayToGameRuner('ready');
                            this._boongoGameRunnerController.sayToGameRuner('freespin_init');
                            this._freeSpinsController.startFreeSpins(this.model.freespins);
                            this._backgroundController['changeBackground'](true);
                            this._decorationsController['changeFrame'](true);
                            this._interfaceController['showAutoplayText'](false);
                            break;
                        case common.StatesConstants.ADD_FREESPINS_STATE:
                            this._freeSpinsController.addedFreeSpins(this.model.freeSpinsAdded);
                            break;
                        case common.StatesConstants.FINISH_FREESPINS_STATE:
                            this._boongoGameRunnerController.sayToGameRuner('ready');
                            this._boongoGameRunnerController.sayToGameRuner('freespin_stop');
                            this._freeSpinsController.endOfFreeSpins(this.model.bonusTotalWin);
                            this._backgroundController['changeBackground'](false);
                            this._decorationsController['changeFrame'](false);
                            break;
                        case common.StatesConstants.GET_GAMBLE_RESPONSE_STATE:
                            state.finishWork();
                            break;
                        case common.StatesConstants.GAMBLE_STATE:
                            this.appProxy.gamble();
                            break;
                        case common.StatesConstants.START_BONUS_GAME_STATE:
                            break;
                        case common.StatesConstants.FINISH_BONUS_GAME_STATE:
                            state.finishWork();
                            break;
                    }
            };
            /**
             *
             */
            BaseGameGameController.prototype.onNotEnoughMoney = function () {
                _super.prototype.onNotEnoughMoney.call(this);
                if (this.model.isAutoPlay && this.model.coins < this.model.totalBet && this.model.freespins == 0) {
                    this.model.auto(false);
                    this.onInterfaceControllerAction(common.ButtonActionConstants.AUTO_ACTION);
                }
                if (!window['GR']) {
                    this._interfaceController.showPopupWindow({ type: 'onNotEnoughMoney' });
                }
                else {
                    var bet = this.model.allowedBets[this.model.currBetIndex] * this.model.denomination;
                    var lines = this.model.allowedLines[this.model.currLinesIndex];
                    var paramsObj = {
                        action: {
                            name: 'spin',
                            params: {
                                lines: lines,
                                bet_per_line: bet
                            }
                        },
                        bet: lines * bet
                    };
                    this._boongoGameRunnerController.sayToGameRuner('play', paramsObj);
                }
            };
            /**
             *
             * @param {SpinResponse} data
             */
            BaseGameGameController.prototype.onSpinResponse = function (data) {
                this.model.onSpinResponse(data);
                this.currState.finishWork();
            };
            /**
             *
             * @param {TypeScriptPhaserSlot.common.BonusResponse} data
             */
            BaseGameGameController.prototype.onBonusResponse = function (data) {
                this.model.onBonusGameResponse(data);
                this.model.goToState(common.StatesConstants.SHOW_WINNING_BONUS_GAME_STATE);
            };
            /**
             * Callback function to handle getting the server response "GetState"
             * Initialising all helper/controllers
             *-ReelsController
             *-InterfaceController
             *-LineController
             *-TextController
             * @param {GameStateResponse} data
             */
            BaseGameGameController.prototype.onGetGameState = function (data) {
                var contentLayer = new Phaser.Group(this.game);
                var backLayer = new Phaser.Group(this.game);
                backLayer.name = 'backLayer';
                var reelsLayer = new Phaser.Group(this.game);
                reelsLayer.name = 'reelsLayer';
                var linesLayer = new Phaser.Group(this.game);
                linesLayer.name = 'linesLayer';
                var paytableLayer = new Phaser.Group(this.game);
                paytableLayer.name = 'paytableLayer';
                var wildsAnimationsLayer = new Phaser.Group(this.game);
                wildsAnimationsLayer.name = 'wildsAnimationsLayer';
                var decorationsLayer = new Phaser.Group(this.game);
                decorationsLayer.name = 'decorationsLayer';
                var symbolsAnimationsLayer = new Phaser.Group(this.game);
                symbolsAnimationsLayer.name = 'symbolsAnimationsLayer';
                var consoleLayer = new Phaser.Group(this.game);
                consoleLayer.name = 'consoleLayer';
                //consoleLayer.scale.setTo(scale, scale);
                var winningOnCenterLayersLayer = new Phaser.Group(this.game);
                winningOnCenterLayersLayer.name = 'winningOnCenterLayersLayer';
                var emotionsLayer = new Phaser.Group(this.game);
                emotionsLayer.name = 'emotionsLayer';
                var freeSpinsLayer = new Phaser.Group(this.game);
                freeSpinsLayer.name = 'freeSpinsLayer';
                var toolbarLayer = new Phaser.Group(this.game);
                toolbarLayer.name = 'toolbarLayer';
                //toolbarLayer.scale.setTo(scale, scale);
                contentLayer.add(backLayer);
                contentLayer.add(reelsLayer);
                contentLayer.add(linesLayer);
                contentLayer.add(paytableLayer);
                contentLayer.add(wildsAnimationsLayer);
                contentLayer.add(decorationsLayer);
                contentLayer.add(symbolsAnimationsLayer);
                //contentLyer.add(consoleLayer);
                contentLayer.add(winningOnCenterLayersLayer);
                contentLayer.add(emotionsLayer);
                contentLayer.add(freeSpinsLayer);
                //contentLyer.add(toolbarLayer);
                this.game.add.existing(contentLayer);
                this.game.add.existing(toolbarLayer);
                this.game.add.existing(consoleLayer);
                var backControllerName = this.getControllerName("BackController");
                this._backgroundController = new TypeScriptPhaserSlot.common[backControllerName](this.game, backControllerName, backLayer, true, this.game.pattern);
                var decorationsControllerName = this.getControllerName("DecorationsController");
                this._decorationsController = new TypeScriptPhaserSlot.common[decorationsControllerName](this.game, decorationsControllerName, decorationsLayer, true, this.game.pattern);
                var icn = 'InterfaceController';
                var icp = this.game.pattern.console;
                if (this.game.device.iPhone || this.game.device.android) {
                    icn = 'InterfaceControllerMobile';
                    icp = this.game.pattern.console_mobile;
                }
                if (this.game.device.iPad) {
                    icn = 'InterfaceControllerIPad';
                    icp = this.game.pattern.console_ipad;
                }
                var interfaceControllerName = this.getControllerName(icn);
                this._interfaceController = new TypeScriptPhaserSlot.common[interfaceControllerName](this.game, interfaceControllerName, consoleLayer, true, this.configuration.buttons, icp);
                this._interfaceController.actionSignal.add(this.onInterfaceControllerAction, this);
                /*
                 ReelsController creates and manages all amount of <IReel>
                 */
                var reelsControllerName = this.getControllerName("ReelsController");
                this._reelsController = new TypeScriptPhaserSlot.common[reelsControllerName](this.game, reelsControllerName, reelsLayer, true, this.configuration.symbols, this.configuration, this.game.pattern, data);
                this._reelsController.actionSignal.add(this.onReelsControllerAction, this);
                /*
                 WinningController creates and manages all stuff of showing the visual results
                 like lines, borders and so on.
                 */
                var linesControllerName = this.getControllerName("LinesController");
                this._linesController = new TypeScriptPhaserSlot.common[linesControllerName](this.game, linesControllerName, linesLayer, true, this.configuration, this.game.pattern);
                this._linesController.actionSignal.add(this.onLinesControllerAction, this);
                this._linesController.finishShowingSignal.add(this.onLinesControllerFinishShowLines, this);
                var scattersControllerName = this.getControllerName("ScattersController");
                this._scattersController = new TypeScriptPhaserSlot.common[scattersControllerName](this.game, scattersControllerName, linesLayer, true, this.configuration, this.game.pattern);
                this._scattersController.actionSignal.add(this.onScattersControllerAction, this);
                this._scattersController.finishShowingSignal.add(this.onScattersControllerFinish, this);
                /*
                 Animations controller
                 */
                var symbolsAnimationsControllerName = this.getControllerName("SymbolsAnimationController");
                this._symbolsAnimationsController = new TypeScriptPhaserSlot.common[symbolsAnimationsControllerName](this.game, symbolsAnimationsControllerName, symbolsAnimationsLayer, true, this.configuration.symbols, this.configuration, this.game.pattern, data);
                this._symbolsAnimationsController.actionSignal.add(this.onReelsControllerAction, this);
                this._symbolsAnimationsController['wildLayer'] = wildsAnimationsLayer;
                var soundControllerName = this.getControllerName("SoundController");
                this._soundController = new TypeScriptPhaserSlot.common[soundControllerName](this.game, soundControllerName, true, this.configuration, data);
                this._soundController.actionSignal.add(this.onReelsControllerAction, this);
                new common.MoneyFormatController(this.game, 'MoneyFormatController', true, this.configuration, data);
                var winningOnCenterControllerName = this.getControllerName("WinningOnCenterController");
                this._winningOnCenterController = new TypeScriptPhaserSlot.common[winningOnCenterControllerName](this.game, winningOnCenterControllerName, winningOnCenterLayersLayer, true);
                this._winningOnCenterController.actionSignal.add(this.onEmotionsControllerAction, this);
                var emotionsControllerName = this.getControllerName("EmotionsController");
                this._emotionsController = new TypeScriptPhaserSlot.common[emotionsControllerName](this.game, emotionsControllerName, emotionsLayer, true, this.configuration, this.game.pattern, data);
                this._emotionsController.actionSignal.add(this.onEmotionsControllerAction, this);
                var freeSpinsControllerName = this.getControllerName("FreeSpinsController");
                this._freeSpinsController = new TypeScriptPhaserSlot.common[freeSpinsControllerName](this.game, freeSpinsControllerName, freeSpinsLayer, true, this.configuration, this.game.pattern, data);
                this._freeSpinsController.actionSignal.add(this.onFreeSpinsControllerAction, this);
                //let paytableControllerName:string = this.getControllerName("PaytableController");
                //this._paytableController = new TypeScriptPhaserSlot.common[paytableControllerName](this.game, paytableControllerName, paytableLayer, true, this.configuration, this.game.pattern, data);
                var toolbarControllerName = this.getControllerName("ToolbarController");
                this._toolbarController = new TypeScriptPhaserSlot.common[toolbarControllerName](this.game, toolbarControllerName, toolbarLayer, true, this.configuration, this.game.pattern, data);
                this._toolbarController.actionSignal.add(this.onToolbarControllerAction, this);
                this._boongoGameRunnerController = new common.BoongoGameRunerController(this.game, 'BoongoGameRunerController', this.debugMode);
                this._boongoGameRunnerController.actionSignal.add(this.onBoongoGameRunnesControllerAction, this);
                this.correctSize(this.game.scale, this.game.scale.width, this.game.scale.height);
                //Sending game state data to the model to init a game proccess.
                this.model.onGetGameState(data);
                var paytableControllerName = this.getControllerName("PaytableController");
                this._paytableController = new TypeScriptPhaserSlot.common[paytableControllerName](this.game, paytableControllerName, paytableLayer, true, this.configuration, this.game.pattern, data);
                //Shwitch autoplay on if Freespins are here
                if (this.model.isFreespins) {
                    this.model.auto(true);
                }
                this._backgroundController['changeBackground'](this.model.isFreespins);
                this._decorationsController['changeFrame'](this.model.isFreespins);
                common.SoundController.instance.playSound('background_sound', 'music', 0.3, true);
                //this.game.camera.resetFX();
                //this.correctSize(this.game.scale, this.game.scale.width, this.game.scale.height);
                this._interfaceController['changePositions']();
            };
            /**
             * Listener of LinesController action
             * Handles the begin showing each lines to animate
             * according symbols on the reels
             * @param data
             */
            BaseGameGameController.prototype.onLinesControllerAction = function (data) {
                this._reelsController.animateSymbolsOfLine(data);
                this._symbolsAnimationsController.animateSymbolsOfLine(data, this.model.result);
                this._interfaceController.changeWinText(data.sum);
                this._winningOnCenterController.hideWinning();
                if (data.sum) {
                    this._winningOnCenterController.showWinning(data.sum);
                }
            };
            /**
             *
             */
            BaseGameGameController.prototype.onLinesControllerFinishShowLines = function () {
                this._reelsController.stopAnimation();
                this._symbolsAnimationsController.stopAnimation();
                //this.onReelsControllerAction(ReelsController.FINISH_SHOW_WINNING);
                this._scattersController.start(this.lastSpinResult);
            };
            /**
             *
             * @param data
             */
            BaseGameGameController.prototype.onScattersControllerAction = function (data) {
                this._reelsController.animateScatters(data);
                this._symbolsAnimationsController.animateScatters(data, this.model.result);
                this._interfaceController.changeWinText(data.sum || 0);
                this._winningOnCenterController.hideWinning();
                if (data.sum) {
                    this._winningOnCenterController.showWinning(data.sum);
                }
            };
            /**
             *
             */
            BaseGameGameController.prototype.onScattersControllerFinish = function () {
                this._reelsController.stopAnimation();
                this._symbolsAnimationsController.stopAnimation();
                this.onReelsControllerAction(common.ReelsController.FINISH_SHOW_WINNING);
            };
            /**
             * Listener of Reels Controller action
             * @param data
             */
            BaseGameGameController.prototype.onReelsControllerAction = function (data) {
                if (data == common.ReelsController.STOP && this.currState.name == common.StatesConstants.STOP_SPIN_STATE) {
                    this.currState.finishWork();
                }
                if (data == common.ReelsController.FINISH_SHOW_WINNING && this.currState.name == common.StatesConstants.START_SHOW_WINNING_STATE) {
                    this.currState.finishWork();
                }
                if (data == common.ReelsController.RESPIN_COMPLETE && this.currState.name == common.StatesConstants.STOP_RESPIN_STATE) {
                    this.currState.finishWork();
                }
                if (data == common.ReelsController.INTRIGUE && this.currState.name == common.StatesConstants.SPIN_STATE) {
                }
            };
            /**
             *
             * @param data
             */
            BaseGameGameController.prototype.onBoongoGameRunnesControllerAction = function (data) {
                switch (data.event) {
                    case 'error':
                        if (data.data.type == 'crit') {
                            common.SoundController.instance.muteAll(true);
                        }
                        break;
                    case 'gameMode':
                        this.game.textFactory.changeLocaleAndRefresh(window['GR'].Options.get('lang'));
                        //let sss:any = window['GR'].Storage.get('sound');
                        //SoundController.instance.muteAll(!window['GR'].Storage.get('sound'));
                        window['GR'].Storage.follow('sound', function (data) {
                            common.SoundController.instance.muteAll(!data);
                        });
                        /*switch (data.data.mode) {
                            case 'stream':
                                this._interfaceController['setGameMode']('stream');
                                break;
                            case 'replay':
                                this._interfaceController['setGameMode']('stream');
                                break;
                        }*/
                        break;
                    case 'stream':
                    case 'replay':
                        //this._paytableController.hide();
                        var targetState = '';
                        var action = '';
                        if (data.data.action.action.name == 'spin') {
                            targetState = common.StatesConstants.GET_SPIN_RESPONSE_STATE;
                            action = 'spin';
                        }
                        /*if(data.data.action.action.name == 'freespin') {
                            targetState = StatesConstants.IDLE_STATE;
                            action = 'freespin';
                        }*/
                        /*if(data.data.action.action.name == 'freespin_init') {
                            targetState = StatesConstants.IDLE_STATE;
                            action = 'freespin_init';
                        }*/
                        /*if(data.data.action.action.name == 'freespin_stop') {
                            targetState = StatesConstants.IDLE_STATE;
                            action = 'freespin_stop';
                        }*/
                        if (data.data.action.action.name == 'respin') {
                            targetState = common.StatesConstants.GET_RESPIN_RESPONSE_STATE;
                            action = 'respin';
                        }
                        this._linesController.stop();
                        this._reelsController.stopAnimation();
                        this._symbolsAnimationsController.stopAnimation();
                        if (targetState != '') {
                            while (this.currState.name != targetState) {
                                this.currState.finishWork();
                            }
                        }
                        //this.model.goToState(targetState);
                        /*switch (action) {
                            case 'spin':
                                this._boongoGameRunnerController.sayToGameRuner('play', {action: 'spin'});
                                break;
                            case 'respin':
                                this._boongoGameRunnerController.sayToGameRuner('play', {action: 'respin'});
                                break;
                            case 'freespin_init':
                                this._boongoGameRunnerController.sayToGameRuner('freespin_init');
                                break;
                        }*/
                        //this._boongoGameRunnerController.sayToGameRuner('play', {action: 'spin'});
                        this._boongoGameRunnerController.sayToGameRuner('play', { action: action });
                        break;
                    case 'available_actions':
                        break;
                    case 'round_start':
                        this.model.setBet(data.data.bet);
                        this.onInterfaceControllerAction(common.ButtonActionConstants.SPIN_ACTION);
                        break;
                    case 'round_result':
                        break;
                    case 'state':
                    case 'restart':
                        switch (data.data.mode) {
                            case 'freebet':
                                this.model.setLines(data.data.freebet.lines);
                                this.model.setBet(data.data.freebet.bet_per_line);
                                //this._interfaceController['setGameMode']('freeBet');
                                break;
                            default:
                                this.model.setLines(data.data.context.spins.lines);
                                this.model.setBet(data.data.context.spins.bet_per_line);
                                //this._interfaceController['setGameMode']('');
                                if (this.model.isAutoPlay) {
                                    this.model.auto(false);
                                }
                                break;
                        }
                        this._boongoGameRunnerController.sayToGameRuner('ready');
                        break;
                    case 'balance':
                        this.model.onBalanceChanged(data.data.balance);
                        this._interfaceController.changeBalance(this.model.coins);
                        break;
                    case 'paytable':
                        if (data.data.visible) {
                            this._paytableController.show();
                        }
                        else {
                            this._paytableController.hide();
                        }
                        break;
                    case 'onSpinButton':
                        var statesForSkip = [];
                        statesForSkip.push(common.StatesConstants.STOP_SHOW_WINNING_STATE);
                        statesForSkip.push(common.StatesConstants.START_SHOW_WINNING_STATE);
                        statesForSkip.push(common.StatesConstants.EMOTIONS_STATE);
                        if (statesForSkip.indexOf(this.currState.name) > -1) {
                            this._linesController.stop();
                            this._scattersController.stop();
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            this.currState.finishWork();
                        }
                        break;
                }
            };
            /**
             *
             * @param data
             */
            BaseGameGameController.prototype.onEmotionsControllerAction = function (data) {
                if (data == "startShowedEmotions" && this.currState.name == common.StatesConstants.EMOTIONS_STATE) {
                    this._boongoGameRunnerController.sayToGameRuner('bigwin_start', this.model.totalWin);
                }
                if (data == "stopShowedEmotions" && this.currState.name == common.StatesConstants.EMOTIONS_STATE) {
                    this._boongoGameRunnerController.sayToGameRuner('bigwin_finish');
                }
                if (data == "emotionsComplete" && this.currState.name == common.StatesConstants.EMOTIONS_STATE) {
                    this.currState.finishWork();
                }
            };
            /**
             *
             * @param data
             */
            BaseGameGameController.prototype.onFreeSpinsControllerAction = function (data) {
                switch (data.type) {
                    case 'start':
                        this.model.auto(true);
                        this.currState.finishWork();
                        break;
                    case 'added':
                        this.currState.finishWork();
                        break;
                    case 'end':
                        this.model.auto(false);
                        this.currState.finishWork();
                        break;
                }
            };
            BaseGameGameController.prototype.onBonusGameControllerAction = function (data) {
            };
            /**
             *
             * @param event
             */
            BaseGameGameController.prototype.onToolbarControllerAction = function (event) {
                switch (event) {
                    case 'exit':
                        if (window['GR']) {
                            window['GR'].Events.navigate.exit();
                            break;
                        }
                        var exitURL = 'http://google.com';
                        if (window['gameConfig'] && window['gameConfig']['lobbyPath']) {
                            exitURL = window['gameConfig']['lobbyPath'];
                        }
                        window.top.location.replace(exitURL);
                        break;
                    case 'switch_fullscreen':
                        if (this.game.scale.isFullScreen) {
                            this.game.scale.stopFullScreen();
                        }
                        else {
                            this.game.scale.startFullScreen(true);
                        }
                        window.scrollTo(0, 1);
                        this.game.stage.smoothed = true;
                        //this.game.scale.refresh();
                        break;
                    case 'all_on':
                        common.SoundController.instance.muteAll(false);
                        this._boongoGameRunnerController.sayToGameRuner('sound_changed', true);
                        break;
                    case 'music_off':
                        common.SoundController.instance.getGroup('music').mute = true;
                        break;
                    case 'all_off':
                        common.SoundController.instance.muteAll(true);
                        this._boongoGameRunnerController.sayToGameRuner('sound_changed', false);
                        break;
                }
            };
            /**
             * Listener of Interface Controller action.
             * Finish work current Model State if it's required.
             * @param action
             */
            BaseGameGameController.prototype.onInterfaceControllerAction = function (action) {
                switch (action) {
                    //case ButtonActionConstants.COLLECT_ACTION:
                    //    if (this.currentStateRequiredAction && this.currState.name == StatesConstants.STOP_SHOW_WINNING_STATE)
                    //        this.currState.finishWork();
                    //    break;
                    case common.ButtonActionConstants.MAXBET_ACTION:
                        this.model.maxBet();
                        var statesForSkip = [];
                        statesForSkip.push(common.StatesConstants.IDLE_STATE);
                        statesForSkip.push(common.StatesConstants.STOP_SHOW_WINNING_STATE);
                        statesForSkip.push(common.StatesConstants.START_SHOW_WINNING_STATE);
                        statesForSkip.push(common.StatesConstants.EMOTIONS_STATE);
                        if (!this._boongoGameRunnerController.enable) {
                            this._paytableController.hide();
                        }
                        this._interfaceController.stateChanged(this.currState.name);
                        //if (this.model.isAutoPlay) return;
                        /*if (/!*this.currentStateRequiredAction && *!/(this.currState.name == StatesConstants.IDLE_STATE ||
                        this.currState.name == StatesConstants.STOP_SHOW_WINNING_STATE || this.currState.name == StatesConstants.START_SHOW_WINNING_STATE)){*/
                        if (statesForSkip.indexOf(this.currState.name) > -1) {
                            this._linesController.stop();
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            this._boongoGameRunnerController.sayToGameRuner('gamePlay', { action: 'spin' });
                            this.currState.finishWork();
                        }
                        break;
                    case common.ButtonActionConstants.SPIN_ACTION:
                        statesForSkip = [];
                        statesForSkip.push(common.StatesConstants.IDLE_STATE);
                        statesForSkip.push(common.StatesConstants.STOP_SHOW_WINNING_STATE);
                        statesForSkip.push(common.StatesConstants.START_SHOW_WINNING_STATE);
                        statesForSkip.push(common.StatesConstants.EMOTIONS_STATE);
                        if (!this._boongoGameRunnerController.enable) {
                            this._paytableController.hide();
                        }
                        this._interfaceController.stateChanged(this.currState.name);
                        //if (this.model.isAutoPlay) return;
                        /*if (/!*this.currentStateRequiredAction && *!/(this.currState.name == StatesConstants.IDLE_STATE ||
                        this.currState.name == StatesConstants.STOP_SHOW_WINNING_STATE || this.currState.name == StatesConstants.START_SHOW_WINNING_STATE)){*/
                        if (statesForSkip.indexOf(this.currState.name) > -1) {
                            this._linesController.stop();
                            this._scattersController.stop();
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            //this._boongoGameRunnerController.sayToGameRuner('play', {action:'spin'});
                            this.currState.finishWork();
                        }
                        break;
                    case common.ButtonActionConstants.STOP_ACTION:
                        this._reelsController.urgentStop();
                        break;
                    case common.ButtonActionConstants.AUTO_ACTION:
                        this.model.auto();
                        break;
                    case common.ButtonActionConstants.BET_DEC_ACTION:
                        common.SoundController.instance.playSound('decrease_bet_sound', "default", 0.5);
                        this.model.prevBet();
                        break;
                    case common.ButtonActionConstants.BET_INC_ACTION:
                        common.SoundController.instance.playSound('increase_bet_sound', "default", 0.5);
                        this.model.nextBet();
                        break;
                    case common.ButtonActionConstants.LINES_DEC_ACTION:
                        common.SoundController.instance.playSound('decrease_bet_sound', "default", 0.5);
                        this.model.prevLine();
                        this._linesController.showLines(this.model.allowedLines[this.model.currLinesIndex]);
                        break;
                    case common.ButtonActionConstants.LINES_INC_ACTION:
                        common.SoundController.instance.playSound('increase_bet_sound', "default", 0.5);
                        this.model.nextLine();
                        this._linesController.showLines(this.model.allowedLines[this.model.currLinesIndex]);
                        break;
                    case common.ButtonActionConstants.INFO_ACTION:
                        this._paytableController.switch();
                        if (this._paytableController.isShown) {
                            this._interfaceController.stateChanged('paytable');
                        }
                        else {
                            this._interfaceController.stateChanged(this.currState.name);
                        }
                        break;
                    case "paytable_next":
                        this._paytableController.goToPage('increase');
                        break;
                    case "paytable_prev":
                        this._paytableController.goToPage('decrease');
                        break;
                }
            };
            Object.defineProperty(BaseGameGameController.prototype, "appProxy", {
                /************************************************************
                 Getters and Setters
                 *************************************************************/
                get: function () {
                    return this._appProxy;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BaseGameGameController.prototype, "url", {
                get: function () {
                    return this._url;
                },
                enumerable: true,
                configurable: true
            });
            return BaseGameGameController;
        }(common.GameController));
        common.BaseGameGameController = BaseGameGameController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/controller/InterfaceController.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/controller/InterfaceController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseGameInterfaceController = /** @class */ (function (_super) {
            __extends(BaseGameInterfaceController, _super);
            function BaseGameInterfaceController(game, name, layer, debugMode, configuration, pattern) {
                var _this = _super.call(this, game, name, layer, debugMode, configuration, pattern) || this;
                _this.gameMode = '';
                return _this;
            }
            /**
            * Initial creation
            */
            BaseGameInterfaceController.prototype.init = function () {
                this.showedWin = 0;
                this.createElements();
                this.createPopupWindow();
                _super.prototype.init.call(this);
                this.addBottomPanel();
                //this.changePositions();
                //this.game.scale.onSizeChange.add(function () {
                //    this.changePositions();
                //},this, -1);
            };
            BaseGameInterfaceController.prototype.createElements = function () {
                var bg = new Phaser.Graphics(this.game, 0, 768 - 40);
                bg.name = 'background';
                bg.beginFill(0xCCCCCC, 0.2);
                bg.drawRect(0, 0, 1366, 40);
                bg.endFill();
                this.layer.add(bg);
                this.label_balance = this.game.textFactory.getTextWithStyle("balance");
                this.label_balance.name = 'label_balance';
                this.setTextPosition(this.label_balance, this.pattern.label_balance);
                this.balanceValueText = this.game.textFactory.getTextWithStyle("0", "style1");
                this.balanceValueText.name = 'value_balance';
                this.setTextPosition(this.balanceValueText, this.pattern.value_balance);
                this.label_win = this.game.textFactory.getTextWithStyle("win");
                this.label_win.name = 'label_win';
                this.setTextPosition(this.label_win, this.pattern.label_win);
                this.winValueText = this.game.textFactory.getTextWithStyle("0", "style1");
                this.winValueText.name = 'value_win';
                this.setTextPosition(this.winValueText, this.pattern.value_win);
                this.label_bet = this.game.textFactory.getTextWithStyle("bet");
                this.label_bet.name = 'label_bet';
                this.setTextPosition(this.label_bet, this.pattern.label_bet);
                this.betValueText = this.game.textFactory.getTextWithStyle("0", "style1");
                this.betValueText.name = 'value_bet';
                this.setTextPosition(this.betValueText, this.pattern.value_bet);
                this.label_lines = this.game.textFactory.getTextWithStyle("lines");
                this.label_lines.name = 'label_lines';
                this.setTextPosition(this.label_lines, this.pattern.label_lines);
                this.linesValueText = this.game.textFactory.getTextWithStyle("0", "style1");
                this.linesValueText.name = 'value_lines';
                this.setTextPosition(this.linesValueText, this.pattern.value_lines);
                this.label_totalBet = this.game.textFactory.getTextWithStyle("totalBet");
                this.label_totalBet.name = 'label_totalBet';
                this.setTextPosition(this.label_totalBet, this.pattern.label_totalBet);
                this.totalBetValueText = this.game.textFactory.getTextWithStyle("0", "style1");
                this.totalBetValueText.name = 'value_totalBet';
                this.setTextPosition(this.totalBetValueText, this.pattern.value_totalBet);
                this.label_autoplay = this.game.textFactory.getTextWithStyle("autoplay");
                this.label_autoplay.name = 'label_autoplay';
                this.setTextPosition(this.label_autoplay, this.pattern.label_autoplay);
                this.label_autoplay.visible = false;
            };
            BaseGameInterfaceController.prototype.addBottomPanel = function () {
                this.bottomPanel = new Phaser.Group(this.game);
                this.bottomPanel.name = 'bottomPanel';
                this.layer.add(this.bottomPanel);
                var xxx = this.layer.getByName('label_balance').x;
                var childNames = ['betInc_btn', 'betDec_btn', 'label_balance', 'value_balance',
                    'label_lines', 'value_lines', 'label_bet', 'value_bet', 'label_totalBet', 'value_totalBet',
                    'label_win', 'value_win'];
                childNames.forEach(function (childName) {
                    var child = this.layer.getByName(childName);
                    if (child) {
                        this.bottomPanel.add(child);
                    }
                }, this);
                this.bottomPanel.children.forEach(function (child) {
                    child.x -= xxx;
                }, this);
                this.bottomPanel.x = xxx;
            };
            BaseGameInterfaceController.prototype.createPopupWindow = function () {
                this.popupWindow = new Phaser.Group(this.game);
                var popupWindowBackground = new Phaser.Graphics(this.game);
                popupWindowBackground.beginFill(0xffffff, 0.9);
                popupWindowBackground.drawRoundedRect(0, 0, 400, 200, 10);
                popupWindowBackground.endFill();
                var popupText = this.game.textFactory.getTextWithStyle('notEnoughMoney');
                popupText.anchor.set(0.5, 0.5);
                popupText.x = popupWindowBackground.width / 2;
                popupText.y = popupWindowBackground.height / 2;
                var buttonActions = {
                    mouseUp: {
                        action: '',
                        params: null,
                        sound: null,
                        enabledAfter: undefined
                    }
                };
                var bottonConfigurationParams = {
                    name: 'bonusStartSpin_btn',
                    className: 'BaseButton',
                    key: 'ui_buttons',
                    caption: '',
                    textStyle: 'style1',
                    overFrame: 'popupClose_btn/over',
                    outFrame: 'popupClose_btn/enabled',
                    downFrame: 'popupClose_btn/down',
                    upFrame: 'popupClose_btn/enabled',
                    disabledFrame: 'popupClose_btn/disabled',
                    onDownSoundKey: 'clicksound',
                    onOverSoundKey: '',
                    onOutSoundKey: '',
                    onUpSoundKey: ''
                };
                var buttonConfiguration = new common.ButtonConfiguration(bottonConfigurationParams);
                var popupCloseButton = new common.BaseButton(this.game, buttonConfiguration, popupWindowBackground.width, 0, this.onPopupCloseButtonClick, this);
                popupCloseButton.actions = buttonActions;
                popupCloseButton.anchor.set(0.5, 0.5);
                this.popupWindow.add(popupWindowBackground);
                this.popupWindow.add(popupText);
                this.popupWindow.add(popupCloseButton);
                this.popupWindow.pivot.set(this.popupWindow.width / 2, this.popupWindow.height / 2);
                this.popupWindow.x = 1366 / 2;
                this.popupWindow.y = 768 / 2;
                this.popupWindow.visible = false;
                this.layer.add(this.popupWindow);
            };
            BaseGameInterfaceController.prototype.setGameMode = function (data) {
                this.gameMode = data;
                this.stateChanged();
            };
            BaseGameInterfaceController.prototype.stateChanged = function (stateName) {
                _super.prototype.stateChanged.call(this, stateName);
                if (this.gameMode == 'freeBet') {
                    this.disableButton('maxBet_btn');
                    this.disableButton('betInc_btn');
                    this.disableButton('betDec_btn');
                    this.disableButton('linesInc_btn');
                    this.disableButton('linesDec_btn');
                }
                if (this.gameMode == 'stream') {
                    this.disableButton('auto_btn');
                    this.disableButton('startSpin_btn');
                    this.disableButton('stopSpin_btn');
                    this.disableButton('maxBet_btn');
                    this.disableButton('betInc_btn');
                    this.disableButton('betDec_btn');
                    this.disableButton('linesInc_btn');
                    this.disableButton('linesDec_btn');
                    this.disableButton('info_btn');
                }
            };
            BaseGameInterfaceController.prototype.onPopupCloseButtonClick = function () {
                this.hidePopupWindow();
            };
            BaseGameInterfaceController.prototype.setTextPosition = function (textField, pattern) {
                if (!textField || !pattern) {
                    return;
                }
                switch (pattern.align) {
                    case 'left':
                        textField.align = 'left';
                        textField.x = pattern.x;
                        textField.y = pattern.y;
                        textField.anchor.set(0, 0);
                        break;
                    case 'right':
                        textField.align = 'right';
                        textField.x = pattern.x + pattern.width;
                        textField.y = pattern.y;
                        textField.anchor.set(1, 0);
                        break;
                    case 'center':
                        textField.align = 'center';
                        textField.x = pattern.x + pattern.width / 2;
                        textField.y = pattern.y;
                        textField.anchor.set(0.5, 0);
                        break;
                }
                if (textField && !textField.parent) {
                    this.layer.add(textField);
                }
            };
            BaseGameInterfaceController.prototype.changePositions = function () {
                var ratio = this.game.camera.width / this.game.camera.height;
                this.layer.children.forEach(function (child) {
                    //child.x -= this.game.camera.x / this.layer.scale.x;
                    if (child.name.indexOf('_btn') > -1) {
                        child.scale.set(0.5, 0.5);
                    }
                }, this);
                this.layer.getByName('bottomPanel').children.forEach(function (child) {
                    //child.x -= this.game.camera.x / this.layer.scale.x;
                    if (child.name.indexOf('_btn') > -1) {
                        child.scale.set(0.5, 0.5);
                    }
                }, this);
            };
            BaseGameInterfaceController.prototype.showAutoplayText = function (autoplay) {
                this.label_autoplay.visible = autoplay;
            };
            /**
             *
             * @param value
             */
            BaseGameInterfaceController.prototype.changeBetText = function (value) {
                _super.prototype.changeBetText.call(this, value);
                this.betValueText.text = common.MoneyFormatController.instance.format(value, true);
            };
            /**
             *
             * @param value
             */
            BaseGameInterfaceController.prototype.changeLinesText = function (value) {
                _super.prototype.changeBetText.call(this, value);
                this.linesValueText.text = String(value);
            };
            /**
             *
             * @param value
             */
            BaseGameInterfaceController.prototype.changeTotalBetText = function (value) {
                _super.prototype.changeBetText.call(this, value);
                this.totalBetValueText.text = common.MoneyFormatController.instance.format(value, true);
            };
            /**
             *
             * @param value
             */
            BaseGameInterfaceController.prototype.changeWinText = function (value) {
                if (value == 0) {
                    return;
                }
                _super.prototype.changeWinText.call(this, value);
                var obj = {
                    winValue: 0
                };
                this.winTween = this.game.add.tween(obj).to({ winValue: value }, 500, Phaser.Easing.Linear.None, true);
                this.winTween.onUpdateCallback(function (tween) {
                    this.winValueText.text = common.MoneyFormatController.instance.format(Math.ceil(this.showedWin + tween.target.winValue), true);
                }, this);
                this.winTween.onComplete.addOnce(function (obj) {
                    this.showedWin += value;
                    this.winValueText.text = common.MoneyFormatController.instance.format(this.showedWin, true);
                }, this);
                //this.showedWin +=value;
                //this.winValueText.text = String(this.showedWin);
            };
            /**
             *
             */
            BaseGameInterfaceController.prototype.resetWinText = function () {
                _super.prototype.resetWinText.call(this);
                if (this.winTween) {
                    this.winTween.stop(false);
                }
                this.showedWin = 0;
                this.winValueText.text = "";
            };
            /**
             *
             * @param value
             */
            BaseGameInterfaceController.prototype.changeBalance = function (value) {
                if (this.gameMode == 'freeBet' && value < parseInt(this.balanceValueText.text)) {
                    return;
                }
                _super.prototype.changeBalance.call(this, value);
                this.balanceValueText.text = common.MoneyFormatController.instance.format(value, true);
            };
            BaseGameInterfaceController.prototype.showPopupWindow = function (data) {
                _super.prototype.showPopupWindow.call(this, data);
                this.popupWindow.visible = true;
            };
            BaseGameInterfaceController.prototype.hidePopupWindow = function (data) {
                _super.prototype.showPopupWindow.call(this, data);
                this.popupWindow.visible = false;
            };
            return BaseGameInterfaceController;
        }(common.InterfaceController));
        common.BaseGameInterfaceController = BaseGameInterfaceController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseGameInterfaceController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./BaseGameInterfaceController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseGameInterfaceControllerIPad = /** @class */ (function (_super) {
            __extends(BaseGameInterfaceControllerIPad, _super);
            function BaseGameInterfaceControllerIPad(game, name, layer, debugMode, configuration, pattern) {
                return _super.call(this, game, name, layer, debugMode, configuration, pattern) || this;
            }
            /**
            * Initial creation
            */
            BaseGameInterfaceControllerIPad.prototype.init = function () {
                this.layer.fixedToCamera = true;
                _super.prototype.init.call(this);
                this.game.scale.onSizeChange.add(function () {
                    this.changePositions();
                }, this, -1);
            };
            BaseGameInterfaceControllerIPad.prototype.createElements = function () {
                var bg = new Phaser.Graphics(this.game, 0, 768 - 60);
                bg.name = 'background';
                bg.beginFill(0xCCCCCC, 0.2);
                bg.drawRect(0, 0, 1366, 60);
                bg.endFill();
                this.layer.add(bg);
                this.label_balance = this.game.textFactory.getTextWithStyle("balance_ipad");
                this.label_balance.name = 'label_balance';
                this.setTextPosition(this.label_balance, this.pattern.label_balance);
                this.balanceValueText = this.game.textFactory.getTextWithStyle("0", "style1_ipad");
                this.balanceValueText.name = 'value_balance';
                this.setTextPosition(this.balanceValueText, this.pattern.value_balance);
                this.label_win = this.game.textFactory.getTextWithStyle("win_ipad");
                this.label_win.name = 'label_win';
                this.setTextPosition(this.label_win, this.pattern.label_win);
                this.winValueText = this.game.textFactory.getTextWithStyle("0", "style1_ipad");
                this.winValueText.name = 'value_win';
                this.setTextPosition(this.winValueText, this.pattern.value_win);
                this.label_bet = this.game.textFactory.getTextWithStyle("bet_ipad");
                this.label_bet.name = 'label_bet';
                this.setTextPosition(this.label_bet, this.pattern.label_bet);
                this.betValueText = this.game.textFactory.getTextWithStyle("0", "style1_ipad");
                this.betValueText.name = 'value_bet';
                this.setTextPosition(this.betValueText, this.pattern.value_bet);
                this.label_lines = this.game.textFactory.getTextWithStyle("lines_ipad");
                this.label_lines.name = 'label_lines';
                this.setTextPosition(this.label_lines, this.pattern.label_lines);
                this.linesValueText = this.game.textFactory.getTextWithStyle("0", "style1_ipad");
                this.linesValueText.name = 'value_lines';
                this.setTextPosition(this.linesValueText, this.pattern.value_lines);
                this.label_totalBet = this.game.textFactory.getTextWithStyle("totalBet_ipad");
                this.label_totalBet.name = 'label_totalBet';
                this.setTextPosition(this.label_totalBet, this.pattern.label_totalBet);
                this.totalBetValueText = this.game.textFactory.getTextWithStyle("0", "style1_ipad");
                this.totalBetValueText.name = 'value_totalBet';
                this.setTextPosition(this.totalBetValueText, this.pattern.value_totalBet);
                this.label_autoplay = this.game.textFactory.getTextWithStyle("autoplay_ipad");
                this.label_autoplay.name = 'label_autoplay';
                this.setTextPosition(this.label_autoplay, this.pattern.label_autoplay);
                this.label_autoplay.visible = false;
            };
            BaseGameInterfaceControllerIPad.prototype.changePositions = function () {
                var ratio = this.game.camera.width / this.game.camera.height;
                this.layer.children.forEach(function (child) {
                    //child.x -= this.game.camera.x / this.layer.scale.x;
                    /*if(!child['baseX']) {
                        child['baseX'] = child.x;
                    }
                    child.x = child['baseX'] + ((1366 - this.game.camera.width) / 2);*/
                    if (child.name.indexOf('_btn') > -1) {
                        child.scale.set(0.75, 0.75);
                    }
                    switch (child.name) {
                        case 'startSpin_btn':
                            child.x = 768 * ratio - /*child.width*/ 248;
                            child.y = (768 - /*child.width*/ 248) / 2;
                            child.alpha = 0.5;
                            break;
                        case 'stopSpin_btn':
                            child.x = 768 * ratio - /*child.width*/ 248;
                            child.y = (768 - /*child.width*/ 248) / 2;
                            child.alpha = 0.5;
                            break;
                        case 'maxBet_btn':
                            child.x = 768 * ratio - /*child.width*/ 146;
                            child.y = (768 - /*child.height*/ 146) / 2 + 180;
                            child.alpha = 0.5;
                            break;
                        case 'auto_btn':
                            child.x = 768 * ratio - /*child.width*/ 116;
                            child.y = (768 - /*child.height*/ 116) / 2 - 157;
                            child.alpha = 0.5;
                            break;
                        case 'info_btn':
                            child.x = 0;
                            child.y = 768 - 120;
                            child.alpha = 0.5;
                            break;
                        case 'background':
                            child.width = 768 * ratio;
                            child.x = 0;
                            break;
                        case 'bottomPanel':
                            child.x = ((768 * ratio) - 400) / 2;
                            child.children.forEach(function (button) {
                                if (button.name.indexOf('_btn') > -1) {
                                    button.scale.set(0.75, 0.75);
                                }
                            }, this);
                            break;
                    }
                }, this);
            };
            return BaseGameInterfaceControllerIPad;
        }(common.BaseGameInterfaceController));
        common.BaseGameInterfaceControllerIPad = BaseGameInterfaceControllerIPad;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./BaseGameInterfaceController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./BaseGameInterfaceController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var BaseGameInterfaceControllerMobile = /** @class */ (function (_super) {
            __extends(BaseGameInterfaceControllerMobile, _super);
            function BaseGameInterfaceControllerMobile() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
            * Initial creation
            */
            BaseGameInterfaceControllerMobile.prototype.init = function () {
                this.layer.fixedToCamera = true;
                _super.prototype.init.call(this);
                this.game.scale.onSizeChange.add(function () {
                    this.changePositions();
                }, this, -1);
            };
            BaseGameInterfaceControllerMobile.prototype.createElements = function () {
                var bg = new Phaser.Graphics(this.game, 0, 768 - 80);
                bg.name = 'background';
                bg.beginFill(0xCCCCCC, 0.2);
                bg.drawRect(0, 0, 1366, 80);
                bg.endFill();
                this.layer.add(bg);
                this.label_balance = this.game.textFactory.getTextWithStyle("balance_mobile");
                this.label_balance.name = 'label_balance';
                this.setTextPosition(this.label_balance, this.pattern.label_balance);
                this.balanceValueText = this.game.textFactory.getTextWithStyle("0", "style1_mobile");
                this.balanceValueText.name = 'value_balance';
                this.setTextPosition(this.balanceValueText, this.pattern.value_balance);
                this.label_win = this.game.textFactory.getTextWithStyle("win_mobile");
                this.label_win.name = 'label_win';
                this.setTextPosition(this.label_win, this.pattern.label_win);
                this.winValueText = this.game.textFactory.getTextWithStyle("0", "style1_mobile");
                this.winValueText.name = 'value_win';
                this.setTextPosition(this.winValueText, this.pattern.value_win);
                this.label_bet = this.game.textFactory.getTextWithStyle("bet_mobile");
                this.label_bet.name = 'label_bet';
                this.setTextPosition(this.label_bet, this.pattern.label_bet);
                this.betValueText = this.game.textFactory.getTextWithStyle("0", "style1_mobile");
                this.betValueText.name = 'value_bet';
                this.setTextPosition(this.betValueText, this.pattern.value_bet);
                this.label_lines = this.game.textFactory.getTextWithStyle("lines_mobile");
                this.label_lines.name = 'label_lines';
                this.setTextPosition(this.label_lines, this.pattern.label_lines);
                this.linesValueText = this.game.textFactory.getTextWithStyle("0", "style1_mobile");
                this.linesValueText.name = 'value_lines';
                this.setTextPosition(this.linesValueText, this.pattern.value_lines);
                this.label_totalBet = this.game.textFactory.getTextWithStyle("totalBet_mobile");
                this.label_totalBet.name = 'label_totalBet';
                this.setTextPosition(this.label_totalBet, this.pattern.label_totalBet);
                this.totalBetValueText = this.game.textFactory.getTextWithStyle("0", "style1_mobile");
                this.totalBetValueText.name = 'value_totalBet';
                this.setTextPosition(this.totalBetValueText, this.pattern.value_totalBet);
                this.label_autoplay = this.game.textFactory.getTextWithStyle("autoplay_mobile");
                this.label_autoplay.name = 'label_autoplay';
                this.setTextPosition(this.label_autoplay, this.pattern.label_autoplay);
                this.label_autoplay.visible = false;
            };
            BaseGameInterfaceControllerMobile.prototype.changePositions = function () {
                var ratio = this.game.camera.width / this.game.camera.height;
                this.layer.children.forEach(function (child) {
                    //child.x -= this.game.camera.x / this.layer.scale.x;
                    /*if(!child['baseX']) {
                        child['baseX'] = child.x;
                    }
                    child.x = child['baseX'] + ((1366 - this.game.camera.width) / 2);*/
                    switch (child.name) {
                        case 'startSpin_btn':
                            child.x = 768 * ratio - /*child.width*/ 331;
                            child.y = (768 - /*child.width*/ 331) / 2;
                            child.alpha = 0.5;
                            break;
                        case 'stopSpin_btn':
                            child.x = 768 * ratio - /*child.width*/ 331;
                            child.y = (768 - /*child.width*/ 331) / 2;
                            child.alpha = 0.5;
                            break;
                        case 'maxBet_btn':
                            child.x = 768 * ratio - /*child.width*/ 195;
                            child.y = (768 - /*child.height*/ 195) / 2 + 240;
                            child.alpha = 0.5;
                            break;
                        case 'auto_btn':
                            child.x = 768 * ratio - /*child.width*/ 155;
                            child.y = (768 - /*child.height*/ 155) / 2 - 210;
                            child.alpha = 0.5;
                            break;
                        case 'info_btn':
                            child.x = 0;
                            child.y = 768 - 160;
                            child.alpha = 0.5;
                            break;
                        case 'background':
                            child.width = 768 * ratio;
                            child.x = 0;
                            break;
                        case 'bottomPanel':
                            child.x = ((768 * ratio) - 800) / 2;
                            break;
                    }
                }, this);
            };
            return BaseGameInterfaceControllerMobile;
        }(common.BaseGameInterfaceController));
        common.BaseGameInterfaceControllerMobile = BaseGameInterfaceControllerMobile;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlResponseExamples = /** @class */ (function () {
            function DragonGirlResponseExamples() {
            }
            DragonGirlResponseExamples.res = [
                '{"status":0,"commands":[],"response":{"jackpot":{"winners":[{"id":"58c73eb18f510e72788b4567","type":"major","slotId":"","userId":"484561cc1149f25824e578b4567","winAmount":5553572380,"winTime":1489452721,"name":"Guest"}]},"result":[{"bet":1,"payout":0,"paylines":[{"index":19,"offset":0,"direction":"ltr","line":[0,2,0,2,0],"icon":9,"icon_count":3,"coin_multiplier":5},{"index":24,"offset":0,"direction":"ltr","line":[2,2,0,2,2],"icon":9,"icon_count":3,"coin_multiplier":5}],"reels":[[2,2,2],[2,2,2],[2,2,2],[2,2,2],[2,2,2]],"mult":1}],"win":0,"totalWin":0,"simpleJackpots":{"state":[],"winners":[]},"state":{"coins":1,"points":1701840,"hasBonus":false,"freespins":0},"user":{"coins":2072875,"points":1701840,"actCS":756,"pFavSpins":761}},"serverInfo":{"time":1489482597}}',
                '{"status":0,"commands":[],"response":{"jackpot":{"winners":[{"id":"58c73eb18f510e72788b4567","type":"major","slotId":"","userId":"484561cc1149f25824e578b4567","winAmount":5553572380,"winTime":1489452721,"name":"Guest"}]},"result":[{"bet":1,"payout":0,"paylines":[],"reels":[[7,100,7],[8,6,5],[100,0,0],[8,2,3],[9,8,6]],"mult":1}],"win":0,"totalWin":0,"simpleJackpots":{"state":[],"winners":[]},"state":{"coins":1,"points":1701840,"hasBonus":false,"freespins":0},"user":{"coins":2072875,"points":1701840,"actCS":756,"pFavSpins":761}},"serverInfo":{"time":1489482597}}',
                '{"status":0,"commands":[],"response":{"jackpot":{"winners":[{"id":"58c73eb18f510e72788b4567","type":"major","slotId":"","userId":"484561cc1149f25824e578b4567","winAmount":5553572380,"winTime":1489452721,"name":"Guest"}]},"result":[{"freespin_icons":{"110":[[0,0]]},"bet":1,"payout":30,"paylines":[{"index":4,"offset":0,"direction":"ltr","line":[2,1,0,1,2],"icon":8,"icon_count":3,"coin_multiplier":15},{"index":18,"offset":0,"direction":"ltr","line":[2,1,0,0,0],"icon":8,"icon_count":5,"coin_multiplier":15}],"reels":[[110,7,8],[8,8,9],[8,10,3],[7,2,8],[2,7,9]],"mult":1},{"freespin_icons":{"110":[[1,0]]},"bet":1,"payout":0,"paylines":[{"index":4,"offset":0,"direction":"ltr","line":[2,1,0,1,2],"icon":8,"icon_count":3,"coin_multiplier":15}],"reels":[[8,10,4],[110,7,6],[8,6,9],[6,10,3],[8,2,8]],"mult":1}],"win":30,"totalWin":30,"simpleJackpots":{"state":[],"winners":[]},"state":{"points":1701870,"hasBonus":false,"freespins":0},"user":{"points":1701870,"actCS":757,"pFavSpins":762,"pWinSpins":170,"coins":2072875}},"serverInfo":{"time":1489482609}}',
                '{"status":0,"commands":[],"response":{"jackpot":{"winners":[{"id":"58c73eb18f510e72788b4567","type":"major","slotId":"","userId":"484561cc1149f25824e578b4567","winAmount":5553572380,"winTime":1489452721,"name":"Guest"}]},"result":[{"freespin_icons":{"110":[[4,2]]},"bet":1,"payout":0,"paylines":[],"reels":[[9,3,7],[10,1,9],[2,4,6],[3,7,6],[9,10,110]],"mult":1}],"win":0,"totalWin":0,"simpleJackpots":{"state":[],"winners":[]},"state":{"points":1701900,"hasBonus":false,"freespins":0},"user":{"coins":2072845,"points":1701900,"actCS":758,"pFavSpins":763}},"serverInfo":{"time":1489482616}}',
                '{"status":0,"commands":[],"response":{"jackpot":{"winners":[{"id":"58c73eb18f510e72788b4567","type":"major","slotId":"","userId":"484561cc1149f25824e578b4567","winAmount":5553572380,"winTime":1489452721,"name":"Guest"}]},"result":[{"freespin_icons":{"110":[[1,0]]},"bet":1,"payout":0,"paylines":[],"reels":[[6,10,9],[110,7,8],[7,9,6],[8,10,3],[2,3,9]],"mult":1}],"win":0,"totalWin":0,"simpleJackpots":{"state":[],"winners":[]},"state":{"points":1701960,"hasBonus":false,"freespins":0},"user":{"coins":2073310,"points":1701960,"actCS":760,"pFavSpins":765}},"serverInfo":{"time":1489482853}}',
                '{"status":0,"commands":[],"response":{"jackpot":{"winners":[{"id":"58c73eb18f510e72788b4567","type":"major","slotId":"","userId":"484561cc1149f25824e578b4567","winAmount":5553572380,"winTime":1489452721,"name":"Guest"}]},"result":[{"bet":1,"payout":0,"paylines":[],"reels":[[5,6,0],[7,8,10],[4,1,6],[8,4,1],[2,6,0]],"mult":1}],"win":0,"totalWin":0,"simpleJackpots":{"state":[],"winners":[]},"state":{"points":1701990,"hasBonus":false,"freespins":0},"user":{"coins":2073280,"points":1701990,"actCS":761,"pFavSpins":766}},"serverInfo":{"time":1489482901}}',
                '{"status":0,"commands":[],"response":{"jackpot":{"winners":[{"id":"58c73eb18f510e72788b4567","type":"major","slotId":"","userId":"484561cc1149f25824e578b4567","winAmount":5553572380,"winTime":1489452721,"name":"Guest"}]},"result":[{"freespin_icons":{"110":[[2,1]]},"bet":1,"payout":10,"paylines":[{"index":19,"offset":0,"direction":"ltr","line":[0,2,0,2,0],"icon":9,"icon_count":3,"coin_multiplier":5},{"index":24,"offset":0,"direction":"ltr","line":[2,2,0,2,2],"icon":9,"icon_count":3,"coin_multiplier":5}],"reels":[[9,100,9],[5,4,9],[9,110,7],[2,6,6],[0,4,1]],"mult":1},{"freespin_icons":{"110":[[3,1]]},"bet":1,"payout":0,"paylines":[],"reels":[[8,10,5],[9,100,9],[5,4,9],[9,110,7],[2,6,6]],"mult":1}],"win":10,"totalWin":10,"simpleJackpots":{"state":[],"winners":[]},"state":{"points":1702560,"hasBonus":false,"freespins":0},"user":{"coins":2072740,"points":1702560,"actCS":780,"pFavSpins":785,"pWinSpins":174}},"serverInfo":{"time":1489483090}}'
            ];
            DragonGirlResponseExamples.getGameState = '{"status":0,"commands":[{"type":"weeklyRating.finish","prop":{"place":1,"award":{"type":"coins","amount":0}}}],"response":{"initialBet":5000,"state":{"coins":5000,"lines":30,"hasBonus":false,"bStage":0,"reels":[[2,2,2],[5,6,7],[8,9,10],[10,9,8],[7,6,5]],"freespins":0,"points":1701660,"fsTotal":0,"fsMult":1,"bonusType":"","bonusGame":[],"giftspins":0},"bets":[2000,5000],"betsPreselect":[],"lineCount":30,"lineCountFreespins":30,"paylineMap":[[1,1,1,1,1],[0,0,0,0,0],[2,2,2,2,2],[0,1,2,1,0],[2,1,0,1,2],[0,0,1,0,0],[2,2,1,2,2],[1,2,2,2,1],[1,0,0,0,1],[1,0,1,0,1],[1,2,1,2,1],[0,1,0,1,0],[2,1,2,1,2],[1,1,0,1,1],[1,1,2,1,1],[0,1,1,1,0],[2,1,1,1,2],[0,1,2,2,2],[2,1,0,0,0],[0,2,0,2,0],[2,0,2,0,2],[0,2,2,2,0],[2,0,0,0,2],[0,0,2,0,0],[2,2,0,2,2],[0,0,1,2,2],[2,2,1,0,0],[1,2,1,0,1],[1,0,1,2,1],[1,0,2,0,1]],"paylineMapFreespins":[[1,1,1,1,1],[0,0,0,0,0],[2,2,2,2,2],[0,1,2,1,0],[2,1,0,1,2],[0,0,1,0,0],[2,2,1,2,2],[1,2,2,2,1],[1,0,0,0,1],[1,0,1,0,1],[1,2,1,2,1],[0,1,0,1,0],[2,1,2,1,2],[1,1,0,1,1],[1,1,2,1,1],[0,1,1,1,0],[2,1,1,1,2],[0,1,2,2,2],[2,1,0,0,0],[0,2,0,2,0],[2,0,2,0,2],[0,2,2,2,0],[2,0,0,0,2],[0,0,2,0,0],[2,2,0,2,2],[0,0,1,2,2],[2,2,1,0,0],[1,2,1,0,1],[1,0,1,2,1],[1,0,2,0,1]],"multipliers":[1],"multipliersFreespins":[1],"paytable":{"0":[1000,200,50,0],"1":[500,90,30,0],"2":[500,90,30,0],"3":[500,90,30,0],"4":[300,75,25,0],"5":[125,50,20,0],"6":[100,40,15,0],"7":[75,20,10,0],"8":[75,20,10,0],"9":[50,10,5,0],"10":[50,10,5,0],"100":[0,0,0,0],"110":[15,10,5,0]},"user":{"coins":2073040},"reelParams":{"reels":[{"Mover":{"RotateTime":0.4},"Starter":{"TimeDelay":0.05,"DelayBeforeSpin":0,"IconDelta":0},"Rotator":{"Acceleration":35,"MaxSpeed":70,"Speed":0},"Stopper":{"DownDelta":55,"DownTime":0.35,"UpDelta":0,"UpTime":0.4,"ToDestinationTime":0}},{"Mover":{"RotateTime":0.5},"Starter":{"TimeDelay":0.05,"DelayBeforeSpin":0,"IconDelta":0},"Rotator":{"Acceleration":35,"MaxSpeed":70,"Speed":0},"Stopper":{"DownDelta":55,"DownTime":0.35,"UpDelta":0,"UpTime":0.4,"ToDestinationTime":0}},{"Mover":{"RotateTime":0.6},"Starter":{"TimeDelay":0.05,"DelayBeforeSpin":0,"IconDelta":0},"Rotator":{"Acceleration":35,"MaxSpeed":70,"Speed":0},"Stopper":{"DownDelta":55,"DownTime":0.35,"UpDelta":0,"UpTime":0.4,"ToDestinationTime":0}},{"Mover":{"RotateTime":0.7},"Starter":{"TimeDelay":0.05,"DelayBeforeSpin":0,"IconDelta":0},"Rotator":{"Acceleration":35,"MaxSpeed":70,"Speed":0},"Stopper":{"DownDelta":55,"DownTime":0.35,"UpDelta":0,"UpTime":0.4,"ToDestinationTime":0}},{"Mover":{"RotateTime":0.8},"Starter":{"TimeDelay":0.05,"DelayBeforeSpin":0,"IconDelta":0},"Rotator":{"Acceleration":35,"MaxSpeed":70,"Speed":0},"Stopper":{"DownDelta":55,"DownTime":0.35,"UpDelta":0,"UpTime":0.4,"ToDestinationTime":0}}],"intrigue":{"IntriguePrefDestroyDelay":0.2,"IntrigueDuration":2.2,"IntrigueSpeedMult":1.5}}},"serverInfo":{"time":1489482296}}';
            DragonGirlResponseExamples.spin = '{"status":{},"commands":[],"results":[{"paylines":[{"index":1,"direction":"ltr","line":[1,2,0,2,0],"symbol":3,"symbol_count":2,"win":100},{"index":2,"direction":"ltr","line":[0,2,2,2,0],"symbol":3,"symbol_count":2,"win":100}],"reels":[[0,0,0],[0,0,0],[7,2,8],[5,3,8],[4,8,5]],"mult":1,"totalWin":200}],"bet": 1,"state":{"freespins":{"total":10,"left": 4,"type":0},"points":60200,"hasBonus":false},"user":{"coins":999,"points":123456},"serverInfo":{}}';
            DragonGirlResponseExamples.collect = '{"status":{},"commands":[],"user":{"coins":999,"points":123456}}';
            DragonGirlResponseExamples.gamble = '{"win":1000,"user":{"coins":999,"points":123456}}';
            DragonGirlResponseExamples.exit = '{"status":{},"commands":[],"user":{"coins":999,"points":123456}}';
            return DragonGirlResponseExamples;
        }());
        common.DragonGirlResponseExamples = DragonGirlResponseExamples;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlSpinResult = /** @class */ (function (_super) {
            __extends(DragonGirlSpinResult, _super);
            function DragonGirlSpinResult() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            DragonGirlSpinResult.prototype.parse = function (rawData) {
                //this._freespin_icons = rawData.freespin_icons;
                //this._bet = rawData.bet;
                this._payout = rawData.winnings.total;
                this._reels = rawData.combination;
                for (var k = 0; k < rawData.additional_info.changed_princess.length; k++) {
                    var changedPrincessCoordinate = rawData.additional_info.changed_princess[k];
                    if (this._reels[changedPrincessCoordinate[0]][changedPrincessCoordinate[1]] == 12) {
                        this._reels[changedPrincessCoordinate[0]][changedPrincessCoordinate[1]] = 13;
                    }
                }
                //this._mult = rawData.mult;
                this._paylines = [];
                this._scatters = [];
                for (var i = 0; i < rawData.winnings.info.length; i++) {
                    var winItem = rawData.winnings.info[i];
                    if (winItem.type == 'line') {
                        this._paylines.push(new PrincessFrogPayline(rawData.winnings.info[i]));
                    }
                    if (winItem.type == 'scatter') {
                        this._scatters.push(new common.Scatter(rawData.winnings.info[i]));
                    }
                }
                this._rawData = rawData;
                /*this._freespin_icons = rawData.freespin_icons;
                this._bet = rawData.bet;
                this._payout = rawData.payout;
                this._reels = rawData.reels;
                this._mult = rawData.mult;
                this._paylines = [];
                for (let i: number = 0; i < rawData.paylines.length; i++) {
                    this._paylines.push(new Payline(rawData.paylines[i]));
                }*/
                return this;
            };
            return DragonGirlSpinResult;
        }(common.SpinResult));
        common.DragonGirlSpinResult = DragonGirlSpinResult;
        var PrincessFrogPayline = /** @class */ (function (_super) {
            __extends(PrincessFrogPayline, _super);
            function PrincessFrogPayline() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            PrincessFrogPayline.prototype.parse = function (rawData) {
                this._index = rawData.line_number;
                this._offset = 0;
                this._direction = "left";
                this._line = [];
                for (var i = 0; i < 5; i++) {
                    var dataForPush = 0;
                    if (rawData.indexes[i]) {
                        dataForPush = rawData.indexes[i][1];
                    }
                    this._line.push(dataForPush);
                }
                this._icon = 1;
                this._icon_count = rawData.indexes.length;
                this._coin_multiplier = rawData.multiplier;
                this._sum = rawData.sum;
                /* this._index = rawData.index;
                 this._offset = rawData.offset;
                 this._direction = rawData.direction;
                 this._line = rawData.line;
                 this._icon = rawData.icon;
                 this._icon_count = rawData.icon_count;
                 this._coin_multiplier = rawData.coin_multiplier;*/
                return this;
            };
            return PrincessFrogPayline;
        }(common.Payline));
        common.PrincessFrogPayline = PrincessFrogPayline;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/controller/BaseController.ts" />
// We can get session right here: http://test-api.playvulkan.com/get-session
var TypeScriptPhaserSlot;
///<reference path="../../../engine/controller/BaseController.ts" />
// We can get session right here: http://test-api.playvulkan.com/get-session
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlAPIController = /** @class */ (function (_super) {
            __extends(DragonGirlAPIController, _super);
            function DragonGirlAPIController(url, name, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, name, debugMode) || this;
                _this.isLocal = false;
                _this.sessionId = '';
                _this.userid = '';
                _this._gameList_signal = new Phaser.Signal();
                _this._gameState_signal = new Phaser.Signal();
                _this._spin_signal = new Phaser.Signal();
                _this._collect_signal = new Phaser.Signal();
                _this._gamble_signal = new Phaser.Signal();
                _this._exit_signal = new Phaser.Signal();
                _this._bonusGame_signal = new Phaser.Signal();
                _this.spinCounter = 0;
                _this.sessionId = window['gameConfig']['session_id'] || '';
                _this.additionlInfo = {
                    api_url: 'stage.lemurgamesystem.com'
                };
                _this.getAdditionalInfo();
                return _this;
            }
            DragonGirlAPIController.prototype.getAdditionalInfo = function () {
                if (window['gameConfig']['additional_info']) {
                    var parseData = this.decodeData(decodeURIComponent(window['gameConfig']['additional_info']));
                    this.additionlInfo = JSON.parse(parseData);
                }
            };
            DragonGirlAPIController.prototype.encodeData = function (data) {
                var keyHEX = CryptoJS.enc.Utf8.parse('MoZfigwm');
                var encryptedString = CryptoJS.DES.encrypt(data, keyHEX, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 });
                return encryptedString.toString();
            };
            DragonGirlAPIController.prototype.decodeData = function (data) {
                var keyHEX = CryptoJS.enc.Utf8.parse('MoZfigwm');
                var dataStr = CryptoJS.DES.decrypt({ ciphertext: CryptoJS.enc.Base64.parse(data) }, keyHEX, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 });
                return dataStr.toString(CryptoJS.enc.Utf8);
            };
            DragonGirlAPIController.prototype.getHash = function (data) {
                return CryptoJS.MD5(data).toString();
                //let sss:any = CryptoJS.MD5(data);
                //return (sss as CryptoJS.WordArray).toString(CryptoJS.enc.Hex);
            };
            DragonGirlAPIController.prototype.getParams = function (data) {
                var arr = [];
                arr.push('data=' + encodeURIComponent(data.data));
                arr.push('hash=' + data.hash);
                return arr.join('&');
            };
            DragonGirlAPIController.prototype.createParams = function (data) {
                var jsonString = JSON.stringify(data);
                var encryptedData = this.encodeData(jsonString);
                var hash = this.getHash(jsonString);
                return this.getParams({ data: encryptedData, hash: hash });
            };
            /**
             * Server requests
             */
            //Making a connection to server
            DragonGirlAPIController.prototype.getGameList = function (data) {
                //send request to server
                //------------------------
                //send signal to ApplicationProxy.ts
                this.gameList_signal.dispatch();
            };
            DragonGirlAPIController.prototype.getGameState = function (gameName) {
                if (!this.isLocal) {
                    this.gameName = gameName;
                    var oReq = new XMLHttpRequest();
                    oReq.onload = this.onGetGameState.bind(this);
                    oReq.onerror = this.onError.bind(this);
                    oReq.open('post', 'https://' + this.additionlInfo.api_url + '/v1/games/dragon_girl/init', true);
                    oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    var paramsObj = {
                        session_id: this.sessionId,
                        platform_id: 2
                    };
                    var params = this.createParams(paramsObj);
                    //let data:string[] = [];
                    //data.push('session_id=' + this.sessionId);
                    //data.push('platform_id=2');
                    //let params: String = data.join('&');
                    oReq.send(params);
                }
                else {
                    this.onGetGameState();
                }
            };
            DragonGirlAPIController.prototype.onGetGameState = function (e) {
                var parseData = e ? e.target.response : common.DragonGirlResponseExamples.getGameState;
                parseData = this.decodeData(JSON.parse(parseData).data);
                this.debuglog("onGetGameState: " + parseData);
                var jsonObject = JSON.parse(parseData);
                this.gameState_signal.dispatch(new common.DragonGirlGameStateResponse(jsonObject));
            };
            DragonGirlAPIController.prototype.onError = function (e) {
                //window.location.href = 'http://test-api.playvulkan.com/get-session';
            };
            /**
             *
             * @param data
             */
            DragonGirlAPIController.prototype.spin = function (data) {
                if (!this.isLocal) {
                    var oReq = new XMLHttpRequest();
                    oReq.onload = this.onSpin.bind(this);
                    oReq.onerror = this.onError;
                    oReq.open('post', 'https://' + this.additionlInfo.api_url + '/v1/games/dragon_girl/events/spins', true);
                    oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    //console.log("spin" + req);
                    var paramsObj = {
                        session_id: this.sessionId,
                        bet_on_line: data.bet,
                        lines_amount: data.lines,
                        coins: 1,
                        denomination: data.denomination,
                    };
                    if (window.hasOwnProperty('shift') && window['shift'] != null) {
                        paramsObj['shifter_combination'] = window['shift'];
                        window['shift'] = null;
                    }
                    var params = this.createParams(paramsObj);
                    oReq.send(params);
                }
                else {
                    this.onSpin();
                }
            };
            DragonGirlAPIController.prototype.respin = function (data) {
                if (!this.isLocal) {
                    var oReq = new XMLHttpRequest();
                    oReq.onload = this.onSpin.bind(this);
                    oReq.onerror = this.onError;
                    oReq.open('post', 'https://' + this.additionlInfo.api_url + '/v1/games/dragon_girl/events/bonuses/automatic', true);
                    oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    //console.log("spin" + req);
                    var paramsObj = {
                        session_id: this.sessionId
                    };
                    var params = this.createParams(paramsObj);
                    //let data:string[] = [];
                    //data.push('session_id=' + this.sessionId);
                    //let params: String = data.join('&');
                    oReq.send(params);
                }
                else {
                    this.onSpin();
                }
            };
            DragonGirlAPIController.prototype.freespin = function (data) {
                if (!this.isLocal) {
                    var oReq = new XMLHttpRequest();
                    oReq.onload = this.onSpin.bind(this);
                    oReq.onerror = this.onError;
                    oReq.open('post', 'https://' + this.additionlInfo.api_url + '/v1/games/dragon_girl/events/bonuses/freespins', true);
                    oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    //console.log("spin" + req);
                    var paramsObj = {
                        session_id: this.sessionId
                    };
                    /*if(window.hasOwnProperty('shift') && window['shift'] != null) {
                        paramsObj['shifter_combination'] = window['shift'];
                        window['shift'] = null;
                    }*/
                    var params = this.createParams(paramsObj);
                    //let data:string[] = [];
                    //data.push('session_id=' + this.sessionId);
                    //let params: String = data.join('&');
                    oReq.send(params);
                }
                else {
                    this.onSpin();
                }
            };
            /**
             *
             * @param {any} e?
             */
            DragonGirlAPIController.prototype.onSpin = function (e) {
                var parseData = e ? e.target.response : common.DragonGirlResponseExamples.res[this.spinCounter];
                this.spinCounter = this.spinCounter + 1 < common.DragonGirlResponseExamples.res.length ? this.spinCounter + 1 : 0;
                parseData = this.decodeData(JSON.parse(parseData).data);
                console.log("onSpin" + parseData);
                var jsonObject = JSON.parse(parseData);
                this.spin_signal.dispatch(new common.DragonGirlSpinResponse(jsonObject));
            };
            /**
             *
             * @param {string} bonusType
             */
            DragonGirlAPIController.prototype.bonus = function (bonusType) {
                var _this = this;
                if (!this.isLocal) {
                    var oReq = new XMLHttpRequest();
                    oReq.onload = function (event) {
                        _this.onBonus(event);
                    };
                    oReq.onerror = this.onError;
                    var req = 'https://ncs-local.fishsticksgames.com/Slot/' + this.gameName + '/bonusGame?userId=' + this.userid + '&debug=1&gameType=' + bonusType;
                    oReq.open('get', req, true);
                    this.debuglog("bonus" + req);
                    oReq.send();
                }
                else {
                    this.onSpin();
                }
            };
            /**
             *
             * @param {any} e?
             */
            DragonGirlAPIController.prototype.onBonus = function (e) {
                var jsonObject;
                if (!this.isLocal) {
                    this.debuglog("onBonus" + e.target.response);
                    jsonObject = JSON.parse(e.target.response);
                    this.debuglog("onSpin" + jsonObject);
                }
                else {
                    var str = common.DragonGirlResponseExamples.res[this.spinCounter];
                    jsonObject = JSON.parse(str);
                }
                this._bonusGame_signal.dispatch(new common.BonusResponse(jsonObject));
            };
            //
            DragonGirlAPIController.prototype.collect = function (data) {
                //send request to server
                //------------------------
                this.debuglog(common.DragonGirlResponseExamples.collect);
                //send signal to ApplicationProxy.ts
                var jsonObject = JSON.parse(common.DragonGirlResponseExamples.collect);
                this.collect_signal.dispatch(jsonObject);
            };
            //
            DragonGirlAPIController.prototype.gamble = function (data) {
                //send request to server
                //------------------------
                //send signal to ApplicationProxy.ts
                var jsonObject = JSON.parse(common.DragonGirlResponseExamples.gamble);
                this.gamble_signal.dispatch(jsonObject);
            };
            //
            DragonGirlAPIController.prototype.exit = function (data) {
                //send request to server
                //------------------------
                //send signal to ApplicationProxy.ts
                var jsonObject = JSON.parse(common.DragonGirlResponseExamples.exit);
                this.exit_signal.dispatch(jsonObject);
            };
            Object.defineProperty(DragonGirlAPIController.prototype, "gameList_signal", {
                /************************************************************
                 Getters and Setters
                 *************************************************************/
                get: function () {
                    return this._gameList_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlAPIController.prototype, "gameState_signal", {
                get: function () {
                    return this._gameState_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlAPIController.prototype, "spin_signal", {
                get: function () {
                    return this._spin_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlAPIController.prototype, "collect_signal", {
                get: function () {
                    return this._collect_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlAPIController.prototype, "gamble_signal", {
                get: function () {
                    return this._gamble_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlAPIController.prototype, "exit_signal", {
                get: function () {
                    return this._exit_signal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlAPIController.prototype, "bonusGame_signal", {
                get: function () {
                    return this._bonusGame_signal;
                },
                enumerable: true,
                configurable: true
            });
            return DragonGirlAPIController;
        }(common.BaseController));
        common.DragonGirlAPIController = DragonGirlAPIController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../baseGame/controllers/BaseGameBackController.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../baseGame/controllers/BaseGameBackController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlBackController = /** @class */ (function (_super) {
            __extends(DragonGirlBackController, _super);
            function DragonGirlBackController(game, name, layer, debugMode, pattern) {
                return _super.call(this, game, name, layer, debugMode, pattern) || this;
            }
            /**
            * Building reel views
            */
            DragonGirlBackController.prototype.init = function (pattern) {
                this.view = new common.SimpleSprite(this.game, pattern.background.x, pattern.background.y, pattern.background.name, this.debugMode);
                this.layer.add(this.view);
                this.freeSpinsBG = new common.SimpleSprite(this.game, pattern.background.x, pattern.background.y, 'background_freeSpins', this.debugMode);
                this.freeSpinsBG.visible = false;
                this.layer.add(this.freeSpinsBG);
                //this.layer.add(new Phaser.Image(this.game, pattern.usual_frame_bg.x, pattern.usual_frame_bg.y, pattern.usual_frame_bg.name));
            };
            /**
             *
             * @param freeSpins
             */
            DragonGirlBackController.prototype.changeBackground = function (freeSpins) {
                var tween;
                this.freeSpinsBG.visible = true;
                if (freeSpins) {
                    this.freeSpinsBG.alpha = 0;
                    tween = this.game.add.tween(this.freeSpinsBG).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                    tween.onComplete.add(function () {
                        this.view.visible = false;
                        common.SoundController.instance.playSound('fs_backsnd', 'default', 0.4, true);
                    }, this);
                }
                else {
                    this.view.visible = true;
                    tween = this.game.add.tween(this.freeSpinsBG).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                    tween.onComplete.add(function () {
                        this.freeSpinsBG.visible = false;
                        var stopSound = common.SoundController.instance.getSound("fs_backsnd", 'default');
                        if (stopSound) {
                            stopSound.stop();
                        }
                    }, this);
                }
            };
            return DragonGirlBackController;
        }(common.BaseGameBackController));
        common.DragonGirlBackController = DragonGirlBackController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/controller/interfaces/IActionController.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/controller/interfaces/IActionController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlDecorationsController = /** @class */ (function (_super) {
            __extends(DragonGirlDecorationsController, _super);
            function DragonGirlDecorationsController(game, name, layer, debugMode, pattern) {
                return _super.call(this, game, name, layer, debugMode, pattern) || this;
            }
            DragonGirlDecorationsController.prototype.init = function (pattern) {
                this.frame = new common.SimpleSprite(this.game, pattern.usual_frame.x, pattern.usual_frame.y, pattern.usual_frame.name, this.debugMode);
                this.layer.add(this.frame);
                this.freeSpinsFrame = new common.SimpleSprite(this.game, pattern.x, pattern.y, 'usual_frame_freeSpins', this.debugMode);
                this.freeSpinsFrame.visible = false;
                this.layer.add(this.freeSpinsFrame);
                var woodLeft = new common.SimpleSprite(this.game, pattern.wood_left.x, pattern.wood_left.y, 'decoration_wood', this.debugMode);
                woodLeft.scale.x = -1;
                this.layer.add(woodLeft);
                var woodRight = new common.SimpleSprite(this.game, pattern.wood_right.x, pattern.wood_right.y, 'decoration_wood', this.debugMode);
                this.layer.add(woodRight);
                var locale = this.game.textFactory.currLocale;
                this.logo = new common.SimpleSprite(this.game, 670, 64, 'game_logo', this.debugMode, 'logo_' + locale);
                this.logo.anchor.set(0.5, 0.5);
                this.layer.add(this.logo);
                this.game.textFactory.actionSignal.add(this.onLocaleChanged, this);
            };
            DragonGirlDecorationsController.prototype.onLocaleChanged = function (data) {
                this.logo.frameName = 'logo_' + data.data;
            };
            DragonGirlDecorationsController.prototype.changeFrame = function (freeSpins) {
                var tween;
                this.freeSpinsFrame.visible = true;
                if (freeSpins) {
                    this.freeSpinsFrame.alpha = 0;
                    tween = this.game.add.tween(this.freeSpinsFrame).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                    tween.onComplete.add(function () {
                        this.frame.visible = false;
                    }, this);
                }
                else {
                    this.frame.visible = true;
                    tween = this.game.add.tween(this.freeSpinsFrame).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true, 0, 0);
                    tween.onComplete.add(function () {
                        this.freeSpinsFrame.visible = false;
                    }, this);
                }
            };
            return DragonGirlDecorationsController;
        }(common.BaseGameDecorationsController));
        common.DragonGirlDecorationsController = DragonGirlDecorationsController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/controller/FreeSpinsController.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/controller/FreeSpinsController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlFreeSpinsController = /** @class */ (function (_super) {
            __extends(DragonGirlFreeSpinsController, _super);
            function DragonGirlFreeSpinsController(game, name, layer, debugMode, gameConfiguration, pattern, gameStateResponse) {
                return _super.call(this, game, name, layer, debugMode, gameConfiguration, pattern, gameStateResponse) || this;
            }
            DragonGirlFreeSpinsController.prototype.init = function () {
                _super.prototype.init.call(this);
            };
            /**
             *
             * @param count
             */
            DragonGirlFreeSpinsController.prototype.startFreeSpins = function (count) {
                var text = this.game.textFactory.getTextWithStyle("start_free_spins", "FREE_SPINS_TEXT_style");
                text.anchor.set(0.5, 0.5);
                text.x = 683;
                text.y = 384;
                text.text = count + text.text;
                text.alpha = 0;
                this.layer.add(text);
                var tween = this.game.add.tween(text).to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0, 0, true);
                tween.yoyoDelay(1000);
                tween.onComplete.addOnce(function () {
                    text.destroy(true);
                    this.actionSignal.dispatch({ type: 'start' });
                }, this);
            };
            /**
             *
             * @param count
             */
            DragonGirlFreeSpinsController.prototype.addedFreeSpins = function (count) {
                var text = this.game.textFactory.getTextWithStyle("added_free_spins", "FREE_SPINS_TEXT_style");
                text.anchor.set(0.5, 0.5);
                text.x = 683;
                text.y = 384;
                text.text = count + text.text;
                text.alpha = 0;
                this.layer.add(text);
                var tween = this.game.add.tween(text).to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0, 0, true);
                tween.yoyoDelay(1000);
                tween.onComplete.addOnce(function () {
                    text.destroy(true);
                    this.actionSignal.dispatch({ type: 'added' });
                }, this);
            };
            /**
             *
             */
            DragonGirlFreeSpinsController.prototype.endOfFreeSpins = function (data) {
                var text = this.game.textFactory.getTextWithStyle("end_free_spins", "FREE_SPINS_TEXT_style");
                text.anchor.set(0.5, 0.5);
                text.x = 683;
                text.y = 384;
                text.alpha = 0;
                text.text += common.MoneyFormatController.instance.format(data, true);
                this.layer.add(text);
                var tween = this.game.add.tween(text).to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0, 0, true);
                tween.yoyoDelay(1000);
                tween.onComplete.addOnce(function () {
                    text.destroy(true);
                    this.actionSignal.dispatch({ type: 'end' });
                }, this);
            };
            /**
             *
             * @param left
             */
            DragonGirlFreeSpinsController.prototype.leftFreeSpins = function (left) {
                var text = this.game.textFactory.getTextWithStyle("left_free_spins", "FREE_SPINS_TEXT_style");
                text.anchor.set(0.5, 0.5);
                text.x = 683;
                text.y = 384;
                var textString = (left - 1) + text.text;
                if (left == 1) {
                    textString = "Last Free Spin";
                }
                text.text = textString;
                text.alpha = 0;
                this.layer.add(text);
                var tween = this.game.add.tween(text).to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0, 0, true);
                tween.yoyoDelay(1000);
                tween.onComplete.addOnce(function () {
                    text.destroy(true);
                }, this);
            };
            return DragonGirlFreeSpinsController;
        }(common.FreeSpinsController));
        common.DragonGirlFreeSpinsController = DragonGirlFreeSpinsController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/view/reels/BaseReelSymbol.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/view/reels/BaseReelSymbol.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlSymbol = /** @class */ (function (_super) {
            __extends(DragonGirlSymbol, _super);
            function DragonGirlSymbol(game, x, y, symbolProperties, debugMode) {
                return _super.call(this, game, x, y, symbolProperties, debugMode) || this;
            }
            DragonGirlSymbol.prototype.init = function () {
                _super.prototype.init.call(this);
                //this.symbolImage.x -= 11.5;
                //this.symbolImage.y -= 7.5;
                this.symbolImage.x -= this.currentSymbolProperties.pivot.x;
                this.symbolImage.y -= this.currentSymbolProperties.pivot.y;
                this._disabledFrame = new common.SimpleSprite(this.game, 0, 0, 'symbols_disabled', this.debugMode, this.currentSymbolProperties.key + '_disabled');
                this._disabledFrame.x -= this.currentSymbolProperties.pivot.x;
                this._disabledFrame.y -= this.currentSymbolProperties.pivot.y;
                this._disabledFrame.visible = false;
                this.add(this._disabledFrame);
                this._blurredFrame = new common.SimpleSprite(this.game, 0, 0, 'symbols_blurred', this.debugMode, this.currentSymbolProperties.key + '_blurred');
                this._blurredFrame.x -= this.currentSymbolProperties.pivot.x;
                this._blurredFrame.y -= this.currentSymbolProperties.pivot.y;
                this._blurredFrame.visible = false;
                this.add(this._blurredFrame);
            };
            DragonGirlSymbol.prototype.changeKey = function (game, symbolProperties) {
                this.ownIndex = symbolProperties.index;
                this.symbolImage.frameName = symbolProperties.key;
                this._disabledFrame.frameName = symbolProperties.key + '_disabled';
                this._blurredFrame.frameName = symbolProperties.key + '_blurred';
                this.game = game;
                this._borderKey = symbolProperties.borderKeys[0];
                this._currentSymbolProperties = symbolProperties;
                this.symbolImage.x = -this.currentSymbolProperties.pivot.x;
                this.symbolImage.y = -this.currentSymbolProperties.pivot.y;
                this._disabledFrame.x = -this.currentSymbolProperties.pivot.x;
                this._disabledFrame.y = -this.currentSymbolProperties.pivot.y;
                this._disabledFrame.visible = false;
                this._blurredFrame.visible = false;
            };
            DragonGirlSymbol.prototype.activate = function () {
                if (this.symbolImage.frameName == 'icon_empty') {
                    this._disabledFrame.visible = false;
                    return;
                }
                //this._disabledFrame.visible = false;
                this.game.tweens.removeFrom(this._disabledFrame, true);
                this._disabledFrame.alpha = 1;
                this.game.add.tween(this._disabledFrame).to({ alpha: 0 }, 300, "Linear", true).onComplete.addOnce(function () {
                    this._disabledFrame.visible = false;
                }.bind(this));
                /*if(this.scale.x == 1) {
                    return;
                }
                this.game.tweens.removeFrom(this, true);
                this.game.add.tween(this).to({ alpha: 1 }, 100, "Linear", true);
                this.game.add.tween(this.scale).to({ x: 1, y: 1 }, 100, "Linear", true);
                let delta:number = ((1 - this.scale.x) * 342) / 2;
                this.game.add.tween(this).to({ x: this.x - delta, y: this.y - delta }, 100, "Linear", true);*/
            };
            DragonGirlSymbol.prototype.deactivate = function () {
                if (this.symbolImage.frameName == 'icon_empty') {
                    this._disabledFrame.visible = false;
                    return;
                }
                this._disabledFrame.visible = true;
                this.game.tweens.removeFrom(this._disabledFrame, true);
                this._disabledFrame.alpha = 0;
                this.game.add.tween(this._disabledFrame).to({ alpha: 1 }, 300, "Linear", true);
                /*if(this.scale.x != 1) {
                    return;
                }
                let newScale:number = 0.9;
                this.game.tweens.removeFrom(this, true);
                this.game.add.tween(this).to({ alpha: 0.9 }, 100, "Linear", true);
                this.game.add.tween(this.scale).to({ x: newScale, y: newScale }, 100, "Linear", true);
                let delta:number = ((this.scale.x - newScale) * 342) / 2;
                this.game.add.tween(this).to({ x: this.x + delta, y: this.y + delta }, 100, "Linear", true);*/
            };
            DragonGirlSymbol.prototype.actionOnStopReel = function () {
                if (this.symbolImage.frameName == 'icon_witch_3') {
                    var text_2 = new common.SimpleSprite(this.game, 0, 0, 'symbols', this.debugMode, 'wild_txt');
                    text_2.x = 10;
                    text_2.y = 110;
                    text_2.alpha = 0;
                    this.game.add.tween(text_2).to({ alpha: 1 }, 500, "Linear", true, 0, 0, true).onComplete.add(function () {
                        text_2.parent.removeChild(text_2);
                        text_2.destroy(true);
                    }.bind(this));
                    this.addChild(text_2);
                }
            };
            DragonGirlSymbol.prototype.transformToWild = function () {
                var data = {};
                data.key = 'transform_to_wild';
                data.scale = { x: 1, y: 1 };
                data.pivot = { x: 50, y: 46 };
                data.anchor = { x: 0, y: 0 };
                data.animations = [
                    {
                        name: 'start',
                        startFrame: 1,
                        endFrame: 36,
                        isLoop: false,
                        frameRate: 30
                    }
                ];
                var params = new common.AnimationConfiguration(data);
                var animation = new common.AnimatedSprite(this.game, 0, 0, params, this.debugMode);
                animation.actionSignal.add(function () {
                    animation.parent.removeChild(animation);
                    this.symbolImage.visible = true;
                }, this);
                animation.start();
                this.addChild(animation);
                this.symbolImage.visible = false;
            };
            DragonGirlSymbol.prototype.blur = function () {
                this.symbolImage.visible = false;
                this._blurredFrame.visible = true;
            };
            DragonGirlSymbol.prototype.unblur = function () {
                this.symbolImage.visible = true;
                this._blurredFrame.visible = false;
            };
            return DragonGirlSymbol;
        }(common.BaseReelSymbol));
        common.DragonGirlSymbol = DragonGirlSymbol;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/view/reels/BaseReelView.ts" />
///<reference path="../../../engine/view/reels/ISymbol.ts" />
///<reference path="./DragonGirlSymbol.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/view/reels/BaseReelView.ts" />
///<reference path="../../../engine/view/reels/ISymbol.ts" />
///<reference path="./DragonGirlSymbol.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlReelView = /** @class */ (function (_super) {
            __extends(DragonGirlReelView, _super);
            function DragonGirlReelView(game, index, name, debugMode, symbolProperties, gameConfig, pattern, data, mapReel, mapReelFreespins, borderLayer) {
                var _this = _super.call(this, game, index, name, debugMode, symbolProperties, gameConfig, pattern, data, mapReel, mapReelFreespins, borderLayer) || this;
                _this.symbolHeight = _this.symbolProperties[0].height;
                _this.mapReelFreespins = mapReelFreespins ? mapReelFreespins : [];
                _this.mapReel = mapReel ? mapReel : [];
                _this.mapReelFreespinsShift = 0;
                _this.mapReelShift = 0;
                _this.topVisibledLimit = 0;
                _this.setRegularMap();
                _this.symbolHeight = _this.gameConfig.standartSymbolSetting.height;
                _this.bottomVisibleLimit = _this.symbolHeight * _this._reelConfig.visibledSymbols;
                _this.bottomRemovedLimit = _this.symbolHeight * (_this._reelConfig.visibledSymbols + 1);
                var symbolsCount = [24, 36, 49, 58, 70];
                _this.generateSymbolsBand(symbolsCount[index], 4, 11);
                _this.updater = _this.emptyFunction;
                _this.init();
                return _this;
            }
            DragonGirlReelView.prototype.init = function () {
                this._symbols = [];
                //this.currentShift = 0;
                for (var i = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                    this.initData.unshift(this.getNextKey());
                }
                for (var j = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                    this.initData.push(this.getNextKey());
                }
                var addWild = this.checkForWild(this.initData);
                for (var k = addWild.startIndex; k < addWild.startIndex + addWild.changeCount; k++) {
                    this.initData[k] = 10;
                }
                this.addSymbols(this.initData);
                var wildIndex = this.initData.indexOf(10);
                this.transformWild(wildIndex);
                for (var k = 0; k < this._symbols.length; k++) {
                    this._symbols[k].y -= this.symbolHeight * this._reelConfig.topInvisibleSymbolsCount;
                }
            };
            /**
             *
             */
            DragonGirlReelView.prototype.generateSymbolsBand = function (length, minNumber, maxNumber) {
                //super.generateSymbolsBand(length, minNumber, maxNumber);
                for (var i = 0; i < length; i++) {
                    this.mapReel.push(this.getRandomKey(minNumber, maxNumber));
                }
                this.mapReel.forEach(function (symbolIndex, index, array) {
                    if (symbolIndex == 10) {
                        array.splice(index, 1);
                    }
                }, this);
                this.mapReel.push(10);
                this.mapReel.push(10);
                this.mapReel.push(10);
            };
            /**
             * Overriden function called by ReelsController to begin rolling
             * @param {any} data
             */
            DragonGirlReelView.prototype.start = function (data) {
                /*this.mapReel = [];
                let symbolsCount:number[] = [24, 36, 49, 58, 70];
                this.generateSymbolsBand(symbolsCount[this.index], 4, this.symbolProperties.length);*/
                this.mapReel.forEach(function (symbolIndex, index, arr) {
                    if (symbolIndex == 100 || symbolIndex == 101 || symbolIndex == 102) {
                        arr[index] = 10;
                    }
                }, this);
                this.currentMap = this.mapReel;
                _super.prototype.start.call(this, data);
                this.tweenToTop();
            };
            /**
             *
             * @param reel
             * @returns {any}
             */
            DragonGirlReelView.prototype.checkForWild = function (reel) {
                var checkResult = {};
                checkResult.changeCount = 0;
                checkResult.startIndex = 0;
                var count = 3;
                var startIndex = reel.indexOf(10);
                var index = startIndex;
                while (reel[index] == 10) {
                    count--;
                    index++;
                }
                checkResult.changeCount = count == 3 ? 0 : count;
                checkResult.startIndex = startIndex == this.reelConfig.topInvisibleSymbolsCount ? this.reelConfig.topInvisibleSymbolsCount - checkResult.changeCount : this.reelConfig.topInvisibleSymbolsCount + this.reelConfig.visibledSymbols;
                return checkResult;
            };
            /**
             *
             * @param data
             * @param additionalSymbols
             */
            DragonGirlReelView.prototype.realStop = function (data, additionalSymbols) {
                this.updater = null;
                this.game.tweens.removeFrom(this.tweenedObject);
                this.resultData = data;
                this.currentShift = 0;
                for (var i = 0; i < this._reelConfig.topInvisibleSymbolsCount; i++) {
                    this.resultData.unshift(this.getNextKey());
                }
                for (var j = 0; j < this._reelConfig.bottomInvisibleSymbolsCount; j++) {
                    this.resultData.push(this.getNextKey());
                }
                for (var j = 0; j < additionalSymbols; j++) {
                    this.resultData.push(this.getNextKey());
                }
                var addWild = this.checkForWild(this.resultData);
                for (var k = addWild.startIndex; k < addWild.startIndex + addWild.changeCount; k++) {
                    this.resultData[k] = 10;
                }
                this.addSymbols(this.resultData, true);
                var wildIndex = this.resultData.indexOf(10);
                this.transformWild(wildIndex);
                this.resultedTween();
            };
            /**
             *
             */
            DragonGirlReelView.prototype.addNewSymbolsInStaticRolling = function () {
                var nextKeys = this.getNextKeys(1);
                this.addSymbols(nextKeys, true);
                if (nextKeys[0] == 10) {
                    this.transformWild(0);
                    this._symbols.forEach(function (symbol) {
                        symbol.blur();
                    }, this);
                }
            };
            /**
             *
             * @param startIndex
             */
            DragonGirlReelView.prototype.transformWild = function (startIndex) {
                if (startIndex === void 0) { startIndex = 0; }
                if (startIndex == -1) {
                    return;
                }
                var count = 0;
                var indexes = [10, 100, 101, 102];
                while (indexes.indexOf(this._symbols[startIndex + count].ownIndex) > -1) {
                    count++;
                }
                for (var i = startIndex; i < startIndex + count; i++) {
                    //if(count == 3 && i == startIndex) {
                    //    this._symbols[i].changeKey(this.game, this.getSymbolPropertiesByIndex(10));
                    //} else {
                    var properties = this.getSymbolPropertiesByIndex(100 + (i - startIndex));
                    this._symbols[i].changeKey(this.game, properties);
                    //}
                }
            };
            return DragonGirlReelView;
        }(common.BaseReelView));
        common.DragonGirlReelView = DragonGirlReelView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/controller/ReelsController.ts" />
///<reference path="../views/DragonGirlReelView.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/controller/ReelsController.ts" />
///<reference path="../views/DragonGirlReelView.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlReelsController = /** @class */ (function (_super) {
            __extends(DragonGirlReelsController, _super);
            function DragonGirlReelsController(game, name, layer, debugMode, symbolProperties, gameConfiguration, pattern, gameStateResponse) {
                var _this = _super.call(this, game, name, layer, debugMode, symbolProperties, gameConfiguration, pattern, gameStateResponse) || this;
                _this.isRespin = false;
                _this.waveAnimation = new common.SimpleSprite(_this.game, -200, -75, 'respin_wave');
                var waveFrames = [];
                for (var i = 0; i < 20; i++) {
                    waveFrames.push(i);
                }
                _this.waveAnimation.animations.add('play', waveFrames, 20, false, true);
                //this.waveAnimation.scale.set(2, 2);
                _this.waveAnimation.events.onAnimationComplete.add(_this.onWaveAnimationComplete, _this);
                var mask = new Phaser.Graphics(_this.game, 0, 0);
                mask.beginFill(0xffffff, 0.5);
                var w = 171;
                var h = 171;
                mask.drawRect(-(w / 2), -(h / 2), w * 5 + w, h * 3 + h);
                //TODO: вернуть маску если нужно
                _this.layer.add(mask);
                _this.layer.mask = mask;
                _this.intrigueBorder = new common.SimpleSprite(_this.game, -55, -90, 'reel_intrigue');
                var intrigueFrames = [];
                for (var i = 0; i < 17; i++) {
                    intrigueFrames.push(i);
                }
                _this.intrigueBorder.animations.add('play', intrigueFrames, 40, true, true);
                return _this;
                //this.intrigueBorder.scale.set(2, 2);
            }
            /**
             *
             * @param sprite
             * @param animation
             */
            DragonGirlReelsController.prototype.onWaveAnimationComplete = function (sprite, animation) {
                if (this.waveAnimation.parent) {
                    this.waveAnimation.parent.removeChild(this.waveAnimation);
                }
            };
            /**
             * DO NOT CALL SUPER!
             * @param {number} index
             * @param {string} name
             * @param {boolean} debugMode
             * @param {SymbolProperties[]} symbolProperties
             * @param {GameConfiguration} gameConfiguration
             * @param {any} pattern
             * @param {number[]} dataReel
             * @returns
             */
            DragonGirlReelsController.prototype.buildReel = function (index, name, debugMode, symbolProperties, gameConfiguration, pattern, dataReel, borderLayer) {
                var reelView;
                reelView = new common.DragonGirlReelView(this.game, index, name, debugMode, symbolProperties, gameConfiguration, pattern, dataReel, null, null, this.borderLayer);
                reelView.justTouchedStopSignal.add(this.onJustTouchedReelStop, this);
                reelView.actionSignal.add(this.onReelFinnalyStopped, this);
                reelView.x = this.pattern.reels[index].x;
                reelView.y = this.pattern.reels[index].y;
                return reelView;
            };
            /**
             *
             * @param data
             */
            DragonGirlReelsController.prototype.respin = function (data) {
                _super.prototype.respin.call(this, data);
                this.respinReelView = this.buildReel(0, "PrincessFrogReelView", this.debugMode, this.symbolProperties, this.gameConfiguration, this.pattern, [1, 1, 1] /*data.reels[0]*/, this.borderLayer);
                this.respinReelView.x = this.pattern.reels[0].x - 171;
                this.reelViews.unshift(this.respinReelView);
                this.layer.add(this.respinReelView);
                this.respinReelView.alpha = 0;
                this.respinReelView.changeSpeed(1);
                this.isRespin = true;
                this.respinReelView.start();
                for (var i = 0; i < this.reelViews.length; i++) {
                    this.reelViews[i].index = i;
                    this.game.add.tween(this.reelViews[i]).to({ x: this.reelViews[i].x + 171 }, 500, Phaser.Easing.Cubic.Out, true);
                    if (i == 0) {
                        this.game.add.tween(this.reelViews[i]).to({ alpha: 1 }, 500, Phaser.Easing.Cubic.Out, true);
                    }
                    if (i == this.reelViews.length - 1) {
                        this.game.add.tween(this.reelViews[i]).to({ alpha: 0 }, 500, Phaser.Easing.Cubic.Out, true);
                    }
                }
                this.waveAnimation.frame = 0;
                this.waveAnimation.animations.play('play');
                common.SoundController.instance.playSound('respin', 'default');
                this.layer.add(this.waveAnimation);
                setTimeout(function () {
                    common.SoundController.instance.playSound('start_spin_sound', 'default', 0.1, true);
                }, 300);
            };
            /**
             *
             * @param data
             */
            DragonGirlReelsController.prototype.stopRespin = function (data) {
                this.alignReels();
                this.respinData = data;
                this.respinReelView.stop(this.respinData.reels[0].concat(), this.additionalSymbolsNumbers[0]);
                /*this.waveAnimation.visible = false;
                 this.waveAnimation.animations.stop();*/
                this.reelViews[this.reelViews.length - 1].destroy(true);
                this.reelViews.splice(this.gameConfiguration.reels.length);
                this.isPendingRespinComplete = true;
            };
            /**
             * Overriden.
             * Signals when particular reel touched the ground:) before ending.
             * @param {number} reelIndex
             */
            DragonGirlReelsController.prototype.onJustTouchedReelStop = function (reelIndex) {
                _super.prototype.onJustTouchedReelStop.call(this, reelIndex);
                for (var l = 0; l < this.reelViews[reelIndex].symbols.length; l++) {
                    var item = this.reelViews[reelIndex].symbols[l];
                    item['actionOnStopReel']();
                }
                if (this.checkForWild(reelIndex)) {
                    common.SoundController.instance.playSound('wild_boom', 'default', 1, false);
                    var tween = this.game.tweens.create(this.layer).to({ y: this.pattern.y + 20 }, 15, Phaser.Easing.Cubic.Out, true, 0, 4, true);
                    tween.onComplete.addOnce(function () {
                        this.layer.y = this.pattern.y;
                    }, this);
                }
                if (this.isRespin) {
                    var stopSound = common.SoundController.instance.getSound("start_spin_sound", 'default');
                    if (stopSound) {
                        stopSound.stop();
                    }
                }
            };
            /**
             *
             * @param reelIndex
             * @returns {boolean}
             */
            DragonGirlReelsController.prototype.checkForWild = function (reelIndex) {
                var data = this.isRespin ? this.respinData : this.stopSpinData;
                for (var i = 0; i < data.reels[reelIndex].length; i++) {
                    if (data.reels[reelIndex][i] == 10) {
                        return true;
                    }
                }
                return false;
            };
            /**
             *
             * @param reelIndex
             */
            DragonGirlReelsController.prototype.onReelFinnalyStopped = function (reelIndex) {
                _super.prototype.onReelFinnalyStopped.call(this, reelIndex);
                //this.game.tweens.create(this.reelViews[reelIndex]).to({ alpha: 1 }, 600, Phaser.Easing.Default, true);
            };
            /**
             *
             */
            DragonGirlReelsController.prototype.startIntrigue = function (reelIndex) {
                this.reelViews[reelIndex].changeSpeed(1);
                this.intrigueBorder.x = this.pattern.reels[reelIndex].x - 105;
                this.layer.add(this.intrigueBorder);
                if (!this.intrigueBorder.animations.currentAnim.isPlaying) {
                    this.intrigueBorder.animations.play("play");
                    common.SoundController.instance.playSound('intrigue', 'default', 0.3, false);
                }
            };
            /**
             * Override this to stop your specific intrigue like a animation and so on.
             */
            DragonGirlReelsController.prototype.stopIntrigue = function () {
                var stopSound = common.SoundController.instance.getSound("intrigue", 'default');
                if (stopSound) {
                    stopSound.stop();
                }
                this.intrigueBorder.animations.stop();
                this.intrigueBorder.animations.frame = 0;
                this.layer.removeChild(this.intrigueBorder);
            };
            /**
             *
             * @param line
             */
            DragonGirlReelsController.prototype.animateSymbolsOfLine = function (line) {
                _super.prototype.animateSymbolsOfLine.call(this, line);
                var index;
                for (var j = line.offset; j < line.offset + line.icon_count; j++) {
                    index = line.line[j];
                    this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount].visible = false;
                    if (this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount - 1].ownIndex == 100) {
                        this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount - 1].visible = false;
                        this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount + 1].visible = false;
                    }
                    if (this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount - 1].ownIndex == 101) {
                        this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount - 1].visible = false;
                        this.reelViews[j].symbols[index + this.reelViews[j].reelConfig.topInvisibleSymbolsCount - 2].visible = false;
                    }
                }
            };
            /**
             *
             * @param data
             */
            DragonGirlReelsController.prototype.start = function (data) {
                _super.prototype.start.call(this, data);
                this.isRespin = false;
                //for(let i:number = 0; i < this.reelViews.length; i++) {
                //    this.game.tweens.create(this.reelViews[i]).to({ alpha: 0.5 }, 600, Phaser.Easing.Default, true);
                //}
            };
            /**
             *
             * @param data
             * @param reelIndex
             */
            DragonGirlReelsController.prototype.reelStop = function (data, reelIndex) {
                _super.prototype.reelStop.call(this, data, reelIndex);
                //this.game.tweens.create(this.reelViews[reelIndex]).to({ alpha: 1 }, 600, Phaser.Easing.Default, true);
            };
            /**
             *
             */
            DragonGirlReelsController.prototype.transformPrincess = function () {
                common.SoundController.instance.playSound('transform', 'default', 0.4, false);
                var result = false;
                for (var i = 0; i < this.reelViews.length; i++) {
                    for (var j = 0; j < this.reelViews[i].symbols.length; j++) {
                        var item = this.reelViews[i].symbols[j];
                        if (item.ownIndex == 12 || item.ownIndex == 13) {
                            item['transformToWild']();
                            item.changeKey(this.game, this.symbolProperties[4]);
                            result = true;
                        }
                    }
                }
                return result;
            };
            return DragonGirlReelsController;
        }(common.ReelsController));
        common.DragonGirlReelsController = DragonGirlReelsController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="./DragonGirlBackController.ts" />
///<reference path="./DragonGirlReelsController.ts" />
var TypeScriptPhaserSlot;
///<reference path="./DragonGirlBackController.ts" />
///<reference path="./DragonGirlReelsController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlGameController = /** @class */ (function (_super) {
            __extends(DragonGirlGameController, _super);
            function DragonGirlGameController() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
             *
             * @param {IState} state
             * @param {any} data
             * @param {boolean} isRequiredAction
             */
            DragonGirlGameController.prototype.modelSays = function (state, data, isRequiredAction) {
                _super.prototype.modelSays.call(this, state, data, isRequiredAction);
                //let isRequired: boolean = !isRequiredAction || this.model.isAutoPlay ? false : true;
                var isRequired = !(!isRequiredAction || this.model.isAutoPlay);
                if (!isRequiredAction) {
                    this.debuglog("do work for " + state.name);
                }
                this._interfaceController.stateChanged(state.name);
                if (state.name == common.StatesConstants.IDLE_STATE) {
                    this._interfaceController.changeBalance(this.model.coins);
                    this._boongoGameRunnerController.sayToGameRuner('ready');
                    this.onBetChangedHandler(this.model.betPerLine);
                }
                if (!isRequired)
                    switch (state.name) {
                        case common.StatesConstants.IDLE_STATE:
                            this._linesController.stop();
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            this._interfaceController.changeBalance(this.model.coins);
                            if (this.model.isAutoPlay && this.model.coins < this.model.totalBet && this.model.freespins == 0) {
                                this.model.auto(false);
                                this.onInterfaceControllerAction(common.ButtonActionConstants.AUTO_ACTION);
                                break;
                            }
                            state.finishWork();
                            break;
                        case common.StatesConstants.PRESPIN_STATE:
                            state.finishWork();
                            break;
                        case common.StatesConstants.GET_SPIN_RESPONSE_STATE:
                            if (this.model.isFreespins) {
                                this.appProxy.freespin(1);
                            }
                            else {
                                this.appProxy.spin(data);
                            }
                            break;
                        case common.StatesConstants.START_SPIN_STATE:
                            this._linesController.stop();
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            this._interfaceController.resetWinText();
                            this._reelsController.start();
                            if (this.model.isFreespins) {
                                this._freeSpinsController.leftFreeSpins(this.model.freespins);
                            }
                            this._interfaceController.changeBalance(this.model.coins - (this.model.isFreespins ? 0 : this.model.totalBet));
                            this._interfaceController.hidePopupWindow();
                            state.finishWork();
                            break;
                        case common.StatesConstants.START_RESPIN_STATE:
                            //this._interfaceController.resetWinText();
                            this._linesController.stop();
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            this._reelsController.respin();
                            //this._reelsController.respin(data);
                            state.finishWork();
                            break;
                        case common.StatesConstants.GET_RESPIN_RESPONSE_STATE:
                            this.appProxy.respin();
                            break;
                        case common.StatesConstants.STOP_RESPIN_STATE:
                            this._reelsController.stopRespin(data);
                            break;
                        case common.StatesConstants.STOP_SPIN_STATE:
                            this._reelsController.stop(data);
                            break;
                        case common.StatesConstants.PRINCESS_FROG_TRANSFORM_PRINCESS_STATE:
                            var result = this._reelsController['transformPrincess']();
                            if (result) {
                                setTimeout(function () {
                                    state.finishWork();
                                }.bind(this), 1000);
                            }
                            else {
                                state.finishWork();
                            }
                            break;
                        case common.StatesConstants.EMOTIONS_STATE:
                            this._emotionsController.start(data);
                            break;
                        case common.StatesConstants.START_SHOW_WINNING_STATE:
                            this.lastSpinResult = data;
                            this._linesController.start(data);
                            this._emotionsController.stopEmotion();
                            //this._reelsController.animateSymbols(data);
                            //this._symbolsAnimationsController.animateSymbols(data);
                            break;
                        case common.StatesConstants.STOP_SHOW_WINNING_STATE:
                            //this._interfaceController.resetWinText();
                            this._interfaceController.changeBalance(this.model.coins);
                            this._winningOnCenterController.hideWinning();
                            state.finishWork();
                            common.SoundController.instance.playSound('ching', 'default', 0.3);
                            break;
                        case common.StatesConstants.COLLECT_STATE:
                            this._reelsController.stopAnimation();
                            this._symbolsAnimationsController.stopAnimation();
                            state.finishWork();
                            break;
                        case common.StatesConstants.START_FREESPINS_STATE:
                            this._boongoGameRunnerController.sayToGameRuner('ready');
                            this._boongoGameRunnerController.sayToGameRuner('freespin_init');
                            this._freeSpinsController.startFreeSpins(this.model.freespins);
                            this._backgroundController['changeBackground'](true);
                            this._decorationsController['changeFrame'](true);
                            this._interfaceController['showAutoplayText'](false);
                            break;
                        case common.StatesConstants.ADD_FREESPINS_STATE:
                            this._freeSpinsController.addedFreeSpins(this.model.freeSpinsAdded);
                            break;
                        case common.StatesConstants.FINISH_FREESPINS_STATE:
                            this._boongoGameRunnerController.sayToGameRuner('ready');
                            this._boongoGameRunnerController.sayToGameRuner('freespin_stop');
                            this._freeSpinsController.endOfFreeSpins(this.model.bonusTotalWin);
                            this._backgroundController['changeBackground'](false);
                            this._decorationsController['changeFrame'](false);
                            break;
                        case common.StatesConstants.GET_GAMBLE_RESPONSE_STATE:
                            state.finishWork();
                            break;
                        case common.StatesConstants.GAMBLE_STATE:
                            this.appProxy.gamble();
                            break;
                        case common.StatesConstants.START_BONUS_GAME_STATE:
                            break;
                        case common.StatesConstants.FINISH_BONUS_GAME_STATE:
                            state.finishWork();
                            break;
                    }
            };
            /**
             * Callback function to handle getting the server response "GetState"
             * Initialising all helper/controllers
             *-ReelsController
             *-InterfaceController
             *-LineController
             *-TextController
             * @param {GameStateResponse} data
             */
            DragonGirlGameController.prototype.onGetGameState = function (data) {
                var contentLayer = new Phaser.Group(this.game);
                var backLayer = new Phaser.Group(this.game);
                backLayer.name = 'backLayer';
                var reelsLayer = new Phaser.Group(this.game);
                reelsLayer.name = 'reelsLayer';
                var linesLayer = new Phaser.Group(this.game);
                linesLayer.name = 'linesLayer';
                var wildsAnimationsLayer = new Phaser.Group(this.game);
                wildsAnimationsLayer.name = 'wildsAnimationsLayer';
                var decorationsLayer = new Phaser.Group(this.game);
                decorationsLayer.name = 'decorationsLayer';
                var symbolsAnimationsLayer = new Phaser.Group(this.game);
                symbolsAnimationsLayer.name = 'symbolsAnimationsLayer';
                var paytableLayer = new Phaser.Group(this.game);
                paytableLayer.name = 'paytableLayer';
                var consoleLayer = new Phaser.Group(this.game);
                consoleLayer.name = 'consoleLayer';
                //consoleLayer.scale.setTo(scale, scale);
                var winningOnCenterLayersLayer = new Phaser.Group(this.game);
                winningOnCenterLayersLayer.name = 'winningOnCenterLayersLayer';
                var emotionsLayer = new Phaser.Group(this.game);
                emotionsLayer.name = 'emotionsLayer';
                var freeSpinsLayer = new Phaser.Group(this.game);
                freeSpinsLayer.name = 'freeSpinsLayer';
                var toolbarLayer = new Phaser.Group(this.game);
                toolbarLayer.name = 'toolbarLayer';
                //toolbarLayer.scale.setTo(scale, scale);
                contentLayer.add(backLayer);
                contentLayer.add(reelsLayer);
                contentLayer.add(linesLayer);
                contentLayer.add(wildsAnimationsLayer);
                contentLayer.add(decorationsLayer);
                contentLayer.add(symbolsAnimationsLayer);
                contentLayer.add(winningOnCenterLayersLayer);
                contentLayer.add(emotionsLayer);
                contentLayer.add(freeSpinsLayer);
                contentLayer.add(paytableLayer);
                //contentLyer.add(toolbarLayer);
                this.game.add.existing(contentLayer);
                this.game.add.existing(toolbarLayer);
                this.game.add.existing(consoleLayer);
                var backControllerName = this.getControllerName("BackController");
                this._backgroundController = new TypeScriptPhaserSlot.common[backControllerName](this.game, backControllerName, backLayer, true, this.game.pattern);
                var decorationsControllerName = this.getControllerName("DecorationsController");
                this._decorationsController = new TypeScriptPhaserSlot.common[decorationsControllerName](this.game, decorationsControllerName, decorationsLayer, true, this.game.pattern);
                var icn = 'InterfaceController';
                var icp = this.game.pattern.console;
                if (this.game.device.iPhone || this.game.device.android) {
                    icn = 'InterfaceControllerMobile';
                    icp = this.game.pattern.console_mobile;
                }
                if (this.game.device.iPad) {
                    icn = 'InterfaceControllerIPad';
                    icp = this.game.pattern.console_ipad;
                }
                var interfaceControllerName = this.getControllerName(icn);
                this._interfaceController = new TypeScriptPhaserSlot.common[interfaceControllerName](this.game, interfaceControllerName, consoleLayer, true, this.configuration.buttons, icp);
                this._interfaceController.actionSignal.add(this.onInterfaceControllerAction, this);
                /*
                 ReelsController creates and manages all amount of <IReel>
                 */
                var reelsControllerName = this.getControllerName("ReelsController");
                this._reelsController = new TypeScriptPhaserSlot.common[reelsControllerName](this.game, reelsControllerName, reelsLayer, true, this.configuration.symbols, this.configuration, this.game.pattern, data);
                this._reelsController.actionSignal.add(this.onReelsControllerAction, this);
                /*
                 WinningController creates and manages all stuff of showing the visual results
                 like lines, borders and so on.
                 */
                var linesControllerName = this.getControllerName("LinesController");
                this._linesController = new TypeScriptPhaserSlot.common[linesControllerName](this.game, linesControllerName, linesLayer, true, this.configuration, this.game.pattern);
                this._linesController.actionSignal.add(this.onLinesControllerAction, this);
                this._linesController.finishShowingSignal.add(this.onLinesControllerFinishShowLines, this);
                var scattersControllerName = this.getControllerName("ScattersController");
                this._scattersController = new TypeScriptPhaserSlot.common[scattersControllerName](this.game, scattersControllerName, linesLayer, true, this.configuration, this.game.pattern);
                this._scattersController.actionSignal.add(this.onScattersControllerAction, this);
                this._scattersController.finishShowingSignal.add(this.onScattersControllerFinish, this);
                /*
                 Animations controller
                 */
                var symbolsAnimationsControllerName = this.getControllerName("SymbolsAnimationController");
                this._symbolsAnimationsController = new TypeScriptPhaserSlot.common[symbolsAnimationsControllerName](this.game, symbolsAnimationsControllerName, symbolsAnimationsLayer, true, this.configuration.symbols, this.configuration, this.game.pattern, data);
                this._symbolsAnimationsController.actionSignal.add(this.onReelsControllerAction, this);
                this._symbolsAnimationsController['wildLayer'] = wildsAnimationsLayer;
                var soundControllerName = this.getControllerName("SoundController");
                this._soundController = new TypeScriptPhaserSlot.common[soundControllerName](this.game, soundControllerName, true, this.configuration, data);
                this._soundController.actionSignal.add(this.onReelsControllerAction, this);
                new common.MoneyFormatController(this.game, 'MoneyFormatController', true, this.configuration, data);
                var winningOnCenterControllerName = this.getControllerName("WinningOnCenterController");
                this._winningOnCenterController = new TypeScriptPhaserSlot.common[winningOnCenterControllerName](this.game, winningOnCenterControllerName, winningOnCenterLayersLayer, true);
                this._winningOnCenterController.actionSignal.add(this.onEmotionsControllerAction, this);
                var emotionsControllerName = this.getControllerName("EmotionsController");
                this._emotionsController = new TypeScriptPhaserSlot.common[emotionsControllerName](this.game, emotionsControllerName, emotionsLayer, true, this.configuration, this.game.pattern, data);
                this._emotionsController.actionSignal.add(this.onEmotionsControllerAction, this);
                var freeSpinsControllerName = this.getControllerName("FreeSpinsController");
                this._freeSpinsController = new TypeScriptPhaserSlot.common[freeSpinsControllerName](this.game, freeSpinsControllerName, freeSpinsLayer, true, this.configuration, this.game.pattern, data);
                this._freeSpinsController.actionSignal.add(this.onFreeSpinsControllerAction, this);
                //let paytableControllerName:string = this.getControllerName("PaytableController");
                //this._paytableController = new TypeScriptPhaserSlot.common[paytableControllerName](this.game, paytableControllerName, paytableLayer, true, this.configuration, this.game.pattern, data);
                var toolbarControllerName = this.getControllerName("ToolbarController");
                this._toolbarController = new TypeScriptPhaserSlot.common[toolbarControllerName](this.game, toolbarControllerName, toolbarLayer, true, this.configuration, this.game.pattern, data);
                this._toolbarController.actionSignal.add(this.onToolbarControllerAction, this);
                this._boongoGameRunnerController = new common.BoongoGameRunerController(this.game, 'BoongoGameRunerController', this.debugMode);
                this._boongoGameRunnerController.actionSignal.add(this.onBoongoGameRunnesControllerAction, this);
                this._boongoGameRunnerController.sayToGameRuner('sound_changed', true);
                this._boongoGameRunnerController.checkGameMode();
                //this._boongoGameRunnerController.sayToGameRuner('loaded',);
                this.correctSize(this.game.scale, this.game.scale.width, this.game.scale.height);
                //Sending game state data to the model to init a game proccess.
                this.model.onGetGameState(data);
                var paytableControllerName = this.getControllerName("PaytableController");
                this._paytableController = new TypeScriptPhaserSlot.common[paytableControllerName](this.game, paytableControllerName, paytableLayer, true, this.configuration, this.game.pattern, data);
                //Shwitch autoplay on if Freespins are here
                if (this.model.isFreespins) {
                    this.model.auto(true);
                }
                this._backgroundController['changeBackground'](this.model.isFreespins);
                this._decorationsController['changeFrame'](this.model.isFreespins);
                common.SoundController.instance.playSound('background_sound', 'music', 0.3, true);
                //this.game.camera.resetFX();
                //this.correctSize(this.game.scale, this.game.scale.width, this.game.scale.height);
                this._interfaceController['changePositions']();
                if (window['GR']) {
                    consoleLayer.visible = false;
                    toolbarLayer.visible = false;
                }
            };
            return DragonGirlGameController;
        }(common.BaseGameGameController));
        common.DragonGirlGameController = DragonGirlGameController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/controller/SymbolsAnimationController.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/controller/SymbolsAnimationController.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlSymbolsAnimationController = /** @class */ (function (_super) {
            __extends(DragonGirlSymbolsAnimationController, _super);
            function DragonGirlSymbolsAnimationController() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
             *
             */
            DragonGirlSymbolsAnimationController.prototype.correctSpinResult = function () {
                for (var i = 0; i < this.spinResult.reels.length; i++) {
                    //Трансформация принцесс в ведьм
                    //if (/*this.game.model.isFreespins &&*/ this.checkForWild()) {
                    for (var k = 0; k < this.spinResult.reels[i].length; k++) {
                        if (this.spinResult.reels[i][k] == 12 || this.spinResult.reels[i][k] == 13) {
                            this.spinResult.reels[i][k] = 200;
                        }
                    }
                    //}
                    var startIndex = this.spinResult.reels[i].indexOf(10);
                    var endIndex = this.spinResult.reels[i].lastIndexOf(10);
                    var count = 0;
                    while (this.spinResult.reels[i][startIndex + count] == 10) {
                        count++;
                    }
                    var arr = [];
                    if (startIndex == 0 && count == 1) {
                        arr = [102];
                    }
                    if (startIndex == 0 && count == 2) {
                        arr = [101, 102];
                    }
                    if (startIndex == 0 && count == 3) {
                        arr = [100, 101, 102];
                    }
                    if (startIndex == 1 && count == 2) {
                        arr = [100, 101];
                    }
                    if (startIndex == 2 && count == 1) {
                        arr = [100];
                    }
                    for (var j = startIndex; j < endIndex + 1; j++) {
                        this.spinResult.reels[i][j] = arr.shift();
                    }
                }
            };
            /**
             *
             * @returns {boolean}
             */
            DragonGirlSymbolsAnimationController.prototype.checkForWild = function () {
                /*let wilds:number[] = [10,100,101,102];
                for(let i:number = 0; i < this.game.model.result.reels.length; i++) {
                    for(let k:number = 0; k < this.game.model.result.reels[i].length; k++) {
                        if (wilds.indexOf(this.game.model.result.reels[i][k]) > -1) {
                            return true;
                        }
                    }
                }
                return false;*/
                for (var i = 0; i < this.game.model.result.reels.length; i++) {
                    var witches = this.game.model.result.reels[i].filter(function (value) {
                        return value == 10;
                    });
                    if (witches.length == this.game.model.result.reels[i].length) {
                        return true;
                    }
                }
                return false;
            };
            /**
             *
             * @param reelIndex
             * @param symbolIndex
             * @param symbolData
             */
            DragonGirlSymbolsAnimationController.prototype.animatSymbol = function (reelIndex, symbolIndex, symbolData) {
                var animation = this.createAnimation(symbolData);
                animation.x = this.pattern.reels[reelIndex].x;
                animation.y = this.pattern.reels[reelIndex]["icon" + symbolIndex].y;
                animation.actionSignal.addOnce(this.onAnimationAction, this);
                if (symbolData < 100) {
                    this.layer.add(animation);
                }
                else {
                    this.wildLayer.add(animation);
                }
                this.animations.push(animation);
                animation.start();
            };
            Object.defineProperty(DragonGirlSymbolsAnimationController.prototype, "wildLayer", {
                /**
                 *
                 * @returns {Phaser.Group}
                 */
                get: function () {
                    return this._wildLayer;
                },
                /**
                 *
                 * @param value
                 */
                set: function (value) {
                    this._wildLayer = value;
                    this._wildLayer.x = this.pattern.x;
                    this._wildLayer.y = this.pattern.y;
                    var mask = new Phaser.Graphics(this.game, 0, 0);
                    mask.beginFill(0xffffff, 0.5);
                    var w = 171;
                    var h = 171;
                    mask.drawRect(-(w / 2), -(h / 2), w * 5 + w, h * 3 + h);
                    this._wildLayer.add(mask);
                    this._wildLayer.mask = mask;
                },
                enumerable: true,
                configurable: true
            });
            return DragonGirlSymbolsAnimationController;
        }(common.SymbolsAnimationController));
        common.DragonGirlSymbolsAnimationController = DragonGirlSymbolsAnimationController;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlGameStateResponse = /** @class */ (function () {
            function DragonGirlGameStateResponse(rawData) {
                this.parse(rawData);
            }
            DragonGirlGameStateResponse.prototype.parse = function (rawData) {
                //this._status = rawData.status;
                //this._denomination = rawData.initial.denomination;
                this._denomination = rawData.denomination;
                this._initialBet = rawData.initial.bet_on_line;
                this._initialLines = +rawData.initial.lines_amount;
                this._coins = rawData.balance;
                //his._hasBonus = rawData.response.state.hasBonus;
                //this._bStage = rawData.response.state.bStage;
                this._reels = rawData.combination;
                //this._freespins = rawData.response.state.freespins;
                //this._points = rawData.response.state.points;
                //this._fsTotal = rawData.response.state.fsTotal;
                //this._fsMult = rawData.response.state.fsMult;
                //this._bonusType = rawData.response.state.bonusType;
                //this._bonusGame = rawData.response.state.bonusGame;
                //this._giftspins = rawData.response.state.giftspins;
                //this._jackpot0 = rawData.response.state.jackpot0;
                //this._jackpot1 = rawData.response.state.jackpot1;
                this._bets = rawData.ranges.bets;
                this._lines = rawData.ranges.lines;
                //this._betsPreselect = rawData.response.betsPreselect;
                //this._lineCount = rawData.response.lineCount;
                //this._lineCountFreespins = rawData.response.lineCountFreespins;
                //this._paylines = rawData.response.paylineMap;
                //this._paylinesFreespins = rawData.response.paylineMapFreespins;
                //this._paytable = rawData.response.paytable;
                this._userCoints = rawData.balance;
                //this._reelMap = rawData.response.reelMap;
                //this._reelMapFreespins = rawData.response.reelMapFreespins;
                //this._multipliers = rawData.response.multipliers;
                //this._multipliersFreespins = rawData.response.multipliersFreespins;
                //this._commands = rawData.commands;
                //this._serverInfo = rawData.serverInfo;
                if (rawData.events.length > 0) {
                    var event_1 = rawData.events[0];
                    if (event_1.hasOwnProperty('combination')) {
                        this._reels = event_1.combination;
                        this._spinRespince = new common.DragonGirlSpinResponse(event_1);
                        this._freespins = event_1.bonuses_summary.freespins_remainder || 0;
                    }
                }
                /*this._status = rawData.status;
                this._initialBet = rawData.response.initialBet;
                this._coins = rawData.response.user.coins;
                this._lines = rawData.response.state.lines;
                this._hasBonus = rawData.response.state.hasBonus;
                this._bStage = rawData.response.state.bStage;
                this._reels = rawData.response.state.reels;
                this._freespins = rawData.response.state.freespins;
                this._points = rawData.response.state.points;
                this._fsTotal = rawData.response.state.fsTotal;
                this._fsMult = rawData.response.state.fsMult;
                this._bonusType = rawData.response.state.bonusType;
                this._bonusGame = rawData.response.state.bonusGame;
                this._giftspins = rawData.response.state.giftspins;
                this._jackpot0 = rawData.response.state.jackpot0;
                this._jackpot1 = rawData.response.state.jackpot1;
                this._bets = rawData.response.bets;
                this._betsPreselect = rawData.response.betsPreselect;
                this._lineCount = rawData.response.lineCount;
                this._lineCountFreespins = rawData.response.lineCountFreespins;
                this._paylines = rawData.response.paylineMap;
                this._paylinesFreespins = rawData.response.paylineMapFreespins;
                this._paytable = rawData.response.paytable;
    
                this._userCoints = rawData.response.user.coins;
                this._reelMap = rawData.response.reelMap;
    
                this._reelMapFreespins = rawData.response.reelMapFreespins;
                this._multipliers = rawData.response.multipliers;
                this._multipliersFreespins = rawData.response.multipliersFreespins;
                this._commands = rawData.commands;
                this._serverInfo = rawData.serverInfo;*/
                return this;
            };
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "status", {
                get: function () {
                    return this._status;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "initialBet", {
                get: function () {
                    return this._initialBet;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "initialLines", {
                get: function () {
                    return this._initialLines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "coins", {
                get: function () {
                    return this._coins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "line", {
                get: function () {
                    return this._line;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "bet", {
                get: function () {
                    return this._bet;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "hasBonus", {
                get: function () {
                    return this._hasBonus;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "bStage", {
                get: function () {
                    return this._bStage;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "freespins", {
                get: function () {
                    return this._freespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "points", {
                get: function () {
                    return this._points;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "fsTotal", {
                get: function () {
                    return this._fsTotal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "fsMult", {
                get: function () {
                    return this._fsMult;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "bonusType", {
                get: function () {
                    return this.bonusType;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "bonusGame", {
                get: function () {
                    return this._bonusGame;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "giftspins", {
                get: function () {
                    return this._giftspins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "jackpot0", {
                get: function () {
                    return this._jackpot0;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "jackpot1", {
                get: function () {
                    return this._jackpot1;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "betsPreselect", {
                get: function () {
                    return this._betsPreselect;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "lineCount", {
                get: function () {
                    return this._lineCount;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "lineCountFreespins", {
                get: function () {
                    return this._lineCountFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "bets", {
                get: function () {
                    return this._bets;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "lines", {
                get: function () {
                    return this._lines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "reels", {
                get: function () {
                    return this._reels;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "paytable", {
                get: function () {
                    return this._paytable;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "userCoints", {
                get: function () {
                    return this._userCoints;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "paylines", {
                get: function () {
                    return this._paylines;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "paylinesFreespins", {
                get: function () {
                    return this._paylinesFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "reelMap", {
                get: function () {
                    return this._reelMap;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "reelMapFreespins", {
                get: function () {
                    return this._reelMapFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "multipliers", {
                get: function () {
                    return this._multipliers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "multipliersFreespins", {
                get: function () {
                    return this._multipliersFreespins;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "commands", {
                get: function () {
                    return this._commands;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "serverInfo", {
                //public get response(): any {
                //    return this._response;
                //}
                get: function () {
                    return this._serverInfo;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "spinRespince", {
                get: function () {
                    return this._spinRespince;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlGameStateResponse.prototype, "denomination", {
                get: function () {
                    return this._denomination;
                },
                enumerable: true,
                configurable: true
            });
            return DragonGirlGameStateResponse;
        }());
        common.DragonGirlGameStateResponse = DragonGirlGameStateResponse;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
var TypeScriptPhaserSlot;
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlSpinResponse = /** @class */ (function () {
            function DragonGirlSpinResponse(rawData) {
                this.parse(rawData);
            }
            DragonGirlSpinResponse.prototype.parse = function (rawData) {
                //this._status = rawData.status;
                //this._commands = rawData.commands;
                //this._jackpot = new Jackpot(rawData.response.jackpot);
                this._results = [];
                this._results.push(new common.DragonGirlSpinResult(rawData));
                this._win = rawData.winnings.total;
                this._totalWin = rawData.winnings.total;
                var fakeStateData = {};
                //fakeStateData.freespins = rawData.bonuses[0].amount || 0;
                fakeStateData.freespins = rawData.bonuses_summary.freespins_remainder || 0;
                fakeStateData.points = 0;
                fakeStateData.hasBonus = false;
                fakeStateData.coins = rawData.balance;
                fakeStateData.total = rawData.bonuses_summary.total;
                this._state = new common.State(fakeStateData);
                this._rawData = rawData;
                //this._user = new User(rawData.response.user);
                //this._serverInfo = new ServerInfo(rawData.serverInfo);
                return this;
                /*this._status = rawData.status;
                this._commands = rawData.commands;
                this._jackpot = new Jackpot(rawData.response.jackpot);
                
                this._results = [];
                for (let i: number = 0; i < rawData.response.result.length; i++) {
                    this._results.push(new SpinResult(rawData.response.result[i]));
                }
                this._win = rawData.response.win;
                this._totalWin = rawData.response.totalWin;
                this._state = new State(rawData.response.state);
                this._user = new User(rawData.response.user);
                this._serverInfo = new ServerInfo(rawData.serverInfo);
                return this;*/
            };
            Object.defineProperty(DragonGirlSpinResponse.prototype, "status", {
                get: function () {
                    return this._status;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlSpinResponse.prototype, "commands", {
                get: function () {
                    return this._commands;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlSpinResponse.prototype, "jackpot", {
                get: function () {
                    return this._jackpot;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlSpinResponse.prototype, "results", {
                get: function () {
                    return this._results;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlSpinResponse.prototype, "win", {
                get: function () {
                    return this._win;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlSpinResponse.prototype, "totalWin", {
                get: function () {
                    return this._totalWin;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlSpinResponse.prototype, "state", {
                get: function () {
                    return this._state;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlSpinResponse.prototype, "user", {
                get: function () {
                    return this._user;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlSpinResponse.prototype, "serverInfo", {
                get: function () {
                    return this._serverInfo;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DragonGirlSpinResponse.prototype, "rawData", {
                get: function () {
                    return this._rawData;
                },
                enumerable: true,
                configurable: true
            });
            return DragonGirlSpinResponse;
        }());
        common.DragonGirlSpinResponse = DragonGirlSpinResponse;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/model/states/BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/model/states/BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlIdleState = /** @class */ (function (_super) {
            __extends(DragonGirlIdleState, _super);
            function DragonGirlIdleState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.first = false;
                _this.name = common.StatesConstants.IDLE_STATE;
                return _this;
            }
            DragonGirlIdleState.prototype.checkData = function () {
                return true;
            };
            DragonGirlIdleState.prototype.doWork = function (data, isRequiredAction) {
                _super.prototype.doWork.call(this, data, isRequiredAction);
                if (this.first) {
                    return;
                }
                this.first = true;
                if (this._model.result && (this._model.result.paylines.length > 0 || this._model.result.scatters.length > 0)) {
                    this._model.goToState(common.StatesConstants.PRINCESS_FROG_TRANSFORM_PRINCESS_STATE);
                }
            };
            DragonGirlIdleState.prototype.finishWork = function () {
                this.debuglog("- finish work");
                this.finishWorkSignal.dispatch(true);
            };
            return DragonGirlIdleState;
        }(common.BaseState));
        common.DragonGirlIdleState = DragonGirlIdleState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/model/states/BaseState.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/model/states/BaseState.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlTransformPrincessState = /** @class */ (function (_super) {
            __extends(DragonGirlTransformPrincessState, _super);
            function DragonGirlTransformPrincessState(model, stateConfiguration, debugMode) {
                if (debugMode === void 0) { debugMode = false; }
                var _this = _super.call(this, model, stateConfiguration, debugMode) || this;
                _this.name = common.StatesConstants.PRINCESS_FROG_TRANSFORM_PRINCESS_STATE;
                return _this;
            }
            DragonGirlTransformPrincessState.prototype.checkData = function () {
                //if (/*this.model.isFreespins && */this.checkForWild() && this.checkForFrincess()) {
                //    return true;
                //}
                if (this.model.rawData &&
                    this.model.rawData.additional_info &&
                    this.model.rawData.additional_info.changed_princess &&
                    this.model.rawData.additional_info.changed_princess.length > 0) {
                    return true;
                }
                return null;
            };
            DragonGirlTransformPrincessState.prototype.checkForWild = function () {
                /*for(let i:number = 0; i < this.model.result.reels.length; i++) {
                    for(let k:number = 0; k < this.model.result.reels[i].length; k++) {
                        if (this.model.result.reels[i][k] == 10) {
                            return true;
                        }
                    }
                }*/
                for (var i = 0; i < this.model.result.reels.length; i++) {
                    var witches = this.model.result.reels[i].filter(function (value) {
                        return value == 10;
                    });
                    if (witches.length == this.model.result.reels[i].length) {
                        return true;
                    }
                }
                return false;
            };
            DragonGirlTransformPrincessState.prototype.checkForFrincess = function () {
                for (var i = 0; i < this.model.result.reels.length; i++) {
                    for (var k = 0; k < this.model.result.reels[i].length; k++) {
                        if (this.model.result.reels[i][k] == 9) {
                            return true;
                        }
                    }
                }
                return false;
            };
            return DragonGirlTransformPrincessState;
        }(common.BaseState));
        common.DragonGirlTransformPrincessState = DragonGirlTransformPrincessState;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
///<reference path="../../../engine/view/BaseView.ts" />
var TypeScriptPhaserSlot;
///<reference path="../../../engine/view/BaseView.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlLinesView = /** @class */ (function (_super) {
            __extends(DragonGirlLinesView, _super);
            function DragonGirlLinesView(game, name, configuration, pattern, debugMode) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.hideLinesTimeout = -1;
                _this.game = game;
                _this.pattern = pattern;
                _this._config = configuration;
                _this.init();
                return _this;
            }
            DragonGirlLinesView.prototype.init = function () {
                this.createLines();
                this.createLineNumbers();
                this.hideLines();
            };
            /**
             * Creation all lines
             */
            DragonGirlLinesView.prototype.createLines = function () {
                this.lines = [];
                var positionsCoordinates = [0, 171, 342];
                var linesPositions = [1, 0, 2, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                var linesContainer = new Phaser.Group(this.game);
                var line;
                for (var i = 0; i < 30; i++) {
                    line = new common.SimpleSprite(this.game, 0, 0, 'line_' + (i + 1) + '_image');
                    line.y = positionsCoordinates[linesPositions[i]];
                    //line.scale.set(2, 2);
                    linesContainer.add(line);
                    this.lines.push(line);
                }
                linesContainer.x = 225;
                linesContainer.y = 195;
                this.add(linesContainer);
            };
            /**
             * Creation all line numbers
             */
            DragonGirlLinesView.prototype.createLineNumbers = function () {
            };
            /**
             *
             */
            DragonGirlLinesView.prototype.hideLines = function (animated) {
                if (this.hideLinesTimeout != -1) {
                    clearInterval(this.hideLinesTimeout);
                    this.hideLinesTimeout = -1;
                }
                var lineImage;
                for (var i = 0; i < this.lines.length; i++) {
                    lineImage = this.lines[i];
                    if (!animated) {
                        lineImage.visible = false;
                    }
                    else {
                        this.game.add.tween(lineImage).to({ alpha: 0 }, 500, Phaser.Easing.Linear.None, true).onComplete.add(function () {
                            lineImage.visible = false;
                        }, true);
                    }
                }
            };
            /**
             *
             * @param count
             */
            DragonGirlLinesView.prototype.showLines = function (count) {
                if (this.hideLinesTimeout != -1) {
                    clearInterval(this.hideLinesTimeout);
                    this.hideLinesTimeout = -1;
                }
                var lineImage;
                for (var i = 0; i < this.lines.length; i++) {
                    lineImage = this.lines[i];
                    this.game.tweens.removeFrom(lineImage, true);
                    lineImage.alpha = 1;
                    lineImage.visible = i + 1 <= count;
                }
                this.hideLinesTimeout = setTimeout(function () {
                    this.hideLines(true);
                }.bind(this), 2000);
            };
            /**
             *
             * @param {PayLine} line
             */
            DragonGirlLinesView.prototype.showWinLine = function (line) {
                var lineImage;
                for (var i = 0; i < this.lines.length; i++) {
                    lineImage = this.lines[i];
                    if (i == line.index) {
                        lineImage.visible = true;
                        lineImage.alpha = 0;
                        this.game.add.tween(lineImage).to({ alpha: 1 }, 250, Phaser.Easing.Linear.None, true, 0, 1, true);
                    }
                    else {
                        lineImage.visible = false;
                    }
                    //lineImage.visible = i == line.index;
                }
                /*var lineConfig: WinLineConfiguration = this._config[line.index];
                for (let i: number = 0; i < line.icon_count; i++) {
                     
                }*/
            };
            return DragonGirlLinesView;
        }(common.BaseView));
        common.DragonGirlLinesView = DragonGirlLinesView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
/**
 * Created by rocket on 21.07.2017.
 */
///<reference path="../../../engine/view/BaseView.ts" />
var TypeScriptPhaserSlot;
/**
 * Created by rocket on 21.07.2017.
 */
///<reference path="../../../engine/view/BaseView.ts" />
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlPreloaderView = /** @class */ (function (_super) {
            __extends(DragonGirlPreloaderView, _super);
            function DragonGirlPreloaderView(game, name, pattern, debugMode) {
                var _this = _super.call(this, game, name, debugMode) || this;
                _this.game = game;
                _this.pattern = pattern;
                _this.init();
                return _this;
            }
            DragonGirlPreloaderView.prototype.init = function () {
                var preloader = new Phaser.Group(this.game);
                var preloaderBackground = new common.SimpleSprite(this.game, 0, 0, 'preloader_bg', this.debugMode);
                var image1 = new common.SimpleSprite(this.game, 0, 0, 'preloader_i1', this.debugMode);
                var image2 = new common.SimpleSprite(this.game, 0, 0, 'preloader_i2', this.debugMode);
                var progressbar = new Phaser.Group(this.game);
                var data = {
                    key: 'preloader_progressbar',
                    scale: { x: 1, y: 1 },
                    pivot: { x: 0, y: 0 },
                    anchor: { x: 0, y: 0 },
                    animations: [
                        {
                            name: 'start',
                            startFrame: 1,
                            endFrame: 50,
                            isLoop: true,
                            frameRate: 30
                        }
                    ]
                };
                var params = new common.AnimationConfiguration(data);
                this.animation = new common.AnimatedSprite(this.game, 65, 175 /*300*/, params, this.debugMode);
                this.animation.start();
                var mask = new Phaser.Graphics(this.game, 125, 215);
                mask.beginFill(0xffffff, 1);
                mask.drawCircle(0, 0, 100);
                mask.endFill();
                progressbar.add(image2);
                progressbar.add(mask);
                progressbar.add(this.animation);
                progressbar.add(image1);
                progressbar.x = 558;
                progressbar.y = 270;
                preloader.add(preloaderBackground);
                preloader.add(progressbar);
                this.animation.mask = mask;
                this.add(preloader);
            };
            DragonGirlPreloaderView.prototype.onLoadingUpdate = function (progress, cacheKey, success, totalLoaded, totalFiles) {
                /*let max:number = 220 / totalFiles;
                let percent:number = max % progress;
                this.animation.y = 520 - ((max * totalLoaded) + percent);*/
                //let oneFileHeight:number = 110 / totalFiles;
                //this.animation.y = 260 - ((oneFileHeight * totalLoaded) + (oneFileHeight % progress));
                var percent = (100 / totalFiles) * totalLoaded + (progress / 100) - 1;
                this.animation.y = 260 - (110 / 100 * percent);
                if (window['GR']) {
                    window['GR'].Events.game.progress({ progress: percent });
                }
            };
            DragonGirlPreloaderView.prototype.onLoadingComplete = function () {
            };
            return DragonGirlPreloaderView;
        }(common.BaseView));
        common.DragonGirlPreloaderView = DragonGirlPreloaderView;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
/**
 * Created by rocket on 04.07.2017.
 */
var TypeScriptPhaserSlot;
/**
 * Created by rocket on 04.07.2017.
 */
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlAwesomeEmotion = /** @class */ (function (_super) {
            __extends(DragonGirlAwesomeEmotion, _super);
            function DragonGirlAwesomeEmotion(game, config) {
                return _super.call(this, game, config) || this;
            }
            /**
             *
             */
            DragonGirlAwesomeEmotion.prototype.init = function () {
            };
            /**
             *
             */
            DragonGirlAwesomeEmotion.prototype.start = function () {
                this.animate();
                common.SoundController.instance.playSound('hammer', 'emotions', 0.1);
            };
            /**
             *
             */
            DragonGirlAwesomeEmotion.prototype.stop = function () {
                clearInterval(this._timeOut);
                this._timeOut = 0;
                _super.prototype.stop.call(this);
            };
            /**
             *
             */
            DragonGirlAwesomeEmotion.prototype.animate = function () {
                var gameWidth = 1366;
                var gameHeight = 768;
                var backgroundAnimation = new common.AnimatedSprite(this.game, 0, 0, this.config.emotion);
                backgroundAnimation.anchor.set(0.5, 0.5);
                backgroundAnimation.x = gameWidth / 2;
                backgroundAnimation.y = gameHeight / 2;
                backgroundAnimation.start();
                backgroundAnimation.alpha = 0;
                this.add(backgroundAnimation);
                this._tweens.push(this.game.add.tween(backgroundAnimation).to({ alpha: 1 }, 700, Phaser.Easing.Cubic.Out, true));
                this._tweens.push(this.game.add.tween(backgroundAnimation).to({ alpha: 0 }, 700, Phaser.Easing.Cubic.Out, false, 1000));
                this._tweens[0].chain(this._tweens[1]);
                var firstImage = new common.SimpleSprite(this.game, 0, 0, 'emotions_images', false, 'texts/awesome');
                firstImage.anchor.set(0.5, 0.5);
                firstImage.y = gameHeight / 2 - 50;
                this.add(firstImage);
                firstImage.x = -firstImage.width;
                this._tweens.push(this.game.add.tween(firstImage).to({ x: gameWidth / 2 }, 450, Phaser.Easing.Cubic.Out, true));
                this._tweens.push(this.game.add.tween(firstImage).to({ x: gameWidth / 2 + 70 }, 1000, Phaser.Easing.Default));
                this._tweens.push(this.game.add.tween(firstImage).to({ x: gameWidth + firstImage.width }, 450, Phaser.Easing.Cubic.Out));
                this._tweens[2].chain(this._tweens[3]);
                this._tweens[3].chain(this._tweens[4]);
                this._tweens[this._tweens.length - 1].onComplete.addOnce(this.onAnimationsComplete, this);
                common.SoundController.instance.playSound('awesome', 'default', 0.2, false);
            };
            /**
             *
             * @param data
             */
            DragonGirlAwesomeEmotion.prototype.onAnimationsComplete = function (data) {
                this._actionSignal.dispatch(common.AnimatedSprite.ALL_ANIMATIONS_COMPLETE);
            };
            return DragonGirlAwesomeEmotion;
        }(common.BaseEmotion));
        common.DragonGirlAwesomeEmotion = DragonGirlAwesomeEmotion;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
/**
 * Created by rocket on 04.07.2017.
 */
var TypeScriptPhaserSlot;
/**
 * Created by rocket on 04.07.2017.
 */
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlBigWinEmotion = /** @class */ (function (_super) {
            __extends(DragonGirlBigWinEmotion, _super);
            function DragonGirlBigWinEmotion(game, config) {
                return _super.call(this, game, config) || this;
            }
            /**
             *
             */
            DragonGirlBigWinEmotion.prototype.init = function () {
            };
            /**
             *
             */
            DragonGirlBigWinEmotion.prototype.start = function () {
                this.animate();
                common.SoundController.instance.playSound('hammer', 'emotions', 0.1);
            };
            /**
             *
             */
            DragonGirlBigWinEmotion.prototype.stop = function () {
                clearInterval(this._timeOut);
                this._timeOut = 0;
                _super.prototype.stop.call(this);
            };
            /**
             *
             */
            DragonGirlBigWinEmotion.prototype.animate = function () {
                var gameWidth = 1366;
                var gameHeight = 768;
                var backgroundAnimation = new common.AnimatedSprite(this.game, 0, 0, this.config.emotion);
                backgroundAnimation.anchor.set(0.5, 0.5);
                backgroundAnimation.x = gameWidth / 2;
                backgroundAnimation.y = gameHeight / 2;
                backgroundAnimation.start();
                backgroundAnimation.alpha = 0;
                this.add(backgroundAnimation);
                this._tweens.push(this.game.add.tween(backgroundAnimation).to({ alpha: 1 }, 700, Phaser.Easing.Cubic.Out, true));
                this._tweens.push(this.game.add.tween(backgroundAnimation).to({ alpha: 0 }, 700, Phaser.Easing.Cubic.Out, false, 1000));
                this._tweens[0].chain(this._tweens[1]);
                var firstImage = new common.SimpleSprite(this.game, 0, 0, 'emotions_images', false, 'texts/big');
                var secondImage = new common.SimpleSprite(this.game, 0, 0, 'emotions_images', false, 'texts/win');
                firstImage.rotation = -5 * Math.PI / 180;
                secondImage.rotation = 1 * Math.PI / 180;
                firstImage.anchor.set(0.5, 0.5);
                secondImage.anchor.set(0.5, 0.5);
                firstImage.y = gameHeight / 2 - 150;
                secondImage.y = firstImage.y + 150;
                this.add(firstImage);
                this.add(secondImage);
                firstImage.x = -firstImage.width;
                secondImage.x = gameWidth + secondImage.width;
                this._tweens.push(this.game.add.tween(firstImage).to({ x: gameWidth / 2 }, 450, Phaser.Easing.Cubic.Out, true));
                this._tweens.push(this.game.add.tween(firstImage).to({ x: gameWidth / 2 + 70 }, 1000, Phaser.Easing.Default));
                this._tweens.push(this.game.add.tween(firstImage).to({ x: gameWidth + firstImage.width }, 450, Phaser.Easing.Cubic.Out));
                this._tweens[2].chain(this._tweens[3]);
                this._tweens[3].chain(this._tweens[4]);
                this._tweens.push(this.game.add.tween(secondImage).to({ x: gameWidth / 2 }, 450, Phaser.Easing.Cubic.Out, true));
                this._tweens.push(this.game.add.tween(secondImage).to({ x: gameWidth / 2 - 70 }, 1000, Phaser.Easing.Default));
                this._tweens.push(this.game.add.tween(secondImage).to({ x: -secondImage.width }, 450, Phaser.Easing.Cubic.Out));
                this._tweens[5].chain(this._tweens[6]);
                this._tweens[6].chain(this._tweens[7]);
                this._tweens[this._tweens.length - 1].onComplete.addOnce(this.onAnimationsComplete, this);
                common.SoundController.instance.playSound('bigwin', 'default', 0.2, false);
            };
            /**
             *
             * @param data
             */
            DragonGirlBigWinEmotion.prototype.onAnimationsComplete = function (data) {
                this._actionSignal.dispatch(common.AnimatedSprite.ALL_ANIMATIONS_COMPLETE);
            };
            return DragonGirlBigWinEmotion;
        }(common.BaseEmotion));
        common.DragonGirlBigWinEmotion = DragonGirlBigWinEmotion;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
/**
 * Created by rocket on 04.07.2017.
 */
var TypeScriptPhaserSlot;
/**
 * Created by rocket on 04.07.2017.
 */
(function (TypeScriptPhaserSlot) {
    var common;
    (function (common) {
        var DragonGirlFiveInLineEmotion = /** @class */ (function (_super) {
            __extends(DragonGirlFiveInLineEmotion, _super);
            function DragonGirlFiveInLineEmotion(game, config) {
                return _super.call(this, game, config) || this;
            }
            /**
             *
             */
            DragonGirlFiveInLineEmotion.prototype.init = function () {
            };
            /**
             *
             */
            DragonGirlFiveInLineEmotion.prototype.start = function () {
                this.animate();
                common.SoundController.instance.playSound('hammer', 'emotions', 0.1);
            };
            /**
             *
             */
            DragonGirlFiveInLineEmotion.prototype.stop = function () {
                clearInterval(this._timeOut);
                this._timeOut = 0;
                _super.prototype.stop.call(this);
            };
            /**
             *
             */
            DragonGirlFiveInLineEmotion.prototype.animate = function () {
                var gameWidth = 1366;
                var gameHeight = 768;
                var backgroundAnimation = new common.AnimatedSprite(this.game, 0, 0, this.config.emotion);
                backgroundAnimation.anchor.set(0.5, 0.5);
                backgroundAnimation.x = gameWidth / 2;
                backgroundAnimation.y = gameHeight / 2;
                backgroundAnimation.start();
                backgroundAnimation.alpha = 0;
                this.add(backgroundAnimation);
                this._tweens.push(this.game.add.tween(backgroundAnimation).to({ alpha: 1 }, 700, Phaser.Easing.Cubic.Out, true));
                this._tweens.push(this.game.add.tween(backgroundAnimation).to({ alpha: 0 }, 700, Phaser.Easing.Cubic.Out, false, 1000));
                this._tweens[0].chain(this._tweens[1]);
                var fiveImage = new common.SimpleSprite(this.game, 0, 0, 'emotions_images', false, 'texts/five');
                var inImage = new common.SimpleSprite(this.game, 0, 0, 'emotions_images', false, 'texts/in');
                var lineImage = new common.SimpleSprite(this.game, 0, 0, 'emotions_images', false, 'texts/line');
                fiveImage.rotation = -5 * Math.PI / 180;
                lineImage.rotation = 1 * Math.PI / 180;
                fiveImage.anchor.set(0.5, 0.5);
                inImage.anchor.set(0.5, 0.5);
                lineImage.anchor.set(0.5, 0.5);
                fiveImage.x = gameWidth / 2;
                fiveImage.y = gameHeight / 2 - 175;
                inImage.x = fiveImage.x - 50;
                inImage.y = fiveImage.y + 100;
                lineImage.x = fiveImage.x;
                lineImage.y = inImage.y + 150;
                this.add(fiveImage);
                this.add(inImage);
                this.add(lineImage);
                fiveImage.scale.set(0, 0);
                inImage.scale.set(0, 0);
                lineImage.scale.set(0, 0);
                this._tweens.push(this.game.add.tween(fiveImage.scale).to({ x: 1, y: 1 }, 800, Phaser.Easing.Elastic.Out, true));
                this._tweens.push(this.game.add.tween(fiveImage).to({ x: fiveImage.x - 70 }, 1500, Phaser.Easing.Circular.Out, true, 0));
                this._tweens.push(this.game.add.tween(inImage.scale).to({ x: 0.7, y: 0.7 }, 800, Phaser.Easing.Elastic.Out, true, 200));
                this._tweens.push(this.game.add.tween(lineImage.scale).to({ x: 1, y: 1 }, 800, Phaser.Easing.Elastic.Out, true, 500));
                this._tweens.push(this.game.add.tween(lineImage).to({ x: lineImage.x + 70 }, 1500, Phaser.Easing.Circular.Out, true, 200));
                this._tweens[this._tweens.length - 1].onComplete.addOnce(this.onAnimationsComplete, this);
                common.SoundController.instance.playSound('fiveinline', 'default', 0.2, false);
            };
            /**
             *
             * @param data
             */
            DragonGirlFiveInLineEmotion.prototype.onAnimationsComplete = function (data) {
                this._actionSignal.dispatch(common.AnimatedSprite.ALL_ANIMATIONS_COMPLETE);
            };
            return DragonGirlFiveInLineEmotion;
        }(common.BaseEmotion));
        common.DragonGirlFiveInLineEmotion = DragonGirlFiveInLineEmotion;
    })(common = TypeScriptPhaserSlot.common || (TypeScriptPhaserSlot.common = {}));
})(TypeScriptPhaserSlot || (TypeScriptPhaserSlot = {}));
//# sourceMappingURL=game.js.map