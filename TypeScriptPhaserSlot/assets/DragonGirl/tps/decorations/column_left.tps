<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.2</string>
        <key>fileName</key>
        <string>/Users/rocket/Projects/soga_slot/TypeScriptPhaserSlot/assets/DragonGirl/tps/decorations/column_left.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-hash</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">jpg</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../ready/decorations/column_left.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGB888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <true/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0000.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0001.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0002.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0003.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0004.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0005.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0006.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0007.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0008.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0009.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0010.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0011.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0012.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0013.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0014.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0015.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0016.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0017.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0018.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0019.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0020.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0021.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0022.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0023.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0024.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0025.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0026.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0027.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0028.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0029.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0030.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0031.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0032.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0033.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0034.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0035.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0036.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0037.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0038.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0039.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0040.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0041.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0042.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0043.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0044.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0045.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0046.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0047.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0048.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0049.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0050.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0051.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0052.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0053.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0054.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0055.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0056.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0057.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0058.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0059.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0060.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0061.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0062.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0063.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0064.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0065.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0066.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0067.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0068.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0069.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0070.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0071.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0072.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0073.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0074.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0075.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0076.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0077.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0078.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0079.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0080.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0081.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0082.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0083.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0084.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0085.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0086.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0087.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0088.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0089.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0090.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0091.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0092.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0093.jpg</key>
            <key type="filename">../../src/decorations/column_left/fire_stolb_left0094.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,192,98,384</rect>
                <key>scale9Paddings</key>
                <rect>49,192,98,384</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../src/decorations/column_left</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
