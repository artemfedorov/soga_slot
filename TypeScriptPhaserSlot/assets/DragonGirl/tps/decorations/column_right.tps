<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.2</string>
        <key>fileName</key>
        <string>/Users/rocket/Projects/soga_slot/TypeScriptPhaserSlot/assets/DragonGirl/tps/decorations/column_right.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-hash</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">jpg</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../ready/decorations/column_right.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGB888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <true/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0000.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0001.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0002.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0003.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0004.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0005.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0006.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0007.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0008.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0009.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0010.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0011.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0012.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0013.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0014.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0015.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0016.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0017.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0018.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0019.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0020.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0021.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0022.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0023.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0024.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0025.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0026.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0027.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0028.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0029.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0030.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0031.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0032.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0033.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0034.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0035.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0036.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0037.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0038.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0039.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0040.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0041.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0042.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0043.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0044.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0045.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0046.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0047.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0048.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0049.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0050.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0051.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0052.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0053.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0054.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0055.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0056.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0057.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0058.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0059.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0060.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0061.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0062.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0063.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0064.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0065.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0066.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0067.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0068.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0069.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0070.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0071.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0072.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0073.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0074.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0075.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0076.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0077.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0078.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0079.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0080.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0081.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0082.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0083.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0084.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0085.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0086.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0087.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0088.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0089.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0090.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0091.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0092.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0093.jpg</key>
            <key type="filename">../../src/decorations/column_right/fire_stolb_right0094.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,192,98,384</rect>
                <key>scale9Paddings</key>
                <rect>49,192,98,384</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../src/decorations/column_right</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
