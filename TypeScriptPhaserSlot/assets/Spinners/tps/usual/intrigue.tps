<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.5.0</string>
        <key>fileName</key>
        <string>/Users/rocket/Projects/soga_slot/TypeScriptPhaserSlot/assets/Spinners/tps/usual/intrigue.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-hash</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../ready/usual/intrigue.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <true/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../src/usual/intrigue/ramka_big_0000.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0002.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0004.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0006.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0008.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0010.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0012.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0014.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0016.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0018.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0020.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0022.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0024.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0026.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0028.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0030.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0032.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0034.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0036.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0038.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0040.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0042.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0044.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0046.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0048.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0050.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0052.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0054.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0056.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0058.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0060.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0062.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0064.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0066.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0068.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0070.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0072.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0074.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0076.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0078.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0080.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0082.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0084.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0086.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0088.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0090.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0092.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0094.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0096.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0098.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0100.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0102.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0104.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0106.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0108.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0110.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0112.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0114.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0116.png</key>
            <key type="filename">../../src/usual/intrigue/ramka_big_0118.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,87,87,173</rect>
                <key>scale9Paddings</key>
                <rect>43,87,87,173</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../src/usual/intrigue</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
